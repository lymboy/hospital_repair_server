CREATE TABLE `ACT_EVT_LOG`  (
  `LOG_NR_` decimal(19, 0) NOT NULL,
  `TYPE_` varchar(64) NULL,
  `PROC_DEF_ID_` varchar(64) NULL,
  `PROC_INST_ID_` varchar(64) NULL,
  `EXECUTION_ID_` varchar(64) NULL,
  `TASK_ID_` varchar(64) NULL,
  `TIME_STAMP_` datetime NOT NULL,
  `USER_ID_` varchar(255) NULL,
  `DATA_` longblob NULL,
  `LOCK_OWNER_` varchar(255) NULL,
  `LOCK_TIME_` datetime NULL,
  `IS_PROCESSED_` tinyint NULL,
  PRIMARY KEY (`LOG_NR_`)
);

CREATE TABLE `ACT_GE_BYTEARRAY`  (
  `ID_` varchar(64) NOT NULL,
  `REV_` int NULL,
  `NAME_` varchar(255) NULL,
  `DEPLOYMENT_ID_` varchar(64) NULL,
  `BYTES_` longblob NULL,
  `GENERATED_` tinyint NULL,
  PRIMARY KEY (`ID_`)
);

CREATE TABLE `ACT_GE_PROPERTY`  (
  `NAME_` varchar(64) NOT NULL,
  `VALUE_` text NULL,
  `REV_` int NULL,
  PRIMARY KEY (`NAME_`)
);

CREATE TABLE `ACT_HI_ACTINST`  (
  `ID_` varchar(64) NOT NULL,
  `PROC_DEF_ID_` varchar(64) NOT NULL,
  `PROC_INST_ID_` varchar(64) NOT NULL,
  `EXECUTION_ID_` varchar(64) NOT NULL,
  `ACT_ID_` varchar(255) NOT NULL,
  `TASK_ID_` varchar(64) NULL,
  `CALL_PROC_INST_ID_` varchar(64) NULL,
  `ACT_NAME_` varchar(255) NULL,
  `ACT_TYPE_` varchar(255) NOT NULL,
  `ASSIGNEE_` varchar(255) NULL,
  `START_TIME_` datetime NOT NULL,
  `END_TIME_` datetime NULL,
  `DURATION_` decimal(19, 0) NULL,
  `TENANT_ID_` varchar(255) NULL,
  PRIMARY KEY (`ID_`),
  INDEX `ACT_IDX_HI_ACT_INST_START`(`START_TIME_` ASC),
  INDEX `ACT_IDX_HI_ACT_INST_END`(`END_TIME_` ASC),
  INDEX `ACT_IDX_HI_ACT_INST_PROCINST`(`PROC_INST_ID_` ASC, `ACT_ID_` ASC),
  INDEX `ACT_IDX_HI_ACT_INST_EXEC`(`EXECUTION_ID_` ASC, `ACT_ID_` ASC)
);

CREATE TABLE `ACT_HI_ATTACHMENT`  (
  `ID_` varchar(64) NOT NULL,
  `REV_` int NULL,
  `USER_ID_` varchar(255) NULL,
  `NAME_` varchar(255) NULL,
  `DESCRIPTION_` text NULL,
  `TYPE_` varchar(255) NULL,
  `TASK_ID_` varchar(64) NULL,
  `PROC_INST_ID_` varchar(64) NULL,
  `URL_` text NULL,
  `CONTENT_ID_` varchar(64) NULL,
  `TIME_` datetime NULL,
  PRIMARY KEY (`ID_`)
);

CREATE TABLE `ACT_HI_COMMENT`  (
  `ID_` varchar(64) NOT NULL,
  `TYPE_` varchar(255) NULL,
  `TIME_` datetime NOT NULL,
  `USER_ID_` varchar(255) NULL,
  `TASK_ID_` varchar(64) NULL,
  `PROC_INST_ID_` varchar(64) NULL,
  `ACTION_` varchar(255) NULL,
  `MESSAGE_` text NULL,
  `FULL_MSG_` longblob NULL,
  PRIMARY KEY (`ID_`)
);

CREATE TABLE `ACT_HI_DETAIL`  (
  `ID_` varchar(64) NOT NULL,
  `TYPE_` varchar(255) NOT NULL,
  `PROC_INST_ID_` varchar(64) NULL,
  `EXECUTION_ID_` varchar(64) NULL,
  `TASK_ID_` varchar(64) NULL,
  `ACT_INST_ID_` varchar(64) NULL,
  `NAME_` varchar(255) NOT NULL,
  `VAR_TYPE_` varchar(255) NULL,
  `REV_` int NULL,
  `TIME_` datetime NOT NULL,
  `BYTEARRAY_ID_` varchar(64) NULL,
  `DOUBLE_` double NULL,
  `LONG_` decimal(19, 0) NULL,
  `TEXT_` text NULL,
  `TEXT2_` text NULL,
  PRIMARY KEY (`ID_`),
  INDEX `ACT_IDX_HI_DETAIL_PROC_INST`(`PROC_INST_ID_` ASC),
  INDEX `ACT_IDX_HI_DETAIL_ACT_INST`(`ACT_INST_ID_` ASC),
  INDEX `ACT_IDX_HI_DETAIL_TIME`(`TIME_` ASC),
  INDEX `ACT_IDX_HI_DETAIL_NAME`(`NAME_` ASC),
  INDEX `ACT_IDX_HI_DETAIL_TASK_ID`(`TASK_ID_` ASC)
);

CREATE TABLE `ACT_HI_IDENTITYLINK`  (
  `ID_` varchar(64) NOT NULL,
  `GROUP_ID_` varchar(255) NULL,
  `TYPE_` varchar(255) NULL,
  `USER_ID_` varchar(255) NULL,
  `TASK_ID_` varchar(64) NULL,
  `PROC_INST_ID_` varchar(64) NULL,
  PRIMARY KEY (`ID_`),
  INDEX `ACT_IDX_HI_IDENT_LNK_USER`(`USER_ID_` ASC),
  INDEX `ACT_IDX_HI_IDENT_LNK_TASK`(`TASK_ID_` ASC),
  INDEX `ACT_IDX_HI_IDENT_LNK_PROCINST`(`PROC_INST_ID_` ASC)
);

CREATE TABLE `ACT_HI_PROCINST`  (
  `ID_` varchar(64) NOT NULL,
  `PROC_INST_ID_` varchar(64) NOT NULL,
  `BUSINESS_KEY_` varchar(255) NULL,
  `PROC_DEF_ID_` varchar(64) NOT NULL,
  `START_TIME_` datetime NOT NULL,
  `END_TIME_` datetime NULL,
  `DURATION_` decimal(19, 0) NULL,
  `START_USER_ID_` varchar(255) NULL,
  `START_ACT_ID_` varchar(255) NULL,
  `END_ACT_ID_` varchar(255) NULL,
  `SUPER_PROCESS_INSTANCE_ID_` varchar(64) NULL,
  `DELETE_REASON_` text NULL,
  `TENANT_ID_` varchar(255) NULL,
  `NAME_` varchar(255) NULL,
  PRIMARY KEY (`ID_`),
  INDEX `ACT_IDX_HI_PRO_INST_END`(`END_TIME_` ASC),
  INDEX `ACT_IDX_HI_PRO_I_BUSKEY`(`BUSINESS_KEY_` ASC)
);

CREATE TABLE `ACT_HI_TASKINST`  (
  `ID_` varchar(64) NOT NULL,
  `PROC_DEF_ID_` varchar(64) NULL,
  `TASK_DEF_KEY_` varchar(255) NULL,
  `PROC_INST_ID_` varchar(64) NULL,
  `EXECUTION_ID_` varchar(64) NULL,
  `NAME_` varchar(255) NULL,
  `PARENT_TASK_ID_` varchar(64) NULL,
  `DESCRIPTION_` text NULL,
  `OWNER_` varchar(255) NULL,
  `ASSIGNEE_` varchar(255) NULL,
  `START_TIME_` datetime NOT NULL,
  `CLAIM_TIME_` datetime NULL,
  `END_TIME_` datetime NULL,
  `DURATION_` decimal(19, 0) NULL,
  `DELETE_REASON_` text NULL,
  `PRIORITY_` int NULL,
  `DUE_DATE_` datetime NULL,
  `FORM_KEY_` varchar(255) NULL,
  `CATEGORY_` varchar(255) NULL,
  `TENANT_ID_` varchar(255) NULL,
  PRIMARY KEY (`ID_`),
  INDEX `ACT_IDX_HI_TASK_INST_PROCINST`(`PROC_INST_ID_` ASC)
);

CREATE TABLE `ACT_HI_VARINST`  (
  `ID_` varchar(64) NOT NULL,
  `PROC_INST_ID_` varchar(64) NULL,
  `EXECUTION_ID_` varchar(64) NULL,
  `TASK_ID_` varchar(64) NULL,
  `NAME_` varchar(255) NOT NULL,
  `VAR_TYPE_` varchar(100) NULL,
  `REV_` int NULL,
  `BYTEARRAY_ID_` varchar(64) NULL,
  `DOUBLE_` double NULL,
  `LONG_` decimal(19, 0) NULL,
  `TEXT_` text NULL,
  `TEXT2_` text NULL,
  `CREATE_TIME_` datetime NULL,
  `LAST_UPDATED_TIME_` datetime NULL,
  PRIMARY KEY (`ID_`),
  INDEX `ACT_IDX_HI_PROCVAR_PROC_INST`(`PROC_INST_ID_` ASC),
  INDEX `ACT_IDX_HI_PROCVAR_NAME_TYPE`(`NAME_` ASC, `VAR_TYPE_` ASC),
  INDEX `ACT_IDX_HI_PROCVAR_TASK_ID`(`TASK_ID_` ASC)
);

CREATE TABLE `ACT_ID_GROUP`  (
  `ID_` varchar(64) NOT NULL,
  `REV_` int NULL,
  `NAME_` varchar(255) NULL,
  `TYPE_` varchar(255) NULL,
  PRIMARY KEY (`ID_`)
);

CREATE TABLE `ACT_ID_INFO`  (
  `ID_` varchar(64) NOT NULL,
  `REV_` int NULL,
  `USER_ID_` varchar(64) NULL,
  `TYPE_` varchar(64) NULL,
  `KEY_` varchar(255) NULL,
  `VALUE_` varchar(255) NULL,
  `PASSWORD_` longblob NULL,
  `PARENT_ID_` varchar(255) NULL,
  PRIMARY KEY (`ID_`)
);

CREATE TABLE `ACT_ID_MEMBERSHIP`  (
  `USER_ID_` varchar(64) NOT NULL,
  `GROUP_ID_` varchar(64) NOT NULL,
  PRIMARY KEY (`USER_ID_`, `GROUP_ID_`)
);

CREATE TABLE `ACT_ID_USER`  (
  `ID_` varchar(64) NOT NULL,
  `REV_` int NULL,
  `FIRST_` varchar(255) NULL,
  `LAST_` varchar(255) NULL,
  `EMAIL_` varchar(255) NULL,
  `PWD_` varchar(255) NULL,
  `PICTURE_ID_` varchar(64) NULL,
  PRIMARY KEY (`ID_`)
);

CREATE TABLE `ACT_PROCDEF_INFO`  (
  `ID_` varchar(64) NOT NULL,
  `PROC_DEF_ID_` varchar(64) NOT NULL,
  `REV_` int NULL,
  `INFO_JSON_ID_` varchar(64) NULL,
  PRIMARY KEY (`ID_`),
  INDEX `ACT_IDX_INFO_PROCDEF`(`PROC_DEF_ID_` ASC)
);

CREATE TABLE `ACT_RE_DEPLOYMENT`  (
  `ID_` varchar(64) NOT NULL,
  `NAME_` varchar(255) NULL,
  `CATEGORY_` varchar(255) NULL,
  `TENANT_ID_` varchar(255) NULL,
  `DEPLOY_TIME_` datetime NULL,
  PRIMARY KEY (`ID_`)
);

CREATE TABLE `ACT_RE_MODEL`  (
  `ID_` varchar(64) NOT NULL,
  `REV_` int NULL,
  `NAME_` varchar(255) NULL,
  `KEY_` varchar(255) NULL,
  `CATEGORY_` varchar(255) NULL,
  `CREATE_TIME_` datetime NULL,
  `LAST_UPDATE_TIME_` datetime NULL,
  `VERSION_` int NULL,
  `META_INFO_` text NULL,
  `DEPLOYMENT_ID_` varchar(64) NULL,
  `EDITOR_SOURCE_VALUE_ID_` varchar(64) NULL,
  `EDITOR_SOURCE_EXTRA_VALUE_ID_` varchar(64) NULL,
  `TENANT_ID_` varchar(255) NULL,
  PRIMARY KEY (`ID_`)
);

CREATE TABLE `ACT_RE_PROCDEF`  (
  `ID_` varchar(64) NOT NULL,
  `REV_` int NULL,
  `CATEGORY_` varchar(255) NULL,
  `NAME_` varchar(255) NULL,
  `KEY_` varchar(255) NOT NULL,
  `VERSION_` int NOT NULL,
  `DEPLOYMENT_ID_` varchar(64) NULL,
  `RESOURCE_NAME_` text NULL,
  `DGRM_RESOURCE_NAME_` text NULL,
  `DESCRIPTION_` text NULL,
  `HAS_START_FORM_KEY_` tinyint NULL,
  `HAS_GRAPHICAL_NOTATION_` tinyint NULL,
  `SUSPENSION_STATE_` tinyint NULL,
  `TENANT_ID_` varchar(255) NULL,
  PRIMARY KEY (`ID_`)
);

CREATE TABLE `ACT_RU_EVENT_SUBSCR`  (
  `ID_` varchar(64) NOT NULL,
  `REV_` int NULL,
  `EVENT_TYPE_` varchar(255) NOT NULL,
  `EVENT_NAME_` varchar(255) NULL,
  `EXECUTION_ID_` varchar(64) NULL,
  `PROC_INST_ID_` varchar(64) NULL,
  `ACTIVITY_ID_` varchar(64) NULL,
  `CONFIGURATION_` varchar(255) NULL,
  `CREATED_` datetime NOT NULL,
  `PROC_DEF_ID_` varchar(64) NULL,
  `TENANT_ID_` varchar(255) NULL,
  PRIMARY KEY (`ID_`),
  INDEX `ACT_IDX_EVENT_SUBSCR_CONFIG_`(`CONFIGURATION_` ASC),
  INDEX `ACT_IDX_EVENT_SUBSCR_EXEC_ID`(`EXECUTION_ID_` ASC)
);

CREATE TABLE `ACT_RU_EXECUTION`  (
  `ID_` varchar(64) NOT NULL,
  `REV_` int NULL,
  `PROC_INST_ID_` varchar(64) NULL,
  `BUSINESS_KEY_` varchar(255) NULL,
  `PARENT_ID_` varchar(64) NULL,
  `PROC_DEF_ID_` varchar(64) NULL,
  `SUPER_EXEC_` varchar(64) NULL,
  `ACT_ID_` varchar(255) NULL,
  `IS_ACTIVE_` tinyint NULL,
  `IS_CONCURRENT_` tinyint NULL,
  `IS_SCOPE_` tinyint NULL,
  `IS_EVENT_SCOPE_` tinyint NULL,
  `SUSPENSION_STATE_` tinyint NULL,
  `CACHED_ENT_STATE_` int NULL,
  `TENANT_ID_` varchar(255) NULL,
  `NAME_` varchar(255) NULL,
  `LOCK_TIME_` datetime NULL,
  PRIMARY KEY (`ID_`),
  INDEX `ACT_IDX_EXEC_BUSKEY`(`BUSINESS_KEY_` ASC),
  INDEX `ACT_IDX_EXECUTION_PROC`(`PROC_DEF_ID_` ASC),
  INDEX `ACT_IDX_EXECUTION_PARENT`(`PARENT_ID_` ASC),
  INDEX `ACT_IDX_EXECUTION_SUPER`(`SUPER_EXEC_` ASC),
  INDEX `ACT_IDX_EXECUTION_IDANDREV`(`ID_` ASC, `REV_` ASC),
  INDEX `ACT_IDX_EXEC_PROC_INST_ID`(`PROC_INST_ID_` ASC)
);

CREATE TABLE `ACT_RU_IDENTITYLINK`  (
  `ID_` varchar(64) NOT NULL,
  `REV_` int NULL,
  `GROUP_ID_` varchar(255) NULL,
  `TYPE_` varchar(255) NULL,
  `USER_ID_` varchar(255) NULL,
  `TASK_ID_` varchar(64) NULL,
  `PROC_INST_ID_` varchar(64) NULL,
  `PROC_DEF_ID_` varchar(64) NULL,
  PRIMARY KEY (`ID_`),
  INDEX `ACT_IDX_IDENT_LNK_USER`(`USER_ID_` ASC),
  INDEX `ACT_IDX_IDENT_LNK_GROUP`(`GROUP_ID_` ASC),
  INDEX `ACT_IDX_ATHRZ_PROCEDEF`(`PROC_DEF_ID_` ASC),
  INDEX `ACT_IDX_IDENT_LNK_TASK`(`TASK_ID_` ASC),
  INDEX `ACT_IDX_IDENT_LNK_PROCINST`(`PROC_INST_ID_` ASC)
);

CREATE TABLE `ACT_RU_JOB`  (
  `ID_` varchar(64) NOT NULL,
  `REV_` int NULL,
  `TYPE_` varchar(255) NOT NULL,
  `LOCK_EXP_TIME_` datetime NULL,
  `LOCK_OWNER_` varchar(255) NULL,
  `EXCLUSIVE_` tinyint NULL,
  `EXECUTION_ID_` varchar(64) NULL,
  `PROCESS_INSTANCE_ID_` varchar(64) NULL,
  `PROC_DEF_ID_` varchar(64) NULL,
  `RETRIES_` int NULL,
  `EXCEPTION_STACK_ID_` varchar(64) NULL,
  `EXCEPTION_MSG_` text NULL,
  `DUEDATE_` datetime NULL,
  `REPEAT_` varchar(255) NULL,
  `HANDLER_TYPE_` varchar(255) NULL,
  `HANDLER_CFG_` text NULL,
  `TENANT_ID_` varchar(255) NULL,
  PRIMARY KEY (`ID_`),
  INDEX `ACT_IDX_JOB_EXCEPTION_STACK_ID`(`EXCEPTION_STACK_ID_` ASC)
);

CREATE TABLE `ACT_RU_TASK`  (
  `ID_` varchar(64) NOT NULL,
  `REV_` int NULL,
  `EXECUTION_ID_` varchar(64) NULL,
  `PROC_INST_ID_` varchar(64) NULL,
  `PROC_DEF_ID_` varchar(64) NULL,
  `NAME_` varchar(255) NULL,
  `PARENT_TASK_ID_` varchar(64) NULL,
  `DESCRIPTION_` text NULL,
  `TASK_DEF_KEY_` varchar(255) NULL,
  `OWNER_` varchar(255) NULL,
  `ASSIGNEE_` varchar(255) NULL,
  `DELEGATION_` varchar(64) NULL,
  `PRIORITY_` int NULL,
  `CREATE_TIME_` datetime NULL,
  `DUE_DATE_` datetime NULL,
  `CATEGORY_` varchar(255) NULL,
  `SUSPENSION_STATE_` int NULL,
  `TENANT_ID_` varchar(255) NULL,
  `FORM_KEY_` varchar(255) NULL,
  PRIMARY KEY (`ID_`),
  INDEX `ACT_IDX_TASK_CREATE`(`CREATE_TIME_` ASC),
  INDEX `ACT_IDX_TASK_EXEC`(`EXECUTION_ID_` ASC),
  INDEX `ACT_IDX_TASK_PROCINST`(`PROC_INST_ID_` ASC),
  INDEX `ACT_IDX_TASK_PROC_DEF_ID`(`PROC_DEF_ID_` ASC)
);

CREATE TABLE `ACT_RU_VARIABLE`  (
  `ID_` varchar(64) NOT NULL,
  `REV_` int NULL,
  `TYPE_` varchar(255) NOT NULL,
  `NAME_` varchar(255) NOT NULL,
  `EXECUTION_ID_` varchar(64) NULL,
  `PROC_INST_ID_` varchar(64) NULL,
  `TASK_ID_` varchar(64) NULL,
  `BYTEARRAY_ID_` varchar(64) NULL,
  `DOUBLE_` double NULL,
  `LONG_` decimal(19, 0) NULL,
  `TEXT_` text NULL,
  `TEXT2_` text NULL,
  PRIMARY KEY (`ID_`),
  INDEX `ACT_IDX_VARIABLE_TASK_ID`(`TASK_ID_` ASC),
  INDEX `ACT_IDX_VARIABLE_BA`(`BYTEARRAY_ID_` ASC),
  INDEX `ACT_IDX_VARIABLE_EXEC`(`EXECUTION_ID_` ASC),
  INDEX `ACT_IDX_VARIABLE_PROCINST`(`PROC_INST_ID_` ASC)
);

CREATE TABLE `CODE_CURRENCY_KIND`  (
  `KIND_ID` varchar(64) NOT NULL COMMENT '分类ID',
  `KIND_NAME` varchar(100) NULL COMMENT '分类名',
  `FLAG` int NULL COMMENT '标识 是否删除 0删， 1存在',
  `KIND_SERIAL_NO` int NULL COMMENT '次序',
  `VIEW_ONLY` int NULL,
  PRIMARY KEY (`KIND_ID`)
);

CREATE TABLE `CODE_CURRENCY_TYPE`  (
  `TYPE_ID` varchar(64) NOT NULL COMMENT '类型ID',
  `TYPE_NAME` varchar(100) NULL COMMENT '类型名',
  `TYPE_BELONGTO` varchar(64) NULL COMMENT '上级类型',
  `TYPE_SERIAL_NO` int NULL COMMENT '次序',
  `KIND_ID` varchar(64) NULL COMMENT '分类',
  `DEFAULT_CHECKED` int NULL COMMENT '页面默认选择， 1默认',
  `FLAG` int NULL COMMENT '标识 是否删除 0删， 1存在',
  PRIMARY KEY (`TYPE_ID`)
);

CREATE TABLE `HOS_AUXILIARY`  (
  `ID` varchar(64) NOT NULL,
  `GROUP_ID` varchar(64) NULL COMMENT '组别ID',
  `MEMBER_NAME` varchar(255) NULL COMMENT '人员姓名',
  `MEMBER_JOB_NUM` varchar(32) NULL COMMENT '员工工号',
  `CREATEDATE` datetime NULL,
  PRIMARY KEY (`ID`)
);

CREATE TABLE `HOS_AUXILIARY_DICTIONARY`  (
  `ID` varchar(64) NOT NULL,
  `AUXILIARY` varchar(64) NULL COMMENT '辅助',
  PRIMARY KEY (`ID`)
);

CREATE TABLE `HOS_DUTY`  (
  `ID` varchar(50) NOT NULL,
  `DUTY_ID` varchar(50) NULL,
  `DUTY_DATE` varchar(50) NULL,
  `IS_HOLIDAY` int NULL,
  `FLAG` int NULL,
  `IS_WORK` varchar(50) NULL
);

CREATE TABLE `HOS_GROUP_DICTIONARY`  (
  `ID` varchar(64) NOT NULL,
  `GROUP_DICTIONARY` varchar(255) NULL COMMENT '组别',
  `FLAG` int NULL,
  PRIMARY KEY (`ID`)
);

CREATE TABLE `HOS_MEMBER`  (
  `ID` varchar(64) NOT NULL COMMENT '主键ID',
  `USER_NAME` varchar(255) NOT NULL COMMENT '人员名单',
  `DEPARTMENT_ID` varchar(255) NULL COMMENT '机构ID',
  `TYPE` varchar(255) NULL COMMENT '权限分类',
  `FLAG` int NULL COMMENT ' 1 存在 0删除',
  `WX_NAME` varchar(255) NULL COMMENT '微信昵称',
  `WX_OPENID` varchar(255) NULL COMMENT '微信得openID',
  `VACATION` varchar(255) NULL COMMENT '是否休假',
  `USER_PHONE` varchar(11) NULL COMMENT '用户手机号',
  `JOB_NUM` varchar(32) NULL COMMENT '员工工号',
  `PASSWORD` varchar(255) NULL COMMENT '密码',
  `SEX` varchar(11) NULL COMMENT '性别',
  `RESPONSIBLE_GROUP` varchar(255) NULL COMMENT '信息科人员所管理得组别',
  `QUES_DICTIONARY_ID` varchar(64) NULL COMMENT '信息科人员 问题分类',
  `SORT` int NULL,
  PRIMARY KEY (`ID`)
);

CREATE TABLE `HOS_MEMBER_MARK`  (
  `ID` varchar(50) NULL,
  `MARK` int NULL,
  `USER_ID` varchar(50) NULL,
  `CREATEDATE` datetime NULL
);

CREATE TABLE `HOS_QUES_DICTIONARY`  (
  `ID` varchar(64) NOT NULL,
  `QUESTION_TYPE` varchar(255) NULL COMMENT '问题分类',
  `QUESTION_LEVEL` varchar(255) NULL COMMENT '问题级别',
  `QUESTION_MARK` int NULL COMMENT '问题分数',
  `QUES_PARENT_ID` varchar(64) NULL COMMENT '父类ID',
  `REMARK` varchar(255) NULL,
  `FLAG` int NULL,
  PRIMARY KEY (`ID`)
);

CREATE TABLE `HOS_QUESTION`  (
  `ID` varchar(64) NOT NULL,
  `DICTIONARY_ID` varchar(64) NULL COMMENT '问题大类',
  `DEPARTMENT_ID` varchar(64) NULL,
  `DUTY_OFFICER` varchar(64) NULL COMMENT '值班人员ID',
  `AUXILIARY_ID` varchar(64) NULL COMMENT '辅助',
  `QUESTION_REMARK` varchar(64) NULL,
  `STATUS_ID` varchar(64) NULL COMMENT '未完成，待确认，已完成',
  `SCORE` int NULL COMMENT '评分',
  `CREATEDATE` datetime NULL,
  `MODIFYDATE` datetime NULL COMMENT '修改时间',
  `FLAG` int NULL COMMENT '0 删除 1 存在',
  `REMARK_IMG1` varchar(255) NULL,
  `REMARK_IMG2` varchar(255) NULL,
  `REMARK_IMG3` varchar(255) NULL,
  `CONDOM_USER` varchar(255) NULL COMMENT '报修人员姓名',
  `DICTIONARY_ID_SMALL` varchar(64) NULL COMMENT '问题小类',
  `SCORE_REMARK` varchar(255) NULL COMMENT '评价',
  `QUESTION_MARK` int NULL,
  `COMPLETEDATE` datetime NULL,
  `REMARK` varchar(255) NULL,
  `IP` varchar(64) NULL,
  PRIMARY KEY (`ID`)
);

CREATE TABLE `HOS_QUESTION_SUBSIDIARY`  (
  `ID` varchar(64) NOT NULL,
  `IMPLEMENT_ID` varchar(64) NULL COMMENT '执行人员',
  `ASSIGNOR_ID` varchar(64) NULL COMMENT '转让人',
  `ASSIGNOR_DATE` datetime NULL COMMENT '转让时候得分配时间',
  `HOS_QUESTION_ID` varchar(64) NULL,
  PRIMARY KEY (`ID`)
);

CREATE TABLE `HOS_STATUS_DICTIONARY`  (
  `ID` varchar(64) NOT NULL,
  `COMPLETION_STATUS` varchar(64) NULL COMMENT '辅助',
  PRIMARY KEY (`ID`)
);

CREATE TABLE `HOS_TASK`  (
  `ID` varchar(64) NOT NULL,
  `TOTAL_TASK` int NOT NULL COMMENT '任务总数',
  `COMPLETE_TASK` int NOT NULL COMMENT '完成数量',
  `UN_FINISH_TASK` int NOT NULL COMMENT '未完成数量',
  `MARK` int NOT NULL COMMENT '分数',
  `MEMBER_ID` varchar(64) NOT NULL COMMENT '人员ID',
  PRIMARY KEY (`ID`)
);

CREATE TABLE `INTEGRAL`  (
  `ID` varchar(64) NOT NULL,
  `TYPE` varchar(255) NOT NULL COMMENT '转账（发给，来自），充值，体现',
  `ROLL_COUT` varchar(64) NOT NULL COMMENT '转出者',
  `SWITCH_TO` varchar(64) NULL COMMENT '转入者',
  `INTEGRAL` int NULL COMMENT '积分',
  `CREATEDATE` datetime NULL COMMENT '创建时间',
  PRIMARY KEY (`ID`)
);

CREATE TABLE `QRTZ_BLOB_TRIGGERS`  (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `BLOB_DATA` longblob NULL
);

CREATE TABLE `QRTZ_CALENDARS`  (
  `SCHED_NAME` varchar(120) NOT NULL,
  `CALENDAR_NAME` varchar(200) NOT NULL,
  `CALENDAR` longblob NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `CALENDAR_NAME`)
);

CREATE TABLE `QRTZ_CRON_TRIGGERS`  (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `CRON_EXPRESSION` varchar(120) NOT NULL,
  `TIME_ZONE_ID` varchar(80) NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
);

CREATE TABLE `QRTZ_FIRED_TRIGGERS`  (
  `SCHED_NAME` varchar(120) NOT NULL,
  `ENTRY_ID` varchar(95) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `INSTANCE_NAME` varchar(200) NOT NULL,
  `FIRED_TIME` bigint NOT NULL,
  `SCHED_TIME` bigint NOT NULL,
  `PRIORITY` int NOT NULL,
  `STATE` varchar(16) NOT NULL,
  `JOB_NAME` varchar(200) NULL,
  `JOB_GROUP` varchar(200) NULL,
  `IS_NONCONCURRENT` varchar(1) NULL,
  `REQUESTS_RECOVERY` varchar(1) NULL,
  PRIMARY KEY (`SCHED_NAME`, `ENTRY_ID`)
);

CREATE TABLE `QRTZ_JOB_DETAILS`  (
  `SCHED_NAME` varchar(120) NOT NULL,
  `JOB_NAME` varchar(200) NOT NULL,
  `JOB_GROUP` varchar(200) NOT NULL,
  `DESCRIPTION` varchar(250) NULL,
  `JOB_CLASS_NAME` varchar(250) NOT NULL,
  `IS_DURABLE` varchar(1) NOT NULL,
  `IS_NONCONCURRENT` varchar(1) NOT NULL,
  `IS_UPDATE_DATA` varchar(1) NOT NULL,
  `REQUESTS_RECOVERY` varchar(1) NOT NULL,
  `JOB_DATA` longblob NULL,
  PRIMARY KEY (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`)
);

CREATE TABLE `QRTZ_LOCKS`  (
  `SCHED_NAME` varchar(120) NOT NULL,
  `LOCK_NAME` varchar(40) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `LOCK_NAME`)
);

CREATE TABLE `QRTZ_PAUSED_TRIGGER_GRPS`  (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_GROUP`)
);

CREATE TABLE `QRTZ_SCHEDULER_STATE`  (
  `SCHED_NAME` varchar(120) NOT NULL,
  `INSTANCE_NAME` varchar(200) NOT NULL,
  `LAST_CHECKIN_TIME` bigint NOT NULL,
  `CHECKIN_INTERVAL` bigint NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `INSTANCE_NAME`)
);

CREATE TABLE `QRTZ_SIMPLE_TRIGGERS`  (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `REPEAT_COUNT` bigint NOT NULL,
  `REPEAT_INTERVAL` bigint NOT NULL,
  `TIMES_TRIGGERED` bigint NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
);

CREATE TABLE `QRTZ_SIMPROP_TRIGGERS`  (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `STR_PROP_1` text NULL,
  `STR_PROP_2` text NULL,
  `STR_PROP_3` text NULL,
  `INT_PROP_1` int NULL,
  `INT_PROP_2` int NULL,
  `LONG_PROP_1` bigint NULL,
  `LONG_PROP_2` bigint NULL,
  `DEC_PROP_1` decimal(13, 4) NULL,
  `DEC_PROP_2` decimal(13, 4) NULL,
  `BOOL_PROP_1` varchar(1) NULL,
  `BOOL_PROP_2` varchar(1) NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
);

CREATE TABLE `QRTZ_TRIGGERS`  (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `JOB_NAME` varchar(200) NOT NULL,
  `JOB_GROUP` varchar(200) NOT NULL,
  `DESCRIPTION` varchar(250) NULL,
  `NEXT_FIRE_TIME` bigint NULL,
  `PREV_FIRE_TIME` bigint NULL,
  `PRIORITY` int NULL,
  `TRIGGER_STATE` varchar(16) NOT NULL,
  `TRIGGER_TYPE` varchar(8) NOT NULL,
  `START_TIME` bigint NOT NULL,
  `END_TIME` bigint NULL,
  `CALENDAR_NAME` varchar(200) NULL,
  `MISFIRE_INSTR` smallint NULL,
  `JOB_DATA` longblob NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
);

CREATE TABLE `S_ATTENDANCE_RECORD`  (
  `ID` varchar(64) NOT NULL,
  `BILL_CODE` varchar(64) NULL COMMENT '单据编号',
  `EDITOR` varchar(64) NULL COMMENT '编制人 EmployeeID',
  `EDIT_DATE` datetime NULL COMMENT '编制日期',
  `EMPLOYEE_ID` varchar(64) NULL COMMENT '考勤人员',
  `ATTENDANCE_DATE` datetime NULL COMMENT '考勤日期',
  `CREATEDATE` datetime NULL COMMENT '创建时间',
  `CREATER` varchar(64) NULL COMMENT '创建人',
  `MODIFYDATE` datetime NULL COMMENT '修改时间',
  `MODIFIER` varchar(64) NULL COMMENT '修改人',
  `FLAG` int NULL COMMENT '逻辑删除标识',
  `REMARK` varchar(255) NULL COMMENT '备注',
  PRIMARY KEY (`ID`)
);

CREATE TABLE `S_ATTENDANCE_RECORD_DETAIL`  (
  `ID` varchar(64) NOT NULL,
  `ATTENDANCE_RECORD_ID` varchar(64) NULL COMMENT '主表ID',
  `SIGN_TYPE` varchar(64) NULL COMMENT '签到类型 常量',
  `SIGN_TIME` datetime NULL COMMENT '考勤时间',
  `SIGN_ADDRESS` varchar(255) NULL COMMENT '考勤签到地点',
  `CREATEDATE` datetime NULL COMMENT '创建时间',
  `CREATER` varchar(64) NULL COMMENT '创建人',
  `MODIFYDATE` datetime NULL COMMENT '修改时间',
  `MODIFIER` varchar(64) NULL COMMENT '修改人',
  `FLAG` int NULL COMMENT '逻辑删除标识',
  `REMARK` varchar(255) NULL COMMENT '备注',
  PRIMARY KEY (`ID`)
);

CREATE TABLE `S_ATTENDANCE_RECORD_DETAIL_IMG`  (
  `ID` varchar(64) NOT NULL COMMENT '签到图片',
  `ATTENDANCE_RECORD_DETAIL_ID` varchar(64) NULL COMMENT '考勤明细 签到 表ID',
  `IMG_PATH` varchar(255) NULL COMMENT '图片路径',
  `UPLOADER` varchar(64) NULL COMMENT '上传人 账号ID',
  `UPLOAD_DATE` datetime NULL COMMENT '上传时间',
  `CREATEDATE` datetime NULL COMMENT '创建时间',
  `CREATER` varchar(64) NULL COMMENT '创建人',
  `MODIFYDATE` datetime NULL COMMENT '修改时间',
  `MODIFIER` varchar(64) NULL COMMENT '修改人',
  `FLAG` int NULL COMMENT '逻辑删除标识',
  `REMARK` varchar(255) NULL COMMENT '备注',
  PRIMARY KEY (`ID`)
);

CREATE TABLE `S_ATTENDANCE_SETTING`  (
  `ID` varchar(64) NOT NULL COMMENT '项目人员考勤设置表 ID',
  `BILL_CODE` varchar(64) NULL COMMENT '单据编号',
  `EDITOR` varchar(64) NULL COMMENT '编制人 EmployeeID',
  `EDIT_DATE` datetime NULL COMMENT '编制日期',
  `POST_ID` varchar(64) NULL COMMENT '岗位 常量 职务',
  `DAYS_OF_MONTH` int NULL COMMENT '每月出勤天数',
  `CREATEDATE` datetime NULL COMMENT '创建时间',
  `CREATER` varchar(64) NULL COMMENT '创建人',
  `MODIFYDATE` datetime NULL COMMENT '修改时间',
  `MODIFIER` varchar(64) NULL COMMENT '修改人',
  `FLAG` int NULL COMMENT '逻辑删除标识',
  `REMARK` varchar(255) NULL COMMENT '备注',
  PRIMARY KEY (`ID`)
) COMMENT = '项目人员考勤设置表';

CREATE TABLE `S_ATTENDANCE_SETTING_DETAIL`  (
  `ID` varchar(64) NOT NULL COMMENT '考勤设置从表 设置考勤人员',
  `ATTENDANCE_SETTING_ID` varchar(64) NULL COMMENT '考勤设置主表ID',
  `EMPLOYEE_ID` varchar(64) NULL COMMENT '考勤人员',
  `CREATEDATE` datetime NULL COMMENT '创建时间',
  `CREATER` varchar(64) NULL COMMENT '创建人',
  `MODIFYDATE` datetime NULL COMMENT '修改时间',
  `MODIFIER` varchar(64) NULL COMMENT '修改人',
  `FLAG` int NULL COMMENT '逻辑删除标识',
  `REMARK` varchar(255) NULL COMMENT '备注',
  PRIMARY KEY (`ID`)
);

CREATE TABLE `S_ATTENDANCE_SIGN_TIME`  (
  `ID` varchar(64) NOT NULL COMMENT '签到时间设置 ID',
  `SIGN_TYPE` varchar(64) NULL COMMENT '签到时间设置名称 常量',
  `START_TIME` varchar(50) NULL COMMENT '开始时间',
  `END_TIME` varchar(50) NULL COMMENT '结束时间',
  `CREATEDATE` datetime NULL COMMENT '创建时间',
  `CREATER` varchar(64) NULL COMMENT '创建人',
  `MODIFYDATE` datetime NULL COMMENT '修改时间',
  `MODIFIER` varchar(64) NULL COMMENT '修改人',
  `FLAG` int NULL COMMENT '逻辑删除标识',
  `REMARK` varchar(255) NULL COMMENT '备注',
  PRIMARY KEY (`ID`)
);

CREATE TABLE `S_LEAVE_RECORD`  (
  `ID` varchar(64) NULL COMMENT '请假单',
  `BILL_CODE` varchar(64) NULL COMMENT '单据编号',
  `START_DATE` datetime NULL COMMENT '请假开始时间',
  `START_DAY_TYPE` int NULL COMMENT '0 上午， 1下午',
  `END_DATE` datetime NULL COMMENT '请假结束时间',
  `END_DAY_TYPE` int NULL COMMENT '0 上午， 1下午',
  `LEAVE_DAYS` double NULL COMMENT '请假天数',
  `LEAVE_TYPE` varchar(64) NULL COMMENT '申请假别 常量',
  `LEAVE_REASON` varchar(255) NULL COMMENT '请假原因说明',
  `EMPLOYEE_ID` varchar(64) NULL COMMENT '请假人',
  `REQUEST_DATE` datetime NULL COMMENT '申请请假日期',
  `EDITOR` varchar(64) NULL COMMENT '编制人 EmployeeID',
  `EDIT_DATE` datetime NULL COMMENT '编制日期',
  `CREATEDATE` datetime NULL COMMENT '创建时间',
  `CREATER` varchar(64) NULL COMMENT '创建人',
  `MODIFYDATE` datetime NULL COMMENT '修改时间',
  `MODIFIER` varchar(64) NULL COMMENT '修改人',
  `FLAG` int NULL COMMENT '逻辑删除标识',
  `REMARK` varchar(255) NULL COMMENT '备注'
);

CREATE TABLE `Z_APPROVAL_STATUS`  (
  `ID` varchar(64) NOT NULL,
  `FLOWWORKID` varchar(64) NULL COMMENT '需要审批的任务的id',
  `APPROVAL_STATUS` varchar(64) NULL COMMENT '审批状态  1待提交  2待审批 3 已审批  4 审批结束  5驳回',
  `PROCESSINSTANCE_ID` varchar(64) NULL,
  `USERID` varchar(64) NULL COMMENT 'EMPLOYEE_ID',
  `URL` varchar(200) NULL,
  `FLOWWORKNAME` varchar(100) NULL,
  `TYPE` varchar(255) NULL,
  PRIMARY KEY (`ID`)
);

CREATE TABLE `Z_ATTACHMENT`  (
  `ID` varchar(64) NOT NULL COMMENT '附件 ID',
  `MASTER_ID` varchar(64) NULL COMMENT '业务ID',
  `SUB_SYSTEM` varchar(128) NULL COMMENT '第一级菜单 （拼音）一级文件夹名',
  `MODULE_NAME` varchar(128) NULL COMMENT '模块名（二级菜单）',
  `FILE_NAME` varchar(64) NULL COMMENT '文件名',
  `FILE_SIZE` varchar(20) NULL COMMENT '文件大小',
  `FILE_NO` int NULL COMMENT '文件次序',
  `UPLOAD_DATE` datetime NULL COMMENT '上传时间',
  `MASTER_TYPE` varchar(64) NULL COMMENT '同一MASTER_ID 不同类型附件',
  `CREATEDATE` datetime NULL,
  `CREATER` varchar(255) NULL,
  `MODIFYDATE` datetime NULL,
  `MODIFIER` varchar(64) NULL,
  `FLAG` int NULL COMMENT '标识 1存在， 0删除',
  `REMARK` varchar(255) NULL COMMENT '备注',
  PRIMARY KEY (`ID`)
);

CREATE TABLE `Z_CATALOG`  (
  `ID` varchar(64) NOT NULL COMMENT '资料 目录',
  `CATALOG_CODE` text NULL COMMENT '资料目录 编号',
  `CATALOG_NAME` text NULL COMMENT '资料目录 名称',
  `PARENT_CATALOG_ID` varchar(64) NULL COMMENT '父级 资料目录ID',
  `CATALOG_NO` int NULL COMMENT '次序',
  `PRIVILEGE_STATUS` int NULL COMMENT '权限状态 0无 1有',
  `PUBLISH_STATUS` int NULL COMMENT '发布状态 0未发布 1已发布',
  `BASE_ID` text NULL COMMENT '目录层级关系 一层四位  0，1 串',
  `CREATEDATE` datetime NULL COMMENT '创建日期',
  `CREATER` varchar(64) NULL COMMENT '创建人',
  `MODIFYDATE` datetime NULL COMMENT '修改日期',
  `MODIFIER` varchar(64) NULL COMMENT '修改人',
  `FLAG` int NULL COMMENT '标识 1存在  0删除',
  `REMARK` text NULL COMMENT '备注（摘要）',
  PRIMARY KEY (`ID`)
) COMMENT = '资料目录表';

CREATE TABLE `Z_DEPARTMENT`  (
  `ID` varchar(64) NOT NULL COMMENT '部门id',
  `DEPARTMENT_NAME` varchar(128) NULL COMMENT '部门名称',
  `PINYIN` varchar(255) NULL,
  `DEPARTMENT_PARENT_ID` varchar(64) NULL COMMENT '部门父级',
  `CREATEDATE` datetime NULL COMMENT '创建时间',
  `CREATER` varchar(64) NULL COMMENT '创建人',
  `MODIFYDATE` datetime NULL,
  `MODIFIER` varchar(64) NULL,
  `FLAG` int NULL,
  `REMARK` text NULL COMMENT '备注',
  `BASE_ID` varchar(64) NULL,
  `DEPARTMENT_NUMBER` varchar(64) NULL COMMENT '(无效字段，待舍去)',
  `DEPARTMENT_ORDER` int NULL COMMENT '次序',
  `DEPARTMENT_CODE` varchar(128) NULL COMMENT '部门编码',
  `DEPARTMENT_TYPE` varchar(255) NULL COMMENT '机构类型',
  `DEPARTMENT_PROPERTY` varchar(64) NULL COMMENT '机构属性 常量',
  `COUNTY_AREA` varchar(255) NULL COMMENT '县市区',
  `TOWNSHIP_STREETS` varchar(255) NULL COMMENT '乡镇街道',
  `COMMUNITY_VILLAGE` varchar(255) NULL COMMENT '社区村',
  `DEPARTMENT_PHONE` varchar(255) NULL,
  `GROUP_ID` varchar(255) NULL,
  PRIMARY KEY (`ID`)
);

CREATE TABLE `Z_DOCUMENT`  (
  `ID` varchar(64) NOT NULL COMMENT '资料文件 ID',
  `FILE_NAME` text NULL COMMENT '资料文件 名称',
  `FILE_SIZE` varchar(53) NULL COMMENT '资料文件 大小',
  `UPLOAD_DATE` datetime NULL COMMENT '资料文件 上传时间',
  `FILE_NO` int NULL COMMENT '资料文件 在目录中的次序',
  `CATALOG_ID` varchar(64) NULL COMMENT '资料文件 所属资料目录',
  `CREATEDATE` datetime NULL COMMENT '创建日期',
  `CREATER` varchar(64) NULL COMMENT '创建人',
  `MODIFYDATE` datetime NULL COMMENT '修改日期',
  `MODIFIER` varchar(64) NULL COMMENT '修改人',
  `FLAG` int NULL COMMENT '标识 1存在  0删除',
  `REMARK` text NULL COMMENT '备注',
  `MATERIAL_ID` varchar(64) NULL COMMENT '资料文件 所属资料',
  PRIMARY KEY (`ID`)
) COMMENT = '资料文档';

CREATE TABLE `Z_EMPLOYEE`  (
  `ID` varchar(64) NOT NULL COMMENT '员工id',
  `EMPLOYEE_NAME` varchar(255) NULL COMMENT '员工名字',
  `EMPLOYEE_GENDER` int NULL COMMENT '员工性别 1男 0女',
  `EMPLOYEE_BIRTHDAY` datetime NULL COMMENT '出生年月',
  `ENGINE_TYPE` varchar(64) NULL COMMENT '从事工程类型',
  `WORK_CLASS` varchar(255) NULL COMMENT '工作性质',
  `POST_ID` varchar(64) NULL COMMENT '职务 （常量表）',
  `CREATEDATE` datetime NULL COMMENT '创建时间',
  `CREATER` varchar(64) NULL COMMENT '创建人',
  `MODIFYDATE` datetime NULL COMMENT '修改时间',
  `MODIFIER` varchar(64) NULL COMMENT '修改人',
  `FLAG` int NULL COMMENT '标识',
  `REMARK` text NULL,
  `DEPARTMENT_ID` varchar(64) NULL COMMENT '所属部门',
  `ENTRY_DATE` datetime NULL COMMENT '入职时间',
  `EMPLOYEE_NATION` varchar(64) NULL COMMENT '民族',
  `OFFICE_PHONE` varchar(20) NULL COMMENT '办公电话',
  `EMAIL` varchar(255) NULL COMMENT '邮件',
  `PHONE` varchar(255) NULL COMMENT '电话',
  `ACCOUNT_NUMBER` varchar(64) NULL COMMENT '账号',
  `EMPLOYEE_POSITION` varchar(255) NULL,
  `PROFESSION` varchar(255) NULL COMMENT '专业',
  `EMPLOYEE_PHOTO` text NULL COMMENT '照片',
  `EDITOR` varchar(255) NULL COMMENT '编制人',
  `EMPLOYEE_ORDER` int NULL,
  `EMPLOYEE_TYPE` varchar(255) NULL,
  `EMPLOYEE_EDU` varchar(255) NULL,
  `DOCUMENT_STATUS` varchar(255) NULL,
  `GRADUATE_ACADEMY` varchar(255) NULL,
  `POSITIONAL_TITLES` varchar(255) NULL,
  `IDENTITY_CARD_POSITIVE` varchar(255) NULL,
  `IDENTITY_CARD_OPPOSITE` varchar(255) NULL,
  `MEMBER_ID` varchar(64) NULL COMMENT '会员ID',
  `INTEGRAL` int NULL COMMENT '积分',
  `SERIAL_NUMBER` int NULL COMMENT '流水号',
  `TYPE` varchar(255) NULL COMMENT '区别时会员还是业务代表或者时验车行得人',
  PRIMARY KEY (`ID`)
);

CREATE TABLE `Z_EMPLOYEE_EDU`  (
  `ID` varchar(64) NOT NULL,
  `PROFESSION` text NULL COMMENT '专业',
  `EMPLOYEE_ID` varchar(64) NULL COMMENT '员工id',
  `EMPLOYEE_EDU` text NULL COMMENT '学历',
  `CERTIFICATE` text NULL COMMENT '证书',
  `REMARK` text NULL,
  `FLAG` int NULL,
  `EDU_START_TIME` datetime NULL COMMENT '教育开始日期',
  `EDU_END_TIME` datetime NULL COMMENT '教育结束日期',
  `EDU_INSTITUTION` text NULL COMMENT '教育机构',
  `CREATEDATE` datetime NULL COMMENT '创建时间',
  `CREATER` varchar(64) NULL COMMENT '创建人',
  `MODIFYDATE` datetime NULL COMMENT '修改时间',
  `MODIFIER` varchar(64) NULL COMMENT '修改人',
  PRIMARY KEY (`ID`)
);

CREATE TABLE `Z_EMPLOYEE_PROFESSION`  (
  `ID` varchar(64) NOT NULL,
  `EMPLOYEE_ID` varchar(64) NULL COMMENT '员工id',
  `PROFESSION` varchar(255) NULL COMMENT '专业',
  PRIMARY KEY (`ID`)
);

CREATE TABLE `Z_EMPLOYEE_REWPUN`  (
  `ID` varchar(64) NOT NULL,
  `EMPLOYEE_ID` varchar(64) NULL COMMENT '员工id',
  `REWPUN_DATE` datetime NULL COMMENT '奖惩日期',
  `REWPUN_TYPE` varchar(255) NULL COMMENT '奖惩类型 72 奖 73 罚',
  `REWPUN_CONTENT` text NULL COMMENT '奖惩描述',
  `REMARK` text NULL,
  `FLAG` int NULL,
  `CREATEDATE` datetime NULL COMMENT '创建时间',
  `CREATER` varchar(64) NULL COMMENT '创建人',
  `MODIFYDATE` datetime NULL COMMENT '修改时间',
  `MODIFIER` varchar(64) NULL COMMENT '修改人',
  `REFERENCE_NUMBER` text NULL COMMENT '文号',
  PRIMARY KEY (`ID`)
);

CREATE TABLE `Z_MATERIAL`  (
  `ID` varchar(64) NOT NULL COMMENT '资料  编辑',
  `MATERIAL_CODE` text NULL COMMENT '资料 编号',
  `MATERIAL_NAME` text NULL COMMENT '资料 名称',
  `MATERIAL_SUMMARY` text NULL COMMENT '资料 摘要',
  `EDITOR` varchar(64) NULL COMMENT '资料 维护人',
  `EDIT_DATE` datetime NULL COMMENT '资料 维护时间',
  `CREATEDATE` datetime NULL COMMENT '资料 创建时间',
  `CREATER` varchar(64) NULL COMMENT '资料 创建人',
  `MODIFYDATE` datetime NULL COMMENT '资料 修改时间',
  `MODIFIER` varchar(64) NULL COMMENT '资料 修改人',
  `FLAG` int NULL COMMENT '标识 1存在  0删除',
  `REMARK` varchar(255) NULL,
  `CATALOG_ID` varchar(64) NULL COMMENT '资料文件 所属资料目录ID',
  `PUBLISH_STATUS` int NULL COMMENT '发布状态 0没发布， 1已发布',
  PRIMARY KEY (`ID`)
);

CREATE TABLE `Z_PERSON_DETAIL`  (
  `ID` varchar(64) NOT NULL,
  `EMPLOYEE_ID` varchar(64) NULL,
  `POST_ID` varchar(64) NULL,
  `DEPARTMENT_ID` varchar(64) NULL,
  `CREATER` varchar(64) NULL,
  `MODIFYDATE` datetime NULL,
  `FLAG` int NULL,
  `CREATEDATE` datetime NULL,
  `MODIFIER` varchar(64) NULL,
  `TENURE_DATE` datetime NULL COMMENT '任职时间',
  `LEAVE_DATE` datetime NULL COMMENT '离职时间',
  PRIMARY KEY (`ID`)
);

CREATE TABLE `Z_PROJECT`  (
  `ID` varchar(64) NOT NULL COMMENT '项目id',
  `PROJECT_NAME` varchar(64) NULL COMMENT '项目名称',
  `SHORT_NAME` varchar(64) NULL COMMENT '项目简称',
  `PROJECT_NO` varchar(64) NULL COMMENT '项目编号',
  `DEPARTMENT_ID` varchar(64) NULL COMMENT '所属机构',
  `PROJECT_DUTY` varchar(255) NULL COMMENT '项目负责人',
  `PROJECT_ESTABLISH_TIME` datetime NULL COMMENT '项目设立时间',
  `CREATEDATE` datetime NULL,
  `CREATER` varchar(64) NULL,
  `MODIFYDATE` datetime NULL,
  `MODIFIER` varchar(64) NULL,
  `FLAG` int NULL,
  `REMARK` varchar(200) NULL,
  `PROJECT_DESC` varchar(255) NULL COMMENT '项目描述',
  `PROJECT_AREA` varchar(64) NULL COMMENT '项目区域',
  `PROJECT_CITY` varchar(255) NULL COMMENT '项目国家/城市',
  `PROJECT_BASE_NUM` int NULL COMMENT 'int型唯一值，用于分区',
  `PROJECT_BUILD_UNIT` varchar(255) NULL,
  `PROJECT_OWNER_UNIT` varchar(255) NULL,
  `PROJECT_BUDGET` varchar(255) NULL,
  `START_DATE_PLAN` datetime NULL,
  `END_DATE_PLAN` datetime NULL,
  `START_DATE_REAL` datetime NULL,
  `END_DATE_REAL` datetime NULL,
  `EDITOR` varchar(64) NULL,
  `EDIT_DATE` datetime NULL,
  `PROJECT_TYPE` varchar(64) NULL,
  PRIMARY KEY (`ID`)
);
CREATE TRIGGER `trigger_project_forSplitProjectQuality` AFTER INSERT ON `Z_PROJECT` FOR EACH ROW begin
	commit transaction
	--BEGIN   TRANSACTION 
	print '创建分区-项目质量单元划分'
	
	-- 文件和文件组查询
	--select * from sys.database_files
	--select * from sys.filegroups f

	--文件和文件组 删除
	--alter database DEMO remove file projectQualityDetailFileName_2
	--alter database DEMO remove filegroup projectQualityDetailFileGroupName_2

	-- 项目质量划分 分区
	--createFileGroupSql_PQD 创建文件组 SQL
	--createFileSql_PQD 创建文件 SQL
	--dir_database_mdf 主要文件本地磁盘全目录
	--dir_data_ndf 次要文件目录
	--@alterPartitionScheme 修改分区架构的SQL
	--@alterPartitionFunction 修改分区函数的SQL

	declare @databasename nvarchar(200)
	declare @createFileGroupSql nvarchar(500)
	declare @createFileSql nvarchar(500)
	declare @dir_database_mdf nvarchar(500)
	declare @dir_data_ndf nvarchar(500)
	declare @alterPartitionScheme nvarchar(500)
	declare @alterPartitionFunction nvarchar(500)

	declare @fileGroupName nvarchar(100)
	declare @fileName nvarchar(100)
	declare @projectBaseNum int

	declare @fileSize nvarchar(20)
	declare @fileGroupSize nvarchar(20)
	set @fileSize = '5Mb'
	set @fileGroupSize = '5Mb'
	set @fileGroupName = 'projectQualityDetailFileGroupName'
	set @fileName = 'projectQualityDetailFileName'

	set @databasename = DB_NAME();
	select TOP 1 @dir_database_mdf=physical_name from sys.database_files
	set @dir_data_ndf = @dir_database_mdf
	set @dir_data_ndf = SUBSTRING(@dir_database_mdf, 1, len(@dir_database_mdf)-CHARINDEX('\', REVERSE(@dir_database_mdf)))

	select @projectBaseNum=MAX(PROJECT_BASE_NUM) from Z_PROJECT
	set @fileGroupName = @fileGroupName +'_'+ convert(nvarchar,@projectBaseNum);
	set @fileName = @fileName +'_'+ convert(nvarchar,@projectBaseNum);

	set @dir_data_ndf = @dir_data_ndf+'\' + @fileName;

	print @projectBaseNum
	IF ((select COUNT(name) from sys.filegroups where name=@fileGroupName)<1)
	begin
		
		-- 文件组 SQL
		set @createFileGroupSql='alter database '+@databasename+' add filegroup '+@fileGroupName;
		-- 文件 SQL
		set @createFileSql = 'alter database '+@databasename+' add file (name=N'''+@fileName+''',filename=N'''+@dir_data_ndf+''',size='+@fileSize+',filegrowth='+@fileGroupSize+') to filegroup '+@fileGroupName;

		-- 执行 SQL
		print '创建文件组'
		exec(@createFileGroupSql)
		print '创建文件'
		exec(@createFileSql)
		
		-- 1. 修改分区架构, 增加用以接受新分区的文件组
		set @alterPartitionScheme = 'ALTER PARTITION SCHEME bgPartitionSchema_projectQuality NEXT USED ['+@fileGroupName+']';
		-- 2. 修改分区函数, 增加分区用以接受新数据
		set @alterPartitionFunction = 'ALTER PARTITION FUNCTION bgPartitionFun_projectQuality() SPLIT RANGE('+convert(nvarchar,@projectBaseNum)+')';
		print '修改分区架构'
		exec(@alterPartitionScheme)
		print '修改分区函数'
		exec(@alterPartitionFunction)
	end
	print '创建分区-项目质量单元划分-完成'
	--commit TRANSACTION
end;

CREATE TABLE `Z_SYS_DATA_PRIVILEGE`  (
  `DATA_PRIVILEGE_ID` decimal(18, 0) NOT NULL,
  `DATA_PRIVILEGE_NAME` varchar(128) NULL,
  `DATA_PRIVILEGE_NO` int NULL,
  `CREATEDATE` datetime NULL,
  `CREATER` varchar(64) NULL,
  `MODIFYDATE` datetime NULL,
  `MODIFIER` varchar(64) NULL,
  `FLAG` int NULL,
  `REMARK` varchar(255) NULL,
  PRIMARY KEY (`DATA_PRIVILEGE_ID`)
);

CREATE TABLE `Z_SYS_GROUP_ROLE`  (
  `ID` decimal(18, 0) NOT NULL,
  `GROUP_ID` decimal(18, 0) NULL COMMENT '用户组id',
  `ROLE_ID` decimal(18, 0) NULL COMMENT '角色id',
  PRIMARY KEY (`ID`)
);

CREATE TABLE `Z_SYS_IDENTITY`  (
  `IDENTITY_ID` decimal(18, 0) NOT NULL,
  `IDENTITY_NAME` varchar(255) NULL,
  `IDENTITY_NO` int NULL,
  `USER_ID` decimal(18, 0) NULL,
  `EMPLOYEE_ID` varchar(64) NULL,
  `DEPARTMENT_ID` varchar(64) NULL,
  `POST_ID` varchar(64) NULL,
  `CREATEDATE` datetime NULL,
  `CREATER` decimal(18, 0) NULL,
  `MODIFYDATE` datetime NULL,
  `MODIFIER` decimal(18, 0) NULL,
  `FLAG` int NULL,
  `REMARK` varchar(255) NULL,
  `DEFAULT_IDENTITY` int NULL COMMENT '默认身份为1， 其他为0',
  PRIMARY KEY (`IDENTITY_ID`)
);

CREATE TABLE `Z_SYS_IDENTITY_GROUP`  (
  `ID` decimal(18, 0) NOT NULL,
  `IDENTITY_ID` decimal(18, 0) NULL COMMENT '身份id',
  `GROUP_ID` decimal(18, 0) NULL COMMENT '用户组id',
  PRIMARY KEY (`ID`)
);

CREATE TABLE `Z_SYS_IDENTITY_RESOURCES`  (
  `ID` decimal(18, 0) NOT NULL,
  `IDENTITY_ID` decimal(18, 0) NULL,
  `RESOURCE_PRIVILEGE_ID` decimal(18, 0) NULL,
  PRIMARY KEY (`ID`)
);

CREATE TABLE `Z_SYS_IDENTITY_RESOURCES_OPERATION`  (
  `ID` decimal(18, 0) NOT NULL,
  `IDENTITY_RESOURCES_ID` decimal(18, 0) NULL,
  `RESOURCES_OPERATION_ID` decimal(18, 0) NULL,
  PRIMARY KEY (`ID`)
);

CREATE TABLE `Z_SYS_IDENTITY_ROLE`  (
  `IDENTITY_ROLE_ID` decimal(18, 0) NOT NULL,
  `IDENTITY_ID` decimal(18, 0) NULL,
  `ROLE_ID` decimal(18, 0) NULL,
  PRIMARY KEY (`IDENTITY_ROLE_ID`)
);

CREATE TABLE `Z_SYS_MENU`  (
  `MENU_ID` decimal(18, 0) NOT NULL COMMENT '菜单ID',
  `MENU_NAME` varchar(40) NULL COMMENT '菜单名',
  `PARENT_MENU_ID` decimal(18, 0) NULL COMMENT '父级菜单',
  `SERIAL_NO` decimal(18, 0) NULL COMMENT '次序',
  `MENU_URL` varchar(100) NULL COMMENT '菜单URL',
  `ICON_URL` varchar(100) NULL COMMENT '图标URL',
  `MENU_TYPE` varchar(50) NULL COMMENT '菜单类型',
  `CREATEDATE` datetime NULL,
  `CREATER` decimal(18, 0) NULL,
  `MODIFYDATE` datetime NULL,
  `MODIFIER` decimal(18, 0) NULL,
  `FLAG` int NULL COMMENT '标识',
  `REMARK` varchar(200) NULL COMMENT '备注',
  `MENU_GROUP` int NULL COMMENT '0项目，1系统， 2查询， 3app',
  `SYSTEM_TYPE` int NULL COMMENT '平台类型 0企业  1项目',
  PRIMARY KEY (`MENU_ID`)
);

CREATE TABLE `Z_SYS_MENU_BUTTON`  (
  `BUTTON_ID` decimal(18, 0) NOT NULL COMMENT '按钮 ID',
  `MENU_ID` decimal(18, 0) NULL COMMENT '所属菜单ID',
  `BUTTON_NAME` varchar(128) NULL,
  `CREATEDATE` datetime NULL,
  `CREATER` decimal(18, 0) NULL,
  `MODIFYDATE` datetime NULL,
  `MODIFIER` decimal(18, 0) NULL,
  `FLAG` varchar(255) NULL COMMENT '标识是否存在 0删',
  `REMARK` varchar(255) NULL,
  `BUTTON_NO` int NULL,
  PRIMARY KEY (`BUTTON_ID`)
);

CREATE TABLE `Z_SYS_PERSON_MENU`  (
  `PERSON_MENU_ID` decimal(18, 0) NULL,
  `MENU_ID` decimal(18, 0) NULL,
  `EMPLOYEE_ID` varchar(64) NULL
);

CREATE TABLE `Z_SYS_PRIVILEGE`  (
  `PRIVILEGE_ID` decimal(18, 0) NOT NULL COMMENT '权限ID',
  `PRIVILEGE_NAME` varchar(40) NULL COMMENT '权限名',
  `PRIVILEGE_NO` decimal(18, 0) NULL COMMENT '次序',
  `CREATEDATE` datetime NULL,
  `CREATER` decimal(18, 0) NULL,
  `MODIFYDATE` datetime NULL,
  `MODIFIER` decimal(18, 0) NULL,
  `FLAG` int NULL COMMENT '标识',
  `REMARK` varchar(200) NULL COMMENT '备注',
  PRIMARY KEY (`PRIVILEGE_ID`)
);

CREATE TABLE `Z_SYS_PRIVILEGE_BUTTON`  (
  `PRIVILEGE_BUTTON_ID` decimal(18, 0) NOT NULL,
  `PRIVILEGE_ID` decimal(18, 0) NULL,
  `BUTTON_ID` decimal(18, 0) NULL,
  PRIMARY KEY (`PRIVILEGE_BUTTON_ID`)
);

CREATE TABLE `Z_SYS_PRIVILEGE_MENU`  (
  `PRIVILEGE_MENU_ID` decimal(18, 0) NOT NULL,
  `PRIVILEGE_ID` decimal(18, 0) NULL,
  `MENU_ID` decimal(18, 0) NULL,
  PRIMARY KEY (`PRIVILEGE_MENU_ID`)
);

CREATE TABLE `Z_SYS_RESOURCES_OPERATION`  (
  `ID` decimal(18, 0) NOT NULL,
  `OPERATION_NAME` varchar(255) NULL,
  `FLAG` int NULL,
  `CREATEDATE` datetime NULL COMMENT '创建时间',
  `CREATER` decimal(18, 0) NULL COMMENT '创建人',
  `MODIFYDATE` datetime NULL COMMENT '修改时间',
  `MODIFIER` decimal(18, 0) NULL COMMENT '修改人',
  `REMARK` varchar(255) NULL,
  PRIMARY KEY (`ID`)
);

CREATE TABLE `Z_SYS_RESOURCES_PRIVILEGE`  (
  `ID` decimal(18, 0) NOT NULL,
  `RESOURCES_ID` varchar(64) NULL,
  `FLAG` int NULL,
  `RESOURCES_TYPE` int NULL COMMENT '0 目录   1 文件',
  PRIMARY KEY (`ID`)
);

CREATE TABLE `Z_SYS_ROLE`  (
  `ROLE_ID` decimal(18, 0) NOT NULL COMMENT '角色ID',
  `ROLE_NAME` varchar(40) NULL COMMENT '角色名',
  `ROLE_NO` decimal(18, 0) NULL COMMENT '次序',
  `CREATEDATE` datetime NULL,
  `CREATER` decimal(18, 0) NULL,
  `MODIFYDATE` datetime NULL,
  `MODIFIER` decimal(18, 0) NULL,
  `FLAG` int NULL COMMENT '标识',
  `REMARK` varchar(200) NULL COMMENT '备注',
  `ROLE_TYPE` int NULL,
  PRIMARY KEY (`ROLE_ID`)
);

CREATE TABLE `Z_SYS_ROLE_DATA_PRIVILEGE`  (
  `ROLE_DATA_PRIVILEGE_ID` decimal(18, 0) NULL,
  `ROLE_ID` decimal(18, 0) NULL,
  `DATA_PRIVILEGE_ID` decimal(18, 0) NULL
);

CREATE TABLE `Z_SYS_ROLE_PRIVILEGE`  (
  `PRIVILEGE_ROLE_ID` decimal(18, 0) NOT NULL,
  `PRIVILEGE_ID` decimal(18, 0) NULL,
  `ROLE_ID` decimal(18, 0) NULL,
  PRIMARY KEY (`PRIVILEGE_ROLE_ID`)
);

CREATE TABLE `Z_SYS_USER`  (
  `USER_ID` decimal(18, 0) NOT NULL COMMENT '用户ID',
  `USER_NAME` varchar(40) NULL COMMENT '用户名',
  `PASSWORD` varchar(40) NULL COMMENT '密码',
  `USER_NO` decimal(18, 0) NULL COMMENT '次序',
  `DUE_TIME` datetime NULL COMMENT '账号到期时间',
  `CREATEDATE` datetime NULL,
  `CREATER` decimal(18, 0) NULL,
  `MODIFYDATE` datetime NULL,
  `MODIFIER` decimal(18, 0) NULL,
  `FLAG` int NULL COMMENT '标识',
  `REMARK` varchar(200) NULL COMMENT '备注',
  `LAST_LOGIN` datetime NULL COMMENT '最后一次登陆时间',
  `IP` varchar(20) NULL COMMENT '最后登陆IP',
  `STATUS` varchar(50) NULL COMMENT '状态-是否登陆(0否, 1是)',
  `SKIN` varchar(50) NULL,
  `EMPLOYEE_ID` varchar(64) NULL COMMENT '员工ID',
  PRIMARY KEY (`USER_ID`)
);

CREATE TABLE `Z_SYS_USER_CLIENT`  (
  `ID` varchar(64) NOT NULL,
  `USER_ID` varchar(64) NULL COMMENT '用户ID',
  `EMPLOYEE_ID` varchar(64) NULL COMMENT '员工ID',
  `CLIENT_ID` varchar(255) NULL COMMENT '客户端ID',
  `LOGIN_TIME` datetime NULL COMMENT '登录时间',
  PRIMARY KEY (`ID`)
);

CREATE TABLE `Z_SYS_USER_GROUP`  (
  `ID` decimal(18, 0) NOT NULL,
  `GROUP_NAME` varchar(255) NULL,
  `REMARK` varchar(255) NULL,
  `CREATEDATE` datetime NULL,
  `CREATER` decimal(18, 0) NULL,
  `MODIFYDATE` datetime NULL,
  `MODIFIER` decimal(18, 0) NULL,
  `FLAG` int NULL COMMENT '标识',
  PRIMARY KEY (`ID`)
);

CREATE TABLE `Z_SYS_USER_ROLE`  (
  `USER_ROLE_ID` decimal(18, 0) NOT NULL,
  `USER_ID` decimal(18, 0) NULL,
  `ROLE_ID` decimal(18, 0) NULL,
  PRIMARY KEY (`USER_ROLE_ID`)
);

ALTER TABLE `ACT_GE_BYTEARRAY` ADD CONSTRAINT `FK__ACT_GE_BY__DEPLO__46486B8E` FOREIGN KEY (`DEPLOYMENT_ID_`) REFERENCES `ACT_RE_DEPLOYMENT` (`ID_`);
ALTER TABLE `ACT_ID_MEMBERSHIP` ADD CONSTRAINT `FK__ACT_ID_ME__USER___4830B400` FOREIGN KEY (`USER_ID_`) REFERENCES `ACT_ID_USER` (`ID_`);
ALTER TABLE `ACT_ID_MEMBERSHIP` ADD CONSTRAINT `FK__ACT_ID_ME__GROUP__473C8FC7` FOREIGN KEY (`GROUP_ID_`) REFERENCES `ACT_ID_GROUP` (`ID_`);
ALTER TABLE `ACT_PROCDEF_INFO` ADD CONSTRAINT `FK__ACT_PROCD__INFO___4924D839` FOREIGN KEY (`INFO_JSON_ID_`) REFERENCES `ACT_GE_BYTEARRAY` (`ID_`);
ALTER TABLE `ACT_PROCDEF_INFO` ADD CONSTRAINT `FK__ACT_PROCD__PROC___4A18FC72` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `ACT_RE_PROCDEF` (`ID_`);
ALTER TABLE `ACT_RE_MODEL` ADD CONSTRAINT `FK__ACT_RE_MO__DEPLO__4B0D20AB` FOREIGN KEY (`DEPLOYMENT_ID_`) REFERENCES `ACT_RE_DEPLOYMENT` (`ID_`);
ALTER TABLE `ACT_RE_MODEL` ADD CONSTRAINT `FK__ACT_RE_MO__EDITO__4C0144E4` FOREIGN KEY (`EDITOR_SOURCE_VALUE_ID_`) REFERENCES `ACT_GE_BYTEARRAY` (`ID_`);
ALTER TABLE `ACT_RE_MODEL` ADD CONSTRAINT `FK__ACT_RE_MO__EDITO__4CF5691D` FOREIGN KEY (`EDITOR_SOURCE_EXTRA_VALUE_ID_`) REFERENCES `ACT_GE_BYTEARRAY` (`ID_`);
ALTER TABLE `ACT_RU_EVENT_SUBSCR` ADD CONSTRAINT `FK__ACT_RU_EV__EXECU__4DE98D56` FOREIGN KEY (`EXECUTION_ID_`) REFERENCES `ACT_RU_EXECUTION` (`ID_`);
ALTER TABLE `ACT_RU_EXECUTION` ADD CONSTRAINT `FK__ACT_RU_EX__PAREN__4EDDB18F` FOREIGN KEY (`PARENT_ID_`) REFERENCES `ACT_RU_EXECUTION` (`ID_`);
ALTER TABLE `ACT_RU_EXECUTION` ADD CONSTRAINT `FK__ACT_RU_EX__PROC___4FD1D5C8` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `ACT_RE_PROCDEF` (`ID_`);
ALTER TABLE `ACT_RU_EXECUTION` ADD CONSTRAINT `FK__ACT_RU_EX__SUPER__50C5FA01` FOREIGN KEY (`SUPER_EXEC_`) REFERENCES `ACT_RU_EXECUTION` (`ID_`);
ALTER TABLE `ACT_RU_IDENTITYLINK` ADD CONSTRAINT `FK__ACT_RU_ID__PROC___52AE4273` FOREIGN KEY (`PROC_INST_ID_`) REFERENCES `ACT_RU_EXECUTION` (`ID_`);
ALTER TABLE `ACT_RU_IDENTITYLINK` ADD CONSTRAINT `FK__ACT_RU_ID__TASK___53A266AC` FOREIGN KEY (`TASK_ID_`) REFERENCES `ACT_RU_TASK` (`ID_`);
ALTER TABLE `ACT_RU_IDENTITYLINK` ADD CONSTRAINT `FK__ACT_RU_ID__PROC___51BA1E3A` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `ACT_RE_PROCDEF` (`ID_`);
ALTER TABLE `ACT_RU_JOB` ADD CONSTRAINT `FK__ACT_RU_JO__EXCEP__54968AE5` FOREIGN KEY (`EXCEPTION_STACK_ID_`) REFERENCES `ACT_GE_BYTEARRAY` (`ID_`);
ALTER TABLE `ACT_RU_TASK` ADD CONSTRAINT `FK__ACT_RU_TA__EXECU__558AAF1E` FOREIGN KEY (`EXECUTION_ID_`) REFERENCES `ACT_RU_EXECUTION` (`ID_`);
ALTER TABLE `ACT_RU_TASK` ADD CONSTRAINT `FK__ACT_RU_TA__PROC___567ED357` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `ACT_RE_PROCDEF` (`ID_`);
ALTER TABLE `ACT_RU_TASK` ADD CONSTRAINT `FK__ACT_RU_TA__PROC___5772F790` FOREIGN KEY (`PROC_INST_ID_`) REFERENCES `ACT_RU_EXECUTION` (`ID_`);
ALTER TABLE `ACT_RU_VARIABLE` ADD CONSTRAINT `FK__ACT_RU_VA__BYTEA__58671BC9` FOREIGN KEY (`BYTEARRAY_ID_`) REFERENCES `ACT_GE_BYTEARRAY` (`ID_`);
ALTER TABLE `ACT_RU_VARIABLE` ADD CONSTRAINT `FK__ACT_RU_VA__EXECU__595B4002` FOREIGN KEY (`EXECUTION_ID_`) REFERENCES `ACT_RU_EXECUTION` (`ID_`);
ALTER TABLE `ACT_RU_VARIABLE` ADD CONSTRAINT `FK__ACT_RU_VA__PROC___5A4F643B` FOREIGN KEY (`PROC_INST_ID_`) REFERENCES `ACT_RU_EXECUTION` (`ID_`);
ALTER TABLE `QRTZ_CRON_TRIGGERS` ADD CONSTRAINT `FK__QRTZ_CRON_TRIGGE__5B438874` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `QRTZ_TRIGGERS` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`);
ALTER TABLE `QRTZ_SIMPLE_TRIGGERS` ADD CONSTRAINT `FK__QRTZ_SIMPLE_TRIG__5C37ACAD` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `QRTZ_TRIGGERS` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`);
ALTER TABLE `QRTZ_SIMPROP_TRIGGERS` ADD CONSTRAINT `FK__QRTZ_SIMPROP_TRI__5D2BD0E6` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `QRTZ_TRIGGERS` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`);
ALTER TABLE `QRTZ_TRIGGERS` ADD CONSTRAINT `FK__QRTZ_TRIGGERS__5E1FF51F` FOREIGN KEY (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) REFERENCES `QRTZ_JOB_DETAILS` (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`);

