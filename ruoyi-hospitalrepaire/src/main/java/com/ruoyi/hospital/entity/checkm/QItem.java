package com.ruoyi.hospital.entity.checkm;

import java.util.Date;

/**
 * 2017年3月21日
 *
 */
public class QItem {
	private String ID;
	private String PERSONAL_SCORE;
	private String DEPARTMENT_SCORE;
	private String COMPANY_SCORE;
	private String QUARTER_SCORE;
	private String FLAG;
	private String QUARTER_ASSESS_RESULT_ID;
	private String DEPARTMENT_ID;
	private String EMPLOYEE_ID;
	private String ASSESS_POST_ID;
	private String CREATER;
	private Date CREATEDATE;

	private String D_REMARK;
	private String C_REMARK;


	@Override
	public String toString() {
		return "QItem [ID=" + ID + ", PERSONAL_SCORE=" + PERSONAL_SCORE
				+ ", DEPARTMENT_SCORE=" + DEPARTMENT_SCORE + ", COMPANY_SCORE="
				+ COMPANY_SCORE + ", QUARTER_SCORE=" + QUARTER_SCORE
				+ ", FLAG=" + FLAG + ", QUARTER_ASSESS_RESULT_ID="
				+ QUARTER_ASSESS_RESULT_ID + ", DEPARTMENT_ID=" + DEPARTMENT_ID
				+ ", EMPLOYEE_ID=" + EMPLOYEE_ID + ", ASSESS_POST_ID="
				+ ASSESS_POST_ID + ", CREATER=" + CREATER + ", CREATEDATE="
				+ CREATEDATE + "]";
	}

	public String getD_REMARK() {
		return D_REMARK;
	}

	public void setD_REMARK(String d_REMARK) {
		D_REMARK = d_REMARK;
	}

	public String getC_REMARK() {
		return C_REMARK;
	}

	public void setC_REMARK(String c_REMARK) {
		C_REMARK = c_REMARK;
	}

	public Date getCREATEDATE() {
		return CREATEDATE;
	}

	public void setCREATEDATE(Date cREATEDATE) {
		CREATEDATE = cREATEDATE;
	}

	public String getCREATER() {
		return CREATER;
	}
	public void setCREATER(String cREATER) {
		CREATER = cREATER;
	}


	public String getID() {
		return ID;
	}
	public void setID(String iD) {
		ID = iD;
	}
	public String getPERSONAL_SCORE() {
		return PERSONAL_SCORE;
	}
	public void setPERSONAL_SCORE(String pERSONAL_SCORE) {
		PERSONAL_SCORE = pERSONAL_SCORE;
	}
	public String getDEPARTMENT_SCORE() {
		return DEPARTMENT_SCORE;
	}
	public void setDEPARTMENT_SCORE(String dEPARTMENT_SCORE) {
		DEPARTMENT_SCORE = dEPARTMENT_SCORE;
	}
	public String getCOMPANY_SCORE() {
		return COMPANY_SCORE;
	}
	public void setCOMPANY_SCORE(String cOMPANY_SCORE) {
		COMPANY_SCORE = cOMPANY_SCORE;
	}
	public String getQUARTER_SCORE() {
		return QUARTER_SCORE;
	}
	public void setQUARTER_SCORE(String qUARTER_SCORE) {
		QUARTER_SCORE = qUARTER_SCORE;
	}
	public String getFLAG() {
		return FLAG;
	}
	public void setFLAG(String fLAG) {
		FLAG = fLAG;
	}
	public String getQUARTER_ASSESS_RESULT_ID() {
		return QUARTER_ASSESS_RESULT_ID;
	}
	public void setQUARTER_ASSESS_RESULT_ID(String qUARTER_ASSESS_RESULT_ID) {
		QUARTER_ASSESS_RESULT_ID = qUARTER_ASSESS_RESULT_ID;
	}
	public String getDEPARTMENT_ID() {
		return DEPARTMENT_ID;
	}
	public void setDEPARTMENT_ID(String dEPARTMENT_ID) {
		DEPARTMENT_ID = dEPARTMENT_ID;
	}
	public String getEMPLOYEE_ID() {
		return EMPLOYEE_ID;
	}
	public void setEMPLOYEE_ID(String eMPLOYEE_ID) {
		EMPLOYEE_ID = eMPLOYEE_ID;
	}
	public String getASSESS_POST_ID() {
		return ASSESS_POST_ID;
	}
	public void setASSESS_POST_ID(String aSSESS_POST_ID) {
		ASSESS_POST_ID = aSSESS_POST_ID;
	}



}
