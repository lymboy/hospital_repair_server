package com.ruoyi.hospital.entity.project;

import com.ruoyi.hospital.util.Const;
import com.ruoyi.hospital.util.Tools;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Author: 郭佳明
 * Date: 2018-03-10 14:48
 * Description: Page类
 */
public class PageBean<T> {
    private int pageSize; //每页显示记录数
    private int totalPage;		//总页数
    private int totalCount;	//总记录数
    private int currentPage;	//当前页
    private int currentResult;	//当前记录起始索引
    private List<T> rows;

    public PageBean(){
        try {
            this.pageSize = Integer.parseInt(Tools.readTxtFile(Const.PAGE));
        } catch (Exception e) {
            this.pageSize = 10;
        }
    }

    public int getTotalPage() {
        if(totalCount %pageSize==0)
            totalPage = totalCount /pageSize;
        else
            totalPage = totalCount /pageSize+1;
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getCurrentPage() {
        if(currentPage<=0)
            currentPage = 1;
        if(currentPage> getTotalPage())
            currentPage = getTotalPage();
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getCurrentResult() {
        currentResult = (getCurrentPage()-1)*getPageSize();
        if(currentResult<0)
            currentResult = 0;
        return currentResult;
    }

    public void setCurrentResult(int currentResult) {
        this.currentResult = currentResult;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public List<T> getRows() {
        return rows;
    }

    public void setRows(List<T> rows) {
        this.rows = rows;
    }
}
