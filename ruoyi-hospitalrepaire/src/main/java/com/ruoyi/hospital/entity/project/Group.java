package com.ruoyi.hospital.entity.project;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Author: 郭佳明
 * Date: 2018-03-09 15:52
 * Description:
 */
public class Group {
    private String uploadMan;
    private String uploadManId;
    private String uploadTime;
    private String billId;
    private List<Pics> pics;

    public Group(Map<String,String> map){
        this.uploadMan = map.get("uploadMan");
        this.uploadManId = map.get("uploadManId");
        this.uploadTime = map.get("uploadTime");
        this.billId = map.get("billId");
        pics = new ArrayList<>();
        addPic(map);
    }

    /**
     * 创建Pic对象，并设置各个属性
     */
    public void addPic(Map<String,String> data){
        Pics pic = new Pics();
        pic.setFileName(data.get("viewName"));
        pic.setSmallPicUrl(sortUrl(data.get("smallPath"),data.get("smallSaveName")));
        pic.setMiddlePicUrl(sortUrl(data.get("middlePath"),data.get("middleSaveName")));
        pic.setBigPicUrl(sortUrl(data.get("rootPath"),data.get("SaveName")));
        pics.add(pic);
    }

    public String sortUrl (String rootPath,String saveName){
        String url = rootPath.substring(rootPath.indexOf("attachment")).
                replace("\\","/")+"/"+saveName;
        return url;
    }

    public String getUploadMan() {
        return uploadMan;
    }

    public void setUploadMan(String uploadMan) {
        this.uploadMan = uploadMan;
    }

    public String getUploadManId() {
        return uploadManId;
    }

    public void setUploadManId(String uploadManId) {
        this.uploadManId = uploadManId;
    }

    public String getUploadTime() {
        return uploadTime;
    }

    public void setUploadTime(String uploadTime) {
        this.uploadTime = uploadTime;
    }

    public List<Pics> getPics() {
        return pics;
    }

    public void setPics(List<Pics> pics) {
        this.pics = pics;
    }

    public String getBillId() {
        return billId;
    }

    public void setBillId(String billId) {
        this.billId = billId;
    }

    public class Pics{
        private String fileName;
        private String smallPicUrl;
        private String middlePicUrl;
        private String bigPicUrl;

        public String getFileName() {
            return fileName;
        }

        public void setFileName(String fileName) {
            this.fileName = fileName;
        }

        public String getSmallPicUrl() {
            return smallPicUrl;
        }

        public void setSmallPicUrl(String smallPicUrl) {
            this.smallPicUrl = smallPicUrl;
        }

        public String getMiddlePicUrl() {
            return middlePicUrl;
        }

        public void setMiddlePicUrl(String middlePicUrl) {
            this.middlePicUrl = middlePicUrl;
        }

        public String getBigPicUrl() {
            return bigPicUrl;
        }

        public void setBigPicUrl(String bigPicUrl) {
            this.bigPicUrl = bigPicUrl;
        }
    }

}
