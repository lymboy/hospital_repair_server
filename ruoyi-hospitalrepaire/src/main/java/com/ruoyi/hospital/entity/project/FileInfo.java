package com.ruoyi.hospital.entity.project;

/**
 * Created with IntelliJ IDEA.
 * Author: 郭佳明
 * Date: 2018-03-15 16:09
 * Description:frame_fileInfo文件信息的实体类
 */
public class FileInfo {
    private String ID;
    private String masterID;
    private String saveName;
    private String viewName;
    private String fileSize;
    private String uploadDate;
    private String uploadType;
    private String rootPath;
    private String userID;
    private String orgID;
    private String smallSaveName;
    private String smallPath;
    private String middleSaveName;
    private String middlePath;

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getMasterID() {
        return masterID;
    }

    public void setMasterID(String masterID) {
        this.masterID = masterID;
    }

    public String getSaveName() {
        return saveName;
    }

    public void setSaveName(String saveName) {
        this.saveName = saveName;
    }

    public String getViewName() {
        return viewName;
    }

    public void setViewName(String viewName) {
        this.viewName = viewName;
    }

    public String getFileSize() {
        return fileSize;
    }

    public void setFileSize(String fileSize) {
        this.fileSize = fileSize;
    }

    public String getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(String uploadDate) {
        this.uploadDate = uploadDate;
    }

    public String getRootPath() {
        return rootPath;
    }

    public void setRootPath(String rootPath) {
        this.rootPath = rootPath;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getOrgID() {
        return orgID;
    }

    public void setOrgID(String orgID) {
        this.orgID = orgID;
    }

    public String getSmallSaveName() {
        return smallSaveName;
    }

    public void setSmallSaveName(String smallSaveName) {
        this.smallSaveName = smallSaveName;
    }

    public String getSmallPath() {
        return smallPath;
    }

    public void setSmallPath(String smallPath) {
        this.smallPath = smallPath;
    }

    public String getMiddleSaveName() {
        return middleSaveName;
    }

    public void setMiddleSaveName(String middleSaveName) {
        this.middleSaveName = middleSaveName;
    }

    public String getMiddlePath() {
        return middlePath;
    }

    public void setMiddlePath(String middlePath) {
        this.middlePath = middlePath;
    }

    public String getUploadType() {
        return uploadType;
    }

    public void setUploadType(String uploadType) {
        this.uploadType = uploadType;
    }
}
