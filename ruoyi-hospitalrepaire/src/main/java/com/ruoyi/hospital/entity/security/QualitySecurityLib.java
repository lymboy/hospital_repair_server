package com.ruoyi.hospital.entity.security;

import java.io.Serializable;
import java.util.Date;

/**
 * <p> 安全隐患清单库 </p>
 *
 * @author Young
 * @since 2018-07-26
 */
public class QualitySecurityLib implements Serializable {

    private static final long serialVersionUID = -6771648925381903708L;

    private String id;

    private String billCode;

    private String billName;

    private String projectType;

    private String editor;

    private Date editDate;

    private Integer fastApproveType;

    private Integer versionNo;

    private Date createdate;

    private String creater;

    private Date modifydate;

    private String modifier;

    private Integer flag;

    private String remark;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBillCode() {
        return billCode;
    }

    public void setBillCode(String billCode) {
        this.billCode = billCode;
    }

    public String getBillName() {
        return billName;
    }

    public void setBillName(String billName) {
        this.billName = billName;
    }

    public String getProjectType() {
        return projectType;
    }

    public void setProjectType(String projectType) {
        this.projectType = projectType;
    }

    public String getEditor() {
        return editor;
    }

    public void setEditor(String editor) {
        this.editor = editor;
    }

    public Date getEditDate() {
        return editDate;
    }

    public void setEditDate(Date editDate) {
        this.editDate = editDate;
    }

    public Integer getFastApproveType() {
        return fastApproveType;
    }

    public void setFastApproveType(Integer fastApproveType) {
        this.fastApproveType = fastApproveType;
    }

    public Integer getVersionNo() {
        return versionNo;
    }

    public void setVersionNo(Integer versionNo) {
        this.versionNo = versionNo;
    }

    public Date getCreatedate() {
        return createdate;
    }

    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }

    public String getCreater() {
        return creater;
    }

    public void setCreater(String creater) {
        this.creater = creater;
    }

    public Date getModifydate() {
        return modifydate;
    }

    public void setModifydate(Date modifydate) {
        this.modifydate = modifydate;
    }

    public String getModifier() {
        return modifier;
    }

    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "QualitySecurityLib{" +
                "id='" + id + '\'' +
                ", billCode='" + billCode + '\'' +
                ", billName='" + billName + '\'' +
                ", projectType='" + projectType + '\'' +
                ", editor='" + editor + '\'' +
                ", editDate=" + editDate +
                ", fastApproveType=" + fastApproveType +
                ", versionNo=" + versionNo +
                ", createdate=" + createdate +
                ", creater='" + creater + '\'' +
                ", modifydate=" + modifydate +
                ", modifier='" + modifier + '\'' +
                ", flag=" + flag +
                ", remark='" + remark + '\'' +
                '}';
    }
}
