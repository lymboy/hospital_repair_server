package com.ruoyi.hospital.entity.currency;

import java.io.Serializable;

public class CurrencyType implements Serializable{
	/**
	 *
	 */
	private static final long serialVersionUID = 1937192619393707602L;

	private Long typeId;

	private String typeName;

	private Long typeBelongto;

	private Integer typeSerialNo;

	private Long kindId;

	public Long getTypeId() {
		return typeId;
	}

	public void setTypeId(Long typeId) {
		this.typeId = typeId;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName == null ? null : typeName.trim();
	}

	public Long getTypeBelongto() {
		return typeBelongto;
	}

	public void setTypeBelongto(Long typeBelongto) {
		this.typeBelongto = typeBelongto;
	}

	public Integer getTypeSerialNo() {
		return typeSerialNo;
	}

	public void setTypeSerialNo(Integer typeSerialNo) {
		this.typeSerialNo = typeSerialNo;
	}

	public Long getKindId() {
		return kindId;
	}

	public void setKindId(Long kindId) {
		this.kindId = kindId;
	}

	@Override
	public String toString() {
		return "CurrencyType [typeId=" + typeId + ", typeName=" + typeName
				+ ", typeBelongto=" + typeBelongto + ", typeSerialNo="
				+ typeSerialNo + ", kindId=" + kindId + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((kindId == null) ? 0 : kindId.hashCode());
		result = prime * result
				+ ((typeBelongto == null) ? 0 : typeBelongto.hashCode());
		result = prime * result + ((typeId == null) ? 0 : typeId.hashCode());
		result = prime * result
				+ ((typeName == null) ? 0 : typeName.hashCode());
		result = prime * result
				+ ((typeSerialNo == null) ? 0 : typeSerialNo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CurrencyType other = (CurrencyType) obj;
		if (kindId == null) {
			if (other.kindId != null)
				return false;
		} else if (!kindId.equals(other.kindId))
			return false;
		if (typeBelongto == null) {
			if (other.typeBelongto != null)
				return false;
		} else if (!typeBelongto.equals(other.typeBelongto))
			return false;
		if (typeId == null) {
			if (other.typeId != null)
				return false;
		} else if (!typeId.equals(other.typeId))
			return false;
		if (typeName == null) {
			if (other.typeName != null)
				return false;
		} else if (!typeName.equals(other.typeName))
			return false;
		if (typeSerialNo == null) {
			if (other.typeSerialNo != null)
				return false;
		} else if (!typeSerialNo.equals(other.typeSerialNo))
			return false;
		return true;
	}

}
