package com.ruoyi.hospital.entity.system;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>Title: DataPrivilege</p>
 * @Desc: 数据权限信息
 * @author: 杨建军
 *	@Date: 2017年3月26日
 * @Time: 下午5:38:13
 * @version:
 */
public class DataPrivilege implements Serializable{

	/**
	 *
	 */
	private static final long serialVersionUID = -1655828410573174110L;

	private Long dataPrivilegeId;
	private String dataPrivilegeName;
	private Integer dataPrivilegeNo;
	private Date createdate;
    private Long creater;
    private Date modifydate;
    private Long modifier;
    private Integer flag;
    private String remark;
	public Long getDataPrivilegeId() {
		return dataPrivilegeId;
	}
	public void setDataPrivilegeId(Long dataPrivilegeId) {
		this.dataPrivilegeId = dataPrivilegeId;
	}
	public String getDataPrivilegeName() {
		return dataPrivilegeName;
	}
	public void setDataPrivilegeName(String dataPrivilegeName) {
		this.dataPrivilegeName = dataPrivilegeName;
	}
	public Integer getDataPrivilegeNo() {
		return dataPrivilegeNo;
	}
	public void setDataPrivilegeNo(Integer dataPrivilegeNo) {
		this.dataPrivilegeNo = dataPrivilegeNo;
	}
	public Date getCreatedate() {
		return createdate;
	}
	public void setCreatedate(Date createdate) {
		this.createdate = createdate;
	}
	public Long getCreater() {
		return creater;
	}
	public void setCreater(Long creater) {
		this.creater = creater;
	}
	public Date getModifydate() {
		return modifydate;
	}
	public void setModifydate(Date modifydate) {
		this.modifydate = modifydate;
	}
	public Long getModifier() {
		return modifier;
	}
	public void setModifier(Long modifier) {
		this.modifier = modifier;
	}
	public Integer getFlag() {
		return flag;
	}
	public void setFlag(Integer flag) {
		this.flag = flag;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}


	@Override
	public String toString() {
		return "DataPrivilege [dataPrivilegeId=" + dataPrivilegeId + ", dataPrivilegeName=" + dataPrivilegeName
				+ ", dataPrivilegeNo=" + dataPrivilegeNo + ", createdate=" + createdate + ", creater=" + creater
				+ ", modifydate=" + modifydate + ", modifier=" + modifier + ", flag=" + flag + ", remark=" + remark
				+ "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((createdate == null) ? 0 : createdate.hashCode());
		result = prime * result + ((creater == null) ? 0 : creater.hashCode());
		result = prime * result + ((dataPrivilegeId == null) ? 0 : dataPrivilegeId.hashCode());
		result = prime * result + ((dataPrivilegeName == null) ? 0 : dataPrivilegeName.hashCode());
		result = prime * result + ((dataPrivilegeNo == null) ? 0 : dataPrivilegeNo.hashCode());
		result = prime * result + ((flag == null) ? 0 : flag.hashCode());
		result = prime * result + ((modifier == null) ? 0 : modifier.hashCode());
		result = prime * result + ((modifydate == null) ? 0 : modifydate.hashCode());
		result = prime * result + ((remark == null) ? 0 : remark.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DataPrivilege other = (DataPrivilege) obj;
		if (createdate == null) {
			if (other.createdate != null)
				return false;
		} else if (!createdate.equals(other.createdate))
			return false;
		if (creater == null) {
			if (other.creater != null)
				return false;
		} else if (!creater.equals(other.creater))
			return false;
		if (dataPrivilegeId == null) {
			if (other.dataPrivilegeId != null)
				return false;
		} else if (!dataPrivilegeId.equals(other.dataPrivilegeId))
			return false;
		if (dataPrivilegeName == null) {
			if (other.dataPrivilegeName != null)
				return false;
		} else if (!dataPrivilegeName.equals(other.dataPrivilegeName))
			return false;
		if (dataPrivilegeNo == null) {
			if (other.dataPrivilegeNo != null)
				return false;
		} else if (!dataPrivilegeNo.equals(other.dataPrivilegeNo))
			return false;
		if (flag == null) {
			if (other.flag != null)
				return false;
		} else if (!flag.equals(other.flag))
			return false;
		if (modifier == null) {
			if (other.modifier != null)
				return false;
		} else if (!modifier.equals(other.modifier))
			return false;
		if (modifydate == null) {
			if (other.modifydate != null)
				return false;
		} else if (!modifydate.equals(other.modifydate))
			return false;
		if (remark == null) {
			if (other.remark != null)
				return false;
		} else if (!remark.equals(other.remark))
			return false;
		return true;
	}

}
