package com.ruoyi.hospital.entity.security;

import java.io.Serializable;
import java.util.Date;

/**
 * <p> 安全隐患清单库 明细 </p>
 *
 * @author Young
 * @since 2018-07-26
 */
public class QualitySecurityLibDetail implements Serializable {

    private static final long serialVersionUID = 1481736392356116107L;

    private String id;

    private String qualitySecurityLibId;

    private String securityDetailCode;

    private String securityDetailName;

    private String hiddenDangerGrade;

    private String modifyRequireDesc;

    private String parentId;

    private String baseId;

    private String baseCode;

    private Date createdate;

    private String creater;

    private Date modifydate;

    private String modifier;

    private Integer flag;

    private String remark;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getQualitySecurityLibId() {
        return qualitySecurityLibId;
    }

    public void setQualitySecurityLibId(String qualitySecurityLibId) {
        this.qualitySecurityLibId = qualitySecurityLibId;
    }

    public String getSecurityDetailCode() {
        return securityDetailCode;
    }

    public void setSecurityDetailCode(String securityDetailCode) {
        this.securityDetailCode = securityDetailCode;
    }

    public String getSecurityDetailName() {
        return securityDetailName;
    }

    public void setSecurityDetailName(String securityDetailName) {
        this.securityDetailName = securityDetailName;
    }

    public String getHiddenDangerGrade() {
        return hiddenDangerGrade;
    }

    public void setHiddenDangerGrade(String hiddenDangerGrade) {
        this.hiddenDangerGrade = hiddenDangerGrade;
    }

    public String getModifyRequireDesc() {
        return modifyRequireDesc;
    }

    public void setModifyRequireDesc(String modifyRequireDesc) {
        this.modifyRequireDesc = modifyRequireDesc;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getBaseId() {
        return baseId;
    }

    public void setBaseId(String baseId) {
        this.baseId = baseId;
    }

    public String getBaseCode() {
        return baseCode;
    }

    public void setBaseCode(String baseCode) {
        this.baseCode = baseCode;
    }

    public Date getCreatedate() {
        return createdate;
    }

    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }

    public String getCreater() {
        return creater;
    }

    public void setCreater(String creater) {
        this.creater = creater;
    }

    public Date getModifydate() {
        return modifydate;
    }

    public void setModifydate(Date modifydate) {
        this.modifydate = modifydate;
    }

    public String getModifier() {
        return modifier;
    }

    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "QualitySecurityLibDetail{" +
                "id='" + id + '\'' +
                ", qualitySecurityLibId='" + qualitySecurityLibId + '\'' +
                ", securityDetailCode='" + securityDetailCode + '\'' +
                ", securityDetailName='" + securityDetailName + '\'' +
                ", hiddenDangerGrade='" + hiddenDangerGrade + '\'' +
                ", modifyRequireDesc='" + modifyRequireDesc + '\'' +
                ", parentId='" + parentId + '\'' +
                ", baseId='" + baseId + '\'' +
                ", baseCode='" + baseCode + '\'' +
                ", createdate=" + createdate +
                ", creater='" + creater + '\'' +
                ", modifydate=" + modifydate +
                ", modifier='" + modifier + '\'' +
                ", flag=" + flag +
                ", remark='" + remark + '\'' +
                '}';
    }
}
