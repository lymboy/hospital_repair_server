package com.ruoyi.hospital.entity.project;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Author: 郭佳明
 * Date: 2018-03-09 8:46
 * Description:ieqm_proQaAcceptance的实体类
 */
public class ProjectQaAcceptance implements Serializable{
    private static final long serialVersionUID = -710919945823405285L;

    private String checkTypeNo;
    private int statusFlag;
    private double x;
    private double y;
    private String gpsAdd;
    private String item;
    private String createDate;
    private String topGrid;
    private String createUserId;
    private String unitName;
    private String id;
    private String billNo;
    private String statusName;
    private String prepare;
    private String prepareId;
    private String prepareDate;
    private String createOrgId;
    private String createOrgNo;
    private String createOrgName;
    private String auditTime;
    private String remark;
    private String projectId;
    private String projectNo;
    private String projectName;
    private String checkType;
    private String unitFullName;
    private String unitId;
    private String unitInfo;
    private int status;
    private String createUserName;
    private Map<String,Group> items = new HashMap<>();

    public Map<String, Group> getItems() {
        return items;
    }

    public void setItems(Group group) {
        this.items.put("group"+(items.size()+1),group);
    }

    public String getCreateUserName() {
        return createUserName;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }

    public String getCheckTypeNo() {
        return checkTypeNo;
    }

    public void setCheckTypeNo(String checkTypeNo) {
        this.checkTypeNo = checkTypeNo;
    }

    public int getStatusFlag() {
        return statusFlag;
    }

    public void setStatusFlag(int statusFlag) {
        this.statusFlag = statusFlag;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public String getGpsAdd() {
        return gpsAdd;
    }

    public void setGpsAdd(String gpsAdd) {
        this.gpsAdd = gpsAdd;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getTopGrid() {
        return topGrid;
    }

    public void setTopGrid(String topGrid) {
        this.topGrid = topGrid;
    }

    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getPrepare() {
        return prepare;
    }

    public void setPrepare(String prepare) {
        this.prepare = prepare;
    }

    public String getPrepareId() {
        return prepareId;
    }

    public void setPrepareId(String prepareId) {
        this.prepareId = prepareId;
    }

    public String getPrepareDate() {
        return prepareDate;
    }

    public void setPrepareDate(String prepareDate) {
        this.prepareDate = prepareDate;
    }

    public String getCreateOrgId() {
        return createOrgId;
    }

    public void setCreateOrgId(String createOrgId) {
        this.createOrgId = createOrgId;
    }

    public String getCreateOrgNo() {
        return createOrgNo;
    }

    public void setCreateOrgNo(String createOrgNo) {
        this.createOrgNo = createOrgNo;
    }

    public String getCreateOrgName() {
        return createOrgName;
    }

    public void setCreateOrgName(String createOrgName) {
        this.createOrgName = createOrgName;
    }

    public String getAuditTime() {
        return auditTime;
    }

    public void setAuditTime(String auditTime) {
        this.auditTime = auditTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getProjectNo() {
        return projectNo;
    }

    public void setProjectNo(String projectNo) {
        this.projectNo = projectNo;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getCheckType() {
        return checkType;
    }

    public void setCheckType(String checkType) {
        this.checkType = checkType;
    }

    public String getUnitFullName() {
        return unitFullName;
    }

    public void setUnitFullName(String unitFullName) {
        this.unitFullName = unitFullName;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getUnitInfo() {
        return unitInfo;
    }

    public void setUnitInfo(String unitInfo) {
        this.unitInfo = unitInfo;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof ProjectQaAcceptance){
            return ((ProjectQaAcceptance) obj).id == this.id;
        }
        return false;
    }
}
