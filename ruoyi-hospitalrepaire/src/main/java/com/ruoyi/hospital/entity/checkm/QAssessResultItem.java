package com.ruoyi.hospital.entity.checkm;

import java.util.Date;

/**
 * 2017年3月19日
 *
 */
public class QAssessResultItem {
	private String ID;
	private String QUARTER1;
	private String QUARTER2;
	private String QUARTER3;
	private String QUARTER4;
	private String CALCULATION_SCORE;
	private String MODIFIED_SCORE;
	private String FINAL_SCORE;
	private String EVALUATION_GRADE_NEWEST;
	private String MODIFIED_EXPLAIN;
	private String YEAR_ASSESS_RESULT_ID;
	private String FLAG;
	private String EMPLOYEE_ID;
	private String DEPARTMENT_ID;
	private String ASSESS_POST_ID;
	private Date CREATEDATE;
	private String CREATER;

	@Override
	public String toString() {
		return "QAssessResultItem [ID=" + ID + ", QUARTER1=" + QUARTER1
				+ ", QUARTER2=" + QUARTER2 + ", QUARTER3=" + QUARTER3
				+ ", QUARTER4=" + QUARTER4 + ", CALCULATION_SCORE="
				+ CALCULATION_SCORE + ", MODIFIED_SCORE=" + MODIFIED_SCORE
				+ ", FINAL_SCORE=" + FINAL_SCORE + ", EVALUATION_GRADE_NEWEST="
				+ EVALUATION_GRADE_NEWEST + ", MODIFIED_EXPLAIN="
				+ MODIFIED_EXPLAIN + ", YEAR_ASSESS_RESULT_ID="
				+ YEAR_ASSESS_RESULT_ID + ", FLAG=" + FLAG + ", EMPLOYEE_ID="
				+ EMPLOYEE_ID + ", DEPARTMENT_ID=" + DEPARTMENT_ID
				+ ", ASSESS_POST_ID=" + ASSESS_POST_ID + ", CREATEDATE="
				+ CREATEDATE + ", CREATER=" + CREATER + "]";
	}
	public Date getCREATEDATE() {
		return CREATEDATE;
	}
	public void setCREATEDATE(Date cREATEDATE) {
		CREATEDATE = cREATEDATE;
	}
	public String getCREATER() {
		return CREATER;
	}
	public void setCREATER(String cREATER) {
		CREATER = cREATER;
	}
	public String getID() {
		return ID;
	}
	public void setID(String iD) {
		ID = iD;
	}
	public String getQUARTER1() {
		return QUARTER1;
	}
	public void setQUARTER1(String qUARTER1) {
		QUARTER1 = qUARTER1;
	}
	public String getQUARTER2() {
		return QUARTER2;
	}
	public void setQUARTER2(String qUARTER2) {
		QUARTER2 = qUARTER2;
	}
	public String getQUARTER3() {
		return QUARTER3;
	}
	public void setQUARTER3(String qUARTER3) {
		QUARTER3 = qUARTER3;
	}
	public String getQUARTER4() {
		return QUARTER4;
	}
	public void setQUARTER4(String qUARTER4) {
		QUARTER4 = qUARTER4;
	}
	public String getCALCULATION_SCORE() {
		return CALCULATION_SCORE;
	}
	public void setCALCULATION_SCORE(String cALCULATION_SCORE) {
		CALCULATION_SCORE = cALCULATION_SCORE;
	}
	public String getMODIFIED_SCORE() {
		return MODIFIED_SCORE;
	}
	public void setMODIFIED_SCORE(String mODIFIED_SCORE) {
		MODIFIED_SCORE = mODIFIED_SCORE;
	}
	public String getFINAL_SCORE() {
		return FINAL_SCORE;
	}
	public void setFINAL_SCORE(String fINAL_SCORE) {
		FINAL_SCORE = fINAL_SCORE;
	}
	public String getEVALUATION_GRADE_NEWEST() {
		return EVALUATION_GRADE_NEWEST;
	}
	public void setEVALUATION_GRADE_NEWEST(String eVALUATION_GRADE_NEWEST) {
		EVALUATION_GRADE_NEWEST = eVALUATION_GRADE_NEWEST;
	}
	public String getMODIFIED_EXPLAIN() {
		return MODIFIED_EXPLAIN;
	}
	public void setMODIFIED_EXPLAIN(String mODIFIED_EXPLAIN) {
		MODIFIED_EXPLAIN = mODIFIED_EXPLAIN;
	}
	public String getYEAR_ASSESS_RESULT_ID() {
		return YEAR_ASSESS_RESULT_ID;
	}
	public void setYEAR_ASSESS_RESULT_ID(String yEAR_ASSESS_RESULT_ID) {
		YEAR_ASSESS_RESULT_ID = yEAR_ASSESS_RESULT_ID;
	}
	public String getFLAG() {
		return FLAG;
	}
	public void setFLAG(String fLAG) {
		FLAG = fLAG;
	}
	public String getEMPLOYEE_ID() {
		return EMPLOYEE_ID;
	}
	public void setEMPLOYEE_ID(String eMPLOYEE_ID) {
		EMPLOYEE_ID = eMPLOYEE_ID;
	}
	public String getDEPARTMENT_ID() {
		return DEPARTMENT_ID;
	}
	public void setDEPARTMENT_ID(String dEPARTMENT_ID) {
		DEPARTMENT_ID = dEPARTMENT_ID;
	}
	public String getASSESS_POST_ID() {
		return ASSESS_POST_ID;
	}
	public void setASSESS_POST_ID(String aSSESS_POST_ID) {
		ASSESS_POST_ID = aSSESS_POST_ID;
	}

}
