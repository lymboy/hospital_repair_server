package com.ruoyi.hospital.entity.system;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>Title: Identity</p>
 * @Desc: 身份信息
 * @author: 杨建军
 *	@Date: 2017年3月30日
 * @Time: 下午2:41:20
 * @version:
 */
public class UserIdentity implements Serializable{

	/**
	 *
	 */
	private static final long serialVersionUID = 5428157164325077627L;
		private Long identityId;
		private String identityName;
		private Integer identityNo;
		private Long userId;
		private String employeeId;
		private String departmentId;
		private String postId;
		private Date crearedate;
		private Long creater;
		private Date modifydate;
		private Long modifier;
		private Integer flag;
		private String departmentType;
		private String remark;
		private String accountNumber;
		private String employeeName;
		private String departmentName;
		private String postName;
		public Long getIdentityId() {
			return identityId;
		}
		public void setIdentityId(Long identityId) {
			this.identityId = identityId;
		}
		public String getIdentityName() {
			return identityName;
		}
		public void setIdentityName(String identityName) {
			this.identityName = identityName;
		}
		public Integer getIdentityNo() {
			return identityNo;
		}
		public void setIdentityNo(Integer identityNo) {
			this.identityNo = identityNo;
		}
		public Long getUserId() {
			return userId;
		}
		public void setUserId(Long userId) {
			this.userId = userId;
		}
		public String getEmployeeId() {
			return employeeId;
		}
		public void setEmployeeId(String employeeId) {
			this.employeeId = employeeId;
		}
		public String getDepartmentId() {
			return departmentId;
		}
		public void setDepartmentId(String departmentId) {
			this.departmentId = departmentId;
		}
		public String getPostId() {
			return postId;
		}
		public void setPostId(String postId) {
			this.postId = postId;
		}
		public Date getCrearedate() {
			return crearedate;
		}
		public void setCrearedate(Date crearedate) {
			this.crearedate = crearedate;
		}
		public Long getCreater() {
			return creater;
		}
		public void setCreater(Long creater) {
			this.creater = creater;
		}
		public Date getModifydate() {
			return modifydate;
		}
		public void setModifydate(Date modifydate) {
			this.modifydate = modifydate;
		}
		public Long getModifier() {
			return modifier;
		}
		public void setModifier(Long modifier) {
			this.modifier = modifier;
		}
		public Integer getFlag() {
			return flag;
		}
		public void setFlag(Integer flag) {
			this.flag = flag;
		}
		public String getDepartmentType() {
			return departmentType;
		}
		public void setDepartmentType(String departmentType) {
			this.departmentType = departmentType;
		}
		public String getRemark() {
			return remark;
		}
		public void setRemark(String remark) {
			this.remark = remark;
		}
		public String getAccountNumber() {
			return accountNumber;
		}
		public void setAccountNumber(String accountNumber) {
			this.accountNumber = accountNumber;
		}
		public String getEmployeeName() {
			return employeeName;
		}
		public void setEmployeeName(String employeeName) {
			this.employeeName = employeeName;
		}
		public String getDepartmentName() {
			return departmentName;
		}
		public void setDepartmentName(String departmentName) {
			this.departmentName = departmentName;
		}
		public String getPostName() {
			return postName;
		}
		public void setPostName(String postName) {
			this.postName = postName;
		}
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((accountNumber == null) ? 0 : accountNumber.hashCode());
			result = prime * result + ((crearedate == null) ? 0 : crearedate.hashCode());
			result = prime * result + ((creater == null) ? 0 : creater.hashCode());
			result = prime * result + ((departmentId == null) ? 0 : departmentId.hashCode());
			result = prime * result + ((departmentName == null) ? 0 : departmentName.hashCode());
			result = prime * result + ((departmentType == null) ? 0 : departmentType.hashCode());
			result = prime * result + ((employeeId == null) ? 0 : employeeId.hashCode());
			result = prime * result + ((employeeName == null) ? 0 : employeeName.hashCode());
			result = prime * result + ((flag == null) ? 0 : flag.hashCode());
			result = prime * result + ((identityId == null) ? 0 : identityId.hashCode());
			result = prime * result + ((identityName == null) ? 0 : identityName.hashCode());
			result = prime * result + ((identityNo == null) ? 0 : identityNo.hashCode());
			result = prime * result + ((modifier == null) ? 0 : modifier.hashCode());
			result = prime * result + ((modifydate == null) ? 0 : modifydate.hashCode());
			result = prime * result + ((postId == null) ? 0 : postId.hashCode());
			result = prime * result + ((postName == null) ? 0 : postName.hashCode());
			result = prime * result + ((remark == null) ? 0 : remark.hashCode());
			result = prime * result + ((userId == null) ? 0 : userId.hashCode());
			return result;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			UserIdentity other = (UserIdentity) obj;
			if (accountNumber == null) {
				if (other.accountNumber != null)
					return false;
			} else if (!accountNumber.equals(other.accountNumber))
				return false;
			if (crearedate == null) {
				if (other.crearedate != null)
					return false;
			} else if (!crearedate.equals(other.crearedate))
				return false;
			if (creater == null) {
				if (other.creater != null)
					return false;
			} else if (!creater.equals(other.creater))
				return false;
			if (departmentId == null) {
				if (other.departmentId != null)
					return false;
			} else if (!departmentId.equals(other.departmentId))
				return false;
			if (departmentName == null) {
				if (other.departmentName != null)
					return false;
			} else if (!departmentName.equals(other.departmentName))
				return false;
			if (departmentType == null) {
				if (other.departmentType != null)
					return false;
			} else if (!departmentType.equals(other.departmentType))
				return false;
			if (employeeId == null) {
				if (other.employeeId != null)
					return false;
			} else if (!employeeId.equals(other.employeeId))
				return false;
			if (employeeName == null) {
				if (other.employeeName != null)
					return false;
			} else if (!employeeName.equals(other.employeeName))
				return false;
			if (flag == null) {
				if (other.flag != null)
					return false;
			} else if (!flag.equals(other.flag))
				return false;
			if (identityId == null) {
				if (other.identityId != null)
					return false;
			} else if (!identityId.equals(other.identityId))
				return false;
			if (identityName == null) {
				if (other.identityName != null)
					return false;
			} else if (!identityName.equals(other.identityName))
				return false;
			if (identityNo == null) {
				if (other.identityNo != null)
					return false;
			} else if (!identityNo.equals(other.identityNo))
				return false;
			if (modifier == null) {
				if (other.modifier != null)
					return false;
			} else if (!modifier.equals(other.modifier))
				return false;
			if (modifydate == null) {
				if (other.modifydate != null)
					return false;
			} else if (!modifydate.equals(other.modifydate))
				return false;
			if (postId == null) {
				if (other.postId != null)
					return false;
			} else if (!postId.equals(other.postId))
				return false;
			if (postName == null) {
				if (other.postName != null)
					return false;
			} else if (!postName.equals(other.postName))
				return false;
			if (remark == null) {
				if (other.remark != null)
					return false;
			} else if (!remark.equals(other.remark))
				return false;
			if (userId == null) {
				if (other.userId != null)
					return false;
			} else if (!userId.equals(other.userId))
				return false;
			return true;
		}


}
