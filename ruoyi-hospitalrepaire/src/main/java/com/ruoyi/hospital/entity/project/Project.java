package com.ruoyi.hospital.entity.project;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>Title: Project.java</p>
 * @Desc: 项目
 * @author 杨建军
 * @Date: 2017年5月1日
 * @Time: 下午2:48:19
 * @version:
 */
public class Project implements Serializable{

	/**
	 *
	 */
	private static final long serialVersionUID = -5420679669734239183L;
	private String id;
	private String departmentId;
	private String projectName;
	private String shortName;
	private String projectNo;
	private String projectDuty;
	private Date projectEstablishTime;
	private String projectDesc;
	private String remark;
	private Integer flag;
	private Integer projectBaseNum;

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDepartmentId() {
		return departmentId;
	}
	public void setDepartmentId(String departmentId) {
		this.departmentId = departmentId;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getShortName() {
		return shortName;
	}
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}
	public String getProjectNo() {
		return projectNo;
	}
	public void setProjectNo(String projectNo) {
		this.projectNo = projectNo;
	}
	public String getProjectDuty() {
		return projectDuty;
	}
	public void setProjectDuty(String projectDuty) {
		this.projectDuty = projectDuty;
	}
	public Date getProjectEstablishTime() {
		return projectEstablishTime;
	}
	public void setProjectEstablishTime(Date projectEstablishTime) {
		this.projectEstablishTime = projectEstablishTime;
	}
	public String getProjectDesc() {
		return projectDesc;
	}
	public void setProjectDesc(String projectDesc) {
		this.projectDesc = projectDesc;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Integer getFlag() {
		return flag;
	}
	public void setFlag(Integer flag) {
		this.flag = flag;
	}
	public Integer getProjectBaseNum() {
		return projectBaseNum;
	}
	public void setProjectBaseNum(Integer projectBaseNum) {
		this.projectBaseNum = projectBaseNum;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((departmentId == null) ? 0 : departmentId.hashCode());
		result = prime * result + ((flag == null) ? 0 : flag.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((projectBaseNum == null) ? 0 : projectBaseNum.hashCode());
		result = prime * result + ((projectDesc == null) ? 0 : projectDesc.hashCode());
		result = prime * result + ((projectDuty == null) ? 0 : projectDuty.hashCode());
		result = prime * result + ((projectEstablishTime == null) ? 0 : projectEstablishTime.hashCode());
		result = prime * result + ((projectName == null) ? 0 : projectName.hashCode());
		result = prime * result + ((projectNo == null) ? 0 : projectNo.hashCode());
		result = prime * result + ((remark == null) ? 0 : remark.hashCode());
		result = prime * result + ((shortName == null) ? 0 : shortName.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Project other = (Project) obj;
		if (departmentId == null) {
			if (other.departmentId != null)
				return false;
		} else if (!departmentId.equals(other.departmentId))
			return false;
		if (flag == null) {
			if (other.flag != null)
				return false;
		} else if (!flag.equals(other.flag))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (projectBaseNum == null) {
			if (other.projectBaseNum != null)
				return false;
		} else if (!projectBaseNum.equals(other.projectBaseNum))
			return false;
		if (projectDesc == null) {
			if (other.projectDesc != null)
				return false;
		} else if (!projectDesc.equals(other.projectDesc))
			return false;
		if (projectDuty == null) {
			if (other.projectDuty != null)
				return false;
		} else if (!projectDuty.equals(other.projectDuty))
			return false;
		if (projectEstablishTime == null) {
			if (other.projectEstablishTime != null)
				return false;
		} else if (!projectEstablishTime.equals(other.projectEstablishTime))
			return false;
		if (projectName == null) {
			if (other.projectName != null)
				return false;
		} else if (!projectName.equals(other.projectName))
			return false;
		if (projectNo == null) {
			if (other.projectNo != null)
				return false;
		} else if (!projectNo.equals(other.projectNo))
			return false;
		if (remark == null) {
			if (other.remark != null)
				return false;
		} else if (!remark.equals(other.remark))
			return false;
		if (shortName == null) {
			if (other.shortName != null)
				return false;
		} else if (!shortName.equals(other.shortName))
			return false;
		return true;
	}


}
