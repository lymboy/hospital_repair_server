package com.ruoyi.hospital.config;

import java.io.FileWriter;
import java.io.IOException;

/* *
 *类名：AlipayConfig
 *功能：基础配置类
 *详细：设置帐户有关信息及返回路径
 *修改日期：2017-04-05
 *说明：
 *以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
 *该代码仅供学习和研究支付宝接口使用，只是提供一个参考。
 */

public class AlipayConfig {

//↓↓↓↓↓↓↓↓↓↓请在这里配置您的基本信息↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓

	// 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号
	public static String app_id = "2016101800712913";

	// 商户私钥，您的PKCS8格式RSA2私钥
	public static String merchant_private_key = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDJ0cbZuUW/7t156JxhXwUXNpHFD7aOxuENFOKuuYXTt27rkAMxLsB35qtpfhR3s1pIog4XeUukUnxiFYsA3B5bcvooi2NVxEdzxiyOq0/wLEYYDuEbf2jO416wzhv99hz2a36r52BvU8/mjwYZHwND+7zQ89Gyw58oNRmN5a9vLxBtxlJFRUaKToyjOvb1CxApd2AN3clBal8MhTrRLrqCOG7mX396fpwpRMY+RZo1JBJvF+m/YVqK8wYQ+K/tZq0SoA0V/TCcQnbbOwn0mkkox7rPYi2H+AeihojzTDSG4pJycKrx2CtR9782Ueh7rvt1I3e73IkPlwEPtIlsNG4JAgMBAAECggEACwsBeHRczq3BTKRfuiw62RRRUZkUgid7tP2Jkcnw+q600QrmQdCvE2B5zKHreMlFjWfgoYQvkWQ6DhvjqT5iItrduU43N0AwMLONSJkzHI8vnq1hv+Fu2LM0QTRl77WBzDIBkmJssmrBm3oD+WfrA9NId55/wOpXURnmtg71VH4D2CnfNGZCiZ/CfITKObhHSTZFy7OVJn1yrFIwQ3SwhX4huWm0OG0lsSH0Mq9ZCRI0nlut49edXbKyk/oGipq2XRphJRSutepql16em4NwrMuRXnLSN5Y5DOnJo2+B0PTpS4Jrbp361vHzYNjpIfwHZb4CZ3c2Vx9m0Zz3wcWIAQKBgQDy3XAm69LOy+p5l5i0xwHJZvFBlvq/LwaYRzQAIRSvnteOkgmr7Xh1/I4W7fVvBEP0cVeIhGxNj9ZoP4E0oeZ64jBtptcXNDE90Kfh7XQpnirvDoaZsDTOVQFD4vXh5ms9seYVVYFmth68bIWuRvs7Lc0/kbK6TNb9VS2vEbjZiQKBgQDUvAv4W8URqoYMLBiRYorYtv2NQvXTPXdBATjqVF7c5p2c1ATl0SyMPZYt6WctBGIRGdt9ZEFZs5NjOzjD7HbYrlANIDSI333XHHp1HkaFLqV+XeY9AUeIdpSmflY7v9NDBIYXUkeHk6cokTy8yIlj6wCK3s3fcRmKS5qZ6HRQgQKBgHLfFiryo/U4MiFs+MeY0VmHnVkepl9DSOVB2HCvNO+UO/StUnHFkpKL8FEcIFhwNgwqh/KEYA2XmRwZ3W0dnrxEvA9BOL866oeIZ7/O5oDA1SHZRwTueSuWvVB0y0QcDmFSAbfZh6kjPwp0cg5qeG4XTdtheJ3xtkaAwWjrAvmxAoGBAJyRkhXGJPJtmJ7YzXQjHKGHfHE9OQOxl1WGescsNW1LZfEIsloaazkEEsXTpJYjRbHkYACXiulkGzPLYGoCXS34jP48F64tHsb1NMOB9tEKlc69eTio+mbbL2iiTwFGm0wVxkLf0blc2Igo7JTc2xFo4ktS1B6BdRSttClWRlUBAoGAIceIPwAYa4p5kUZTjOqLgLFS6N495HQ4Sm2Zy9OMfiFWy8TRiFNR4eCGZl2Oid7S1O37OhNND9J0SY6tkFnmm+FYbfi4R7ihaxyFcw2FP1fhXuuL8c7NID4Az6GJT/dlet4MZNqa95Ce785mA34SGbYrZaX4wUwUE0wTf3Spxv8=";

	// 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
	public static String alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAo0vasw8lpOkfnR8rOcu8ViktiCrAzOPkeDr3EJFlvxLN28C2nYWSPi+CS06XKO4XJPEp2MqIvMnVk9jUTg7yMEYQRslKyzReTeHRuFjzQqFvfqKhKVs/9QowvnqMYLGPTcSLFZn0PsgpsWsry1FPVtMWOyuiprxKS0ET1SXILunkfxT87AbxCJWNdZUy3oEkZeOgP7+cxkcpMqtoyoLD7MccNCui9lWBLqhSRaTeq/T5dYnHgIQOJEpjrXQQYag7ukx9BW1o5yR/BhBqvEyNMxiIQczMGtA4nw575nym7i65Fs0TLt/D5lXsb4xabmEEmLiKy+7Hx8Dt13dr1BxyMQIDAQAB";

	// 服务器异步通知页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
	public static String notify_url = "http://jintong123.e2.luyouxia.net:31618/ZLGL/login_toLogin";

	// 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
	public static String return_url = "http://jintong123.e2.luyouxia.net:31618/ZLGL/login_toLogin";

	// 签名方式
	public static String sign_type = "RSA2";

	// 字符编码格式
	public static String charset = "utf-8";

	// 支付宝网关
	public static String gatewayUrl = "https://openapi.alipaydev.com/gateway.do";







//↑↑↑↑↑↑↑↑↑↑请在这里配置您的基本信息↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑

	/**
	 * 写日志，方便测试（看网站需求，也可以改成把记录存入数据库）
	 * @param sWord 要写入日志里的文本内容
	 */
	public static void logResult(String sWord) {
		FileWriter writer = null;
		try {
			writer = new FileWriter("/usr/local/tomcat/logs/alipay_log_" + System.currentTimeMillis()+".log");
			writer.write(sWord);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (writer != null) {
				try {
					writer.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
