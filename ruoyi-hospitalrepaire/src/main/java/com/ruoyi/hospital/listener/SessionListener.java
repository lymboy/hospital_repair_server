package com.ruoyi.hospital.listener;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import com.ruoyi.hospital.framework.context.MySessionContext;

public class SessionListener implements HttpSessionListener{

	private MySessionContext myc=MySessionContext.getInstance();

	@Override
	public void sessionCreated(HttpSessionEvent sessionEvent) {
		HttpSession session = sessionEvent.getSession();
		myc.AddSession(session);
		//logger.info("******新增访问******当前系统访问人数："+myc.getMymap().size()+"***********");
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent sessionEvent) {
		HttpSession session = sessionEvent.getSession();
		myc.DelSession(session);
		//logger.info("******销毁******当前系统访问人数："+ myc.getMymap().size()+"***********");
	}

}
