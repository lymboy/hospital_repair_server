package com.ruoyi.hospital.framework.exception;

/**
 * <p>Title: AppException</p>
 * @Desc: 运行时异常处理类
 * @author: 杨建军
 *	@Date: 2017年3月7日
 * @Time: 上午10:37:33
 * @version:
 */
public class AppException extends Exception {

	/**
	 *
	 */
	private static final long serialVersionUID = 6271986083409569781L;
	private String msgDes;  //异常对应的描述信息

	public AppException(){
		super();
	}

	public AppException(String message){
		super(message);
		msgDes = message;
	}

	public String getMsgDes() {
		return msgDes;
	}

    public AppException(String message, Throwable cause) {
        super(message, cause);
    }

    public AppException(Throwable cause) {
        super(cause);
    }
}
