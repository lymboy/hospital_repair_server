package com.ruoyi.hospital.framework.exception;

/**
 * <p>Title: GenException</p>
 * @Desc: 一般异常处理类
 * @author: 杨建军
 *	@Date: 2017年3月7日
 * @Time: 上午10:37:43
 * @version:
 */
public class GenException extends Exception {

    /**
	 *
	 */
	private static final long serialVersionUID = 30863010378755890L;

	public GenException(String message, Throwable cause) {
        super(message, cause);
    }

    public GenException(Throwable cause) {
        super(cause);
    }
}
