package com.ruoyi.hospital.framework.log;

import java.lang.reflect.Method;

import javax.annotation.Resource;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import com.ruoyi.hospital.framework.exception.GenException;
import com.ruoyi.hospital.service.system.log.LogService;


/**
 * <p>Title: LogAop</p>
 * @Desc: 系统日志记录切点设置
 * @author: 杨建军
 *	@Date: 2017年3月6日
 * @Time: 下午11:13:33
 * @version:
 */
@Slf4j
@Aspect
@Component
public class LogAop {

	@Resource(name="logService")
	private LogService logService;

	@Pointcut("@annotation(com.ruoyi.hospital.framework.log.Log)")
	public void log(){
	}

	@After("log()")
	public void afterExec(JoinPoint joinPoint) throws GenException{
		MethodSignature ms=(MethodSignature) joinPoint.getSignature();
		Method method=ms.getMethod();

/*		Subject currentUser = SecurityUtils.getSubject();
		Session session = currentUser.getSession();
		User user = (User)session.getAttribute(Const.SESSION_USER);

		PageData pd = new PageData();
		pd.put("logId", sequence.nextId());
		pd.put("userId", user.getUserId());
		pd.put("logMethod", method.getName());
		pd.put("logName", method.getAnnotation(Log.class).name());
		pd.put("logTime", new Date());

		logService.addNewLog(pd);*/

		log.debug(method.getName()+method.getAnnotation(Log.class).name());
	}

}
