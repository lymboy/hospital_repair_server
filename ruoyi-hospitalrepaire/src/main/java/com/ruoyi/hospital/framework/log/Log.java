package com.ruoyi.hospital.framework.log;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.ElementType;

/**
 * <p>Title: Log</p>
 * @Desc: 系统日志记录
 * @author: 杨建军
 *	@Date: 2017年3月7日
 * @Time: 上午10:37:56
 * @version:
 */

@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.METHOD })
public @interface Log {
    String name() default "";
}
