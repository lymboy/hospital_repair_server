package com.ruoyi.hospital.util;

import java.io.File;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class PublicUtil {

	public static void main(String[] args) {
		System.out.println("本机的ip=" + PublicUtil.getIp());
	}

	public static String getPorjectPath(){
		String nowpath = "";
		nowpath=System.getProperty("user.dir")+"/";

		return nowpath;
	}

	/**
	 * 获取本机ip
	 * @return
	 */
	public static String getIp(){
		String ip = "";
		try {
			InetAddress inet = InetAddress.getLocalHost();
			ip = inet.getHostAddress();
			//System.out.println("本机的ip=" + ip);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}

		return ip;
	}

	public static PageData createQrCodeImage1(String rootPath, String url) {
        try {
        	PageData map = new PageData();
            // 分享链接
            String qrCodeContents =url;

            String Id = UuidUtil.get32UUID();

            // 生成路径
            String qrImgPath = getImgPath(Id, Id, "qr_", "png");

            // 生成二维码本地路径
            String qrImgLocalPath = getLocalImgPath(rootPath, qrImgPath);

            // 1. 先创建 二维码图片
            if (!new File(qrImgLocalPath).exists()) {
            	createQrCodeImage(qrCodeContents,qrImgLocalPath);
            }

            map.put("qrImgPath", qrImgPath);
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

	/**
     * 分享文件路径 二维码、分享主图
     * @param rootPath 服务器根路径
     * @return 文件路径
     */
    private static String getLocalImgPath(String rootPath, String path) {
        String imgPath = "";
        if (rootPath != null) {
            rootPath = rootPath.replaceAll("\\\\", "/");
            if (rootPath.endsWith("/")) {
                imgPath = rootPath +path;
            } else {
                imgPath = rootPath + "/" + path;
            }
        }
        return imgPath;
    }

	/**
     * 分享文件路径 二维码、分享主图
     * @param prefix 文件名前缀 qr_、pic_
     * @param suffix 文件格式 jpg、png
     * @return 文件路径
     */
    private static String getImgPath(String uid, String name_id, String prefix, String suffix) {
        String imgPath = "uploadFiles/qrCode/"+ uid +"/" + prefix+name_id + "." +suffix;
        return imgPath;
    }

    /**
     * 生成二维码
     * @param appShareUrl
     * @param qrImgPath
     */
    private static void createQrCodeImage(String qrCodeContents, String qrImgPath) throws Exception {
        File qrFile = new File(qrImgPath);
        if (!qrFile.getParentFile().exists()) {
            qrFile.getParentFile().mkdirs();
        }
        QRCode.encodeQRCode(qrImgPath, qrCodeContents, 420, 420);
    }

}
