package com.ruoyi.hospital.util;

import net.coobird.thumbnailator.Thumbnails;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;

/**
 * <p>Title: ThumbImage.java</p>
 * @Desc: 生成 图片 缩略图
 * @author 杨建军
 *	@Date: 2017年6月19日
 * @Time: 下午5:54:26
 * @version:
 */
public class ThumbImage {

	private static String DEFAULT_PREVFIX_DIR = "thumb"; //  "_"+w+"_"+h
	private static Boolean DEFAULT_FORCE = false;

	/**
	 * <p>Title: thumbnailImage</p>
	 * <p>Description: 根据图片路径生成缩略图 </p>
	 * @param imgFile    原图片路径
	 * @param w            缩略图宽
	 * @param h            缩略图高
	 * @param prevfixDir    生成缩略图的前缀
	 */
	private static void thumbnailImage(File imgFile, int w, int h, String prevfixDir){
		prevfixDir = prevfixDir+"_"+w+"_"+h; // 缩略图前缀
		if(imgFile.exists()){
			try {
				// ImageIO 支持的图片类型 : [BMP, bmp, jpg, JPG, wbmp, jpeg, png, PNG, JPEG, WBMP, GIF, gif]
				String types = Arrays.toString(ImageIO.getReaderFormatNames());
				String suffix = null;
				// 获取图片后缀
				if(imgFile.getName().indexOf(".") > -1) {
					suffix = imgFile.getName().substring(imgFile.getName().lastIndexOf(".") + 1);
				}// 类型和图片后缀全部小写，然后判断后缀是否合法
				if(suffix == null || types.toLowerCase().indexOf(suffix.toLowerCase()) < 0){
					return ;
				}

				String p = imgFile.getPath();
				File outPutFileDir = new File(p.substring(0,p.lastIndexOf(File.separator))+File.separator +prevfixDir);
				File targetFile = new File(p.substring(0,p.lastIndexOf(File.separator))+File.separator + prevfixDir +File.separator+imgFile.getName());
				if(!outPutFileDir.exists()){
					outPutFileDir.mkdirs();
				}

				Thumbnails.of(imgFile).size(w, h).toFile(targetFile);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}else{ // 源图片不存在

		}
	}

	/**
	 * 指定 图片路径， 宽，高，前缀，是否按比例生成最佳缩略图
	 * @param imagePath
	 * @param w
	 * @param h
	 * @param prevfixDir
	 */
	public static void thumbnailImage(String imagePath, int w, int h, String prevfixDir){
		File imgFile = new File(imagePath);
		thumbnailImage(imgFile, w, h, prevfixDir);
	}

	/**
	 * 指定 图片路径， 宽，高，是否按比例生成最佳缩略图
	 * @param imagePath
	 * @param w
	 * @param h
	 */
	public static void thumbnailImage(String imagePath, int w, int h){
		thumbnailImage(imagePath, w, h, DEFAULT_PREVFIX_DIR);
	}

	/**
	 * 指定 图片路径
	 * @param imagePath
	 */
	public static void thumbnailImages(String imagePath) {
		thumbnailImage(imagePath, Const.image_mini_w, Const.image_mini_h);
		thumbnailImage(imagePath, Const.image_medium_w, Const.image_medium_h);
	}

	public static void main(String[] args) {
		thumbnailImages("C:\\Users\\Young\\Desktop\\th\\de.jpg");
	}
}
