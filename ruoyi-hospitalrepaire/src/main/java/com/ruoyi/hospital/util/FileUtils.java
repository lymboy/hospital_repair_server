package com.ruoyi.hospital.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * <p> 文件操作 </p>
 *
 * @author Young
 * @since 2018-03-17
 */
public class FileUtils {

    private static Logger logger = LoggerFactory.getLogger(FileUtils.class);

    /**
     * MultipartFile 文件上传
     * @param filePath 文件上传到服务器上的路径
     * @param multipartFile 文件对象
     */
    public static boolean uploadMultipartFile(String filePath, MultipartFile multipartFile) throws FileNotFoundException {
        try {
            File file = new File(filePath);
            if (!file.getParentFile().exists()) {
                file.getParentFile().mkdirs();
            }
            multipartFile.transferTo(file);
            // 生成缩略图
            ThumbImage.thumbnailImages(filePath);

            logger.info("文件上传成功");
            return true;
        } catch (IOException e) {
            logger.error(e.getMessage());
            return false;
        }
    }

    /**
     * 下载
     * @param response
     * @param fileName
     * @param filePath
     * @return
     */
    public static boolean downloadFile (HttpServletResponse response, String fileName, String filePath) {
        File file = new File(filePath);
        if (!file.exists()) {
            return false;
        }
        try {
            fileName += "."+getFileExtension(filePath); // 下载显示的文件名
            response.setHeader("Content-Disposition", "attachment; filename=" + URLEncoder.encode(fileName,"utf-8"));
            response.setContentType("application/octet-stream");
            FileInputStream fileInputStream = new FileInputStream(file);
            try {
                ServletOutputStream servletOutputStream = response.getOutputStream();
                int len = 0;
                byte[] bytes = new byte[1024];
                while ((len=fileInputStream.read(bytes))!=-1){
                    servletOutputStream.write(bytes, 0, len);
                }
            }finally {
                if (fileInputStream != null){
                    fileInputStream.close();
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }

        return true;
    }

    /**
     * 获取文件扩展名
     * @param fileFullName
     * @return
     */
    public static String getFileExtension(String fileFullName) {
        if (fileFullName == null || "".equals(fileFullName)) {
            return null;
        }
        return fileFullName.substring(fileFullName.lastIndexOf(".")+1);
    }

    /**
     * 获取文件名 不带有扩展名
     * @param fileFullName
     * @return
     */
    public static String getFileName(String fileFullName) {
        if (fileFullName == null || "".equals(fileFullName)) {
            return null;
        }
        return fileFullName.substring(0, fileFullName.lastIndexOf("."));
    }

    /**
     * 获取文件上传路径
     * @param fileFullName
     * @return
     */
    public static String getFileUploadPath(String fileMiddlePath, String fileFullName) {
        if (fileFullName == null || "".equals(fileFullName)) {
            return null;
        }
        String fileId = sequence.nextId();
        String fileExtension = getFileExtension(fileFullName);
        return fileMiddlePath + File.separator + fileExtension
                + File.separator + fileId + "." + fileExtension;
    }

    /**
     * 获取请求中的 文件
     * @param request
     * @return
     */
    public static List<MultipartFile> getMultipartFileListFromRequest(HttpServletRequest request) {
        List<MultipartFile> multipartFileList = new ArrayList<>();
        // 获取 上传的文件
        MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest)request;
        Iterator<String> iter = multiRequest.getFileNames();
        while(iter.hasNext()) {
            // 遍历所有文件
            MultipartFile multipartFile = multiRequest.getFile(iter.next().toString());
            if(multipartFile != null) {
                multipartFileList.add(multipartFile);
            }
        }
        return multipartFileList;
    }
}
