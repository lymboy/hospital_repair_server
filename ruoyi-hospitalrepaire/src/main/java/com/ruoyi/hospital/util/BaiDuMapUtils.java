package com.ruoyi.hospital.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * <p>  </p>
 *
 * @author Young
 * @since 2018-05-22
 */
public class BaiDuMapUtils {

    private static double EARTH_RADIUS = 6378.137;

    private static double rad(double d) {
        return d * Math.PI / 180.0;
    }

    /**
     * 计算巡查里程
     * @param qualityPatrolTrackList
     * @return
     */
    public static String calculatePatrolMileage(List<PageData> qualityPatrolTrackList) {
        try {
            if (qualityPatrolTrackList == null || qualityPatrolTrackList.size() <= 1) {
                return "0";
            }
            double patrolMileage = 0;
            for (int i = 1; i < qualityPatrolTrackList.size(); i++) {
                PageData p1 = qualityPatrolTrackList.get(i - 1);
                PageData p2 = qualityPatrolTrackList.get(i);
                String lng1 = String.valueOf(p1.get("POINT_Y"));
                String lat1 = String.valueOf(p1.get("POINT_X"));
                String lng2 = String.valueOf(p2.get("POINT_Y"));
                String lat2 = String.valueOf(p2.get("POINT_X"));
                patrolMileage += getDistance(lng1, lat1, lng2, lat2);
            }

            NumberFormat nf = NumberFormat.getNumberInstance();
            // 保留两位小数
            nf.setMaximumFractionDigits(3);
            // 如果不需要四舍五入，可以使用RoundingMode.DOWN
            nf.setRoundingMode(RoundingMode.UP);
            return nf.format(patrolMileage);
        } catch (Exception e) {
            return "0";
        }
    }

    /**
     * 通过经纬度获取距离(单位：千米)
     * @return
     */
    public static double getDistance(String longitudeA, String latitudeA, String longitudeB, String latitudeB) {
        try {
            if (!isDouble(longitudeA) || !isDouble(latitudeA) || !isDouble(longitudeB) || !isDouble(latitudeB)) {
                return 0;
            }

            double lon1 = (Math.PI/180)*Double.parseDouble(longitudeA);
            double lat1 = (Math.PI/180)*Double.parseDouble(latitudeA);
            double lon2 = (Math.PI/180)*Double.parseDouble(longitudeB);
            double lat2 = (Math.PI/180)*Double.parseDouble(latitudeB);

            //地球半径
            double R = 6371;
            //两点间距离 km，如果想要米的话，结果*1000
            double d =  Math.abs(Math.acos(Math.sin(lat1)*Math.sin(lat2)+Math.cos(lat1)*Math.cos(lat2)*Math.cos(lon2-lon1))*R);
            if (Double.isNaN(d)) {
                return 0;
            }
            return d;
        } catch (Exception e) {
            return 0;
        }
    }

    /**
     * 判断两点是否接近
     * @param longitudeA
     * @param latitudeA
     * @param longitudeB
     * @param latitudeB
     * @return
     */
    public static double isNearestPoint(String longitudeA, String latitudeA, String longitudeB, String latitudeB) {
        if (!isDouble(longitudeA) || !isDouble(latitudeA) || !isDouble(longitudeB) || !isDouble(latitudeB)) {
            return -1;
        }
        double d =  getDistance(longitudeA, latitudeA, longitudeB, latitudeB);
        if (d*1000 < Const.pile_area_distance) {
            return d;
        }
        return -1;
    }

    /**
     * 坐标转换
     * @param longitude
     * @param latitude
     * @return
     */
    public static Map<String, Object> getBaiduLongitudeAndLatitude(String longitude, String latitude) {
        Map<String, Object> map = new HashMap<>();
        if (!isDouble(longitude) || !isDouble(latitude)) {
            map.put("longitude", "");
            map.put("latitude", "");
            return map;
        }
        double x = Double.valueOf(longitude);
        double y = Double.valueOf(latitude);

        double[] doubles = zhuanhuan(x, y,119,"80");
        DecimalFormat df = new DecimalFormat("#.000000");
        map.put("longitude", df.format(doubles[0]));
        map.put("latitude", df.format(doubles[1]));
        return map;
    }
    private static boolean isDouble(String num) {
        try {
            Double.parseDouble(num);
        } catch (Exception e){
            return false;
        }
        return true;
    }

    private static double[] zhuanhuan(double x,double y,double jx, String ty){
        int i, j, counter;

        double B, l ;
        double tempx ;
        double tempy ;

        double l01 ;

        double pi = 3.1415926535898;
        l01 = jx;
        l01 = l01 * pi / 180;
        //System.out.println("l01=="+l01);
        tempx = x;
        tempy = y - 500000;
        double a= 0.0,bx=0.0, e1=0.0, e2=0.0, ep2=0.0, Bf=0.0, Rf=0.0, Mf=0.0, Tf= 0.0,Cf=0.0, D=0.0, fai=0.0;

        if("54".equals(ty)){
            a = 6378245;
            bx = 6356863.0188;
            e2 = 0.006693421622966;
            ep2 = 0.006738525414683;
        }else if("jy".equals(ty)){
            a = 6378428.2;
            bx = 6356863.0188;
            e2 = 0.006693421622966;
            ep2 = 0.006738525414683;
        }else if("80".equals(ty)){
            a = 6378140;
            bx = 6356755.2882;
            e2 = 0.00669438499959;
            ep2 = 0.00673950181947;
        }else if("84".equals(ty)){
            a = 6378137;
            bx = 6356752.3142;
            e2 = 0.00669437999013;
            ep2 = 0.006739496742227;
        }else if("0".equals(ty)){
            a = 6378137;
            bx = 6356752.31414;
            e2 = 0.0066943800229;
            ep2 = 0.00673949677548;
        }

        e1 = (1 - bx / a) / (1 + bx / a);
        //System.out.println("e1=="+e1);
        Mf = tempx;
        fai = Mf / (a * (1 - e2 / 4.0 - 3 * e2 * e2 / 64.0 - 5 * e2 * e2 * e2 / 256.0));
        //System.out.println("fai=="+fai);
        Bf = fai + (3 * e1 / 2.0 - 27.0 * (e1 * e1 * e1) / 32.0) * Math.sin(2 * fai) + (21 * (e1 * e1) / 16 - 55 * (e1 * e1 * e1 * e1 ) / 32) * Math.sin(4 * fai) + (151 * (e1 * e1 * e1 ) / 96) * Math.sin(6 * fai) + (1097 * (e1 * e1 * e1 * e1 ) / 512) * Math.sin(8 * fai);
        double Nf = a / Math.sqrt(1 - e2 * Math.sin(Bf) * Math.sin(Bf));
        D = tempy / Nf;
        Rf = a * (1 - e2) / Math.pow((1 - e2 * Math.sin(Bf) * Math.sin(Bf) ) , 1.5);
        Tf = Math.tan(Bf) * Math.tan(Bf);
        Cf = ep2 * (Math.cos(Bf) * Math.cos(Bf));
        B = Bf - (Nf * Math.tan(Bf) / Rf) * (D * D / 2 - (5 + 3 * Tf + Cf - 9 * Tf * Cf) * (D * D * D * D) / 24 + (61 + 90 * Tf + 45 * (Tf * Tf)) * (D * D * D * D * D * D) / 720);
        l = l01 + (1 / Math.cos(Bf)) * (D - (1 + 2 * Tf + Cf) * (D  * D * D) / 6 + (5 + 28 * Tf + 6 * Cf + 8 * Tf * Cf + 24 * Tf * Tf) * (D  * D * D * D * D) / 120);
        //zbfs2 = Array(B, l)

        //System.out.println(B*180.0/pi  + "   " + l*180.0/pi);
        double[] output = new double[2];
        output[0] = l*180.0/pi;
        output[1] = B*180.0/pi;
        return output;
    }



    public static void main(String[] args) {

        //zhuanhuan(3215282.436,417478.031,120,"80");
        //double[] zhuanhuan = zhuanhuan(3223558.171, 456000.892, 119, "80");
    	double getDistance = getDistance("34.262766","108.925315","34.262704","108.923268");
    	System.out.println(getDistance);
        /*System.out.println(getDistance[0] + ", " +getDistance[1]);*/

    }
}
