package com.ruoyi.hospital.util;

import java.util.UUID;

public class UuidUtil {

	public static String get32UUID() {
		String uuid = UUID.randomUUID().toString().trim().replaceAll("-", "");
		return uuid;
	}

	public static String get36UUID(){
		return UUID.randomUUID().toString();
	}

	public static void main(String[] args) {
		//System.out.println(get32UUID());
		System.out.println( UUID.randomUUID().toString().length());
	}
}

