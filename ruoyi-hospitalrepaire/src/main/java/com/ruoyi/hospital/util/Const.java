package com.ruoyi.hospital.util;

import org.springframework.context.ApplicationContext;

import java.util.HashMap;
import java.util.Map;

/**
 * 项目名称：
 * @author:fh
 *
*/
public class Const {
	public static final String SESSION_SECURITY_CODE = "sessionSecCode";
	public static final String SESSION_USER = "h";
	public static final String SESSION_ROLE_RIGHTS = "sessionRoleRights";
	public static final String SESSION_menuList = "menuList";			//当前菜单
	public static final String SESSION_allmenuList = "allmenuList";		//全部菜单
	public static final String SESSION_dataPrivilegeList = "dataPrivilegeList";		//全部菜单
	public static final String SESSION_PRIVILEGE_BUTTON = "privilegeMenuButton";	//按钮权限
	public static final String SESSION_QX = "QX";
	public static final String SESSION_userpds = "userpds";
	public static final String SESSION_USERROL = "USERROL";				//用户对象
	public static final String SESSION_USERNAME = "USERNAME";			//用户名
	public static final String SESSION_IDENTITY_ID = "identityId";			//身份ID
	public static final String SESSION_DEFAULT_IDENTITY_ID = "defaultIdentityId";			//身份ID
	public static final String SESSION_IDENTITY = "identityInfo";			//身份 信息
	public static final String SESSION_DEPARTMENT_TYPE = "DEPARTMENT_TYPE";	//机构类型
	public static final String SESSION_IDENTITY_PROJECT = "identityProject";	//身份所在项目部
	public static final String TRUE = "T";
	public static final String FALSE = "F";
	public static final String LOGIN = "/login_toLogin.do";				//登录地址
	public static final String SYSNAME = "admin/config/SYSNAME.txt";	//系统名称路径
	public static final String PAGE	= "admin/config/PAGE.txt";			//分页条数配置路径
	public static final String EMAIL = "admin/config/EMAIL.txt";		//邮箱服务器配置路径
	public static final String SMS1 = "admin/config/SMS1.txt";			//短信账户配置路径1
	public static final String SMS2 = "admin/config/SMS2.txt";			//短信账户配置路径2
	public static final String FWATERM = "admin/config/FWATERM.txt";	//文字水印配置路径
	public static final String IWATERM = "admin/config/IWATERM.txt";	//图片水印配置路径
	public static final String WEIXIN	= "admin/config/WEIXIN.txt";	//微信配置路径
	public static final String FILEPATHIMG = "uploadFiles/uploadImgs/";	//图片上传路径
	public static final String FILEPATHFILE = "uploadFiles/file/";		//文件上传路径
	public static final String FILEPATHTWODIMENSIONCODE = "uploadFiles/twoDimensionCode/"; //二维码存放路径
	public static final String NO_INTERCEPTOR_PATH = ".*/((login)|(logout)|(wxAuth)|(repair)|(code)|(app)|(api)|(version.txt)|(uploadFiles)|(appservice)|(weixin)|(static)|(plugins/attention/zDialog)|(main)|(websocket)).*";	//不对匹配该值的访问路径拦截（正则）

	//BMP, bmp, JPG, jpg, JPEG, jpeg, PNG, png, WBMP, wbmp, GIF, gif
	public static final String[] imagType={".BMP", ".bmp", ".JPG", ".jpg", ".JPEG", ".jpeg", ".PNG", ".png", ".WBMP", ".wbmp", ".GIF", ".gif"}; // 图片类型数组

	// 缩略图 尺寸
	public static final int image_mini_w = 80;	//图片上传路径
	public static final int image_mini_h = 60;	//图片上传路径
	public static final int image_medium_w = 800;	//图片上传路径
	public static final int image_medium_h = 600;	//图片上传路径

	public static ApplicationContext WEB_APP_CONTEXT = null; //该值会在web容器启动时由WebAppContextListener初始化
	public static String APP_UPLOAD_PATH = null; //该值会在web容器启动时由WebAppContextListener初始化

	/**
	 * APP Constants
	 */
	//app注册接口_请求协议参数)
	public static final String[] APP_REGISTERED_PARAM_ARRAY = new String[]{"countries","uname","passwd","title","full_name","company_name","countries_code","area_code","telephone","mobile"};
	public static final String[] APP_REGISTERED_VALUE_ARRAY = new String[]{"国籍","邮箱帐号","密码","称谓","名称","公司名称","国家编号","区号","电话","手机号"};

	//app根据用户名获取会员信息接口_请求协议中的参数
	public static final String[] APP_GETAPPUSER_PARAM_ARRAY = new String[]{"USERNAME"};
	public static final String[] APP_GETAPPUSER_VALUE_ARRAY = new String[]{"用户名"};
	public static final String SESSION_CATALOG_RESOURCES_PRIVILEGE = "catalogResourcesPrivilege"; //资源权限
	public static final String SESSION_DOCUMENT_RESOURCES_PRIVILEGE = "documentResourcesPrivilege";//文件资源权限
	public static String FILE_UPLOAD_ROOT_URL = null;

	// 个推 应用配置  定义常量, appId、appKey、masterSecret 采用本文档 "第二步 获取访问凭证 "中获得的应用配置
	/*public static String getui_appId = "9x74dmTysP6aSPCAzgE1aA";
	public static String getui_appKey = "nxyDgE2e6h6Rr1T6YhvR57";
	public static String getui_masterSecret = "TKBc5rgqj47y4RNPK3zhf6";
	public static String getui_host_url = "http://sdk.open.api.igexin.com/apiex.htm";*/

	/*public static String getui_appId = "Nn8i304MoF7MIztMs6n0x6";
	public static String getui_appKey = "7ZaYb572xm7URLc6RTpOM7";
	public static String getui_masterSecret = "x4yTiDRjAy5OXDJrYrEzQ8";
	public static String getui_host_url = "http://sdk.open.api.igexin.com/apiex.htm";*/

	public static String getui_appId = "v7ka58Ip1285t4Nik4sc91";
	public static String getui_appKey = "X2BZZfs8MV8Fkk3jXi3Mw9";
	public static String getui_masterSecret = "0g84O9aDtH9KPqW0dE3ct6";
	public static String getui_host_url = "http://sdk.open.api.igexin.com/apiex.htm";

	// 工作流 业务明细路径地址 映射
	public static final Map<String, Object> WORK_FLOW_BUSINESS_URL = new HashMap(){{
		put("routineProcess", "qualityInspect/toQualityInspectEdit.do?qualityInspectId="); //常规工序质量验收审批流程
		put("firstPieceProjectProcess", "qualityInspect/toQualityInspectEdit.do?qualityInspectId="); // 首件工程工序质量验收审批流程
		put("firstAndHiddenProcess", "qualityInspect/toQualityInspectEdit.do?qualityInspectId="); // 班组首件,隐蔽工程工序质量验收审批流程
		put("projectAutoInspectionProcess", "autoCheck/toAutoCheckEdit.do?AUTOCHECK_ID="); // 项目完工自检审批流程
		put("leaveProcess", "leave_record/edit.do?ID="); // 请假审批流程
		put("qualityPatrolProcess", "quality_patrol_problem/to_view?ID="); // 质量巡查审批流程
		put("qualityPatrolProblemProcess", "quality_patrol_problem/to_view?ID="); // 质量巡查整改审批流程
		put("QualityInspectionProcessConsistent", "quality_patrol_problem/to_view?ID="); //质量巡查审批流程  一致
		put("QualityInspectionProcess", "quality_patrol_problem/to_view?ID="); //质量巡查审批流程  不一致
		put("SafetyInspectionProcessConsistent", "quality_patrol_problem/to_view?ID="); //安全巡查审批流程  一致
		put("SafetyInspectionProcess", "quality_patrol_problem/to_view?ID="); //安全巡查审批流程  不一致
	}};

	// 上班签到，先办签退
	public static final String SIGN_TYPE_IN ="69";
	public static final String SIGN_TYPE_OUT ="70";

	// 质量巡查  分类 【附件，】
	public static final String MASTER_TYPE_PROBLEM = "problem"; // 问题
	public static final String MASTER_TYPE_MODIFY = "modify"; // 整改
	public static final String MASTER_TYPE_CHECK = "check"; // 复查

	public static final int pile_area_distance = 300; // 方圆300米 桩号
}
