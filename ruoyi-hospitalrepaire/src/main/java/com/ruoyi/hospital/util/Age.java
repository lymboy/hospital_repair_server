package com.ruoyi.hospital.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Age {

	public static Date getBeforeAge(Date date,int year) {
        Date dt= new Date();
        Calendar rightNow = Calendar.getInstance();
        rightNow.setTime(dt);
        rightNow.add(Calendar.YEAR,year);//日期减1年
        Date dt1=rightNow.getTime();
		return dt1;
	}


	public static int getAge(Date birthDate) {

		if (birthDate == null) {
			throw new RuntimeException("出生日期不能为null");
		}
		int age = 0;

		Date now = new Date();

		SimpleDateFormat format_y = new SimpleDateFormat("yyyy");
		SimpleDateFormat format_M = new SimpleDateFormat("MM");

		String birth_year = format_y.format(birthDate);
		String this_year = format_y.format(now);

		String birth_month = format_M.format(birthDate);
		String this_month = format_M.format(now);

		// 初步，估算
		age = Integer.parseInt(this_year) - Integer.parseInt(birth_year);

		// 如果未到出生月份，则age - 1
		if (this_month.compareTo(birth_month) < 0)
			age -= 1;
		if (age < 0)
			age = 0;
		return age;
	}
}
