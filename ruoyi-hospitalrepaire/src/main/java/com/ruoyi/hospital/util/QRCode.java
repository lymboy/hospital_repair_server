package com.ruoyi.hospital.util;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.NotFoundException;
import com.google.zxing.Result;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import lombok.extern.slf4j.Slf4j;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.Hashtable;

/**
 * <p> 二维码工具类 </p>
 *
 * @author Young
 * @since 2018-03-18
 */
@Slf4j
public class QRCode {

    private static final int QR_WIDTH = 300;
    private static final int QR_HEIGHT = 300;
    private static final String QR_FORMAT = "png";
    private static final String QR_CHARSET = "UTF-8";
    private static final ErrorCorrectionLevel QR_ERROR_LEVEL = ErrorCorrectionLevel.M;
    private static final int QR_MARGIN = 1;

    public static void main(String[] args) {
        System.out.println(decodeQRCode("D:\\s.png"));
    }
    /**
     * 解析二维码
     * @param imgPath 二维码图片路径
     */
    public static String decodeQRCode(String imgPath) {
        try {
            InputStream inputStream = new FileInputStream(new File(imgPath));
            return decodeQRCode(inputStream);
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        return null;
    }

    /**
     * 解析二维码
     * @param inputStream 二维码图片流
     */
    public static String decodeQRCode(InputStream inputStream) {
        try {
            MultiFormatReader multiFormatReader = new MultiFormatReader();
            BufferedImage bufferedImage = ImageIO.read(inputStream);
            BinaryBitmap binaryBitmap =
                    new BinaryBitmap(
                            new HybridBinarizer(
                                    new BufferedImageLuminanceSource(bufferedImage)));
            Hashtable hashtable = new Hashtable();
            hashtable.put(EncodeHintType.CHARACTER_SET, "UTF-8");
            Result result = multiFormatReader.decode(binaryBitmap, hashtable);
            return result.getText();
        } catch (IOException e) {
            log.error(e.getMessage());
        } catch (NotFoundException e) {
            log.error(e.getMessage());
        } finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (IOException e) {
                log.error(e.getMessage());
            }
        }
        return null;
    }

    public static boolean encodeQRCode(String imgPath, String contents) {
        return encodeQRCode(imgPath, contents, QR_WIDTH, QR_HEIGHT, QR_FORMAT);
    }
    public static boolean encodeQRCode(OutputStream outputStream, String contents) {
        return encodeQRCode(outputStream, contents, QR_WIDTH, QR_HEIGHT, QR_FORMAT);
    }
    public static boolean encodeQRCode(String imgPath, String contents, int width, int height) {
        return encodeQRCode(imgPath, contents, width, height, QR_FORMAT);
    }
    public static boolean encodeGoodsQRCode(String imgPath, String contents, int width, int height) {
        return encodeGoodsQRCode(imgPath, contents, width, height, QR_FORMAT);
    }
    public static boolean encodeQRCode(OutputStream outputStream, String contents, int width, int height) {
        return encodeQRCode(outputStream, contents, width, height, QR_FORMAT);
    }

    /**
     * 生成二维码
     * @param imgPath 二维码图片路径
     * @param contents 二维码内容
     * @param width 二维码宽度
     * @param height 二维码高度
     * @param format 二维码类型 png,jpg
     */
    public static boolean encodeQRCode(String imgPath, String contents, int width, int height, String format) {
        try {
            // 生成二维码图片
            File imgFile = new File(imgPath);
            if (!imgFile.getParentFile().exists())
                imgFile.getParentFile().mkdirs();
            OutputStream outputStream = new FileOutputStream(imgFile);
            return encodeQRCode(outputStream, contents, width, height, format);
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        return false;
    }
    /**
     * 生成商品二维码
     * @param imgPath 二维码图片路径
     * @param contents 二维码内容
     * @param width 二维码宽度
     * @param height 二维码高度
     * @param format 二维码类型 png,jpg
     */
    public static boolean encodeGoodsQRCode(String imgPath, String contents, int width, int height, String format) {
        try {
            // 生成二维码图片
            File imgFile = new File(imgPath);
            if (!imgFile.getParentFile().exists())
                imgFile.getParentFile().mkdirs();
            OutputStream outputStream = new FileOutputStream(imgFile);
            return encodeGoodsQRCode(outputStream, contents, width, height, format);
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        return false;
    }

    /**
     * 生成二维码
     * @param outputStream 二维码图片输出流
     * @param contents 二维码内容
     * @param width 二维码宽度
     * @param height 二维码高度
     * @param format 二维码类型 png,jpg
     */
    public static boolean encodeQRCode(OutputStream outputStream, String contents, int width, int height, String format) {
        try {
            Hashtable hashtable = initHashTable(QR_CHARSET, QR_ERROR_LEVEL, QR_MARGIN);
            // 生成矩阵
            BitMatrix bitMatrix = new MultiFormatWriter().encode(contents, BarcodeFormat.QR_CODE, width, height, hashtable);
            // 裁剪白边
            // bitMatrix = deleteWhite(bitMatrix);
            MatrixToImageWriter.writeToStream(bitMatrix, format, outputStream);
            return true;
        } catch (WriterException e) {
            log.error(e.getMessage());
        } catch (IOException e) {
            log.error(e.getMessage());
        } finally {
            try {
                if (outputStream != null)
                    outputStream.close();
            } catch (IOException e) {
                log.error(e.getMessage());
            }
        }
        return false;
    }

    public static boolean encodeGoodsQRCode(OutputStream outputStream, String contents, int width, int height, String format) {
        try {
            Hashtable hashtable = initHashTable(QR_CHARSET, QR_ERROR_LEVEL, QR_MARGIN);
            // 生成矩阵
            BitMatrix bitMatrix = new MultiFormatWriter().encode(contents, BarcodeFormat.QR_CODE, width, height, hashtable);
            // 裁剪白边
            bitMatrix = deleteWhite(bitMatrix);
            MatrixToImageWriter.writeToStream(bitMatrix, format, outputStream);
            return true;
        } catch (WriterException e) {
            log.error(e.getMessage());
        } catch (IOException e) {
            log.error(e.getMessage());
        } finally {
            try {
                if (outputStream != null)
                    outputStream.close();
            } catch (IOException e) {
                log.error(e.getMessage());
            }
        }
        return false;
    }

    public static boolean encodeQRCodeWithLogo(String imgPath, String logoPath, String contents) {
        return encodeQRCodeWithLogo(imgPath, logoPath, contents, QR_WIDTH, QR_HEIGHT, QR_FORMAT);
    }
    public static boolean encodeQRCodeWithLogo(OutputStream outputStream, String logoPath, String contents) {
        return encodeQRCodeWithLogo(outputStream, logoPath, contents, QR_WIDTH, QR_HEIGHT, QR_FORMAT);
    }
    public static boolean encodeQRCodeWithLogo(String imgPath, String logoPath, String contents, int width, int height) {
        return encodeQRCodeWithLogo(imgPath, logoPath, contents, width, height, QR_FORMAT);
    }
    public static boolean encodeQRCodeWithLogo(OutputStream outputStream, String logoPath, String contents, int width, int height) {
        return encodeQRCodeWithLogo(outputStream, logoPath, contents, width, height, QR_FORMAT);
    }

    /**
     * 生成二维码[内嵌logo]
     * @param imgPath 二维码图片路径
     * @param logoPath logo 路径
     * @param contents 二维码内容
     * @param width 二维码宽度
     * @param height 二维码高度
     * @param format 二维码类型 png,jpg
     */
    public static boolean encodeQRCodeWithLogo(String imgPath, String logoPath, String contents, int width, int height, String format) {
        try {
            File imgFile = new File(imgPath);
            if (!imgFile.getParentFile().exists())
                imgFile.getParentFile().mkdirs();
            OutputStream outputStream = new FileOutputStream(imgFile);
            return encodeQRCodeWithLogo(outputStream, logoPath, contents, width, height, format);
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        return false;
    }

    /**
     * 生成二维码[内嵌logo]
     * @param outputStream 二维码图片输出流
     * @param logoPath logo 路径
     * @param contents 二维码内容
     * @param width 二维码宽度
     * @param height 二维码高度
     * @param format 二维码类型 png,jpg
     */
    public static boolean encodeQRCodeWithLogo(OutputStream outputStream, String logoPath, String contents, int width, int height, String format) {
        try {
            // 初始化 配置
            Hashtable hashtable = initHashTable(QR_CHARSET, QR_ERROR_LEVEL, QR_MARGIN);
            // 生成矩阵
            BitMatrix bitMatrix = new MultiFormatWriter().encode(contents, BarcodeFormat.QR_CODE, width, height, hashtable);
            // 裁剪白边
            // bitMatrix = deleteWhite(bitMatrix);

            // 载入 logo 图片
            BufferedImage bufferedImage = bitMatrixToBufferedImage(bitMatrix);
            Graphics2D gs = bufferedImage.createGraphics();

            int ratioWidth = bufferedImage.getWidth()*2/10;
            int ratioHeight = bufferedImage.getHeight()*2/10;
            //载入logo
            URL url = new URL(logoPath);
            InputStream is = url.openConnection().getInputStream();
            //Image img = ImageIO.read(new File(logoPath));
            Image img = ImageIO.read(is);
            int logoWidth = img.getWidth(null) > ratioWidth ? ratioWidth : img.getWidth(null);
            int logoHeight = img.getHeight(null) > ratioHeight ? ratioHeight : img.getHeight(null);

            int x = (bufferedImage.getWidth() - logoWidth) / 2;
            int y = (bufferedImage.getHeight() - logoHeight) / 2;

            gs.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            gs.fillRoundRect(0, 0,x, y, 10, 10);
            gs.setComposite(AlphaComposite.SrcIn);
            gs.drawImage(img.getScaledInstance(logoWidth, logoHeight, Image.SCALE_SMOOTH), x, y, null);
            gs.dispose();
            img.flush();
            // 输出图片
            ImageIO.write(bufferedImage, format, outputStream);
            return true;
        } catch (WriterException e) {
            log.error(e.getMessage());
        } catch (IOException e) {
            log.error(e.getMessage());
        } finally {
            try {
                if (outputStream != null)
                    outputStream.close();
            } catch (IOException e) {
                log.error(e.getMessage());
            }
        }
        return false;
    }

    /**
     * 生成二维码[内嵌文字]
     * @param imgPath 二维码图片路径
     * @param contents 二维码内容
     * @param pressText 文字
     * @param width 二维码宽度
     * @param height 二维码高度
     * @param format 二维码类型 png,jpg
     */
    public static void encodeQRCodeWithText(String imgPath, String contents, String pressText, int width, int height, String format) {
        int fontStyle = 5;
        Color color = Color.RED;
        int fontSize = 32;
        String fontName = "宋体";
        try {
            // 初始化 配置
            Hashtable hashtable = initHashTable(QR_CHARSET, QR_ERROR_LEVEL, QR_MARGIN);
            // 生成矩阵
            BitMatrix bitMatrix = new MultiFormatWriter().encode(contents, BarcodeFormat.QR_CODE, width, height, hashtable);
            // 裁剪白边
            // bitMatrix = deleteWhite(bitMatrix);

            // 载入 文字
            BufferedImage bufferedImage = bitMatrixToBufferedImage(bitMatrix);
            Graphics2D gs = bufferedImage.createGraphics();

            int ratioWidth = bufferedImage.getWidth();
            int ratioHeight = bufferedImage.getHeight();

            //设置画笔的颜色
            gs.setColor(color);
            //设置字体
            Font font = new Font(fontName, fontStyle, fontSize);
            FontMetrics metrics = gs.getFontMetrics(font);
            //文字在图片中的坐标 这里设置在中间
            int startX = (ratioWidth - metrics.stringWidth(pressText)) / 2;
            int startY = (ratioHeight - metrics.getHeight())/2;
            gs.setFont(font);
            gs.drawString(pressText, startX, startY);
            gs.setColor(Color.black);
            gs.setBackground(Color.WHITE);
            gs.dispose();
            // 输出图片
            File imgFile = new File(imgPath);
            if (!imgFile.getParentFile().exists())
                imgFile.getParentFile().mkdirs();
            ImageIO.write(bufferedImage, format, imgFile);
        } catch (WriterException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 初始化配置 Hashtable
     * @param charset utf-8
     * @param errorCorrectionLevel ErrorCorrectionLevel.Q
     * @param margin 1
     * @return
     */
    private static Hashtable initHashTable(String charset, ErrorCorrectionLevel errorCorrectionLevel, int margin) {
        Hashtable hashtable = new Hashtable();
        // 设置UTF-8， 防止中文乱码
        hashtable.put(EncodeHintType.CHARACTER_SET, charset);
        //设置二维码的容错性 排错率越高可存储的信息越少，但对二维码清晰度的要求越小
        hashtable.put(EncodeHintType.ERROR_CORRECTION, errorCorrectionLevel);
        //设置二维码四周白色区域的大小
        hashtable.put(EncodeHintType.MARGIN, margin);
        // hashtable.put(EncodeHintType.QR_VERSION, 10);
        return hashtable;
    }

    /**
     * BitMatrix 转换为 BufferedImage
     *
     * @param bitMatrix
     * @return
     */
    private static BufferedImage bitMatrixToBufferedImage(BitMatrix bitMatrix) {
        int onColor = 0xFF000000;     //前景色
        int offColor = 0xFFFFFFFF;    //背景色
        int width = bitMatrix.getWidth();
        int height = bitMatrix.getHeight();
        BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        for(int x=0;x<width;x++){
            for(int y=0;y<height;y++){
                bufferedImage.setRGB(x, y, bitMatrix.get(x, y) ? onColor : offColor);
            }
        }
        return bufferedImage;
    }

    /**
     * 裁剪白边
     *
     * @param matrix
     * @return
     */
    private static BitMatrix deleteWhite(BitMatrix matrix){
        int[] rec = matrix.getEnclosingRectangle();
        int resWidth = rec[2] + 1;
        int resHeight = rec[3] + 1;

        BitMatrix resMatrix = new BitMatrix(resWidth, resHeight);
        resMatrix.clear();
        for (int i = 0; i < resWidth; i++) {
            for (int j = 0; j < resHeight; j++) {
                if (matrix.get(i + rec[0], j + rec[1])) {
                    resMatrix.set(i, j);
                }
            }
        }
        return resMatrix;
    }
}
