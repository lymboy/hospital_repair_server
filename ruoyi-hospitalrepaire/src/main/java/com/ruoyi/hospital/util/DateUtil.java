package com.ruoyi.hospital.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {
	private final static SimpleDateFormat sdfYear = new SimpleDateFormat("yyyy");

	private final static SimpleDateFormat sdfYM = new SimpleDateFormat("yyyy-MM");

	private final static SimpleDateFormat sdfDay = new SimpleDateFormat("yyyy-MM-dd");

	private final static SimpleDateFormat sdfDays = new SimpleDateFormat("yyyyMMdd");

	private final static SimpleDateFormat sdfyM = new SimpleDateFormat("yyyyMMd");

	private final static SimpleDateFormat sdfTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	private final static SimpleDateFormat sdfHm = new SimpleDateFormat("HH:mm");


	/**
	 * 获取 YYYY 格式
	 * @return
	 */
	public static String getYear(){
		return sdfYear.format(new Date());
	}

	/**
	 * 获取 YYYY-MM-DD 格式
	 * @return
	 */
	public static String getDay(){
		return sdfDay.format(new Date());
	}

	/**
	 * 获取 YYYY-MM-DD 格式
	 * @return
	 */
	public static String getDayYM(){
		return sdfYM.format(new Date());
	}

	/**
	 * 获取 YYYYMMDD 格式
	 * @return
	 */
	public static String getDays(){
		return sdfDays.format(new Date());
	}

	public static String getyM(){
		return sdfyM.format(new Date());
	}

	/**
	 * 获取 YYYY-MM-DD HH:mm:ss
	 * @return
	 */
	public static String getTime(){
		return sdfTime.format(new Date());
	}

	/**
	 * 格式化日期
	 * @param date
	 * @return
	 */
	public static Date formatDateYM(String date){
		try {
			return sdfYM.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 格式化日期
	 * @param date
	 * @return
	 */
	public static Date formatDate(String date){
		DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
			return sdf.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}



	/**
	 * 获取 YYYY.MM格式的日期
	 * @return
	 */
	public static String getYM() {
		return getDay().substring(0, 7).replace("-", ".");
	}

	/**
	* @Title: compareDate
	* @Description: TODO(日期比较，如果s>=e 返回true 否则返回false)
	* @param s
	* @param e
	* @return boolean
	* @throws
	* @author luguosui
	 */
	public static boolean compareDate(String s, String e) {
		if(fomatDate(s)==null||fomatDate(e)==null){
			return false;
		}
		return fomatDate(s).getTime() >=fomatDate(e).getTime();
	}

	/**
	 * 格式化日期
	 *
	 * @return
	 */
	public static Date fomatToDate(String date) {
		DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
		try {
			return fmt.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}
	/**
	 * 格式化日期yyyy.MM
	 * @param date
	 * @return
	 */
	public static Date fomatDate(String date) {
		String day = getDay();
		date+="."+day.split("-")[2];
		DateFormat fmt = new SimpleDateFormat("yyyy.MM.dd");
		try {
			return fmt.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 将日期格式化成yyyy.MM字符串
	 * @param date
	 * @return
	 */
	public static String formatDate(Date date){
		DateFormat fmt = new SimpleDateFormat("yyyy.MM");
		String s = fmt.format(date);

		return s;
	}

	/**
	 * 将日期格式化成yyyy.MM.dd字符串
	 * @param date
	 * @return
	 */
	public static String formatDateYMD(Date date){
		DateFormat fmt = new SimpleDateFormat("yyyy.MM.dd");
		String s = fmt.format(date);

		return s;
	}

	/**
	 * yyyy.MM 转为 YYYY-MM-DD
	 * @param date
	 * @return
	 */
	public static String fomatDateYMD(String date) {
		return sdfDay.format(fomatDate(date));
	}

	/**
	 * yyyy.MM 转为 YYYY-MM-DD HH:mm:ss
	 * @param date
	 * @return
	 */
	public static String fomatDateYMDHMS(String date) {
		return sdfTime.format(fomatDate(date));
	}

	/**
	 * 校验日期是否合法
	 *
	 * @return
	 */
	public static boolean isValidDate(String s) {
		DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
		try {
			fmt.parse(s);
			return true;
		} catch (Exception e) {
			// 如果throw java.text.ParseException或者NullPointerException，就说明格式不对
			return false;
		}
	}
	public static int getDiffYear(String startTime,String endTime) {
		DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
		try {
			int years=(int) (((fmt.parse(endTime).getTime()-fmt.parse(startTime).getTime())/ (1000 * 60 * 60 * 24))/365);
			return years;
		} catch (Exception e) {
			// 如果throw java.text.ParseException或者NullPointerException，就说明格式不对
			return 0;
		}
	}
	  /**
     * <li>功能描述：时间相减得到天数
     * @param beginDateStr
     * @param endDateStr
     * @return
     * long
     * @author Administrator
     */
    public static long getDaySub(String beginDateStr,String endDateStr){
        long day=0;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date beginDate = null;
        Date endDate = null;

            try {
				beginDate = format.parse(beginDateStr);
				endDate= format.parse(endDateStr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
            day=(endDate.getTime()-beginDate.getTime())/(24*60*60*1000);
            //System.out.println("相隔的天数="+day);

        return day;
    }

    /**
     * 得到n天之后的日期
     * @param days
     * @return
     */
    public static String getAfterDayDate(String days) {
    	int daysInt = Integer.parseInt(days);

        Calendar canlendar = Calendar.getInstance(); // java.util包
        canlendar.add(Calendar.DATE, daysInt); // 日期减 如果不够减会将月变动
        Date date = canlendar.getTime();

        SimpleDateFormat sdfd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateStr = sdfd.format(date);

        return dateStr;
    }

    /**
     * 得到n天之后是周几
     * @param days
     * @return
     */
    public static String getAfterDayWeek(String days) {
    	int daysInt = Integer.parseInt(days);

        Calendar canlendar = Calendar.getInstance(); // java.util包
        canlendar.add(Calendar.DATE, daysInt); // 日期减 如果不够减会将月变动
        Date date = canlendar.getTime();

        SimpleDateFormat sdf = new SimpleDateFormat("E");
        String dateStr = sdf.format(date);

        return dateStr;
    }

    public static String getAfterDateMMD(String days) {
    	int daysInt = Integer.parseInt(days);

        Calendar canlendar = Calendar.getInstance(); // java.util包
        canlendar.add(Calendar.DATE, daysInt); // 日期减 如果不够减会将月变动
        Date date = canlendar.getTime();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMd");
        String dateStr = sdf.format(date);

        return dateStr;
    }

    public static String getAfterDate(String days) {
    	int daysInt = Integer.parseInt(days);

        Calendar canlendar = Calendar.getInstance(); // java.util包
        canlendar.add(Calendar.DATE, daysInt); // 日期减 如果不够减会将月变动
        Date date = canlendar.getTime();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String dateStr = sdf.format(date);

        return dateStr;
    }

	/**
	 * 巡查时长
	 * @return
	 */
	/**
	 * 两个时间相差距离多少天多少小时多少分多少秒
	 * @param str1 时间参数 1 格式：1990-01-01 12:00:00
	 * @param str2 时间参数 2 格式：2009-01-01 12:00:00
	 * @return String 返回值为：xx天xx小时xx分xx秒
	 */
	public static String getDistanceTime(String str1, String str2) {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date one, two;
		long day = 0, hour = 0, min = 0, sec = 0;
		try {
			one = df.parse(str1);
			two = df.parse(str2);
			long time1 = one.getTime();
			long time2 = two.getTime();
			long diff ;
			if(time1<time2) {
				diff = time2 - time1;
			} else {
				diff = time1 - time2;
			}
			day = diff / (24 * 60 * 60 * 1000);
			hour = (diff / (60 * 60 * 1000) - day * 24);
			min = ((diff / (60 * 1000)) - day * 24 * 60 - hour * 60);
			sec = (diff/1000-day*24*60*60-hour*60*60-min*60);
		} catch (ParseException e) {
			return "0";
		}
		if (day == 0) {
			return 	  ("00"+hour).substring(("00"+hour).length()-2) + ":"
					+ ("00"+min).substring(("00"+min).length()-2) + ":"
					+ ("00"+sec).substring(("00"+sec).length()-2);
		}
		return day + " "
				+ ("00"+hour).substring(("00"+hour).length()-2) + ":"
				+ ("00"+min).substring(("00"+min).length()-2) + ":"
				+ ("00"+sec).substring(("00"+sec).length()-2);
	}

	/**
	 * 两个时间相差距离多少天多少小时多少分多少秒
	 * @param one 时间参数 1
	 * @param two 时间参数 2
	 * @return String 返回值为：xx天xx小时xx分xx秒
	 */
	public static String getDistanceTime(Date one, Date two) {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		long day = 0, hour = 0, min = 0, sec = 0;

		long time1 = one.getTime();
		long time2 = two.getTime();
		long diff ;
		if(time1<time2) {
			diff = time2 - time1;
		} else {
			diff = time1 - time2;
		}
		day = diff / (24 * 60 * 60 * 1000);
		hour = (diff / (60 * 60 * 1000) - day * 24);
		min = ((diff / (60 * 1000)) - day * 24 * 60 - hour * 60);
		sec = (diff/1000-day*24*60*60-hour*60*60-min*60);

		if (day == 0) {
			return 	  ("00"+hour).substring(("00"+hour).length()-2) + ":"
					+ ("00"+min).substring(("00"+min).length()-2) + ":"
					+ ("00"+sec).substring(("00"+sec).length()-2);
		}
		return day + " "
				+ ("00"+hour).substring(("00"+hour).length()-2) + ":"
				+ ("00"+min).substring(("00"+min).length()-2) + ":"
				+ ("00"+sec).substring(("00"+sec).length()-2);
	}

	/**
	 *
	 * @param startTime 06:00
	 * @param endTime 10:00
	 * @return
	 */
	public static boolean isCurrentTimeBetweenStartAndEnd(String startTime, String endTime) {

		String currentTimeHm = sdfHm.format(new Date());

		try {
			String startTimeHm = sdfHm.format(sdfHm.parse(startTime));
			int res = currentTimeHm.compareTo(startTimeHm);
			System.out.println(res);
			if (res < 0) {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			String endTimeHm = sdfHm.format(sdfHm.parse(endTime));
			int res = currentTimeHm.compareTo(endTimeHm);
			System.out.println(res);
			if (res > 0) {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}


	public static void main(String[] args) throws Exception {
//    	System.out.println(getDistanceTime("2018-03-02 08:55:10", "2018-03-03 08:55:59"));


//		System.out.println(sdfHm.format(new Date()));
//		System.out.println(sdfHm.format(sdfHm.parse("06:00")));
//		System.out.println(sdfHm.format(sdfHm.parse("10:00")));
																//09:10
		System.out.println(getAfterDate("-1"));
    }

}
