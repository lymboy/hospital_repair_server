package com.ruoyi.hospital.util;

import java.security.MessageDigest;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.codec.binary.Base64;

//import com.gexin.fastjson.JSONObject;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class MD5 {

	public static String md5(String str) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(str.getBytes());
			byte b[] = md.digest();

			int i;

			StringBuffer buf = new StringBuffer("");
			for (int offset = 0; offset < b.length; offset++) {
				i = b[offset];
				if (i < 0) {
                    i += 256;
                }
				if (i < 16) {
                    buf.append("0");
                }
				buf.append(Integer.toHexString(i));
			}
			str = buf.toString();
		} catch (Exception e) {
			e.printStackTrace();

		}
		return str;
	}

	public static final String ENCRYPTION_ALGORITHM = "AES";
    public static final String CIPHER_PARAM = "AES/CBC/PKCS5Padding";

    /**
     * 自定义秘钥、偏移量加密
     * @Title: encrypt
     * @Description: AES加密
     * @param enString 用来加密的明文
     * @param enString 用来加密的密钥
     * @param enString 用来加密的偏移量
     * @throws Exception
     * @return String 加密后密文
     */
    public static String ownEncrypt(String enString, String secretKey, String ivParameter) {

        try{
            Base64 base64 = new Base64();
            //使用自定义的偏移量
            IvParameterSpec iv = new IvParameterSpec(ivParameter.getBytes());
            //使用自定义的秘钥
            SecretKeySpec key = new SecretKeySpec(secretKey.getBytes(), ENCRYPTION_ALGORITHM);

            Cipher cipher = Cipher.getInstance(CIPHER_PARAM);
            cipher.init(Cipher.ENCRYPT_MODE, key, iv);
            byte[] encrypted = cipher.doFinal(enString.getBytes("utf-8"));
            return byte2hex(encrypted).toLowerCase();
        }catch (Exception e){
            return null;
        }

    }

    /**
     * 自定义秘钥、偏移量解密
     * @Title: decrypt
     * @Description: AES解密
     * @param deString 用来解密的密文
     * @throws Exception
     * @return String 解密后的明文
     */
    public static String ownDecrypt(String deString,String secretKey, String ivParameter){
        try {
            //使用自定义的偏移量
            IvParameterSpec iv = new IvParameterSpec(ivParameter.getBytes());
            //使用自定义的秘钥
            SecretKeySpec key = new SecretKeySpec(secretKey.getBytes(), ENCRYPTION_ALGORITHM);

            Base64 base64 = new Base64();
            Cipher cipher = Cipher.getInstance(CIPHER_PARAM);
            cipher.init(Cipher.DECRYPT_MODE, key, iv);
            byte[] decrypted = hex2byte(deString);
            byte[] original = cipher.doFinal(decrypted);
            return new String(original, "utf-8");
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * string 转byte
     * @param strhex
     * @return
     */
    private static byte[] hex2byte(String strhex) {
        if (strhex == null) {
            return null;
        }
        int l = strhex.length();
        if (l % 2 == 1) {
            return null;
        }
        byte[] b = new byte[l / 2];
        for (int i = 0; i != l / 2; i++) {
            b[i] = (byte) Integer.parseInt(strhex.substring(i * 2, i * 2 + 2),
                    16);
        }
        return b;
    }

    /**
     * byte 转 string
     * @param b
     * @return
     */
    private static String byte2hex(byte[] b) {
        String hs = "";
        String stmp = "";
        for (int n = 0; n < b.length; n++) {
            stmp = (Integer.toHexString(b[n] & 0XFF));
            if (stmp.length() == 1) {
                hs = hs + "0" + stmp;
            } else {
                hs = hs + stmp;
            }
        }
        return hs.toUpperCase();
    }

	public static void main(String[] args) {
		JSONObject paramJo = new JSONObject();

        paramJo.put("telephone","15733290550");
        paramJo.put("content","测试一下");


        System.out.println(paramJo.toJSONString());

        String result = MD5.ownEncrypt(paramJo.toJSONString(), "abbd7b0d9a7111ea864e0050569411b0", "c9a7111ea864e006");

        System.out.println(result);

        /*System.out.println(MD5.ownDecrypt(result, "1234567890123456", "1234567890123456"));
		System.out.println(md5("1"));*/
	}
}
