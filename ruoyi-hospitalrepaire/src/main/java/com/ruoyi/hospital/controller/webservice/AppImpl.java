//package com.ruoyi.hospital.controller.webservice;
//
//import java.text.DateFormat;
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.List;
//
//import javax.annotation.Resource;
//import javax.servlet.http.HttpSession;
//
//import org.activiti.engine.HistoryService;
//import org.activiti.engine.TaskService;
//import org.activiti.engine.history.HistoricActivityInstance;
//import org.activiti.engine.history.HistoricActivityInstanceQuery;
//import org.activiti.engine.history.HistoricProcessInstance;
//import org.activiti.engine.history.HistoricProcessInstanceQuery;
//import org.activiti.engine.history.HistoricTaskInstance;
//import org.activiti.engine.task.Comment;
//import org.apache.shiro.SecurityUtils;
//import org.apache.shiro.authc.AuthenticationException;
//import org.apache.shiro.authc.UsernamePasswordToken;
//import org.apache.shiro.session.Session;
//import org.apache.shiro.subject.Subject;
//import org.springframework.beans.factory.annotation.Autowired;
//
//import com.ruoyi.hospital.entity.system.DataPrivilege;
//import com.ruoyi.hospital.entity.system.Menu;
//import com.ruoyi.hospital.entity.system.User;
//import com.ruoyi.hospital.entity.system.UserIdentity;
//import com.ruoyi.hospital.entity.workflow.HistoricProcessInstanceVo;
//import com.ruoyi.hospital.framework.context.MySessionContext;
//import com.ruoyi.hospital.framework.exception.GenException;
//import com.ruoyi.hospital.service.app.AppService;
//import com.ruoyi.hospital.service.currency.CurrencyTypeService;
//import com.ruoyi.hospital.service.system.identity.UserIdentityService;
//import com.ruoyi.hospital.service.system.menu.MenuService;
//import com.ruoyi.hospital.service.system.privilege.DataPrivilegeService;
//import com.ruoyi.hospital.service.system.role.RoleService;
//import com.ruoyi.hospital.service.system.user.UserService;
//import com.ruoyi.hospital.util.Const;
//import com.ruoyi.hospital.util.DateUtil;
//import com.ruoyi.hospital.util.FileUtil;
//import com.ruoyi.hospital.util.MD5;
//import com.ruoyi.hospital.util.PageData;
//import com.ruoyi.hospital.util.Tools;
//import com.ruoyi.hospital.util.UuidUtil;
//
//import net.sf.json.JSONObject;
//
///**
// * <p>Title: AppController.java</p>
// * @Desc: App 接口
// * @author 杨建军
// * @Date: 2017年4月27日
// * @Time: 下午4:39:56
// * @version:
// */
//public class AppImpl implements App {
//
//	@Resource(name="appService")
//	private AppService appService;
//	@Resource(name="userService")
//	private UserService userService;
//	@Resource(name="menuService")
//	private MenuService menuService;
//	@Resource(name="roleService")
//	private RoleService roleService;
//	@Resource(name="dataPrivilegeService")
//	private DataPrivilegeService dataPrivilegeService;
//	@Resource(name="userIdentityService")
//	private UserIdentityService userIdentityService;
//	@Resource(name="currencyTypeService")
//	private CurrencyTypeService currencyTypeService;
//	/*@Resource(name="approvalStatusService")
//	private ApprovalStatusService approvalStatusService;*/
//	@Resource(name="taskService")
//	private TaskService taskService;
//	@Autowired
//    private HistoryService historyService;
//
//	/**
//	 * 请求登录，验证用户
//	 */
//	@Override
//	public String login(String userName, String password) {
//		JSONObject obj = new JSONObject();
//		PageData pd = new PageData();
//		PageData appUserInfo = new PageData();
//		PageData appIdentityInfo = new PageData();
//		PageData appProjectInfo = new PageData();
//		List<PageData> appMenuList = new ArrayList<PageData>();
//		String errInfo = "";
//
//		try {
//			if(null != userName && password != null && !"".equals(userName) && !"".equals(password)){
//				//shiro管理的session
//				Subject currentUser = SecurityUtils.getSubject();
//				Session session = currentUser.getSession();
//
//				String USERNAME = userName;
//				String PASSWORD  = password;
//				pd.put("USERNAME", USERNAME);
//				String passwd = MD5.md5(PASSWORD);	//密码加密
//					pd.put("PASSWORD", passwd);
//
//					pd = userService.getUserByNameAndPwd(pd);
//
//					if(pd != null){
//						// 判断账户是否过期
//						Date dueTime = null;
//						if(!"".equals(pd.getString("DUE_TIME")) && pd.getString("DUE_TIME")!=null ){
//							DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//							try {
//								dueTime = sdf.parse(pd.getString("DUE_TIME"));
//							} catch (ParseException e) {
//								dueTime=null;
//							}
//						}
//						if(dueTime==null || (dueTime!=null && dueTime.getTime()>new Date().getTime())){
//							User user = new User();
//							user.setUserId(pd.getString("ID"));
//							user.setUserName(pd.getString("USER_NAME"));
//							user.setEmployeeId(pd.getString("EMPLOYEE_ID"));
//							user.setEmployeeName(pd.getString("EMPLOYEE_NAME"));
//							user.setDepartmentId(pd.getString("DEPARTMENT_ID"));
//							user.setDepartmentName(pd.getString("DEPARTMENT_NAME"));
//							user.setEmployeePhoto(pd.getString("EMPLOYEE_PHOTO"));
//
//							// 用户信息封装
//							appUserInfo.put("userId", user.getUserId());
//							appUserInfo.put("userName", user.getUserName());
//							appUserInfo.put("employeeId", user.getEmployeeId());
//							appUserInfo.put("employeeName", user.getEmployeeName());
//							appUserInfo.put("departmentId", user.getDepartmentId());
//							appUserInfo.put("departmentName", user.getDepartmentName());
//							appUserInfo.put("employeePhoto", user.getEmployeePhoto());
//
//
//							pd.put("userId", user.getUserId());
//
//							session.setAttribute(Const.SESSION_USER, user);
//							session.removeAttribute(Const.SESSION_SECURITY_CODE);
//
//							//shiro加入身份验证
//							Subject subject = SecurityUtils.getSubject();
//						    UsernamePasswordToken token = new UsernamePasswordToken(USERNAME, PASSWORD);
//						    try {
//						        subject.login(token);
//						    } catch (AuthenticationException e) {
//						    	errInfo = "usererror";
//						    }
//
//							// 判断用户是否有身份
//							UserIdentity identityInfo = userIdentityService.getDefaultIdentityByUserId(pd);
//							if(identityInfo==null){
//								errInfo="noIdentity";
//							}else{
//								// 身份ID
//								String identityId = (String) session.getAttribute(Const.SESSION_IDENTITY_ID);
//								pd.put("userId", user.getUserId());
//								if(identityId==null || "".equals(identityId)){
//									UserIdentity identityInfoCh = userIdentityService.getDefaultIdentityByUserId(pd);
//									identityId = String.valueOf(identityInfoCh.getIdentityId());
//								}
//								// 用户 登陆身份存入SESSION
//								pd.put("identityId", identityId);
//								identityInfo= userIdentityService.getIdentityInfoByIdentityId(pd);
//
//								// 身份 信息封装
//								appIdentityInfo.put("userIdentityId", identityInfo.getIdentityId());
//								appIdentityInfo.put("userIdentityDepartmentId", identityInfo.getDepartmentId());
//								appIdentityInfo.put("userIdentityDepartmentName", identityInfo.getDepartmentName());
//
//								//if(null == session.getAttribute(Const.SESSION_dataPrivilegeList)){
//									// 通过用户身份ID 获取用户的数据权限
//									pd.put("identityId", identityId);
//									List<DataPrivilege> dataPrivilegeList = dataPrivilegeService.getDataPrivilegeByIdentityId(pd);
//									// 向session 添加 用户数据权限
//									session.setAttribute(Const.SESSION_dataPrivilegeList, dataPrivilegeList);
//								//}
//
//									// 菜单
//									List<Menu> allmenuList = new ArrayList<Menu>();
//									pd.put("identityId", identityId);
//									allmenuList = menuService.listAllMenu(pd);
//									session.setAttribute(Const.SESSION_allmenuList, allmenuList);			//菜单权限放入session中
//									PageData menuPd = null;
//									for (Menu menu : allmenuList) {
//										menuPd =  new PageData();
//										menuPd.put("id", menu.getMenuId());
//										menuPd.put("menuName", menu.getMenuName());
//										menuPd.put("menuUrl", menu.getMenuUrl());
//										menuPd.put("menuGroup", menu.getMenuGroup());
//										List<PageData> subPdList = new ArrayList<PageData>();
//										PageData subMenuPd = null;
//										for (Menu submenu : menu.getSubMenu()) {
//											subMenuPd = new PageData();
//											subMenuPd.put("id", submenu.getMenuId());
//											subMenuPd.put("menuName", submenu.getMenuName());
//											subMenuPd.put("menuUrl", submenu.getMenuUrl());
//											subMenuPd.put("menuGroup", submenu.getMenuGroup());
//											subPdList.add(subMenuPd);
//										}
//										menuPd.put("subMenu", subPdList);
//										appMenuList.add(menuPd);
//									}
//							}
//
//							// 项目部信息
//							PageData projectInfo = appService.getUserProjectByIdentityPartmentId(identityInfo.getDepartmentId());
//							// 项目部信息封装
//							appProjectInfo.put("departmentId", projectInfo.get("DEPARTMENT_ID"));
//							appProjectInfo.put("departmentCode", projectInfo.get("DEPARTMENT_CODE"));
//							appProjectInfo.put("departmentName", projectInfo.get("DEPARTMENT_NAME"));
//							appProjectInfo.put("departmentParentId", projectInfo.get("DEPARTMENT_PARENT_ID"));
//							appProjectInfo.put("departmentType", projectInfo.get("DEPARTMENT_TYPE"));
//							appProjectInfo.put("departmentTypeValue", projectInfo.get("DEPARTMENT_TYPE_VALUE"));
//							appProjectInfo.put("projectId", projectInfo.get("PROJECT_ID"));
//							appProjectInfo.put("projectName", projectInfo.get("PROJECT_NAME"));
//							appProjectInfo.put("projectShortName", projectInfo.get("SHORT_NAME"));
//							appProjectInfo.put("projectCode", projectInfo.get("PROJECT_NO"));
//							appProjectInfo.put("projectDuty", projectInfo.get("PROJECT_DUTY"));
//
//							obj.put("userInfo", appUserInfo);// 用户信息存入map
//							obj.put("sessionId", session.getId());// sessionId存入map
//							obj.put("userIdentityInfo", appIdentityInfo);// 用户身份信息
//							obj.put("projectInfo", appProjectInfo);
//							obj.put("menuInfoList", appMenuList);// 用户 菜单 信息存入map
//						}else{
//							errInfo = "outDueTime";
//						}
//					}else{
//						errInfo = "usererror"; 				//用户名或密码有误
//					}
//			}else{
//				errInfo = "errorparameter";	//参数 错误
//			}
//		} catch (Exception e1) {
//			e1.printStackTrace();
//			errInfo = "loginerror";	// 登录 错误
//		}
//		if(Tools.isEmpty(errInfo)){
//			obj.put("result", "success");//验证成功
//		}else{
//			obj.put("result", "error");//验证失败
//			obj.put("msg", errInfo);
//		}
//		return obj.toString();
//	}
//
//	/**
//	 * 修改用户 头像
//	 * @param sessionId
//	 * @param employeeId
//	 * @param file
//	 * @return
//	 */
//	@Override
//	public String changeEmployeePhoto(String sessionId, String employeeId, String fileName, byte[] fileDataByteArray) {
//		JSONObject obj = new JSONObject();
//		MySessionContext mysc = MySessionContext.getInstance();
//		HttpSession session = mysc.getSession(sessionId);
//
//		if(session!=null){
//			PageData pd = new PageData();
//			if(employeeId!=null && !"".equals(employeeId)){
//				pd.put("employeeId", employeeId);
//				String rootPath = Const.APP_UPLOAD_PATH;
//				String midPath = "/uploadFiles/employeePhoto/";
//				String folderPath = rootPath+midPath;
//
//				fileName = UuidUtil.get32UUID()+fileName.substring(fileName.lastIndexOf("."));
//				try {
//					FileUtil.byte2File(fileDataByteArray, folderPath, fileName);
//					pd.put("employeePhoto",midPath + fileName);
//					appService.editEmployeePhoto(pd);
//					obj.put("employeePhoto", midPath + fileName);
//					obj.put("result", "success");
//					obj.put("msg", "success");
//				} catch (Exception e) {
//					obj.put("result", "error");
//				}
//			}else{
//				obj.put("result", "error");
//				obj.put("msg", "noEmployeeId");//缺少用户ID
//			}
//		}else{
//			obj.put("result", "error");
//			obj.put("msg", "sessionError");//缺少用户ID
//		}
//		return obj.toString();
//	}
//
//
//	/**
//	 * 质检查看列表
//	 */
//	@Override
//	public String qualityCheck(String sessionId, String optionType, String userId, String projectId, String status,String queryConditon) {
//		JSONObject obj = new JSONObject();
//		MySessionContext mysc = MySessionContext.getInstance();//通过sessionId得到session
//		PageData pd=new PageData();
//		HttpSession session = mysc.getSession(sessionId);
//		List<PageData> pds=null;
//		List<PageData> checkTypeList=null;
//		if(session!=null){
//		try {
//				User user = (User) session.getAttribute(Const.SESSION_USER);
//				checkTypeList=currencyTypeService.getCurrencyType("14");
//				pd.put("optionType", optionType);
//				pd.put("userId", userId);
//				pd.put("projectId", projectId);
//				pd.put("queryConditon", queryConditon);
//				pd.put("status", status);
//				pd.put("departmentId", user.getDepartmentId());
//				//pds=appService.getQualityCheckList(pd);
//			} catch (GenException e) {
//				obj.put("result", "error");
//			}
//			obj.put("checkTypeList", checkTypeList);//质量检验类型
//			obj.put("dataList", pds);
//			obj.put("result", "success");
//			obj.put("msg", "success");
//		}else{
//			obj.put("result", "error");
//			obj.put("msg", "sessionError");//没有登陆
//		}
//		return obj.toString();
//	}
//
//	/*@Override
//	public String seeQualityCheckInfo(String sessionId, String qualityId) {
//		// TODO Auto-generated method stub
//		return null;
//	}*/
//
//	/**
//	 * 查看质量验收的信息
//	 *//*
//	@Override
//	public String seeQualityCheckInfo(String sessionId, String qualityId) {
//		JSONObject obj=new JSONObject();
//		MySessionContext mysc=MySessionContext.getInstance();
//		HttpSession session=mysc.getSession(sessionId);
//		PageData qualityInfo=null;
//		List<PageData> projectQualityList=null;
//		List<PageData> FileList=null;
//		List<Comment> commentList = new ArrayList<Comment>();
//		List<PageData> pds = new ArrayList<PageData>();
//		PageData pageData = new PageData();
//		if(session!=null){
//			try {
//				qualityInfo=appService.getQualityCheckInfo(qualityId);
//				projectQualityList=appService.getProjectQualityList();//质量单元从表
//				FileList=appService.getFileList(qualityId);//上传文件从表
//				String processInstanceId = approvalStatusService.getProcessInstanceId(qualityId);
//				commentList = taskService.getProcessInstanceComments(processInstanceId);
//				for (Comment comment : commentList){
//					HistoricTaskInstance singleResult = historyService.createHistoricTaskInstanceQuery().taskId(comment.getTaskId()).singleResult();
//					String taskName = historyService.createHistoricTaskInstanceQuery().taskId(comment.getTaskId()).singleResult().getName();
//					String assigneeId = singleResult.getAssignee();
//					String approvePersonName=appService.getApproveName(assigneeId);
//					pageData.put("approvePersonName", approvePersonName);
//					pageData.put("taskName", taskName);
//					pds.add(pageData);
//					pageData = new PageData();
//				}
//			} catch (GenException e) {
//				obj.put("result", "error");
//			}
//			obj.put("comment", pds);//审批列表
//			obj.put("FileList", FileList);//从表文件
//			obj.put("projectQualityList", projectQualityList);
//			obj.put("GPS_ADDRESS", qualityInfo.getString("GPS_ADDRESS"));//gps地址
//			obj.put("PROJECT_DIVIDE_NAME", qualityInfo.getString("PROJECT_DIVIDE_NAME"));//工程还分名称
//			//obj.put("dataList", qualityInfo);
//			obj.put("result", "success");
//			obj.put("msg", "success");
//		}else{
//			obj.put("result", "error");
//			obj.put("msg", "sessionError");//没有登陆
//		}
//		return obj.toString();
//	}*/
//
//}
