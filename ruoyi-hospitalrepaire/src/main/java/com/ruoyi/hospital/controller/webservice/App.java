package com.ruoyi.hospital.controller.webservice;

import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface App {

	/**
	 * 请求登录，验证用户
	 */
	String login(@WebParam(name="userName") String userName,
					 @WebParam(name="password") String password);

	/**
	 * 修改用户 头像
	 * @param sessionId
	 * @param employeeId
	 * @param file
	 * @return
	 */
	String changeEmployeePhoto(@WebParam(name="sessionId") String sessionId,
										@WebParam(name="employeeId") String employeeId,
										@WebParam(name="fileName") String fileName,
										@WebParam(name="fileDataByteArray") byte[] fileDataByteArray);

	/**
	 * 质检列表查看
	 */
	String qualityCheck(@WebParam(name="sessionId") String sessionId,
								@WebParam(name="optionType") String optionType,
								@WebParam(name="userId") String userId,
								@WebParam(name="projectId") String projectId,
								@WebParam(name="status") String status,
								@WebParam(name="queryConditon") String queryConditon
								);
	/**
	 * 质检信息查看
	 *//*
	String seeQualityCheckInfo(@WebParam(name="sessionId") String sessionId,
								@WebParam(name="qualityId") String qualityId);*/

}
