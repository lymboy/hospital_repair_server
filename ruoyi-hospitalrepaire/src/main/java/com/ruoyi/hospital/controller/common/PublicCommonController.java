package com.ruoyi.hospital.controller.common;

import com.ruoyi.hospital.service.currency.CurrencyTypeService;
import com.ruoyi.hospital.util.PageData;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p> 共有 请求 </p>
 *
 * @author Young
 * @since 2018-07-27
 */
@Controller("publicCommonController")
@RequestMapping("/public_common")
public class PublicCommonController {

    @Resource
    private CurrencyTypeService currencyTypeService;

    /**
     * 查询下拉框 by kindId
     * @param kindId
     * @return
     */
    @RequestMapping(value = "/getCurrencyTypeListForSpinner/{kindId}", produces = "application/json;charset=UTF-8")
    public @ResponseBody List<PageData> getCurrencyTypeListByKindIDForSpinner(@PathVariable String kindId) {
        try {
            List<PageData> patrolTypeList = currencyTypeService.getCurrencyType(kindId);
            return patrolTypeList;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
