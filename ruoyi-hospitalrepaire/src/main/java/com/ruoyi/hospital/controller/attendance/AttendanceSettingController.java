package com.ruoyi.hospital.controller.attendance;

import com.ruoyi.hospital.controller.base.BaseController;
import com.ruoyi.hospital.entity.Page;
import com.ruoyi.hospital.entity.system.User;
import com.ruoyi.hospital.framework.exception.GenException;
import com.ruoyi.hospital.service.attendance.AttendanceSettingService;
import com.ruoyi.hospital.service.currency.CurrencyTypeService;
import com.ruoyi.hospital.util.PageData;
import com.ruoyi.hospital.util.UuidUtil;
import com.ruoyi.hospital.util.sequence;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p> 考勤设置 </p>
 *
 * @author Young
 * @since 2018/4/2
 */
@Controller
@RequestMapping("attendance_setting")
public class AttendanceSettingController extends BaseController {

    @Autowired
    private AttendanceSettingService attendanceSettingService;
    @Autowired
    private CurrencyTypeService currencyTypeService;

    /**
     * 转到 考勤设置
     * @return
     * @throws GenException
     */
    @RequestMapping(value="list", produces="text/html;charset=UTF-8")
    public ModelAndView list() throws GenException{
        ModelAndView mv = this.getModelAndView();
        PageData pd = this.getPageData();
        mv.addObject("pd", pd);
        mv.setViewName("attendance/attendance_setting_list");
        List<PageData> postList = currencyTypeService.getCurrencyType("5");// 5 职务（岗位）
        mv.addObject("postList", postList);
        return mv;
    }

    /**
     * 分页获取考勤设置
     * @throws GenException
     */
    @RequestMapping(value="attendance_setting_list_page",produces="text/html;charset=UTF-8")
    public @ResponseBody String dataListPage(@RequestParam(value = "page", defaultValue = "1") int currentPage,
                        @RequestParam(value = "rows", defaultValue = "20") int showCount) throws GenException {
        Page page = this.getPage();
        PageData pd = this.getPageData();
        page.setShowCount(showCount);
        page.setCurrentPage(currentPage);
        page.setPd(pd);

        JSONObject obj = new JSONObject();
        List<PageData> list = attendanceSettingService.getAttendanceSettingListPage(page);

        obj.put("records", page.getTotalResult());//总记录数
        obj.put("page", page.getCurrentPage());//当前页
        obj.put("total", page.getTotalPage());//总页数
        obj.put("rows", list);

        return obj.toString();
    }

    /**
     * 转到 考勤设置 编辑
     * @return
     * @throws GenException
     */
    @RequestMapping(value="edit", produces="text/html;charset=UTF-8")
    public ModelAndView edit(@RequestParam(value = "ID", required = true) String ID) throws GenException{
        ModelAndView mv = this.getModelAndView();
        PageData pdInfo = attendanceSettingService.getAttendanceSettingInfoById(ID);
        mv.addObject("data", pdInfo);

        List<PageData> postList = currencyTypeService.getCurrencyType("5");// 5 职务（岗位）
        mv.addObject("postList", postList);

        mv.setViewName("attendance/attendance_setting_edit");
        return mv;
    }

    /**
     * 转到 考勤设置 新增
     * @return
     * @throws GenException
     */
    @RequestMapping(value="add", produces="text/html;charset=UTF-8")
    public ModelAndView add() throws GenException{
        ModelAndView mv = this.getModelAndView();
        PageData pd = this.getPageData();
//		Subject subject = SecurityUtils.getSubject();
//		Session session = subject.getSession();
//		User user = (User) session.getAttribute(Const.SESSION_USER);

        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        // 主键
        pd.put("ID", UuidUtil.get32UUID());
        // 生成单据编号
        pd.put("BILL_CODE", sequence.nextId());
        pd.put("EDITOR", user.getEmployeeId());
        pd.put("EDITOR_NAME", user.getEmployeeName());
        pd.put("EDITOR_DEPARTMENT_NAME", user.getDepartmentName());
        pd.put("EDITOR_ORGANIZATION_NAME", user.getOrganizationName());
        mv.addObject("data", pd);

        List<PageData> postList = currencyTypeService.getCurrencyType("5");// 5 职务（岗位）
        mv.addObject("postList", postList);

        mv.setViewName("attendance/attendance_setting_edit");
        return mv;
    }

    /**
     * 新增 或 编辑
     * @return
     * @throws GenException
     */
    @RequestMapping(value="insert_or_update", produces="application/json;charset=UTF-8")
    public @ResponseBody Map<String, Object> insertOrUpdate() throws GenException {
        try {
            Map<String, Object> map = new HashMap<String, Object>();
            PageData pd = this.getPageData();
//		Subject subject = SecurityUtils.getSubject();
//		Session session = subject.getSession();
//		User user = (User) session.getAttribute(Const.SESSION_USER);

            User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

            pd.put("CREATER", user.getUserId());
            pd.put("CREATEDATE", new Date());
            pd.put("MODIFIER", user.getUserId());
            pd.put("MODIFYDATE", new Date());
            int rows = attendanceSettingService.insertOrUpdate(pd);
            if (rows > 0) {
                map.put("data", pd);
                map.put("result", "success");
            } else {
                map.put("data", pd);
                map.put("result", "error");
            }
            return map;
        } catch (GenException e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     * 批量删除
     * @return
     * @throws GenException
     */
    @RequestMapping(value="batchDeleteAttendanceSetting", produces="application/json;charset=UTF-8")
    public @ResponseBody Map<String, Object> batchDeleteAttendanceSetting(@RequestParam(value = "ids[]", required = true) List<String> ids) throws GenException {
        try {
            Map<String, Object> map = new HashMap<String, Object>();
//		Subject subject = SecurityUtils.getSubject();
//		Session session = subject.getSession();
//		User user = (User) session.getAttribute(Const.SESSION_USER);

            User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

            int rows = attendanceSettingService.batchDeleteAttendanceSetting(ids, String.valueOf(user.getUserId()));
            if (rows > 0) {
                map.put("result", "success");
            } else {
                map.put("result", "error");
            }
            return map;
        } catch (GenException e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     * 从表 分页获取考勤设置
     * @throws GenException
     */
    @RequestMapping(value="attendance_setting_detail_list_page",produces="text/html;charset=UTF-8")
    public @ResponseBody String dataDetailListPage(@RequestParam(value = "page", defaultValue = "1") int currentPage,
                                             @RequestParam(value = "rows", defaultValue = "20") int showCount) throws GenException {
        Page page = this.getPage();
        PageData pd = this.getPageData();
        page.setShowCount(showCount);
        page.setCurrentPage(currentPage);
        page.setPd(pd);

        JSONObject obj = new JSONObject();
        List<PageData> list = attendanceSettingService.getAttendanceSettingDetailListPage(page);

        obj.put("records", page.getTotalResult());//总记录数
        obj.put("page", page.getCurrentPage());//当前页
        obj.put("total", page.getTotalPage());//总页数
        obj.put("rows", list);

        return obj.toString();
    }

    /**
     * 从表 编辑
     * @return
     * @throws GenException
     */
    @RequestMapping(value="updateAttendanceSettingDetail", produces="application/json;charset=UTF-8")
    public @ResponseBody Map<String, Object> updateDetail() throws GenException {
        try {
            Map<String, Object> map = new HashMap<String, Object>();
            PageData pd = this.getPageData();
//		Subject subject = SecurityUtils.getSubject();
//		Session session = subject.getSession();
//		User user = (User) session.getAttribute(Const.SESSION_USER);

            User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

            pd.put("MODIFIER", user.getUserId());
            pd.put("MODIFYDATE", new Date());
            int rows = attendanceSettingService.updateAttendanceSettingDetail(pd);
            if (rows > 0) {
                map.put("data", pd);
                map.put("result", "success");
            } else {
                map.put("data", pd);
                map.put("result", "error");
            }
            return map;
        } catch (GenException e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     * 从表 批量删除
     * @return
     * @throws GenException
     */
    @RequestMapping(value="batchDeleteAttendanceSettingDetail", produces="application/json;charset=UTF-8")
    public @ResponseBody Map<String, Object> batchDeleteAttendanceSettingDetail(@RequestParam(value = "ids[]", required = true) List<String> ids) throws GenException {
        try {
            Map<String, Object> map = new HashMap<String, Object>();
//		Subject subject = SecurityUtils.getSubject();
//		Session session = subject.getSession();
//		User user = (User) session.getAttribute(Const.SESSION_USER);

            User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

            int rows = attendanceSettingService.batchDeleteAttendanceSettingDetail(ids, String.valueOf(user.getUserId()));
            if (rows > 0) {
                map.put("result", "success");
            } else {
                map.put("result", "error");
            }
            return map;
        } catch (GenException e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     * 转到 选择考勤人员数据 页面
     * @return
     * @throws GenException
     */
    @RequestMapping(value="to_select_emp_for_attendance", produces="text/html;charset=UTF-8")
    public ModelAndView toSelectEmpForAttendance(String ATTENDANCE_SETTING_ID, String POST_ID) throws GenException{
        ModelAndView mv = this.getModelAndView();
        PageData pd = this.getPageData();
        mv.addObject("data", pd);
        mv.setViewName("attendance/attendance_setting_select_emp");
        return mv;
    }

    /**
     * 分页获取 选择考勤人员数据
     * @throws GenException
     */
    @RequestMapping(value="get_select_emp_list_page",produces="text/html;charset=UTF-8")
    public @ResponseBody String getSelectEmpListPage(@RequestParam(value = "page", defaultValue = "1") int currentPage,
                                                     @RequestParam(value = "rows", defaultValue = "20") int showCount) throws GenException {
        Page page = this.getPage();
        PageData pd = this.getPageData();
        page.setShowCount(showCount);
        page.setCurrentPage(currentPage);
        page.setPd(pd);

        JSONObject obj = new JSONObject();

        List<PageData> list = attendanceSettingService.getSelectEmpListPage(page);

        obj.put("records", page.getTotalResult());//总记录数
        obj.put("page", page.getCurrentPage());//当前页
        obj.put("total", page.getTotalPage());//总页数
        obj.put("rows", list);
        return obj.toString();
    }

    /**
     * 从表 批量保存 选择考勤人员数据
     * @return
     * @throws GenException
     */
    @RequestMapping(value="batchInsertAttendanceSettingDetail", produces="application/json;charset=UTF-8")
    public @ResponseBody Map<String, Object> batchInsertAttendanceSettingDetail(@RequestParam(value = "empIds[]", required = true) List<String> empIds,
                                                                                @RequestParam(value = "attendanceSettingId", required = true) String attendanceSettingId) throws GenException {
        try {
            Map<String, Object> map = new HashMap<String, Object>();
//		Subject subject = SecurityUtils.getSubject();
//		Session session = subject.getSession();
//		User user = (User) session.getAttribute(Const.SESSION_USER);

            User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

            int rows = attendanceSettingService.batchInsertAttendanceSettingDetail(empIds, attendanceSettingId, String.valueOf(user.getUserId()));
            if (rows > 0) {
                map.put("result", "success");
            } else {
                map.put("result", "error");
            }
            return map;
        } catch (GenException e) {
            throw new GenException(e.getCause());
        }
    }

}
