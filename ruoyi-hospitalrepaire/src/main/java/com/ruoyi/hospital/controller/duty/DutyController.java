package com.ruoyi.hospital.controller.duty;

import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.hospital.controller.base.BaseController;
import com.ruoyi.hospital.entity.Page;
import com.ruoyi.hospital.entity.system.User;
import com.ruoyi.hospital.framework.exception.GenException;
import com.ruoyi.hospital.service.duty.DutyService;
import com.ruoyi.hospital.service.system.user.UserService;
import com.ruoyi.hospital.util.FileDownload;
import com.ruoyi.hospital.util.PageData;
import com.ruoyi.hospital.util.ThumbImage;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONObject;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.*;

@Slf4j
@Controller
@RequestMapping({"/duty"})
public class DutyController
        extends BaseController {
    @Resource(name = "dutyService")
    private DutyService dutyService;
    @Resource(name = "userService")
    private UserService userService;

    @RequestMapping({"/listDuty"})
    public ModelAndView listEmployee() throws GenException {
        ModelAndView mv = getModelAndView();
        mv.setViewName("duty/duty_list");
        return mv;
    }

    @RequestMapping(value = {"/getDutyList"}, produces = {"application/json;charset=UTF-8"})
    @ResponseBody
    public String getDutyList(Page page) throws GenException {
        JSONObject obj = new JSONObject();

//			Subject subject = SecurityUtils.getSubject();
//			Session session = subject.getSession();
//			User user = (User) session.getAttribute(Const.SESSION_USER);

        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        PageData pd = getPageData();
        pd.put("userId", user.getEmployeeId());
        int start = Integer.valueOf(pd.getString("page")).intValue();
        int length = Integer.valueOf(pd.getString("rows")).intValue();
        page.setShowCount(length);

        List<String> list = null;
        page.setPd(pd);
        page.setCurrentPage(start);
        List<PageData> dutyList = null;
        try {
            dutyList = this.dutyService.getDutyListPage(page);
            obj.put("records", Integer.valueOf(page.getTotalResult()));
            obj.put("page", Integer.valueOf(page.getCurrentPage()));
            obj.put("total", Integer.valueOf(page.getTotalPage()));
            obj.put("rows", dutyList);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
        return obj.toString();
    }

    @RequestMapping({"/toAddDuty"})
    public ModelAndView goAddDuty() throws GenException {
        ModelAndView mv = getModelAndView();
        PageData pd = new PageData();


        List<PageData> condomMemberList = this.dutyService.getcondomMemberList();
        //List<PageData> memberList = this.dutyService.getMemberList();
        List<PageData> quesDictionaryList = this.dutyService.getQuestionDictionaryList();
        List<PageData> quesDictionarySmallList = this.dutyService.getQuesDictionarySmallList();
        List<PageData> groupDictionaryList = this.dutyService.getGroupDictionaryList();
        List<PageData> statusDictionaryList = this.dutyService.getStatusDictionaryList();
        List<PageData> auxiliaryDictionaryList = this.dutyService.getAuxiliaryDictionaryList();

        mv.setViewName("duty/add_dutyQuestion");
        mv.addObject("msg", "addDutyQuestion");
        mv.addObject("condomMemberList", condomMemberList);
        //mv.addObject("memberList", memberList);
        mv.addObject("quesDictionaryList", quesDictionaryList);
        mv.addObject("quesDictionarySmallList", quesDictionarySmallList);
        mv.addObject("groupDictionaryList", groupDictionaryList);
        mv.addObject("statusDictionaryList", statusDictionaryList);
        mv.addObject("auxiliaryDictionaryList", auxiliaryDictionaryList);
        return mv;
    }

    @RequestMapping({"/getMemberByDepartmentId"})
    @ResponseBody
    public String getMemberByDepartmentId() {
        JSONObject obj = new JSONObject();
        PageData pd = getPageData();
        Map<String, String> map = new HashMap<>();
        try {
            List<PageData> memberList = this.dutyService.getMemberByDepartmentId(pd);
            obj.put("rows", memberList);
            obj.put("result", "success");
        } catch (Exception e) {
            e.printStackTrace();
            obj.put("result", "error");
        }
        return obj.toString();
    }

    @RequestMapping({"/toEditDuty"})
    public ModelAndView goEditDuty() throws GenException {
        ModelAndView mv = getModelAndView();
        PageData pd = new PageData();
        pd = getPageData();
        List<PageData> groupDictionaryList = this.dutyService.getGroupDictionaryList();
        pd = this.dutyService.getDutyQuestionById(pd);
        List<PageData> implementList = dutyService.getMmberList();
        mv.setViewName("duty/edit_dutyQuestion");
        mv.addObject("msg", "editDutyQuestion");
        mv.addObject("implementList", implementList);
        mv.addObject("groupDictionaryList", groupDictionaryList);
        mv.addObject("pd", pd);
        return mv;
    }

    /**
     * 后台调用，修改问题、问题的分配执行情况
     *
     * @return
     */
    @RequestMapping({"/editDutyQuestion"})
    @ResponseBody
    public Map<String, String> editDutyQuestion() {
        PageData pd = getPageData();
        Map<String, String> map = new HashMap<>();
        try {
//			Subject subject = SecurityUtils.getSubject();
//			Session session = subject.getSession();
//			User user = (User) session.getAttribute(Const.SESSION_USER);

            User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

            String id = pd.getString("ID");
            if (StringUtils.isNotBlank(id)) {
                pd.put("MODIFYDATE", new Date());
                this.dutyService.editDutyQuestionAndSendMsg(pd);
            }
            map.put("result", "success");
        } catch (Exception e) {
            e.printStackTrace();
            map.put("result", "error");
        }
        return map;
    }

    @RequestMapping({"/addDutyQuestion"})
    @ResponseBody
    public Map<String, String> addDutyQuestion() {
        PageData pd = getPageData();
        Map<String, String> map = new HashMap<>();
        try {
//			Subject subject = SecurityUtils.getSubject();
//			Session session = subject.getSession();
//			User user = (User) session.getAttribute(Const.SESSION_USER);

            User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

            PageData pd2 = new PageData();
            pd2.put("ID", pd.getString("DEPARTMENT_ID"));
            PageData pd1 = userService.findByDepartmentId(pd2);
            pd.put("GROUP_ID", pd1.getString("GROUP_ID"));

            String DICTIONARY_ID_SMALL = pd.getString("DICTIONARY_ID_SMALL");
            PageData quesDictionary = dutyService.getHosQuesDictionary(DICTIONARY_ID_SMALL);
            pd.put("QUESTION_MARK", quesDictionary.getString("QUESTION_MARK"));

            String id = pd.getString("ID");
            if (StringUtils.isNotBlank(id)) {
                pd.put("DUTY_OFFICER", user.getEmployeeId());
                pd.put("MODIFYDATE", new Date());
                this.dutyService.editDutyQuestionAndSendMsg(pd);
            } else {
                pd.put("MODIFYDATE", new Date());
                pd.put("DUTY_OFFICER", user.getEmployeeId());
                pd.put("CREATEDATE", new Date());
                pd.put("ID", get32UUID());
                pd.put("ASSIGNOR_DATE", new Date());
                pd.put("SUB_ID", get32UUID());
                this.dutyService.addDutyQuestionAndSendMsg(pd);
            }

            map.put("result", "success");
        } catch (Exception e) {
            e.printStackTrace();
            map.put("result", "error");
        }
        return map;
    }

    @RequestMapping({"/deleteDuty"})
    @ResponseBody
    public Object deleteDuty() throws GenException {
        PageData pd = new PageData();
        Map<String, Object> map = new HashMap<>();
        try {
            pd = getPageData();
            this.dutyService.deleteDuty(pd);
            map.put("result", "success");
        } catch (Exception e) {
            log.error(e.toString(), e);
            map.put("result", "error");
        }
        return map;
    }

    /**
     * 问题列表  树控件
     */
    @ResponseBody
    @RequestMapping(value = "/getquestionTypeList", produces = "application/json;charset=UTF-8")
    public Object getquestionTypeList() {
        Map<String, Object> map = new HashMap<String, Object>();
        PageData pd = this.getPageData();
        List<PageData> departmentList = null;
        try {
            departmentList = dutyService.getquestionTypeList(pd);
            map.put("questionTypeList", departmentList);
            map.put("result", "success");
        } catch (Exception e) {
            e.printStackTrace();
            map.put("result", "error");
        }
        return map;
    }

    @RequestMapping({"/getQuestionTypeById"})
    @ResponseBody
    public String getQuestionTypeById() {
        JSONObject obj = new JSONObject();
        PageData pd = getPageData();
        Map<String, String> map = new HashMap<>();
        List<PageData> implementList = new ArrayList<PageData>();
        PageData implement = new PageData();
        try {
            List<PageData> questionTypeList = this.dutyService.getQuestionTypeById(pd);
            if (StringUtils.equals("1", pd.getString("DICTIONARY_ID"))) {
                implement = dutyService.getMemberByDictionaryId(pd);
                if (implement != null) {
                    implementList.add(implement);
                }
                obj.put("pd", implement);
            }
            obj.put("pd", implement);
            obj.put("implementList", implementList);
            obj.put("rows", questionTypeList);
            obj.put("result", "success");
        } catch (Exception e) {
            e.printStackTrace();
            obj.put("result", "error");
        }
        return obj.toString();
    }

    @RequestMapping({"/findByUiId"})
    @ResponseBody
    public String findByUiId() {
        JSONObject obj = new JSONObject();
        PageData pd = getPageData();
        List<PageData> implementList = new ArrayList<PageData>();
        try {
            String IMPLEMENT_ID = pd.getString("IMPLEMENT_ID");
            if (StringUtils.isEmpty(IMPLEMENT_ID)) {
                PageData pd1 = userService.findByDepartmentId(pd);
                pd.put("GROUP_ID", pd1.getString("GROUP_ID"));
                PageData implement = dutyService.getMemberByGroupId(pd);
                if (implement != null) {
                    implementList.add(implement);
                }
                obj.put("pd", pd1);
                obj.put("implementList", implementList);
                obj.put("result", "success");
            } else {
                obj.put("result", "success1");
            }

        } catch (Exception e) {
            e.printStackTrace();
            obj.put("result", "error");
        }
        return obj.toString();
    }

    @RequestMapping({"/uploadFile"})
    @ResponseBody
    public Map<String, String> uploadFile(HttpServletRequest request, @RequestParam(required = false) String ID, @RequestParam(required = true) String type) throws Exception {
        Map<String, String> map = new HashMap<String, String>();
//			Subject subject = SecurityUtils.getSubject();
//			Session session = subject.getSession();
//			User user = (User) session.getAttribute(Const.SESSION_USER);

        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        PageData pd = new PageData();
        if (StringUtils.isBlank(ID)) {
            pd.put("ID", get32UUID());
            pd.put("CREATEDATE", new Date());
            pd.put("DUTY_OFFICER", user.getEmployeeId());
            this.dutyService.addDutyQuestion1(pd);
        } else {
            pd.put("ID", ID);
        }
        String rootPath = request.getServletContext().getRealPath("/");
        String midPath = "/uploadFiles/HosQuestion/question/";
        File folder = new File(String.valueOf(rootPath) + midPath);


        MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;

        Iterator iter = multiRequest.getFileNames();
        while (iter.hasNext()) {

            MultipartFile file = multiRequest.getFile(iter.next().toString());
            if (file != null) {
                String fileName = String.valueOf(get32UUID()) + file.getOriginalFilename();

                String filePath = folder + "/" + fileName;
                File endfolder = new File(filePath);

                try {
                    if (!folder.exists()) {
                        folder.mkdirs();
                    }
                    if (!endfolder.exists()) {
                        file.transferTo(endfolder);

                        ThumbImage.thumbnailImages(filePath);
                    }
                    pd.put("MODIFYDATE", new Date());
                    if (StringUtils.equals("REMARK_IMG1", type)) {
                        pd.put("REMARK_IMG1", String.valueOf(midPath) + fileName);
                        this.dutyService.editDutyQuestionImg(pd);
                        map.put("REMARK_IMG1", String.valueOf(midPath) + fileName);
                    } else if (StringUtils.equals("REMARK_IMG2", type)) {
                        pd.put("REMARK_IMG2", String.valueOf(midPath) + fileName);
                        this.dutyService.editDutyQuestionImg(pd);
                        map.put("REMARK_IMG2", String.valueOf(midPath) + fileName);
                    } else if (StringUtils.equals("REMARK_IMG3", type)) {
                        pd.put("REMARK_IMG3", String.valueOf(midPath) + fileName);
                        this.dutyService.editDutyQuestionImg(pd);
                        map.put("REMARK_IMG3", String.valueOf(midPath) + fileName);
                    }
                    map.put("result", "success");
                } catch (IllegalStateException e) {
                    log.error(e.toString(), e);
                    map.put("result", "error");
                } catch (IOException e) {
                    log.error(e.toString(), e);
                    map.put("result", "error");
                } catch (Exception e) {
                    log.error(e.toString(), e);
                    map.put("result", "error");
                }
            }
        }
        map.put("ID", pd.getString("ID"));
        map.put("result", "success");

        return map;
    }

    /**
     * 下载文件
     *
     * @throws GenException
     */
    @RequestMapping(value = "/downloadFile")
    public ModelAndView downloadFile(HttpServletRequest request, HttpServletResponse response) throws IOException, GenException {
        ModelAndView mv = this.getModelAndView();
        PageData pd = this.getPageData();
        try {
            // 得到要下载的文件信息
            PageData fileInfo = dutyService.getFileInfoById(pd);
            String fileUrl = "";
            if (StringUtils.equals("remarkImg1", pd.getString("type"))) {
                fileUrl = fileInfo.getString("REMARK_IMG1");
            } else if (StringUtils.equals("remarkImg2", pd.getString("type"))) {
                fileUrl = fileInfo.getString("REMARK_IMG2");
            } else if (StringUtils.equals("remarkImg3", pd.getString("type"))) {
                fileUrl = fileInfo.getString("REMARK_IMG3");
            }
            String[] str = fileUrl.split("/");
            String fileName = str[str.length - 1];
            //String fileName = str[str.length-1].substring(str[str.length-1].lastIndexOf("."));

            // 找出文件
            String path = this.getRequest().getServletContext().getRealPath("/uploadFiles/HosQuestion/");
            String filePath = path + "question" + File.separator + fileName;
            File file = new File(filePath);
            if (!file.exists()) {
//			Subject subject = SecurityUtils.getSubject();
//			Session session = subject.getSession();
//			User user = (User) session.getAttribute(Const.SESSION_USER);

                User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

                pd.put("modifier", user.getUserId());
                pd.put("modifydate", new Date());
                mv.setViewName("attachment/file_404");
                return mv;
            } else {
                FileDownload.fileDownload(response, filePath, fileName);
            }
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
        return null;
    }

    /**
     * 加分管理
     */
    @RequestMapping({"/toAddMark"})
    public ModelAndView toAddMark() throws GenException {
        ModelAndView mv = getModelAndView();
        PageData pd = new PageData();


        List<PageData> memberList = this.dutyService.getMmberList();
        List<PageData> quesDictionaryList = this.dutyService.getQuestionDictionaryList();
        List<PageData> quesDictionarySmallList = this.dutyService.getQuesDictionarySmallList();

        mv.setViewName("duty/add_mark");
        mv.addObject("msg", "addDutyMark");
        mv.addObject("memberList", memberList);
        mv.addObject("quesDictionaryList", quesDictionaryList);
        mv.addObject("quesDictionarySmallList", quesDictionarySmallList);
        return mv;
    }

    @RequestMapping({"/addDutyMark"})
    @ResponseBody
    public Map<String, String> addDutyMark() {
        PageData pd = getPageData();
        Map<String, String> map = new HashMap<>();
        try {
//			Subject subject = SecurityUtils.getSubject();
//			Session session = subject.getSession();
//			User user = (User) session.getAttribute(Const.SESSION_USER);

            User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

            PageData pageData = dutyService.findMarkById(pd);
            pd.put("DUTY_OFFICER", user.getEmployeeId());
            pd.put("COMPLETEDATE", new Date());
            pd.put("CREATEDATE", new Date());
            pd.put("ID", get32UUID());
            pd.put("MARK", pageData.getString("QUESTION_MARK"));
            this.dutyService.addDutyMark(pd);
            map.put("result", "success");
        } catch (Exception e) {
            e.printStackTrace();
            map.put("result", "error");
        }
        return map;
    }

    @RequestMapping({"/findMarkById"})
    @ResponseBody
    public String findMarkById() {
        JSONObject obj = new JSONObject();
        PageData pd = getPageData();
        try {
            pd = dutyService.findMarkById(pd);
            obj.put("pd", pd);
            obj.put("result", "success");
        } catch (Exception e) {
            e.printStackTrace();
            obj.put("result", "error");
        }
        return obj.toString();
    }

    /**
     * 部门列表  树控件
     */
    @ResponseBody
    @RequestMapping(value = "/getPartmentList", produces = "application/json;charset=UTF-8")
    public Object getPartmentList() {
        Map<String, Object> map = new HashMap<String, Object>();
        PageData pd = this.getPageData();
        List<PageData> departmentList = null;
        try {
            departmentList = dutyService.getPartmentList(pd);
            map.put("departmentList", departmentList);
            map.put("result", "success");
        } catch (Exception e) {
            e.printStackTrace();
            map.put("result", "error");
        }
        return map;
    }
}
