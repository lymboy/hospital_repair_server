package com.ruoyi.hospital.controller.codenature;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.ruoyi.hospital.controller.base.BaseController;
import com.ruoyi.hospital.entity.Page;
import com.ruoyi.hospital.framework.exception.GenException;
import com.ruoyi.hospital.service.codenature.CodeNatureService;
import com.ruoyi.hospital.util.PageData;
import com.ruoyi.hospital.util.UuidUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import net.sf.json.JSONObject;

/**
 * <p>Title: CodeNatureController</p>
 * @Desc: 常量 数据 管理
 * @author: 杨建军
 *	@Date: 2017年4月16日
 * @Time: 下午12:15:41
 * @version:
 */
@Controller
@RequestMapping(value="/codeNature")
public class CodeNatureController extends BaseController {

	@Resource(name="codeNatureService")
	private CodeNatureService codeNatureService;

	/**
	 * 跳转到 常量管理
	 */
	@RequestMapping(value="/toCodeNatureManager")
	public ModelAndView toCodeNatureManager() throws GenException {
		ModelAndView mv = this.getModelAndView();
		mv.setViewName("codenature/codenature_list");
		return mv;
	}

	/**
	 * 获取 常量 分类 列表
	 * @param page
	 * @return
	 * @throws GenException
	 */
	@RequestMapping(value="/getCodeNaturesList",produces="text/html;charset=UTF-8")
	public @ResponseBody String getCodeNaturesList(Page page) throws GenException{
		PageData pd = this.getPageData();

        JSONObject obj = new JSONObject();

        List<PageData> codeNatureList = new ArrayList<PageData>();

        List<PageData> rootList = null;
        // 第一级
        rootList = codeNatureService.getAllRootCodeNaturesList(pd);
		if(rootList !=null){
			for (int i = 0; i < rootList.size(); i++) {
				PageData rootPd = rootList.get(i);
				rootPd.put("ID", rootPd.getString("TBL_ID"));
				rootPd.put("kindId", rootPd.getString("TBL_ID"));
				rootPd.put("REAL_PARENT_ID", "0");
				List<PageData> secondList = codeNatureService.getSubRootCodeNatures(rootPd);
				if(secondList!=null && !secondList.isEmpty()){
					rootPd.put("isLeaf", false);
				}else{
					rootPd.put("isLeaf", true);
				}
				rootPd.put("level", 1);
				rootPd.put("expanded", false);
				rootPd.put("loaded", true);
				rootPd.put("parent", rootPd.getString("TBL_PARENT_ID"));

				codeNatureList.add(rootPd);
				// 第二级
				for (PageData secondPd : secondList) {
					secondPd.put("ID", secondPd.getString("TYPE_ID"));
					secondPd.put("typeId", secondPd.getString("TYPE_ID"));
					secondPd.put("TBL_ID", UuidUtil.get36UUID());
					secondPd.put("TBL_PARENT_ID", rootPd.getString("TBL_ID"));
					secondPd.put("VIEW_ONLY", rootPd.getString("VIEW_ONLY"));
					secondPd.put("REAL_PARENT_ID", secondPd.getString("KIND_ID"));
					List<PageData> thirdList = codeNatureService.getSubCodeNatures(secondPd);
					if(thirdList!=null && !thirdList.isEmpty()){
						secondPd.put("isLeaf", false);
					}else{
						secondPd.put("isLeaf", true);
					}
					secondPd.put("level", 2);
					secondPd.put("expanded", false);
					secondPd.put("loaded", true);
					secondPd.put("parent", secondPd.getString("TBL_PARENT_ID"));

					codeNatureList.add(secondPd);

					// 第三级
					for (PageData thirdPd : thirdList) {
						thirdPd.put("ID", thirdPd.getString("TYPE_ID"));
						thirdPd.put("TBL_ID", UuidUtil.get36UUID());
						thirdPd.put("TBL_PARENT_ID", secondPd.getString("TBL_ID"));
						thirdPd.put("isLeaf", true);
						thirdPd.put("level", 3);
						thirdPd.put("expanded", false);
						thirdPd.put("loaded", true);
						thirdPd.put("parent", thirdPd.getString("TBL_PARENT_ID"));
						thirdPd.put("VIEW_ONLY", secondPd.getString("VIEW_ONLY"));
						thirdPd.put("REAL_PARENT_ID", thirdPd.getString("TYPE_BELONGTO"));
						codeNatureList.add(thirdPd);
					}

				}
			}
		}
		obj.put("rows", codeNatureList);
		return obj.toString();
	}

	/**
	 * 新增 常量
	 * @return
	 * @throws GenException
	 */
	@ResponseBody
	@RequestMapping(value="/addSubCodeNature", produces="application/json;charset=UTF-8")
	public Map<String, Object> addSubCodeNature() throws GenException {
		Map<String, Object> map = new HashMap<String, Object>();
		PageData pd = this.getPageData();
		try {
			pd.put("typeId", get36UUID());
			String level = pd.getString("level");
			if(level!=null && !"".equals(level)){
				if("2".equals(level)){
					pd.put("kindId", pd.getString("realParentId"));
				}else{
					pd.put("typeBelongTo", pd.getString("realParentId"));
				}
				codeNatureService.addSubCodeNature(pd);
			}
			map.put("result", "success");
		} catch (GenException e) {
			throw new GenException(e.getCause());
		}
		return map;
	}

	/**
	 * 更新 常量
	 * @return
	 * @throws GenException
	 */
	@ResponseBody
	@RequestMapping(value="/updateCodeNature", produces="application/json;charset=UTF-8")
	public Map<String, Object> updateSubCodeNature() throws GenException {
		Map<String, Object> map = new HashMap<String, Object>();
		PageData pd = this.getPageData();
		try {
			String level = pd.getString("level");
			if(level!=null && !"".equals(level)){
				if("1".equals(level)){
					pd.put("KIND_NAME", pd.getString("TYPE_NAME"));
					pd.put("KIND_SERIAL_NO", pd.getString("TYPE_SERIAL_NO"));
					codeNatureService.updateRootCodeNature(pd);
				}else{
					codeNatureService.updateSubCodeNature(pd);
				}
			}
			map.put("result", "success");
		} catch (GenException e) {
			throw new GenException(e.getCause());
		}
		return map;
	}

	/**
	 * 批量 删除 常量
	 * @return
	 * @throws GenException
	 */
	@ResponseBody
	@RequestMapping(value="/deleteSubCodeNature", produces="application/json;charset=UTF-8")
	public Map<String, Object> deleteSubCodeNature() throws GenException {
		Map<String, Object> map = new HashMap<String, Object>();
		PageData pd = this.getPageData();
		try {
			codeNatureService.deleteSubCodeNature(pd);
			map.put("result", "success");
		} catch (GenException e) {
			throw new GenException(e.getCause());
		}
		return map;
	}

	/**
	 * 更新 常量 设置默认
	 * @return
	 * @throws GenException
	 */
	@ResponseBody
	@RequestMapping(value="/setDefaultChecked", produces="application/json;charset=UTF-8")
	public Map<String, Object> setDefaultChecked() throws GenException {
		Map<String, Object> map = new HashMap<String, Object>();
		PageData pd = this.getPageData();
		try {
			codeNatureService.setDefaultChecked(pd);
			map.put("result", "success");
		} catch (GenException e) {
			throw new GenException(e.getCause());
		}
		return map;
	}







	/**
	 * 转到分类
	 * @return
	 */
	@RequestMapping(value="/toAddRootCodeNature")
	public ModelAndView toAddRootCodeNature(){
		ModelAndView mv = this.getModelAndView();
		mv.setViewName("codenature/code_nature_edit");
		return mv;
	}

	/**
	 * 新增分类
	 * @return
	 * @throws GenException
	 */
	@ResponseBody
	@RequestMapping(value="/addRootCodeNature", produces="application/json;charset=UTF-8")
	public Map<String, Object> addRootCodeNature() throws GenException {
		Map<String, Object> map = new HashMap<String, Object>();
		PageData pd = this.getPageData();
		try {
			String kindId = this.get36UUID();
			pd.put("kindId", kindId);
			pd.put("typeSerialNo", codeNatureService.getRootCodeNatureNum(pd)+1);
			codeNatureService.addRootCodeNature(pd);
			map.put("result", "success");
		} catch (GenException e) {
			throw new GenException(e.getCause());
		}
		return map;
	}

	/**
	 * 转到 常量
	 * @return
	 */
	@RequestMapping(value="/toAddSubCodeNature")
	public ModelAndView toAddSubCodeNature(){
		ModelAndView mv = this.getModelAndView();
		mv.setViewName("codenature/code_nature_edit");
		return mv;
	}


	/**
	 * 删除分类
	 * @return
	 * @throws GenException
	 */
	@ResponseBody
	@RequestMapping(value="/deleteRootCodeNature", produces="application/json;charset=UTF-8")
	public Map<String, Object> deleteRootCodeNature() throws GenException {
		Map<String, Object> map = new HashMap<String, Object>();
		PageData pd = this.getPageData();
		try {
			codeNatureService.deleteRootCodeNature(pd);
			map.put("result", "success");
		} catch (GenException e) {
			throw new GenException(e.getCause());
		}
		return map;
	}

	/**
	 * 更新 分类
	 * @return
	 * @throws GenException
	 */
	@ResponseBody
	@RequestMapping(value="/updateRootCodeNature", produces="application/json;charset=UTF-8")
	public Map<String, Object> updateRootCodeNature() throws GenException {
		Map<String, Object> map = new HashMap<String, Object>();
		PageData pd = this.getPageData();
		try {
			codeNatureService.updateRootCodeNature(pd);
			map.put("result", "success");
		} catch (GenException e) {
			throw new GenException(e.getCause());
		}
		return map;
	}



}
