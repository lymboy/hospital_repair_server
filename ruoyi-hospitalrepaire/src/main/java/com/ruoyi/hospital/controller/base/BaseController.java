package com.ruoyi.hospital.controller.base;


import javax.servlet.http.HttpServletRequest;

import com.ruoyi.hospital.entity.Page;
import com.ruoyi.hospital.util.PageData;
import com.ruoyi.hospital.util.UuidUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;

@Slf4j
public class BaseController {

	/**
	 * 得到PageData
	 */
	public PageData getPageData(){
		return new PageData(this.getRequest());
	}

	/**
	 * 得到ModelAndView
	 */
	public ModelAndView getModelAndView(){
		return new ModelAndView();
	}
	/**
	 * 得到ModelAndView
	 */
	public ModelAndView getModelAndView(String view){
		return new ModelAndView(view);
	}

	/**
	 * 得到request对象
	 */
	public HttpServletRequest getRequest() {
		HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();

		return request;
	}

	/**
	 * 得到32位的uuid
	 * @return
	 */
	public String get32UUID(){

		return UuidUtil.get32UUID();
	}
	/**
	 * 得到36位的uuid
	 * @return
	 */
	public String get36UUID(){

		return UuidUtil.get36UUID();
	}

	/**
	 * 得到分页列表的信息
	 */
	public Page getPage(){

		return new Page();
	}

	/**
	 * 得到分页列表的信息
	 */
	public Page getPage(PageData pd, int currentPage, int showCount){
		Page page = new Page();
		page.setPd(pd);
		page.setCurrentPage(currentPage);
		page.setShowCount(showCount);
		return page;
	}

	public static void logBefore(String interfaceName){
		log.info("");
		log.info("start");
		log.info(interfaceName);
	}

	public static void logAfter(){
		log.info("end");
		log.info("");
	}

}
