package com.ruoyi.hospital.controller.information.transfer;

import com.ruoyi.hospital.controller.base.BaseController;
import com.ruoyi.hospital.entity.Page;
import com.ruoyi.hospital.entity.system.User;
import com.ruoyi.hospital.framework.exception.GenException;
import com.ruoyi.hospital.service.information.TransferService;
import com.ruoyi.hospital.util.PageData;
import net.sf.json.JSONObject;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>Title: TransferController</p>
 *
 * @Desc: 员工调动
 * @author: 杨建军
 * @Date: 2017年3月25日
 * @Time: 下午1:48:57
 * @version:
 */
@Controller
@RequestMapping(value = "/transfer")
public class TransferController extends BaseController {

    @Resource(name = "transferService")
    private TransferService transferService;

    /**
     * 转到 员工调动页面
     *
     * @return
     */
    @RequestMapping(value = "/toTransfer")
    public ModelAndView toTransfer() {
        ModelAndView mv = this.getModelAndView();
        mv.setViewName("information/transfer/transfer_list");
        return mv;
    }

    /**
     * 员工调动
     *
     * @return
     * @throws GenException
     */
    @ResponseBody
    @RequestMapping(value = "/transferEmployee", produces = "application/json;charset=UTF-8")
    public Map<String, Object> transferEmployee() throws GenException {
        Map<String, Object> map = new HashMap<String, Object>();
        PageData pd = this.getPageData();
        try {
//			Subject subject = SecurityUtils.getSubject();
//			Session session = subject.getSession();
//			User user = (User) session.getAttribute(Const.SESSION_USER);

            User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

            pd.put("creater", user.getEmployeeId());
            pd.put("createdate", new Date());
            pd.put("modifier", user.getEmployeeId());
            pd.put("modifydate", new Date());
            pd.put("personDetailId", get36UUID());
            pd.put("tenureDate", new Date());// 履历 当前 任职时间
            pd.put("leaveDate", new Date());// 履历 上一个 离职时间

            transferService.transferEmployeeDep(pd);
        } catch (GenException e) {
            throw new GenException(e.getCause());
        }
        map.put("result", "success");
        return map;
    }

    /**
     * 分页得到员工列表
     *
     * @throws GenException
     */
    @RequestMapping(value = "/getEmployeeList", produces = "application/json;charset=UTF-8")
    @ResponseBody
    public String getEmployeeList(Page page) throws GenException {
        //{nd=1489235959200, departmentId=1, page=1, sord=asc, rows=10, _search=false, sidx=}
        JSONObject obj = new JSONObject();
        PageData pd = this.getPageData();
        int start = Integer.valueOf(pd.getString("page"));
        int length = Integer.valueOf(pd.getString("rows"));
        page.setShowCount(length);
        page.setPd(pd);
        page.setCurrentPage(start);
        List<PageData> employeeList = null;
        try {
            employeeList = transferService.getEmployeeListPage(page);
            obj.put("records", page.getTotalResult());//总记录数
            obj.put("page", page.getCurrentPage());//当前页
            obj.put("total", page.getTotalPage());//总页数
            obj.put("rows", employeeList);
        } catch (GenException e) {
            throw new GenException(e.getCause());
        }
        return obj.toString();
    }

}
