package com.ruoyi.hospital.controller.dictionary;

import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.hospital.controller.base.BaseController;
import com.ruoyi.hospital.entity.Page;
import com.ruoyi.hospital.entity.system.User;
import com.ruoyi.hospital.framework.exception.GenException;
import com.ruoyi.hospital.service.dictionary.DictionaryService;
import com.ruoyi.hospital.service.duty.DutyService;
import com.ruoyi.hospital.util.PageData;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONObject;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Slf4j
@Controller
@RequestMapping({"/dictionary"})
public class DictionaryController
        extends BaseController {
    @Resource(name = "dictionaryService")
    private DictionaryService dictionaryService;
    @Resource(name = "dutyService")
    private DutyService dutyService;

    @RequestMapping({"/listGroup"})
    public ModelAndView listGroup() throws GenException {
        ModelAndView mv = getModelAndView();
        mv.setViewName("dictionary/Group_list");
        return mv;
    }

    @ResponseBody
    @RequestMapping({"/getGroupList"})
    public String getGroupList(Page page) throws GenException {
        JSONObject obj = new JSONObject();

//		Subject subject = SecurityUtils.getSubject();
//		Session session = subject.getSession();
//		User user = (User) session.getAttribute(Const.SESSION_USER);

        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        PageData pd = getPageData();
        int start = Integer.valueOf(pd.getString("page")).intValue();
        int length = Integer.valueOf(pd.getString("rows")).intValue();
        page.setShowCount(length);

        List<String> list = null;
        page.setPd(pd);
        page.setCurrentPage(start);
        List<PageData> dutyList = null;
        try {
            dutyList = this.dictionaryService.getGroupListPage(page);
            obj.put("records", Integer.valueOf(page.getTotalResult()));
            obj.put("page", Integer.valueOf(page.getCurrentPage()));
            obj.put("total", Integer.valueOf(page.getTotalPage()));
            obj.put("rows", dutyList);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
        return obj.toString();
    }

    @RequestMapping({"/toAddGroup"})
    public ModelAndView goEditU() throws GenException {
        ModelAndView mv = getModelAndView();
        PageData pd = new PageData();
        pd = getPageData();
        pd = this.dictionaryService.getGroupQuestionById(pd);

        mv.setViewName("dictionary/edit_Group");
        mv.addObject("msg", "addGroupQuestion");
        mv.addObject("pd", pd);
        return mv;
    }

    @RequestMapping({"/addGroupQuestion"})
    @ResponseBody
    public Map<String, String> addGroupQuestion() {
        PageData pd = getPageData();
        Map<String, String> map = new HashMap<>();
        try {
            String id = pd.getString("ID");
            if (StringUtils.isNotBlank(id)) {
                this.dictionaryService.editOwnerInformation(pd);
            } else {
                pd.put("ID", get32UUID());
                this.dictionaryService.addOwnerInformation(pd);
            }
            map.put("result", "success");
        } catch (Exception e) {
            e.printStackTrace();
            map.put("result", "error");
        }
        return map;
    }

    @RequestMapping({"/deleteGroup"})
    @ResponseBody
    public Object deleteGroup() throws GenException {
        PageData pd = new PageData();
        Map<String, Object> map = new HashMap<>();
        try {
            pd = getPageData();
            this.dictionaryService.deleteGroup(pd);
            map.put("result", "success");
        } catch (Exception e) {
            log.error(e.toString(), e);
            map.put("result", "error");
        }
        return map;
    }

    @RequestMapping({"/listQuestion"})
    public ModelAndView listQuestion() throws GenException {
        ModelAndView mv = getModelAndView();
        mv.setViewName("dictionary/Question_list");
        return mv;
    }

    @ResponseBody
    @RequestMapping(value = {"/getQuestionDictionaryById"}, produces = {"application/json;charset=UTF-8"})
    public Object getDepartmentInformation() {
        Map<String, Object> map = new HashMap<>();
        PageData pd = getPageData();
        PageData QuestionDictionary = null;
        try {
            QuestionDictionary = this.dictionaryService.getQuestionDictionaryById(pd);
            map.put("QuestionDictionary", QuestionDictionary);
            map.put("result", "success");
        } catch (Exception e) {
            e.printStackTrace();
            map.put("result", "error");
        }
        return map;
    }

    @ResponseBody
    @RequestMapping(value = {"/getQuestionDictionaryList"}, produces = {"application/json;charset=UTF-8"})
    public Object getPartmentList() {
        Map<String, Object> map = new HashMap<>();
        PageData pd = getPageData();
        List<PageData> questionTypeList = null;
        try {
            questionTypeList = this.dictionaryService.getQuestionDictionaryList(pd);
            map.put("questionTypeList", questionTypeList);
            map.put("result", "success");
        } catch (Exception e) {
            e.printStackTrace();
            map.put("result", "error");
        }
        return map;
    }

    @RequestMapping({"/toAddLowerLevel"})
    public ModelAndView addLowerLevelDepartment() throws GenException {
        ModelAndView mv = getModelAndView();
        PageData pd = getPageData();
        try {
            mv.addObject("parentId", pd.getString("parentId"));
            mv.setViewName("dictionary/question_addLower");
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
        return mv;
    }

    @RequestMapping(value = {"/addLowerLevelQuestionType"}, produces = {"application/json;charset=UTF-8"})
    @ResponseBody
    public Object addLowerLevelDpartment() {
        Map<String, Object> map = new HashMap<>();
        PageData pd = getPageData();
        PageData pda = new PageData();
        try {
            pd.put("ID", get36UUID());
            this.dictionaryService.addLowerLevelQuestionType(pd);
            map.put("result", "success");
        } catch (Exception e) {
            e.printStackTrace();
            map.put("result", "error");
        }
        return map;
    }

    @RequestMapping(value = {"deleteQuestionType"}, produces = {"application/json;charset=UTF-8"})
    @ResponseBody
    public Object deleteDepartment() {
        Map<String, Object> map = new HashMap<>();
        PageData pd = getPageData();
        try {
            String departmentId = String.valueOf(pd.get("departmentId"));
            this.dictionaryService.deleteQuestionType(pd);
            this.dictionaryService.deleteChildQuestionType(pd);
            map.put("result", "success");
        } catch (GenException e) {
            e.printStackTrace();
            map.put("result", "error");
        }
        return map;
    }

    @RequestMapping({"/saveQuestionType"})
    @ResponseBody
    public Object saveQuestionType() {
        Map<String, Object> map = new HashMap<>();
        PageData pd = getPageData();
        try {
            this.dictionaryService.saveQuestionType(pd);
            map.put("result", "success");
        } catch (Exception e) {
            e.printStackTrace();
            map.put("result", "error");
        }
        return map;
    }

    GregorianCalendar currentDay = new GregorianCalendar();
    int year = currentDay.get(Calendar.YEAR);
    int month = currentDay.get(Calendar.MONTH);// 月份从0开始的，所以要加1
    int today = currentDay.get(Calendar.DAY_OF_MONTH);
    int weekday = currentDay.get(Calendar.DAY_OF_WEEK); // 星期是从周日开始的，所以要减一
    int hour = currentDay.get(Calendar.HOUR_OF_DAY); // 获取当前是几点
    int minute = currentDay.get(Calendar.MINUTE);
    String hourMinute = hour + ":" + minute;
    String strDay = year + "0" + (month + 1) + "" + today;

    /**
     * 值班人员管理
     */

    @RequestMapping({"/personnelDuty"})
    public ModelAndView personnelDuty(HttpServletRequest request) throws GenException {
        ModelAndView mv = getModelAndView();
        PageData pd = new PageData();
        GregorianCalendar currentDay = new GregorianCalendar();
        int today = currentDay.get(Calendar.DAY_OF_MONTH); //这个月中的第几天
        pd.put("num", strDay);
        PageData pd1 = dictionaryService.findByNum(pd);
        List<PageData> memberList = this.dutyService.getMmberList();
        mv.addObject("pd", pd1);
        request.setAttribute("memberList", memberList);
        mv.setViewName("dictionary/duty_personnel");
        return mv;
    }

    @ResponseBody
    @RequestMapping({"/getMemberList"})
    public String getMemberList() throws GenException {
        PageData pd = getPageData();
        JSONObject obj = new JSONObject();
        List<PageData> dutyList = null;
        try {
            dutyList = dictionaryService.selectHosDuty(pd);
            obj.put("rows", dutyList);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
        return obj.toString();
    }


    @RequestMapping({"/updateHosDuty"})
    @ResponseBody
    public Object updateHosDuty() {
        Map<String, Object> map = new HashMap<>();
        PageData pd = getPageData();
        try {
            int i = (int) this.dictionaryService.updateHosDuty(pd);
            if (i > 0) {
                map.put("result", "success");
            } else {
                map.put("result", "error");
            }
        } catch (Exception e) {
            e.printStackTrace();
            map.put("result", "error");
        }
        return map;
    }

    @RequestMapping({"/addHosDuty"})
    @ResponseBody
    public Object addHosDuty() {
        Map<String, Object> map = new HashMap<>();
        PageData pd = getPageData();
        pd.put("ID", get36UUID());
        pd.put("DUTY_DATE", pd.getString("DUTY_DATE").trim());
        try {
            int i = (int) this.dictionaryService.addHosDuty(pd);
            if (i > 0) {
                map.put("result", "success");
            } else {
                map.put("result", "error");
            }
        } catch (Exception e) {
            e.printStackTrace();
            map.put("result", "error");
        }
        return map;
    }

    @RequestMapping({"/findByNum"})
    @ResponseBody
    public String findByNum() {
        JSONObject obj = new JSONObject();
        PageData pd = getPageData();
        List<PageData> implementList = new ArrayList<PageData>();
        try {
            PageData pd1 = dictionaryService.findByNum(pd);
            List<PageData> memberList = this.dutyService.getMmberList();
            obj.put("pd", pd1);
            obj.put("memberList", memberList);
            obj.put("result", "success");
        } catch (Exception e) {
            e.printStackTrace();
            obj.put("result", "error");
        }
        return obj.toString();
    }
}
