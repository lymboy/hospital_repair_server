package com.ruoyi.hospital.controller.documentcatalog.catalog;

import com.ruoyi.hospital.controller.base.BaseController;
import com.ruoyi.hospital.entity.system.User;
import com.ruoyi.hospital.framework.exception.GenException;
import com.ruoyi.hospital.service.documentcatalog.catalog.CatalogService;
import com.ruoyi.hospital.util.PageData;
import com.ruoyi.hospital.util.sequence;
import net.sf.json.JSONObject;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>Title: CatalogController</p>
 * @Desc: 资料目录管理
 * @author: 杨建军
 *	@Date: 2017年4月21日
 * @Time: 上午10:20:34
 * @version:
 */
@Controller
@RequestMapping(value="/catalog")
public class CatalogController extends BaseController {

	@Resource(name="catalogService")
	private CatalogService catalogService;


	/**
	 * 转到 资料目录管理
	 * @return
	 */
	@RequestMapping(value="/toCatalogManager")
	public ModelAndView toCatalogManager(){
		ModelAndView mv = this.getModelAndView();
		mv.setViewName("documentcatalog/catalog/catalog_list");
		return mv;
	}

	/**
	 * 获取资料目录信息
	 */
	@RequestMapping(value="/getCatalog", produces="text/html;charset=UTF-8")
	public @ResponseBody String getCatalogList() throws GenException {
		PageData pd = this.getPageData();

		List<PageData> catalogList;
		JSONObject obj = new JSONObject();
		try {
			catalogList = catalogService.getCatalogList(pd);
			if(catalogList !=null){
				for (int i = 0; i < catalogList.size(); i++) {
					PageData catalog = catalogList.get(i);
					String baseId = catalog.getString("BASE_ID");
					catalog.put("isLeaf", true);
					if(i<catalogList.size()-1){
						PageData nextPost = catalogList.get(i+1);
						String nextBaseId = nextPost.getString("BASE_ID");
						if(nextBaseId!=null && nextBaseId.length()>baseId.length()){
							catalog.put("isLeaf", false);
						}
					}
					int level =baseId.split("-").length; // 层级关系
					catalog.put("level", level);
					catalog.put("expanded", false);
					catalog.put("loaded", true);
					catalog.put("parent", catalog.getString("PARENT_CATALOG_ID"));

				}
			}
			obj.put("rows", catalogList);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return obj.toString();
	}

	/**
	 * 转到 资料目录管理添加
	 * @return
	 */
	@RequestMapping(value="/toAddCatalog")
	public ModelAndView toAddCatalog(){
		ModelAndView mv = this.getModelAndView();
		mv.setViewName("documentcatalog/catalog/catalog_edit");
		return mv;
	}
	/**
	 * 质量资料目录新增  参数  Add_Type 0为同级，1为下级
	 * @return
	 * @throws GenException
	 */
	@ResponseBody
	@RequestMapping(value="/addCatalog", produces="application/json;charset=UTF-8")
	public Map<String, Object> addCatalog() throws GenException {
		Map<String, Object> map = new HashMap<String, Object>();
		PageData pd = this.getPageData();
		try {

			String addType = pd.getString("Add_Type");
			if(addType!=null && addType.equals("0")){//新增同级
				String parentId = pd.getString("parentId");
				int baseNum = catalogService.getCountByParentId(parentId)+1;
				String bsId = pd.getString("bsId");
				String baseNumStr = String.valueOf(baseNum);
				if(baseNum==1){// 新增的第一条数据
					bsId="0000";
					parentId="0";
				}
				String baseId = bsId.substring(0, bsId.length()-baseNumStr.length())+baseNumStr;
				pd.put("baseId", baseId);
				pd.put("parentCatalogId", parentId);
			}else if(addType!=null && addType.equals("1")){// 下级新增

				String currentId = pd.getString("currentId");
				int baseNum = catalogService.getCountByParentId(currentId)+1;
				String bsId = pd.getString("bsId");
				String baseNumStr = "0000"+String.valueOf(baseNum);

				String baseId = bsId+"-"+baseNumStr.substring(baseNumStr.length()-4);
				pd.put("baseId", baseId);
				pd.put("parentCatalogId", currentId);
			}
			pd.put("catalogId", get36UUID());
//		Subject subject = SecurityUtils.getSubject();
//		Session session = subject.getSession();
//		User user = (User) session.getAttribute(Const.SESSION_USER);

			User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

			pd.put("creater", user.getEmployeeId());
			pd.put("createdate", new Date());
			pd.put("modifier", user.getEmployeeId());
			pd.put("modifydate", new Date());
			pd.put("remark", pd.getString("remark"));
			catalogService.addCatalog(pd);
			//增加catalog权限
			pd.put("privilegeId", sequence.nextId());
			pd.put("type", 0);
			catalogService.addCatalogPrivilege(pd);
			map.put("result", "success");
		} catch (GenException e) {
			map.put("result", "error");
			throw new GenException(e.getCause());
		}
		return map;
	}

	/**
	 * 质量资料目录批量删除
	 * @return
	 * @throws GenException
	 */
	@ResponseBody
	@RequestMapping(value="/deleteCatalog", produces="application/json;charset=UTF-8")
	public Map<String, Object> deleteCatalog() throws GenException {
		Map<String, Object> map = new HashMap<String, Object>();
		PageData pd = this.getPageData();
		try {

			String catalogId = pd.getString("deleteIdsArrayStr");
			if(!"".equals(catalogId) && catalogId!=null  ){
				String[] ids=catalogId.split(",");
				for(String baseid : ids){
					pd.put("catalogId", baseid);
//		Subject subject = SecurityUtils.getSubject();
//		Session session = subject.getSession();
//		User user = (User) session.getAttribute(Const.SESSION_USER);

					User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

					pd.put("modifier", user.getEmployeeId());
					pd.put("modifydate", new Date());
					catalogService.deleteCatalog(pd);
				}
			}
				map.put("result", "success");
		} catch (GenException e) {
			map.put("result", "error");
			throw new GenException(e.getCause());
		}
		return map;
	}


	/**
	 * 更新修改的资料目录信息
	 */
	@ResponseBody
	@RequestMapping(value="/updateCatalog", produces="application/json;charset=UTF-8")
	public Map<String, Object> updateCatalogByCatalogId() throws GenException {
		 Map<String, Object> map = new HashMap<String, Object>();
		 PageData pd = this.getPageData();
//		Subject subject = SecurityUtils.getSubject();
//		Session session = subject.getSession();
//		User user = (User) session.getAttribute(Const.SESSION_USER);

		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		pd.put("modifier", user.getEmployeeId());
		 pd.put("modifydate", new Date());
		if(pd.getString("privilegeStatus").trim()!=null && !"".equals(pd.getString("privilegeStatus").trim())){
			 pd.put("privilegeStatus", Integer.valueOf(pd.getString("privilegeStatus")));
		 }
		 if(pd.getString("publishStatus")!=null && !"".equals(pd.getString("publishStatus"))){
			 pd.put("publishStatus", Integer.valueOf(pd.getString("publishStatus")));
		 }
		 try {
			catalogService.updateCatalogByCatalogId(pd);
			map.put("result", "success");
		} catch (GenException e) {
			throw new GenException(e.getCause());
		}
		 return map;
	}


}
