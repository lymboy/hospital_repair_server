package com.ruoyi.hospital.controller.repair;

import com.ruoyi.hospital.controller.base.BaseController;
import com.ruoyi.hospital.entity.Page;
import com.ruoyi.hospital.framework.exception.GenException;
import com.ruoyi.hospital.service.duty.DutyService;
import com.ruoyi.hospital.service.repair.RepairService;
import com.ruoyi.hospital.util.PageData;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.*;

@Slf4j
@Controller
@RequestMapping({"/repair"})
public class RepairController extends BaseController {
    @Resource(name = "dutyService")
    private DutyService dutyService;
    @Resource(name = "repairService")
    private RepairService repairService;

    @RequestMapping({"{name}"})
    public ModelAndView getHTML(@PathVariable String name) {
        ModelAndView mv = getModelAndView();
        PageData pd = getPageData();
        String ID = pd.getString("ID");
        String openid = pd.getString("openid");
        String nickname = pd.getString("nickname");
        mv.setViewName(name);
//        mv.addObject("ID", JSON.toJSONString(ID));
//        mv.addObject("openid", JSON.toJSONString(openid));
//        mv.addObject("nickname", JSON.toJSONString(nickname));
        return mv;
    }

    @RequestMapping({"/listRepair"})
    public ModelAndView listEmployee() throws GenException {
        ModelAndView mv = getModelAndView();
        List<PageData> memberList = this.dutyService.getMmberList();
        List<PageData> quesDictionaryList = this.dutyService.getQuesDictionarySmallList();
        List<PageData> groupDictionaryList = this.dutyService.getGroupDictionaryList();
        mv.setViewName("repair/repair_list");
        mv.addObject("memberList", memberList);
        mv.addObject("quesDictionaryList", quesDictionaryList);
        mv.addObject("groupDictionaryList", groupDictionaryList);
        return mv;
    }

    @RequestMapping(value = {"/getRepairlist"}, produces = {"application/json;charset=UTF-8"})
    @ResponseBody
    public String getRepairlistPage(Page page) throws GenException {
        JSONObject obj = new JSONObject();

//        Subject currentUser = SecurityUtils.getSubject();
//        Session session = currentUser.getSession();
//        User user = (User) session.getAttribute(Const.SESSION_USER);

        PageData pd = getPageData();
        int start = Integer.valueOf(pd.getString("page")).intValue();
        int length = Integer.valueOf(pd.getString("rows")).intValue();
        page.setShowCount(length);

        List<String> list = null;
        page.setPd(pd);
        page.setCurrentPage(start);
        List<PageData> dutyList = null;
        try {
            dutyList = this.repairService.getRepairClientlistPage(page);
            obj.put("records", Integer.valueOf(page.getTotalResult()));
            obj.put("page", Integer.valueOf(page.getCurrentPage()));
            obj.put("total", Integer.valueOf(page.getTotalPage()));
            obj.put("rows", dutyList);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
        return obj.toString();
    }

    @RequestMapping({"/toEditRepair"})
    public ModelAndView goEditU() throws Exception {
        ModelAndView mv = getModelAndView();
        PageData pd = new PageData();
        pd = getPageData();

        pd = this.repairService.getRepairQuestionById(pd);
        List<PageData> implementList = dutyService.getMmberList();
        mv.setViewName("repair/edit_repairQuestion");
        mv.addObject("msg", "addRepairQuestion");
        mv.addObject("pd", pd);
        mv.addObject("implementList", implementList);
        return mv;
    }

    @RequestMapping({"/addRepairQuestion"})
    @ResponseBody
    public Map<String, String> editEmployee() {
        PageData pd = getPageData();
        Map<String, String> map = new HashMap<>();
        try {
//            Subject currentUser = SecurityUtils.getSubject();
//            Session session = currentUser.getSession();
//            User user = (User) session.getAttribute(Const.SESSION_USER);
            String id = pd.getString("ID");
            if (StringUtils.isNotBlank(id)) {
                pd.put("MODIFYDATE", new Date());
                if (StringUtils.equals("2", pd.getString("STATUS_ID"))) {
                    pd.put("COMPLETEDATE", new Date());
                }
                this.repairService.editRepairQuestion(pd);
            }
            map.put("result", "success");
        } catch (Exception e) {
            e.printStackTrace();
            map.put("result", "error");
        }
        return map;
    }

    @RequestMapping({"/deleteRepair"})
    @ResponseBody
    public Object deleteRepair() throws GenException {
        PageData pd = new PageData();
        Map<String, Object> map = new HashMap<>();
        try {
            pd = getPageData();
            this.dutyService.deleteDuty(pd);
            map.put("result", "success");
        } catch (Exception e) {
            log.error(e.toString(), e);
            map.put("result", "error");
        }
        return map;
    }

    /**
     * 导出员工excel
     *
     * @throws GenException
     */
    @RequestMapping(value = "/exportExcelQuestion")
    public ModelAndView exportExcelEmployee() throws GenException {
        ModelAndView mv = this.getModelAndView();
        PageData pd = this.getPageData();
        Map<String, Object> map = new HashMap<String, Object>();
        List<String> titles = new ArrayList<String>();
        String professionValue = "";
        titles.add("报修日期");//2
        titles.add("分类");//3
        titles.add("问题分类");//4
        titles.add("报修人");//5
        titles.add("值班人员");//6
        titles.add("问题科室");//7
        titles.add("组别");//8
        titles.add("执行人员");//9
        titles.add("转让人员");//10
        titles.add("辅助");//11
        titles.add("问题描述");//12
        titles.add("完成情况");//13
        map.put("titles", titles);
        try {
            List<PageData> varOList = this.repairService.getRepairlist(pd);
            List<PageData> varList = new ArrayList<PageData>();
            for (int i = 0; i < varOList.size(); i++) {
                PageData vpd = new PageData();
                vpd.put("var1", varOList.get(i).get("CREATEDATE") == null ? "" : varOList.get(i).get("CREATEDATE").toString());//1
                vpd.put("var2", varOList.get(i).get("QUESTION_TYPE") == null ? "" : varOList.get(i).get("QUESTION_TYPE").toString());//2
                vpd.put("var3", varOList.get(i).get("QUESTION_TYPE_SMALL") == null ? "" : varOList.get(i).get("QUESTION_TYPE_SMALL").toString());//2
                vpd.put("var4", varOList.get(i).get("CONDOM_USER_NAME") == null ? "" : varOList.get(i).get("CONDOM_USER_NAME").toString());//3
                vpd.put("var5", varOList.get(i).get("DUTY_OFFICER_NAME") == null ? "" : varOList.get(i).get("DUTY_OFFICER_NAME").toString());//4
                vpd.put("var6", varOList.get(i).get("DEPARTMENT_NAME") == null ? "" : varOList.get(i).get("DEPARTMENT_NAME").toString());//5
                vpd.put("var7", varOList.get(i).get("GROUP_DICTIONARY") == null ? "" : varOList.get(i).get("GROUP_DICTIONARY").toString());//6
                vpd.put("var8", varOList.get(i).get("USER_NAME") == null ? "" : varOList.get(i).get("USER_NAME").toString());//7
                vpd.put("var9", (varOList.get(i).get("ASSIGNOR_NAME") == null ? "" : varOList.get(i).get("ASSIGNOR_NAME").toString()));//8
                vpd.put("var10", (varOList.get(i).get("AUXILIARY") == null ? "" : varOList.get(i).get("AUXILIARY").toString()));//9
                vpd.put("var11", (varOList.get(i).get("QUESTION_REMARK") == null ? "" : varOList.get(i).get("QUESTION_REMARK").toString()));//9
                vpd.put("var12", (varOList.get(i).get("COMPLETION_STATUS") == null ? "" : varOList.get(i).get("COMPLETION_STATUS").toString()));//9
                varList.add(vpd);
            }
            map.put("varList", varList);
//            ObjectExcelView erv = new ObjectExcelView();
//            mv = new ModelAndView(erv, map);
        } catch (Exception e) {

        }
        return mv;
    }
}
