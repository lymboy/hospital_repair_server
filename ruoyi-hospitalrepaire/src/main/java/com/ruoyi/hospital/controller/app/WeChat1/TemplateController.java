package com.ruoyi.hospital.controller.app.WeChat1;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/template")
public class TemplateController {

	@Autowired
	private Template template;


	/**
	 * @param openid
	 * @param request
	 * @return
	 */
	@RequestMapping("/simp")
	public @ResponseBody TTResult testSimp(String openid,HttpServletRequest request){
		Token token = new Token();
		try {
			return template.send_simp_message(token.getAPPID(), token.getAPPSECRET(), openid);
			//openid  你想发送给你公众号上的人    这个openid获取方法下次写，到这一步的话openid应该早就获取过了吧。
		} catch (Exception e) {
			e.printStackTrace();
		}
		return TTResult.fail();
	}


	/**
	 * @param openid
	 * @param request
	 * @return
	 */
	@RequestMapping("/adopt")
	public @ResponseBody TTResult test1(String openid,HttpServletRequest request){

		try {
			return template.send_template_message("你的APPID", "APPID对应的秘钥", openid);
			//openid  你想发送给你公众号上的人    这个openid获取方法下次写，到这一步的话openid应该早就获取过了吧。
		} catch (Exception e) {
			// TODO: handle exception
		}
		return TTResult.fail();
	}
}
