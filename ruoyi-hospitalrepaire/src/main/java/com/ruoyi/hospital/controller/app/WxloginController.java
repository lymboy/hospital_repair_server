package com.ruoyi.hospital.controller.app;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ruoyi.hospital.controller.app.WeChat1.Token;
import org.apache.commons.httpclient.util.HttpURLConnection;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

//import com.gexin.fastjson.JSON;
//import com.gexin.fastjson.JSONObject;


@RestController
@RequestMapping("/wxAuth")
public class WxloginController {

//	private  static  String APPID="wxaf3b50a89593bc0d";//填你自己的
//    private  static  String APPSECRET="4773cb80cfe560eea6e1f0100e0e1a49";//填你自己的
	//@Autowired
	//private Token token;

    /**
     * 用于给微信验证token
     * @param request
     * @param response
     * @return
     * @throws IOException
     */
    @RequestMapping("/checkToken")
    public String checkToken(HttpServletRequest request,HttpServletResponse response) throws IOException {
        // 微信加密签名
        String signature = request.getParameter("signature");
        // 时间戳
        String timestamp = request.getParameter("timestamp");
        // 随机数
        String nonce = request.getParameter("nonce");
        // 随机字符串
        String echostr = request.getParameter("echostr");

        if (SignUtil.checkSignature(signature, timestamp, nonce)) {
            System.out.println("校验token成功");
            return echostr;
        }else{
            System.out.println("校验token不成功");
            return  null;
        }
    }

    /**
     * 用于获取出回调地址  （引导用户调用此接口，成功后自动调取回调地址然后取出用户信息）
     * @param response
     * @throws IOException
     */
    @RequestMapping("/login")
    public void wxLogin(HttpServletResponse response,String openId) throws IOException {
        //请求获取code的回调地址
        //用线上环境的域名或者用内网穿透，不能用ip
        String callBack = "http://localhost:8080/ZLGL/repair/login?openid="+openId;//域名填你自己的
        //String callBack = "http://localhost:8080/ZLGL/html/login.html?openId="+openId;//域名填你自己的

      //String zhu_ce = "http://www.wanxide.cn/static/html/zhu_ce.html";
        response.sendRedirect(callBack);
        /*Token token = new Token();
        String APPID = token.getAPPID();
        //请求地址
        String url = "https://open.weixin.qq.com/connect/oauth2/authorize" +
                "?appid=" + APPID +
                "&redirect_uri=" + URLEncoder.encode(callBack) +
                "&response_type=code" +
                "&scope=snsapi_userinfo" +
                "&state=STATE#wechat_redirect";

        System.out.println(url);
        //重定向
        response.sendRedirect(url);*/
    }

    /**
     * 	绑定openid
     * @param response
     * @throws IOException
     */
    @RequestMapping("/updateMemberOpenId")
    public static void updateMemberOpenId(HttpServletResponse response) throws IOException {
    	//String callBack = "http://www.wanxide.cn/repair/weixiu_zhuce";//域名填你自己的
    	String callBack = "http://hem.wanxide.cn/repair/weixiu_zhuce";//域名填你自己的
        Token token = new Token();
        String APPID = token.getAPPID();
        //请求地址
        String url = "https://open.weixin.qq.com/connect/oauth2/authorize" +
                "?appid=" + APPID +
                "&redirect_uri=" + URLEncoder.encode(callBack) +
                "&response_type=code" +
                "&scope=snsapi_userinfo" +
                "&state=STATE#wechat_redirect";

        System.out.println(url);
        //重定向
        response.sendRedirect(url);
    }


    /**
     * 回调方法
     * @param request
     * @param response
     * @throws IOException
     */
    //	回调方法
    @RequestMapping("/callBack")
    @ResponseBody
    public String wxCallBack(HttpServletRequest request, HttpServletResponse response,String code) throws IOException {
    	Token token = new Token();
    	JSONObject obj = new JSONObject();
    	//String code = request.getParameter("code");

        //获取access_token
        String url = "https://api.weixin.qq.com/sns/oauth2/access_token" +
                "?appid=" + token.getAPPID() +
                "&secret=" + token.getAPPSECRET() +
                "&code=" + code +
                "&grant_type=authorization_code";

        String result = HttpClientUtil.doGet(url);

        System.out.println("请求获取access_token:" + result);
        //返回结果的json对象
        JSONObject resultObject = JSON.parseObject(result);
        String access_tokenString = resultObject.getString("access_token");

        //创建菜单createMenu
        //String msgString = createMenu(access_tokenString);
        //请求获取userInfo
        String infoUrl = "https://api.weixin.qq.com/sns/userinfo" +
                "?access_token=" + access_tokenString +
                "&openid=" + resultObject.getString("openid") +
                "&lang=zh_CN";

        String resultInfo = HttpClientUtil.doGet(infoUrl);

        //此时已获取到userInfo，再根据业务进行处理
        System.out.println("请求获取userInfo:" + resultInfo);


        JSONObject resultInfoObject = JSON.parseObject(resultInfo);
        String openid = resultInfoObject.getString("openid");
        String nickname = resultInfoObject.getString("nickname");


        System.out.println("++++++++++++++openid++++++++++++"+openid);
        System.out.println("++++++++++++++nickname++++++++++++"+nickname);
        //登录,返回是否为用户

        //String zhu_ce = "http://www.wanxide.cn/static/html/zhu_ce.html";
        //response.sendRedirect(zhu_ce);
        obj.put("result", "success");
        obj.put("openid", openid);
        obj.put("nickname", nickname);
        return  obj.toString();
    }


    /**
     * @param access_tokenString
     * 创建Menu
    * @Title: createMenu
    * @Description: 创建Menu
    * @param @return
    * @param @throws IOException    设定文件
    * @return int    返回类型
    * @throws
     */
       public String createMenu(String access_tokenString) {
         //String menu = "{\"button\":[{\"type\":\"click\",\"name\":\"MENU01\",\"key\":\"1\"},{\"type\":\"click\",\"name\":\"天气查询\",\"key\":\"西安\"},{\"name\":\"日常工作\",\"sub_button\":[{\"type\":\"click\",\"name\":\"待办工单\",\"key\":\"01_WAITING\"},{\"type\":\"click\",\"name\":\"已办工单\",\"key\":\"02_FINISH\"},{\"type\":\"click\",\"name\":\"我的工单\",\"key\":\"03_MYJOB\"},{\"type\":\"click\",\"name\":\"公告消息箱\",\"key\":\"04_MESSAGEBOX\"},{\"type\":\"click\",\"name\":\"签到\",\"key\":\"05_SIGN\"}]}]}";
         String menu = "{\"button\":[{\"name\":\"菜单\",\"sub_button\":[{\"type\":\"view\",\"name\":\"点一下\",\"url\":\"http://www.wanxide.cn/static/html/login.html\"}]}]}";
//         String menu = "{\"button\":[{\"name\":\"菜单\",\"sub_button\":[{\"type\":\"view\",\"name\":\"点一下\",\"url\":\"http://www.wanxide.cn/repair/login\"},{\"type\":\"view\",\"name\":\"测试报修\",\"url\":\"http://hem.wanxide.cn/repair/login\"}]}]}";

           //此处改为自己想要的结构体，替换即可
           //String access_token= access_tokenString();

           String action = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token="+access_tokenString;
           try {
              URL url = new URL(action);
              HttpURLConnection http =   (HttpURLConnection) url.openConnection();

              http.setRequestMethod("POST");
              http.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
              http.setDoOutput(true);
              http.setDoInput(true);
              System.setProperty("sun.net.client.defaultConnectTimeout", "30000");//连接超时30秒
              System.setProperty("sun.net.client.defaultReadTimeout", "30000"); //读取超时30秒

              http.connect();
              OutputStream os= http.getOutputStream();
              os.write(menu.getBytes("UTF-8"));//传入参数
              os.flush();
              os.close();

              InputStream is =http.getInputStream();
              int size =is.available();
              byte[] jsonBytes =new byte[size];
              is.read(jsonBytes);
              String message=new String(jsonBytes,"UTF-8");
              return "返回信息"+message;
              } catch (MalformedURLException e) {
                  e.printStackTrace();
              } catch (IOException e) {
                  e.printStackTrace();
              }
           return "createMenu 失败";
      }

}
