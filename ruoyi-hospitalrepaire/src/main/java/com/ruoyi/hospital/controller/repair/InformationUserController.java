package com.ruoyi.hospital.controller.repair;

import com.ruoyi.hospital.controller.base.BaseController;
import com.ruoyi.hospital.entity.Page;
import com.ruoyi.hospital.entity.system.User;
import com.ruoyi.hospital.framework.exception.GenException;
import com.ruoyi.hospital.service.duty.DutyService;
import com.ruoyi.hospital.service.repair.InformationUserService;
import com.ruoyi.hospital.util.MD5;
import com.ruoyi.hospital.util.PageData;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Controller
@RequestMapping({"/information"})
public class InformationUserController  extends BaseController {

	@Resource(name = "informationUserService")
	private InformationUserService informationUserService;
	@Resource(name = "dutyService")
	private DutyService dutyService;

	@RequestMapping({"/listInformationUser"})
	public ModelAndView listEmployee() throws GenException {
		ModelAndView mv = getModelAndView();
		mv.setViewName("repair/information_user_list");
		return mv;
	}

	@RequestMapping(value = {"/getInformationUserlist"}, produces = {"application/json;charset=UTF-8"})
	@ResponseBody
	public String getInformationUserlistPage(Page page) throws GenException {
		JSONObject obj = new JSONObject();

//			Subject subject = SecurityUtils.getSubject();
//			Session session = subject.getSession();
//			User user = (User) session.getAttribute(Const.SESSION_USER);

		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		PageData pd = getPageData();
		int start = Integer.valueOf(pd.getString("page")).intValue();
		int length = Integer.valueOf(pd.getString("rows")).intValue();
		page.setShowCount(length);

		List<String> list = null;
		page.setPd(pd);
		page.setCurrentPage(start);
		List<PageData> dutyList = null;
		try {
			dutyList = this.informationUserService.getInformationUserlistPage(page);
			obj.put("records", Integer.valueOf(page.getTotalResult()));
			obj.put("page", Integer.valueOf(page.getCurrentPage()));
			obj.put("total", Integer.valueOf(page.getTotalPage()));
			obj.put("rows", dutyList);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return obj.toString();
	}

	@RequestMapping({"/editInformationUser"})
	public ModelAndView goEditU() throws Exception {
		ModelAndView mv = getModelAndView();
		PageData pd = new PageData();
		pd = getPageData();
		List<PageData> groupDictionaryList = this.dutyService.getGroupDictionaryList();
		mv.addObject("groupDictionaryList", groupDictionaryList);

		pd = this.informationUserService.getInformationUserById(pd);
		mv.setViewName("repair/edit_information_user");
		mv.addObject("msg", "addInformationUser");
		mv.addObject("pd", pd);
		return mv;
	}

	@RequestMapping({"/addInformationUser"})
	@ResponseBody
	public Map<String, String> editEmployee() {
		PageData pd = getPageData();
		Map<String, String> map = new HashMap<>();
		try {
//			Subject subject = SecurityUtils.getSubject();
//			Session session = subject.getSession();
//			User user = (User) session.getAttribute(Const.SESSION_USER);

			User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

			String id = pd.getString("ID");
			if (StringUtils.isNotBlank(id)) {
				this.informationUserService.editInformationUser(pd);
			}else {
				pd.put("ID", get32UUID());
				pd.put("DEPARTMENT_ID", "7d7a7078-f653-4947-839b-afbafd9496bb");
				pd.put("PASSWORD", MD5.md5("8888"));
				pd.put("TYPE", "f8afdcc2-9587-4d01-a495-875ac62ee521");
				pd.put("MARK", 0);
				this.informationUserService.addInformationUser(pd);
			}
			map.put("result", "success");
		} catch (Exception e) {
			e.printStackTrace();
			map.put("result", "error");
		}
		return map;
	}

	/**
	 * 判断用户名是否存在
	 */
	@RequestMapping(value="/hasN")
	@ResponseBody
	public Map<String,String> hasN(){
		Map<String,String> map = new HashMap<String,String>();
		String errInfo = "success";
		PageData pd = new PageData();
		try{
			pd = this.getPageData();
			if(informationUserService.findByUN(pd) != null){
				errInfo = "error";
			}
		} catch(Exception e){
			errInfo = "error";
			log.error(e.toString(), e);
		}
		map.put("result", errInfo);				//返回结果
		return map;
	}

	@RequestMapping(value="/hasUserName")
	@ResponseBody
	public Map<String,String> hasUserName(){
		Map<String,String> map = new HashMap<String,String>();
		String errInfo = "success";
		PageData pd = new PageData();
		try{
			pd = this.getPageData();
			if(informationUserService.findByUserName(pd) != null){
				errInfo = "error";
			}
		} catch(Exception e){
			errInfo = "error";
			log.error(e.toString(), e);
		}
		map.put("result", errInfo);				//返回结果
		return map;
	}

	@RequestMapping({"/markInformation"})
	public ModelAndView markInformation(String ID,String PATROL_DATE_START,String PATROL_DATE_END) throws Exception {
		ModelAndView mv = getModelAndView();
		PageData pd = new PageData();
		pd = getPageData();

		mv.setViewName("repair/mark_list_info");
		mv.addObject("msg", "checkMarkInformation");
		mv.addObject("pd", pd);
		return mv;
	}

	@RequestMapping(value = {"/checkMarkInformation"}, produces = {"application/json;charset=UTF-8"})
	@ResponseBody
	public String checkMarkInformation(Page page) throws GenException {
		JSONObject obj = new JSONObject();

//			Subject subject = SecurityUtils.getSubject();
//			Session session = subject.getSession();
//			User user = (User) session.getAttribute(Const.SESSION_USER);

		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		PageData pd = getPageData();
		int start = Integer.valueOf(pd.getString("page")).intValue();
		int length = Integer.valueOf(pd.getString("rows")).intValue();
		page.setShowCount(length);

		List<String> list = null;
		page.setPd(pd);
		page.setCurrentPage(start);
		List<PageData> dutyList = null;
		try {
			dutyList = this.informationUserService.checkMarkInformationlistPage(page);
			obj.put("records", Integer.valueOf(page.getTotalResult()));
			obj.put("page", Integer.valueOf(page.getCurrentPage()));
			obj.put("total", Integer.valueOf(page.getTotalPage()));
			obj.put("rows", dutyList);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return obj.toString();
	}

}
