package com.ruoyi.hospital.controller.app.WeChat1;

public class Token {

    //接口访问凭证
    private String accessToken;
    //接口有效期，单位：秒
    private int expiresIn;

    private int expireData;

    private String APPID="wxaf3b50a89593bc0d";//填你自己的
    private String APPSECRET="4773cb80cfe560eea6e1f0100e0e1a49";//填你自己的

    public String getAccessToken() {
        return accessToken;
    }
    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
    public int getExpiresIn() {
        return expiresIn;
    }
    public void setExpiresIn(int expiresIn) {
        this.expiresIn = expiresIn;
    }
	public int getExpireData() {
		return expireData;
	}
	public void setExpireData(int expireData) {
		this.expireData = expireData;
	}
	public String getAPPID() {
		return APPID;
	}
	public String getAPPSECRET() {
		return APPSECRET;
	}



}
