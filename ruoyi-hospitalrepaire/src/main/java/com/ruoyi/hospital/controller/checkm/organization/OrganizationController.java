package com.ruoyi.hospital.controller.checkm.organization;

import com.ruoyi.hospital.controller.base.BaseController;
import com.ruoyi.hospital.entity.system.User;
import com.ruoyi.hospital.framework.exception.GenException;
import com.ruoyi.hospital.service.basicsetting.project.ProjectService;
import com.ruoyi.hospital.service.checkm.OrganizationService;
import com.ruoyi.hospital.service.currency.CurrencyTypeService;
import com.ruoyi.hospital.service.duty.DutyService;
import com.ruoyi.hospital.util.GetPinyin;
import com.ruoyi.hospital.util.PageData;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value="/organization")
public class OrganizationController extends BaseController {

	@Resource(name="organizationService")
	private OrganizationService organizationService;
	@Resource(name="currencyTypeService")
	private CurrencyTypeService currencyTypeService;
	@Resource(name="projectService")
	private ProjectService projectService;
	@Resource(name = "dutyService")
	private DutyService dutyService;

	/**
	 * 跳转到机构页面
	 */
	@RequestMapping(value="/listOrganization")
	public ModelAndView listOrganization(){
		ModelAndView mv=this.getModelAndView();
		//List<PageData> departmentTypeList=null;
		List<PageData> departmentPropertyList=null;
		List<PageData> groupList=null;
		try {
			//departmentTypeList=currencyTypeService.getCurrencyType("11");
			departmentPropertyList=currencyTypeService.getCurrencyType("27");
			//mv.addObject("departmentTypeList", departmentTypeList);
			groupList=dutyService.getGroupDictionaryList();
			mv.addObject("groupList", groupList);
			mv.addObject("departmentPropertyList", departmentPropertyList);
			mv.setViewName("checkm/organization/organization_list");
		} catch (GenException e) {
			e.printStackTrace();
		}
		return mv;
	}

	/**
	 * 通过id得到部门的基本信息
	 */
	@ResponseBody
	@RequestMapping(value="/getDepartmentInformation",produces="application/json;charset=UTF-8")
	public Object getDepartmentInformation(){
		Map<String,Object> map=new HashMap<String,Object>();
		PageData pd=this.getPageData();
		PageData departmentInformation=null;
		try {
			departmentInformation=organizationService.getDepartmentInformation(pd);
			map.put("departmentInformation", departmentInformation);
			map.put("result", "success");
		} catch (GenException e) {
			e.printStackTrace();
			map.put("result", "error");
		}
		return map;
	}

	/**
	 * 跳转到添加下级部门页面
	 * @throws GenException
	 */
	@RequestMapping(value="/toAddLowerLevelDepartment")
	public ModelAndView addLowerLevelDepartment() throws GenException{
		ModelAndView mv=this.getModelAndView();
		List<PageData> departmentTypeList=null;
		List<PageData> departmentPropertyList=null;
		List<PageData> groupList=null;
		PageData pd=this.getPageData();
		try {
			departmentTypeList=currencyTypeService.getCurrencyType("11");
			departmentPropertyList=currencyTypeService.getCurrencyType("27");
			groupList=dutyService.getGroupDictionaryList();
			mv.addObject("groupList", groupList);
			mv.addObject("departmentTypeList", departmentTypeList);
			mv.addObject("departmentPropertyList", departmentPropertyList);
			mv.addObject("parentId", pd.getString("parentId"));
			mv.addObject("projectId", pd.getString("projectId"));
			mv.setViewName("checkm/organization/organization_addLower");
		} catch (GenException e) {
			throw new GenException(e.getCause());
		}
		return mv;
	}

	/**
	 * 添加下级部门
	 */
	@RequestMapping(value="/addLowerLevelDpartment",produces="application/json;charset=UTF-8")
	@ResponseBody
	public Object addLowerLevelDpartment(){
		Map<String,Object> map=new HashMap<String,Object>();
		PageData pd=this.getPageData();
		PageData pda = new PageData();
		try {
//		Subject subject = SecurityUtils.getSubject();
//		Session session = subject.getSession();
//		User user = (User) session.getAttribute(Const.SESSION_USER);

			User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

			pd.put("ID", get36UUID());
			pd.put("CREATER", user.getEmployeeId());
			pd.put("MODIFIER", user.getEmployeeId());
			pd.put("MODIFYDATE", new Date());
			String pinyin = GetPinyin.getPinYinHeadChar(pd.getString("DEPARTMENT_NAME"));
			pd.put("PINYIN", pinyin);
			organizationService.addLowerLevelDpartment(pd);
			map.put("result", "success");
			/*if(pd.getString("projectId") != null && pd.getString("projectId") != "")
			{
				String departmentId = pd.getString("ID");
				pda.put("DEPARTMENT_ID", departmentId);
				pda.put("ID", pd.getString("projectId"));
				projectService.editProject(pda);
			}*/
		} catch (GenException e) {
			e.printStackTrace();
			map.put("result", "error");
		}
		return map;
	}

	/**
	 * 删除部门
	 */
	@RequestMapping(value="/deleteDepartment",produces="application/json;charset=UTF-8")
	@ResponseBody
	public Object deleteDepartment(){
		Map<String,Object> map=new HashMap<String,Object>();
		PageData pd=this.getPageData();
		try {
			String departmentId = String.valueOf(pd.get("departmentId"));
			if (departmentId != null && !"".equals(departmentId)) {
				PageData empIdenPd = organizationService.getDeptAndSubDeptEmpAndIdendityNum(departmentId);
				int empNum = 0, identityNum = 0;
				if (empIdenPd != null) {
					String employeeTotal = String.valueOf(empIdenPd.get("EMPLOYEE_TOTAL"));
					String identityTotal = String.valueOf(empIdenPd.get("IDENTITY_TOTAL"));
					try {
						empNum = Integer.parseInt(employeeTotal);
					} catch (Exception e) {
						empNum = 0;
					}
					try {
						identityNum = Integer.parseInt(identityTotal);
					} catch (Exception e) {
						identityNum = 0;
					}
				}
				if (empNum > 0 || identityNum > 0) {
					if (empNum > 0) {
						map.put("msg", "hasEmp");
						map.put("empNum", empNum);
					}
					if (identityNum > 0) {
						map.put("msg", "hasIdentity");
						map.put("identityNum", identityNum);
					}
					if (empNum > 0 && identityNum >0) {
						map.put("msg", "hasEmpAndIdentity");
					}
					map.put("result", "error");
				} else {
					organizationService.deleteDepartment(pd);//删除本级
					organizationService.deleteChildDepartment(pd);//删除下级
					map.put("result", "success");
				}
			} else {
				map.put("result", "success");
			}
		} catch (GenException e) {
			e.printStackTrace();
			map.put("result", "error");
		}
		return map;
	}

	/**
	 * 通过name搜索部门
	 */
	@ResponseBody
	@RequestMapping(value="/getDepartmentInformationByName",produces="application/json;charset=UTF-8")
	public Object getDepartmentInformationByName(){
		Map<String,Object> map=new HashMap<String,Object>();
		PageData pd=this.getPageData();
		List<PageData> departmentInformation=null;
		try {
			departmentInformation=organizationService.getDepartmentInformationByName(pd);
			map.put("departmentInformation", departmentInformation);
			map.put("result", "success");
		} catch (GenException e) {
			e.printStackTrace();
			map.put("result", "error");
		}
		return map;
	}

	/**
	 * 机构调整
	 */
	@RequestMapping(value="/departmentAdjust")
	@ResponseBody
	public Object departmentAdjust(){
		Map<String,Object> map=new HashMap<String,Object>();
		PageData pd=this.getPageData();
		try {
			organizationService.departmentAdjust(pd);
			map.put("result", "success");
		} catch (GenException e) {
			e.printStackTrace();
			map.put("result", "error");
		}
		return map;
	}

	/**
	 * 编辑保存部门信息
	 */
	@RequestMapping(value="/saveDepartmentInformation")
	@ResponseBody
	public Object saveDepartmentInformation(){
		Map<String,Object> map=new HashMap<String,Object>();
		PageData pd=this.getPageData();
		try {
			organizationService.saveDepartmentInformation(pd);
			map.put("result", "success");
		} catch (GenException e) {
			e.printStackTrace();
			map.put("result", "error");
		}
		return map;
	}
}
