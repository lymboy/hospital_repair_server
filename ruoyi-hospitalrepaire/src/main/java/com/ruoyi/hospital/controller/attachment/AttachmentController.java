package com.ruoyi.hospital.controller.attachment;

import com.ruoyi.hospital.controller.base.BaseController;
import com.ruoyi.hospital.entity.Page;
import com.ruoyi.hospital.entity.system.User;
import com.ruoyi.hospital.framework.exception.GenException;
import com.ruoyi.hospital.service.attachment.AttachmentService;
import com.ruoyi.hospital.util.FileDownload;
import com.ruoyi.hospital.util.PageData;
import com.ruoyi.hospital.util.ThumbImage;
import net.sf.json.JSONObject;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * <p>Title: AttachmentController</p>
 * @Desc: 通用 附件 管理  --/uploadFiles/attachment
 * @author: 杨建军
 *	@Date: 2017年4月8日
 * @Time: 下午8:50:24
 * @version:
 */
@Controller
@RequestMapping(value="/attachment")
public class AttachmentController extends BaseController {

	@Resource(name="attachmentService")
	private AttachmentService attachmentService;

	@RequestMapping(value="/toManagerAttachment")
	public ModelAndView toManagerAttachment() throws GenException {
		ModelAndView mv = this.getModelAndView();
		PageData pd = this.getPageData();
		mv.addObject("pd", pd);
		mv.setViewName("attachment/file_manager");
		return mv;
	}

	@RequestMapping(value="/toManagerAttachmentMult")
	public ModelAndView toManagerAttachmentFile() throws GenException{
		ModelAndView mv = this.getModelAndView();
		PageData pd = this.getPageData();
		mv.addObject("pd", pd);
		mv.setViewName("attachment/file_manager_mult");
		return mv;
	}


	/**
	 * 分页 查询文件列表 业务ID  [MASTER_ID] [MASTER_TYPE可选]
	 * @throws GenException
	 */
	@RequestMapping(value="/getFileListPage",produces="text/html;charset=UTF-8")
	public @ResponseBody String getFileListPage(Page page){
		PageData pd = this.getPageData();
		int start = Integer.valueOf(pd.getString("page"));
		int length = Integer.valueOf(pd.getString("rows"));

		page.setShowCount(length);
		page.setPd(pd);
		page.setCurrentPage(start);

		List<PageData> fileList;
		JSONObject obj = new JSONObject();
		try {
			fileList = attachmentService.getFileListPage(page);

			obj.put("records", page.getTotalResult());//总记录数
			obj.put("page", page.getCurrentPage());//当前页
			obj.put("total", page.getTotalPage());//总页数
			obj.put("rows", fileList);
		} catch (GenException e) {
			e.printStackTrace();
		}
		return obj.toString();
	}

	/**
	 * 获取 查询文件列表 业务ID  [MASTER_ID] [MASTER_TYPE可选]
	 * @return
	 * @throws GenException
	 */
	@RequestMapping(value="/getFileList", produces="application/json;charset=UTF-8")
	public @ResponseBody String getFileList() throws GenException{
		PageData pd = this.getPageData();
		List<PageData> fileList;
		JSONObject obj = new JSONObject();
		try {
			fileList = attachmentService.getFileList(pd);
			if(fileList!=null){
				obj.put("fileCount", fileList.size());//个数
			}else{
				obj.put("fileCount", 0);//个数
			}
			obj.put("fileList", fileList);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return obj.toString();
	}

	/**
	 * 分页 获取 业务ID [MASTER_ID]  下的所有文件
	 * @throws GenException
	 */
	@RequestMapping(value="/getFileListPageByMasterId",produces="text/html;charset=UTF-8")
	public @ResponseBody String getFileListPageByMasterId(Page page){
		PageData pd = this.getPageData();
		int start = Integer.valueOf(pd.getString("page"));
		int length = Integer.valueOf(pd.getString("rows"));

		page.setShowCount(length);
		page.setPd(pd);
		page.setCurrentPage(start);

		List<PageData> fileList;
        JSONObject obj = new JSONObject();
		try {
			fileList = attachmentService.getFileListPageByMasterId(page);

			obj.put("records", page.getTotalResult());//总记录数
			obj.put("page", page.getCurrentPage());//当前页
			obj.put("total", page.getTotalPage());//总页数
			obj.put("rows", fileList);
		} catch (GenException e) {
			e.printStackTrace();
		}
		return obj.toString();
	}

	/**
	 * 获取 业务ID [MASTER_ID]  下的所有文件
	 * @return
	 * @throws GenException
	 */
	@RequestMapping(value="/getFileListByMasterId", produces="application/json;charset=UTF-8")
	public @ResponseBody String getFileListByMasterId() throws GenException{
		PageData pd = this.getPageData();
		List<PageData> fileList;
		JSONObject obj = new JSONObject();
		try {
			fileList = attachmentService.getFileListByMasterId(pd);
			if(fileList!=null){
				obj.put("fileCount", fileList.size());//个数
			}else{
				obj.put("fileCount", 0);//个数
			}
			obj.put("fileList", fileList);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return obj.toString();
	}

	/**
	 *  上传 附件 ， 保存记录
	 * @return
	 * @throws GenException
	 */
	@ResponseBody
	@RequestMapping(value="/uploadFile", produces="application/json;charset=UTF-8")
	public Map<String, Object> uploadFile(HttpServletRequest request) throws GenException {
		Map<String, Object> map = new HashMap<String, Object>();
		PageData pd = new PageData();

		String rootPath = request.getServletContext().getRealPath("/");
		String midPath = "/uploadFiles/attachment/";

		String id =  get36UUID();
		String subSystem = request.getParameter("subSystem");
		String moduleName = request.getParameter("moduleName");
		String masterId = request.getParameter("masterId");
		String masterType = request.getParameter("masterType");
		String fileUrl = subSystem+File.separator+moduleName+File.separator+masterId;

		File folder = new File(rootPath+midPath+fileUrl);
		// 上传文件
		// 获取 上传的文件
		//将request变成 multiRequest
        MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest)request;
        //获取multiRequest 中所有的文件名
        Iterator<String> iter = multiRequest.getFileNames();
        while(iter.hasNext()) {
            //一次遍历所有文件
            MultipartFile file = multiRequest.getFile(iter.next().toString());
            if(file != null)
            {
            	String fileName = file.getOriginalFilename();
            	String fileSize = String.valueOf(file.getSize());
                String filePath = folder + File.separator+ id+fileName.substring(fileName.lastIndexOf("."));
                File endfolder = new File(filePath);
                //上传
                try {
                	if(!folder.exists()) {
						folder.mkdirs();
					}
                	if(!endfolder.exists()) {
                		file.transferTo(endfolder);
						// 生成缩略图
						ThumbImage.thumbnailImages(filePath);

						User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

            			pd.put("creater", user.getUserId());
            			pd.put("createdate", new Date());
            			pd.put("modifier", user.getUserId());
            			pd.put("modifydate", new Date());
            			int fileNo = attachmentService.getFileNumByMasterId(pd)+1;
            			pd.put("fileNo", fileNo);
            			pd.put("uploadDate", new Date());
            			pd.put("id", id);
            			pd.put("fileName", fileName);
                    	pd.put("fileSize", fileSize);
                    	pd.put("subSystem", subSystem);
                		pd.put("moduleName", moduleName);
                		pd.put("masterId", masterId);
                		pd.put("masterType", masterType);
                    	attachmentService.addFileInfo(pd);
                    	map.put("fileInfo", pd);
                    	map.put("result", "success");
					}
				} catch (IllegalStateException e) {
					map.put("result", "error");
				} catch (IOException e) {
					map.put("result", "error");
				}
            }
        }
		return map;
	}

	/**
	 *  更新文件信息记录 的 业务ID [MASTER_ID]
	 * @return
	 * @throws GenException
	 */
	@ResponseBody
	@RequestMapping(value="/updateMasterIdByMasterId", produces="application/json;charset=UTF-8")
	public Map<String, Object> updateMasterIdByMasterId() throws GenException {
		 Map<String, Object> map = new HashMap<String, Object>();
		 PageData pd = this.getPageData();
//		Subject subject = SecurityUtils.getSubject();
//		Session session = subject.getSession();
//		User user = (User) session.getAttribute(Const.SESSION_USER);
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		 pd.put("modifier", user.getUserId());
		 pd.put("modifydate", new Date());
		 try {
			attachmentService.updateMasterIdByMasterId(pd);
			map.put("result", "success");
		} catch (GenException e) {
			throw new GenException(e.getCause());
		}
		 return map;
	}
	/**
	 *  更新文件信息
	 * @return
	 * @throws GenException
	 */
	@ResponseBody
	@RequestMapping(value="/updateAttachInfoById", produces="application/json;charset=UTF-8")
	public Map<String, Object> updateAttachInfoById() throws GenException {
		Map<String, Object> map = new HashMap<String, Object>();
		PageData pd = this.getPageData();
//		Subject subject = SecurityUtils.getSubject();
//		Session session = subject.getSession();
//		User user = (User) session.getAttribute(Const.SESSION_USER);

		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		pd.put("modifier", user.getUserId());
		pd.put("modifydate", new Date());
		try {
			attachmentService.updateAttachInfoById(pd);
			map.put("result", "success");
		} catch (GenException e) {
			throw new GenException(e.getCause());
		}
		return map;
	}

	/**
	 * 删除文件记录 通过 主键
	 * @return
	 * @throws GenException
	 */
	@ResponseBody
	@RequestMapping(value="/deleteFileInfoById", produces="application/json;charset=UTF-8")
	public Map<String, Object> deleteFileInfoById() throws GenException {
		Map<String, Object> map = new HashMap<String, Object>();
		PageData pd = this.getPageData();
//		Subject subject = SecurityUtils.getSubject();
//		Session session = subject.getSession();
//		User user = (User) session.getAttribute(Const.SESSION_USER);

		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		pd.put("modifier", user.getUserId());
		pd.put("modifydate", new Date());
		try {
			attachmentService.deleteFileInfoById(pd);
			map.put("result", "success");
		} catch (GenException e) {
			throw new GenException(e.getCause());
		}
		return map;
	}

	/**
	 * 下载文件
	 * @throws GenException
	 */
    	@RequestMapping(value = "/downloadFile")
	    public ModelAndView downloadFile(HttpServletRequest request, HttpServletResponse response) throws IOException, GenException {
    	ModelAndView mv = this.getModelAndView();
    	PageData pd = this.getPageData();
    	try {
			// 得到要下载的文件信息
			PageData fileInfo = attachmentService.getFileInfoById(pd);
			String fileUrl = fileInfo.getString("FILE_URL");
			String fileName = fileInfo.getString("FILE_NAME");
			// 找出文件
			String path = this.getRequest().getServletContext().getRealPath("/uploadFiles/attachment/");
			String filePath = path+File.separator+fileUrl+File.separator+fileInfo.getString("ID")+fileName.substring(fileName.lastIndexOf("."));
			File file = new File(filePath);
			if(!file.exists()){
//				Subject subject = SecurityUtils.getSubject();
//				Session session = subject.getSession();
//				User user = (User) session.getAttribute(Const.SESSION_USER);

				User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

				pd.put("modifier", user.getUserId());
				pd.put("modifydate",new Date());
				mv.setViewName("attachment/file_404");
				return mv;
			}else{
					/*byte[] data = FileUtil.toByteArray2(filePath);
				    fileName = URLEncoder.encode(fileName, "UTF-8");
				    response.reset();
				    response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
				    response.addHeader("Content-Length", "" + data.length);
				    response.setContentType("application/octet-stream;charset=UTF-8");
				    OutputStream outputStream = new BufferedOutputStream(response.getOutputStream());
				    outputStream.write(data);
				    outputStream.flush();
				    outputStream.close();
				    response.flushBuffer();*/
					FileDownload.fileDownload(response, filePath, fileName);
			}
    	} catch (Exception e) {
    		throw new GenException(e.getCause());
    	}
		return null;
	 }


}
