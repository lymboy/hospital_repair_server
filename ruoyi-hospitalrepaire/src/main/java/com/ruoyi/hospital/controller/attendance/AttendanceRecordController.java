package com.ruoyi.hospital.controller.attendance;

import com.ruoyi.hospital.controller.base.BaseController;
import com.ruoyi.hospital.entity.Page;
import com.ruoyi.hospital.entity.system.User;
import com.ruoyi.hospital.framework.exception.GenException;
import com.ruoyi.hospital.service.attendance.AttendanceRecordService;
import com.ruoyi.hospital.service.currency.CurrencyTypeService;
import com.ruoyi.hospital.service.information.EmployeeService;
import com.ruoyi.hospital.util.PageData;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p> 考勤记录 </p>
 *
 * @author Young
 * @since 2018/4/2
 */
@Controller
@RequestMapping("attendance_record")
public class AttendanceRecordController extends BaseController {

    @Autowired
    private AttendanceRecordService attendanceRecordService;
    @Autowired
    private CurrencyTypeService currencyTypeService;
    @Resource(name="employeeService")
	private EmployeeService employeeService;

    /**
     * 转到 考勤记录
     * @return
     * @throws GenException
     */
    @RequestMapping(value="list", produces="text/html;charset=UTF-8")
    public ModelAndView list() throws GenException{
        ModelAndView mv = this.getModelAndView();
        PageData pd = this.getPageData();
        mv.addObject("pd", pd);
        mv.setViewName("attendance/attendance_record_list");
        List<PageData> postList = currencyTypeService.getCurrencyType("5"); // 5 职务（岗位）
        mv.addObject("postList", postList);
        return mv;
    }

    /**
     * 分页 获取 考勤记录
     * @throws GenException
     */
    @RequestMapping(value="attendance_record_list_page",produces="text/html;charset=UTF-8")
    public @ResponseBody
    String dataListPage(@RequestParam(value = "page", defaultValue = "1") int currentPage,
                        @RequestParam(value = "rows", defaultValue = "20") int showCount) throws GenException {
        Page page = this.getPage();
        PageData pd = this.getPageData();
        String baseId = pd.getString("BASE_ID");
        if (baseId==null || baseId=="") {
            //		Subject subject = SecurityUtils.getSubject();
//		Session session = subject.getSession();
//		User user = (User) session.getAttribute(Const.SESSION_USER);

            User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

            String departmentId = user.getDepartmentId();
			Page page1 = new Page();
			PageData pd1 = new PageData();
			pd1.put("DEPARTMENT_ID", departmentId);
			page1.setPd(pd1);

			List<PageData> employeeInfoListForSelect = employeeService.getEmployeeInfoListForSelect(page1);

			for (PageData pageData : employeeInfoListForSelect) {
				baseId = pageData.getString("BASE_ID");

			}
			pd.put("BASE_ID", baseId);
		}

        page.setShowCount(showCount);
        page.setCurrentPage(currentPage);
        page.setPd(pd);


        JSONObject obj = new JSONObject();
        List<PageData> list = attendanceRecordService.getAttendanceRecordlistPage(page);

        obj.put("records", page.getTotalResult());//总记录数
        obj.put("page", page.getCurrentPage());//当前页
        obj.put("total", page.getTotalPage());//总页数
        obj.put("rows", list);
        return obj.toString();
    }

    /**
     * 转到 考勤记录 查看
     * @return
     * @throws GenException
     */
    @RequestMapping(value="view", produces="text/html;charset=UTF-8")
    public ModelAndView view(@RequestParam(value = "ID", required = true) String ID) throws GenException{
        ModelAndView mv = this.getModelAndView();
        PageData pdInfo = attendanceRecordService.getAttendanceRecordById(ID);
        mv.addObject("data", pdInfo);
        mv.setViewName("attendance/attendance_record_view");
        return mv;
    }

    /**
     * 从表 获取 签到记录
     * @throws GenException
     */
    @RequestMapping(value="attendance_record_detail_list",produces="text/html;charset=UTF-8")
    public @ResponseBody
    String getAttendanceRecordDetailList() throws GenException {
        PageData pd = this.getPageData();
        JSONObject obj = new JSONObject();
        List<PageData> list = attendanceRecordService.getAttendanceRecordDetailList(pd);
        obj.put("rows", list);
        return obj.toString();
    }

    /**
     * 转到 考勤记录 签到图片 查看
     * @return
     * @throws GenException
     */
    @RequestMapping(value="view_img", produces="text/html;charset=UTF-8")
    public ModelAndView viewImg() throws GenException{
        ModelAndView mv = this.getModelAndView();
        PageData pd = this.getPageData();
        List<PageData> attendanceRecordDetailImgList = attendanceRecordService.getAttendanceRecordDetailImgList(pd);
        mv.addObject("imgList", attendanceRecordDetailImgList);
        mv.setViewName("attendance/attendance_record_view_img");
        return mv;
    }


}
