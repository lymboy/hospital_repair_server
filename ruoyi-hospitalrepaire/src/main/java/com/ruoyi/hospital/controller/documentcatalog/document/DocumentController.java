package com.ruoyi.hospital.controller.documentcatalog.document;

import com.ruoyi.hospital.controller.base.BaseController;
import com.ruoyi.hospital.entity.Page;
import com.ruoyi.hospital.entity.system.User;
import com.ruoyi.hospital.framework.exception.GenException;
import com.ruoyi.hospital.service.documentcatalog.document.DocumentService;
import com.ruoyi.hospital.util.*;
import net.sf.json.JSONObject;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * <p>Title: DocumentController</p>
 *
 * @Desc: 资料文件管理
 * @author: 杨建军, 裴雅莉
 * @Date: 2017年4月21日
 * @Time: 上午10:20:34
 * @version:
 */
@Controller
@RequestMapping(value = "/document")
public class DocumentController extends BaseController {

    @Resource(name = "documentService")
    private DocumentService documentService;

    /**
     * 转到 资料文件管理
     *
     * @return
     */
    @RequestMapping(value = "/toDocumentManager")
    public ModelAndView toDocumentManager() {
        ModelAndView mv = this.getModelAndView();
        mv.setViewName("documentcatalog/document/document_list");
        return mv;
    }

    /**
     * 目录列表  树控件
     */
    @ResponseBody
    @RequestMapping(value = "/getCatalogList", produces = "application/json;charset=UTF-8")
    public Object getCatalogList() {
        Map<String, Object> map = new HashMap<String, Object>();
        List<PageData> catalogList = null;
        try {
            catalogList = documentService.getCatalogList();
            map.put("catalogList", catalogList);
            map.put("result", "success");
        } catch (GenException e) {
            e.printStackTrace();
            map.put("result", "error");
        }
        return map;
    }

    /**
     * 资料列表
     */
    @ResponseBody
    @RequestMapping(value = "/getMaterialList", produces = "application/json;charset=UTF-8")
    public Object getMaterialList(Page page) throws GenException {
        JSONObject obj = new JSONObject();
        PageData pd = this.getPageData();
        int start = Integer.valueOf(pd.getString("page"));
        int length = Integer.valueOf(pd.getString("rows"));

        String editDateStartStr = pd.getString("editDateStart");
        if (editDateStartStr != null && !"".equals(editDateStartStr)) {
            pd.put("editDateStart", DateUtil.fomatToDate(editDateStartStr));
        } else {
            pd.remove("editDateStart");
        }

        String editDateEndStr = pd.getString("editDateEnd");
        if (editDateEndStr != null && !"".equals(editDateEndStr)) {
            pd.put("editDateEnd", DateUtil.fomatToDate(editDateEndStr));
        } else {
            pd.remove("editDateEnd");
        }

        page.setShowCount(length);
        page.setPd(pd);
        page.setCurrentPage(start);
        List<PageData> materialList = null;
        try {
            materialList = documentService.getMaterialListPage(page);
            obj.put("records", page.getTotalResult());//总记录数
            obj.put("page", page.getCurrentPage());//当前页
            obj.put("total", page.getTotalPage());//总页数
            obj.put("rows", materialList);
        } catch (GenException e) {
            throw new GenException(e.getCause());
        }
        return obj.toString();
    }

    /**
     * 文件列表
     */
    @ResponseBody
    @RequestMapping(value = "/getDocumentList", produces = "application/json;charset=UTF-8")
    public Object getDocumentList(Page page) throws GenException {
        JSONObject obj = new JSONObject();
        PageData pd = this.getPageData();
        int start = Integer.valueOf(pd.getString("page"));
        int length = Integer.valueOf(pd.getString("rows"));
        page.setShowCount(length);
        page.setPd(pd);
        page.setCurrentPage(start);
        List<PageData> documentlList = null;
        try {
            documentlList = documentService.getDocumentListPage(page);
            obj.put("records", page.getTotalResult());//总记录数
            obj.put("page", page.getCurrentPage());//当前页
            obj.put("total", page.getTotalPage());//总页数
            obj.put("rows", documentlList);
        } catch (GenException e) {
            throw new GenException(e.getCause());
        }
        return obj.toString();
    }


    /**
     * 质量资料维护编辑
     */
    @RequestMapping(value = "/toMaterialDetail", produces = "application/json;charset=UTF-8")
    public ModelAndView toMaterialDetail() throws GenException {
        ModelAndView mv = this.getModelAndView();
        PageData pd = this.getPageData();
        PageData materialInfo = null;
        materialInfo = documentService.toMaterialDetail(pd);
        if (materialInfo == null) {
//			Subject subject = SecurityUtils.getSubject();
//			Session session = subject.getSession();
//			User user = (User) session.getAttribute(Const.SESSION_USER);

            User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

            materialInfo = new PageData();
            //构造资料编号
            Calendar calendar = Calendar.getInstance();
            int materialNum = documentService.getMaterialNum(pd) + 1;
            String materialNumStr = "0000000" + materialNum;
            String MaterialCode = "ZLZL-" + pd.getString("catalogName") + "-" + calendar.get(Calendar.YEAR) + "-" + materialNumStr.substring(materialNumStr.length() - 3);
            materialInfo.put("CATALOG_ID", pd.get("catalogId"));
            materialInfo.put("BASE_ID", pd.get("baseId"));
            materialInfo.put("MATERIAL_CODE", MaterialCode);
            materialInfo.put("ID", get36UUID());
            materialInfo.put("EDITOR", user.getEmployeeId());
            materialInfo.put("EDITOR_NAME", user.getEmployeeName());
            materialInfo.put("optionType", "add");
        }
        mv.addObject("materialInfo", materialInfo);
        mv.setViewName("documentcatalog/document/material_edit");
        return mv;
    }

    /**
     * 批量删除资料
     *
     * @return
     * @throws GenException
     */
    @ResponseBody
    @RequestMapping(value = "/deleteMaterial", produces = "application/json;charset=UTF-8")
    public Map<String, Object> deleteMaterial() throws GenException {
        Map<String, Object> map = new HashMap<String, Object>();
        PageData pd = this.getPageData();
        try {

            String materialIds = pd.getString("materialIds");
            if (!"".equals(materialIds) && materialIds != null) {
                String[] ids = materialIds.split(",");

                List<String> idsList = Arrays.asList(ids);
                pd.put("materialIdsList", idsList);

//			Subject subject = SecurityUtils.getSubject();
//			Session session = subject.getSession();
//			User user = (User) session.getAttribute(Const.SESSION_USER);

                User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

                pd.put("modifier", user.getEmployeeId());
                pd.put("modifydate", new Date());
                documentService.deleteMaterialByMaterialId(pd);
            }
            map.put("result", "success");
        } catch (GenException e) {
            map.put("result", "error");
            throw new GenException(e.getCause());
        }
        return map;
    }

    /**
     * 批量发布资料
     *
     * @return
     * @throws GenException
     */
    @ResponseBody
    @RequestMapping(value = "/updateMaterialPublishStatus", produces = "application/json;charset=UTF-8")
    public Map<String, Object> updateMaterialPublishStatus() throws GenException {
        Map<String, Object> map = new HashMap<String, Object>();
        PageData pd = this.getPageData();
        try {
            // 发布
            String publishIds = pd.getString("publishIds");
            if (!"".equals(publishIds) && publishIds != null) {
                String[] pbIds = publishIds.split(",");

                List<String> pbIdsList = Arrays.asList(pbIds);
                pd.put("publishIdList", pbIdsList);

//			Subject subject = SecurityUtils.getSubject();
//			Session session = subject.getSession();
//			User user = (User) session.getAttribute(Const.SESSION_USER);

                User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

                pd.put("modifier", user.getEmployeeId());
                pd.put("modifydate", new Date());
                documentService.updateMaterialPublishStatus(pd);
            }
            // 取消发布
            String not_publishIds = pd.getString("notPublishIds");
            if (!"".equals(not_publishIds) && not_publishIds != null) {
                String[] notPbIds = not_publishIds.split(",");

                List<String> notPbIdsList = Arrays.asList(notPbIds);
                pd.put("notPublishIdList", notPbIdsList);

                documentService.updateMaterialPublishStatusToNot(pd);
            }
            map.put("result", "success");
        } catch (GenException e) {
            map.put("result", "error");
            throw new GenException(e.getCause());
        }
        return map;
    }

    /**
     * 将发布状态转变为不发布
     *
     * @return
     * @throws GenException
     */
    @ResponseBody
    @RequestMapping(value = "/updateMaterialPublishStatusToNot", produces = "application/json;charset=UTF-8")
    public Map<String, Object> updateMaterialPublishStatusToNot() throws GenException {
        Map<String, Object> map = new HashMap<String, Object>();
        PageData pd = this.getPageData();
        try {
            String not_publishIds = pd.getString("notPublishIds");
            if (!"".equals(not_publishIds) && not_publishIds != null) {
                String[] notPbIds = not_publishIds.split(",");

                List<String> notPbIdsList = Arrays.asList(notPbIds);
                pd.put("notPublishIdList", notPbIdsList);

//			Subject subject = SecurityUtils.getSubject();
//			Session session = subject.getSession();
//			User user = (User) session.getAttribute(Const.SESSION_USER);

                User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

                pd.put("modifier", user.getEmployeeId());
                pd.put("modifydate", new Date());
                documentService.updateMaterialPublishStatusToNot(pd);
            }
            map.put("result", "success");
        } catch (GenException e) {
            map.put("result", "error");
            throw new GenException(e.getCause());
        }
        return map;
    }

    /**
     * 发布单条资料
     *
     * @return
     * @throws GenException
     */
    @ResponseBody
    @RequestMapping(value = "/materialpublish", produces = "application/json;charset=UTF-8")
    public Map<String, Object> materialpublish() throws GenException {
        Map<String, Object> map = new HashMap<String, Object>();
        PageData pd = this.getPageData();
        try {
//			Subject subject = SecurityUtils.getSubject();
//			Session session = subject.getSession();
//			User user = (User) session.getAttribute(Const.SESSION_USER);

            User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

            pd.put("modifier", user.getEmployeeId());
            pd.put("modifydate", new Date());
            documentService.materialpublish(pd);
            map.put("result", "success");
        } catch (GenException e) {
            map.put("result", "error");
            throw new GenException(e.getCause());
        }
        return map;
    }

    //保存资料
    @RequestMapping(value = "/materialsave", produces = "application/json;charset=UTF-8")
    public ModelAndView materialsave() throws GenException {
        ModelAndView mv = this.getModelAndView();
        PageData pd = this.getPageData();
        String editDate = pd.getString("editDate");
        if (editDate != null && editDate != "") {
            pd.put("editDate", DateUtil.formatDate(editDate));
        }
        PageData materialInfo = documentService.toMaterialDetail(pd);
        if (materialInfo == null) {
//			Subject subject = SecurityUtils.getSubject();
//			Session session = subject.getSession();
//			User user = (User) session.getAttribute(Const.SESSION_USER);

			User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

            pd.put("editor", user.getEmployeeName());
            pd.put("creater", user.getEmployeeId());
            pd.put("createdate", new Date());
            pd.put("modifier", user.getEmployeeId());
            pd.put("modifydate", new Date());
            documentService.materialsave(pd);
            //增加catalog权限
            pd.put("privilegeId", sequence.nextId());
            pd.put("type", 1);
            documentService.addDocumentPrivilege(pd);
        } else {
            documentService.materialEdit(pd);
        }
        mv.addObject("materialNews", materialInfo);
        mv.setViewName("documentcatalog/document/document_list");
        return mv;
    }


    /**
     * 上传 文件 ， 保存记录
     *
     * @return
     * @throws GenException
     */
    @ResponseBody
    @RequestMapping(value = "/uploadFile", produces = "application/json;charset=UTF-8")
    public Map<String, Object> uploadFile(HttpServletRequest request) throws GenException {
        Map<String, Object> map = new HashMap<String, Object>();
        PageData pd = new PageData();

        String rootPath = request.getServletContext().getRealPath("/");
        String midPath = "/uploadFiles/document";

        String id = get36UUID();
        String materialId = request.getParameter("materialId");
        String catalogId = request.getParameter("catalogId");
        String baseId = request.getParameter("baseId");
        String catalogPath = baseId.replaceAll("-", "/");
        File folder = new File(rootPath + midPath + File.separator + catalogPath);
        // 上传文件
        // 获取 上传的文件
        //将request变成 multiRequest
        MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
        //获取multiRequest 中所有的文件名
        Iterator<String> iter = multiRequest.getFileNames();
        while (iter.hasNext()) {
            //一次遍历所有文件
            MultipartFile file = multiRequest.getFile(iter.next().toString());
            if (file != null) {
                String fileName = file.getOriginalFilename();
                String fileSize = String.valueOf(file.getSize());
                String filePath = folder + File.separator + id + fileName.substring(fileName.lastIndexOf("."));
                File endfolder = new File(filePath);
                //上传
                try {
                    if (!folder.exists()) {
                        folder.mkdirs();
                    }
                    if (!endfolder.exists()) {
                        file.transferTo(endfolder);
                        // 生成缩略图
                        ThumbImage.thumbnailImages(filePath);

                        // 构造普通字段数据
//			Subject subject = SecurityUtils.getSubject();
//			Session session = subject.getSession();
//			User user = (User) session.getAttribute(Const.SESSION_USER);

						User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

                        int fileNo = documentService.getFileNumByMaterailId(pd) + 1;
                        pd.put("fileNo", fileNo);
                        pd.put("uploadDate", new Date());
                        pd.put("id", id);
                        pd.put("fileName", fileName);
                        pd.put("fileSize", fileSize);
                        pd.put("materialId", materialId);
                        pd.put("catalogId", catalogId);

                        pd.put("createdate", new Date());
                        pd.put("creater", user.getEmployeeId());
                        pd.put("modifydate", new Date());
                        pd.put("modifier", user.getEmployeeId());

                        documentService.addFileInfo(pd);
                        map.put("fileInfo", pd);
                        map.put("result", "success");
                    }
                } catch (IllegalStateException e) {
                    map.put("result", "error");
                } catch (IOException e) {
                    map.put("result", "error");
                }
            }
        }
        return map;
    }

    /**
     * 更新文件信息记录 的 资料ID [MATERIALR_ID]
     *
     * @return
     * @throws GenException
     */
    @ResponseBody
    @RequestMapping(value = "/updateMaterialrIdByMaterialrId", produces = "application/json;charset=UTF-8")
    public Map<String, Object> updateMaterialrIdByMaterialrId() throws GenException {
        Map<String, Object> map = new HashMap<String, Object>();
        PageData pd = this.getPageData();
//			Subject subject = SecurityUtils.getSubject();
//			Session session = subject.getSession();
//			User user = (User) session.getAttribute(Const.SESSION_USER);

		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        pd.put("modifier", user.getEmployeeId());
        pd.put("modifydate", new Date());
        try {
            documentService.updateMaterialrIdByMaterialrId(pd);
            map.put("result", "success");
        } catch (GenException e) {
            throw new GenException(e.getCause());
        }
        return map;
    }

    /**
     * 删除文件记录 通过 主键
     *
     * @return
     * @throws GenException
     */
    @ResponseBody
    @RequestMapping(value = "/deleteFileInfoById", produces = "application/json;charset=UTF-8")
    public Map<String, Object> deleteFileInfoById() throws GenException {
        Map<String, Object> map = new HashMap<String, Object>();
        PageData pd = this.getPageData();
//			Subject subject = SecurityUtils.getSubject();
//			Session session = subject.getSession();
//			User user = (User) session.getAttribute(Const.SESSION_USER);

		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        pd.put("modifier", user.getEmployeeId());
        pd.put("modifydate", new Date());
        try {
            documentService.deleteFileInfoById(pd);
            map.put("result", "success");
        } catch (GenException e) {
            throw new GenException(e.getCause());
        }
        return map;
    }

    /**
     * 下载文件
     *
     * @throws GenException
     */
    @RequestMapping(value = "/downloadFile")
    public ModelAndView downloadFile(HttpServletRequest request, HttpServletResponse response) throws IOException, GenException {
        ModelAndView mv = this.getModelAndView();
        PageData pd = this.getPageData();
        try {
            // 得到要下载的文件信息
            PageData fileInfo = documentService.getFileInfoById(pd);
            String fileName = fileInfo.getString("FILE_NAME");
            // 找出文件
            String path = this.getRequest().getServletContext().getRealPath("/uploadFiles/document/");
            String baseId = fileInfo.getString("BASE_ID");
            String catalogPath = baseId.replaceAll("-", "/");
            String filePath = path + File.separator + catalogPath + File.separator + fileInfo.getString("ID") + fileName.substring(fileName.lastIndexOf("."));
            File file = new File(filePath);
            if (!file.exists()) {
//			Subject subject = SecurityUtils.getSubject();
//			Session session = subject.getSession();
//			User user = (User) session.getAttribute(Const.SESSION_USER);

				User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

                pd.put("modifier", user.getEmployeeId());
                pd.put("modifydate", new Date());
                mv.setViewName("attachment/file_404");
                return mv;
            } else {
					/*byte[] data = FileUtil.toByteArray2(filePath);
				    fileName = URLEncoder.encode(fileName, "UTF-8");
				    response.reset();
				    response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
				    response.addHeader("Content-Length", "" + data.length);
				    response.setContentType("application/octet-stream;charset=UTF-8");
				    OutputStream outputStream = new BufferedOutputStream(response.getOutputStream());
				    outputStream.write(data);
				    outputStream.flush();
				    outputStream.close();
				    response.flushBuffer();*/
                FileDownload.fileDownload(response, filePath, fileName);
            }
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
        return null;
    }


}
