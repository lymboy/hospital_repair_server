package com.ruoyi.hospital.controller.documentcatalog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.annotation.Resource;

import com.ruoyi.hospital.controller.base.BaseController;
import com.ruoyi.hospital.entity.Page;
import com.ruoyi.hospital.entity.system.User;
import com.ruoyi.hospital.framework.exception.GenException;
import com.ruoyi.hospital.service.documentcatalog.ResourcesService;
import com.ruoyi.hospital.service.system.resources.ResourcesPrivilegeService;
import com.ruoyi.hospital.util.PageData;
import com.ruoyi.hospital.util.sequence;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import net.sf.json.JSONObject;

/**
 *
 * @author 翠翠
 * @Description 资料权限分配
 *
 */
@Controller
@RequestMapping(value="/resources")
public class ResourcesController extends BaseController {

	@Resource(name="resourcesService")
	private ResourcesService resourcesService;
	@Resource(name="resourcesPrivilegeService")
	private ResourcesPrivilegeService resourcesPrivilegeService;

	/**
	 * 跳转到文件管理页面
	 * @throws GenException
	 */
	@RequestMapping(value="/resourcesPrivilegeManager")
	public ModelAndView catalogPrivilegeManager() throws GenException {
		ModelAndView mv=this.getModelAndView();
		PageData pd=this.getPageData();
		mv.addObject("resourcesId", pd.getString("resourcesId"));
		mv.setViewName("documentcatalog/catalog/catalog_privilege");
		return mv;
	}

	//分页得到下级人员
		@RequestMapping(value="/getAllLowUserlistPage")
		@ResponseBody
		public String getAllLowUserlistPage(Page page) throws GenException{
			JSONObject obj = new JSONObject();
			PageData pd=this.getPageData();
			int start=Integer.valueOf(pd.getString("page"));
			int length=Integer.valueOf(pd.getString("rows"));
			page.setShowCount(length);
			page.setPd(pd);
			page.setCurrentPage(start);

//			Subject subject = SecurityUtils.getSubject();
//			Session session = subject.getSession();
//			User user = (User) session.getAttribute(Const.SESSION_USER);

			User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

			List<PageData> lowUser=null;
			pd.put("nowUserId", user.getEmployeeId());
			try {
				lowUser=resourcesService.getAllLowUser(page);//得到下级
				obj.put("records", page.getTotalResult());//总记录数
				obj.put("page", page.getCurrentPage());//当前页
				obj.put("total", page.getTotalPage());//总页数
				obj.put("rows", lowUser);
			} catch (GenException e) {
				throw new GenException(e.getCause());
			}
			return obj.toString();
		}

		/**
		 * 得到当前资源及其下级
		 * @throws GenException
		 */
		@RequestMapping(value="/getLowResources")
		@ResponseBody
		public Map<String,Object> getLowResources() throws GenException{
			Map<String,Object> map=new HashMap<String,Object>();
			PageData pd=this.getPageData();
			pd.getString("resourcesId");
			List<PageData> lowResourcesCatalog=null;
			List<PageData> resourcesList=null;
			try {
				//resourcesPrivilegeList=resourcesPrivilegeService.getResourcesPrivilegeList(pd);//通过身份id得到资源权限列表

				pd.put("type", 0);//查看下级资源
				resourcesList=resourcesService.getAllRessourcesByIdentity(pd);//通过身份权限表查看资源权限表
				//查询当前资源及其下级
				lowResourcesCatalog=resourcesService.getLowCatalog(pd);
				//遍历选中目录
				for (PageData resources : lowResourcesCatalog) {
					resources.put("checked", false);
					for (PageData resourcesP : resourcesList) {
						if(resources.get("RESOURCES_ID").toString().equals(resourcesP.get("RESOURCES_ID").toString())){
							resources.put("checked", true);
							break;
						}
					}
			}
				map.put("lowResourcesCatalog", lowResourcesCatalog);
				map.put("result", "success");
			}catch(GenException e){
				map.put("result", "error");
				throw new GenException(e.getCause());
			}
			return map;
		}

		/**
		 * 编辑资源权限信息
		 * @throws GenException
		 */
		@RequestMapping(value="/editCatalogPrivilege")
		@ResponseBody
		public Map<String,Object> editResourcesPrivilege() throws GenException{
		 Map<String,Object> map=new HashMap<String, Object>();
		 PageData pd=this.getPageData();
		 try {
			 //jsp页面选中值
			 String resourcesID=pd.getString("resourcesID");//资源权限id
			 List<String> nowResourcesId=new ArrayList<String>();
			 StringTokenizer st=new StringTokenizer(resourcesID,",");
			 while(st.hasMoreTokens()){
				 nowResourcesId.add(String.valueOf(st.nextToken()));
			 }
		 	//获取身份所有资料权限
			pd.put("identityId", pd.getString("identityId"));
			List<PageData> resources=resourcesPrivilegeService.getResourcesPrivilegeList(pd);
			List<String> mIds=new ArrayList<String>();
			for(PageData m: resources){
				mIds.add(String.valueOf(m.getString("ID")));//资源权限id
			}
			//判断之前的身份是否有资源权限
			for(String resourcesId : nowResourcesId){
					if(!mIds.contains(resourcesId)){
						pd.put("resourcesPrivilegeId", resourcesId);//资源权限id
						pd.put("id", sequence.nextId());
						//添加资源权限
						//resourcesPrivilegeService.addResourcesP(pd);//添加资源权限
						pd.put("identityId", pd.getString("identityId"));
						resourcesPrivilegeService.addResourcesPrivilege(pd);//添加身份资源权限
					}
			}
			for(String mid : mIds){
				if(!nowResourcesId.contains(mIds)){
					//删除现在没有的资源权限
					pd.put("id", mid);
					resourcesPrivilegeService.deleteResourcesPrivilege(pd);
				}
			}
			map.put("result", "success");
		 } catch (GenException e) {
			 map.put("result", "error");
			throw new GenException(e.getCause());
		}
		return map;
		}

		/**
		 * 文件资源权限管理
		 */
		@RequestMapping(value="/docPrivilegeManager")
		public ModelAndView docPrivilegeManager(){
			ModelAndView mv=this.getModelAndView();
			PageData pd=this.getPageData();
			mv.addObject("resourcesId", pd.getString("catalogId"));
			mv.setViewName("documentcatalog/document/document_privilege");
			return mv;
		}

		/**
		 * 得到当前资源文件
		 * @throws GenException
		 */
		@RequestMapping(value="/getLowDocResources")
		@ResponseBody
		public Map<String,Object> getLowDocResources() throws GenException{
			Map<String,Object> map=new HashMap<String,Object>();
			PageData pd=this.getPageData();

			List<PageData> lowResourcesCatalog=null;
			List<PageData> lowResourcesDocument=null;
			List<PageData> resourcesPrivilegeList=null;
			List<PageData> resourcesList=null;
			try {
				//resourcesPrivilegeList=resourcesPrivilegeService.getResourcesPrivilegeList(pd);//通过身份id得到资源权限列表
				pd.put("type", 1);
				resourcesList=resourcesService.getAllRessourcesByIdentity(pd);
				//查询当前资源及其下级
				//lowResourcesCatalog=resourcesService.getLowCatalog(pd);
				pd.put("lowrId", pd.getString("resourcesId"));
				lowResourcesDocument=resourcesService.getAllLowDocument(pd);

				//遍历选中目录
				for (PageData resources : lowResourcesDocument) {
					resources.put("checked", false);
					for (PageData resourcesP : resourcesList) {
						if(resources.get("RESOURCES_ID").toString().equals(resourcesP.get("RESOURCES_ID").toString())){
							resources.put("checked", true);
							break;
						}
					}
			}
				map.put("lowResourcesDocument", lowResourcesDocument);
				map.put("result", "success");
			}catch(GenException e){
				map.put("result", "error");
				throw new GenException(e.getCause());
			}
			return map;
		}

}
