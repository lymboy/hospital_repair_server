package com.ruoyi.hospital.controller.attendance;

import com.ruoyi.hospital.controller.base.BaseController;
import com.ruoyi.hospital.entity.system.User;
import com.ruoyi.hospital.framework.exception.GenException;
import com.ruoyi.hospital.service.attendance.AttendanceSignTimeService;
import com.ruoyi.hospital.service.currency.CurrencyTypeService;
import com.ruoyi.hospital.util.PageData;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p> 考勤时间设置 </p>
 *
 * @author Young
 * @since 2018/4/2
 */
@Controller
@RequestMapping("attendance_sign_time")
public class AttendanceSignTimeController extends BaseController {

    @Autowired
    private AttendanceSignTimeService attendanceSignTimeService;
    @Autowired
    private CurrencyTypeService currencyTypeService;

    /**
     * 转到 考勤时间设置
     * @return
     * @throws GenException
     */
    @RequestMapping(value="list", produces="text/html;charset=UTF-8")
    public ModelAndView list() throws GenException{
        ModelAndView mv = this.getModelAndView();
        PageData pd = this.getPageData();
        // 校验 初始化考勤时间设置
        // 根据原始数据初始化 基础数据，已有不改变， 添加缺失的，删除多余的
        List<PageData> currencyTypeList = currencyTypeService.getCurrencyType("23"); // 签到行数据原始信息
//		Subject subject = SecurityUtils.getSubject();
//		Session session = subject.getSession();
//		User user = (User) session.getAttribute(Const.SESSION_USER);

        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        boolean b = attendanceSignTimeService.checkInitAttendanceSignTime(currencyTypeList, String.valueOf(user.getUserId()));

        mv.addObject("pd", pd);
        mv.setViewName("attendance/attendance_sign_time_list");
        return mv;
    }

    /**
     * 获取 考勤时间设置
     * @throws GenException
     */
    @RequestMapping(value="attendance_sign_time_list",produces="text/html;charset=UTF-8")
    public @ResponseBody
    String getAttendanceSignTimeList() throws GenException {
        PageData pd = this.getPageData();
        JSONObject obj = new JSONObject();
        List<PageData> list = attendanceSignTimeService.getAttendanceSignTimeList(pd);
        obj.put("rows", list);
        return obj.toString();
    }

    /**
     * 新增 或 编辑
     * @return
     * @throws GenException
     */
    @RequestMapping(value="update_attendance_sign_time", produces="application/json;charset=UTF-8")
    public @ResponseBody
    Map<String, Object> updateAttendanceSignTime() throws GenException {
        try {
            Map<String, Object> map = new HashMap<String, Object>();
            PageData pd = this.getPageData();
//		Subject subject = SecurityUtils.getSubject();
//		Session session = subject.getSession();
//		User user = (User) session.getAttribute(Const.SESSION_USER);

            User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

            pd.put("MODIFIER", user.getUserId());
            pd.put("MODIFYDATE", new Date());
            int rows = attendanceSignTimeService.updateAttendanceSignTime(pd);
            if (rows > 0) {
                map.put("data", pd);
                map.put("result", "success");
            } else {
                map.put("data", pd);
                map.put("result", "error");
            }
            return map;
        } catch (GenException e) {
            throw new GenException(e.getCause());
        }
    }



}
