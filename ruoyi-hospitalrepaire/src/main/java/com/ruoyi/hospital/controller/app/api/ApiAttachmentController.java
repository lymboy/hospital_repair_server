package com.ruoyi.hospital.controller.app.api;

import com.ruoyi.hospital.controller.base.BaseController;
import com.ruoyi.hospital.entity.system.User;
import com.ruoyi.hospital.framework.context.MySessionContext;
import com.ruoyi.hospital.service.attachment.AttachmentService;
import com.ruoyi.hospital.util.Const;
import com.ruoyi.hospital.util.FileDownload;
import com.ruoyi.hospital.util.PageData;
import com.ruoyi.hospital.util.ThumbImage;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * <p> 附件 </p>
 *
 * @author Young
 * @since 2018-04-14
 */
@Slf4j
@Controller
@RequestMapping(value = "/api/attachment")
public class ApiAttachmentController extends BaseController {

    @Resource(name = "attachmentService")
    private AttachmentService attachmentService;

    /**
     * 上传
     */
    @RequestMapping(value = "/upload", produces = "application/json;charset=UTF-8")
    public @ResponseBody
    String upload(HttpServletRequest request) {
        String sessionId = request.getParameter("sessionId");
        JSONObject obj = new JSONObject();
        MySessionContext mysc = MySessionContext.getInstance();//通过sessionId得到session
        HttpSession session = mysc.getSession(sessionId);
        if (session != null) {
            try {
                List<PageData> fileList = new ArrayList<>();
                String rootPath = request.getServletContext().getRealPath("/");
                String midPath = "/uploadFiles/attachment/";
                String subSystem = request.getParameter("subSystem");
                String moduleName = request.getParameter("moduleName");
                String masterId = request.getParameter("masterId");
                String masterType = request.getParameter("masterType");
                String fileUrl = subSystem + File.separator + moduleName + File.separator + masterId;
                File folder = new File(rootPath + midPath + fileUrl);
                // 上传文件
                // 获取 上传的文件
                //将request变成 multiRequest
                MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
                //获取multiRequest 中所有的文件名
                Iterator<String> iter = multiRequest.getFileNames();
                while (iter.hasNext()) {
                    //一次遍历所有文件
                    MultipartFile file = multiRequest.getFile(iter.next().toString());
                    if (file != null) {
                        String id = get36UUID();
                        String fileName = file.getOriginalFilename();
                        String fileSize = String.valueOf(file.getSize());
                        String filePath = folder + File.separator + id + fileName.substring(fileName.lastIndexOf("."));
                        File endfolder = new File(filePath);
                        //上传
                        if (!folder.exists()) {
                            folder.mkdirs();
                        }
                        if (!endfolder.exists()) {
                            PageData pd = new PageData();
                            file.transferTo(endfolder);
                            // 生成缩略图
                            ThumbImage.thumbnailImages(filePath);

                            // 构造普通字段数据
                            User user = (User) session.getAttribute(Const.SESSION_USER);
                            pd.put("creater", user.getUserId());
                            pd.put("createdate", new Date());
                            pd.put("modifier", user.getUserId());
                            pd.put("modifydate", new Date());
                            pd.put("masterId", masterId);
                            int fileNo = attachmentService.getFileNumByMasterId(pd) + 1;
                            pd.put("fileNo", fileNo);
                            pd.put("uploadDate", new Date());
                            pd.put("id", id);
                            pd.put("fileName", fileName);
                            pd.put("fileSize", fileSize);
                            pd.put("subSystem", subSystem);
                            pd.put("moduleName", moduleName);
                            pd.put("masterId", masterId);
                            pd.put("masterType", masterType);
                            attachmentService.addFileInfo(pd);
                            pd.put("FILE_URL", midPath + fileUrl + "/" + id + fileName.substring(fileName.lastIndexOf(".")));
                            pd.put("FILE_URL_big", midPath + fileUrl + "/thumb_800_600" + "/" + id + fileName.substring(fileName.lastIndexOf(".")));
                            pd.put("FILE_URL_small", midPath + fileUrl + "/thumb_80_60" + "/" + id + fileName.substring(fileName.lastIndexOf(".")));
                            fileList.add(pd);
                        }
                    }
                }
                obj.put("fileList", fileList);
                obj.put("result", "success");
            } catch (Exception e) {
                obj.put("result", "error");
                obj.put("msg", "error");
                obj.put("error_desc", e.getMessage());
                log.error(e.getMessage());
            }
        } else {
            obj.put("result", "error");
            obj.put("msg", "sessionError"); // sessionError：没有登陆
        }
        return obj.toString();
    }

    /**
     * 删除文件记录 通过 主键
     *
     * @param sessionId
     * @param id        文件记录ID
     */
    @ResponseBody
    @RequestMapping(value = "/delete", produces = "application/json;charset=UTF-8")
    public String delete(@RequestParam(required = true) String sessionId,
                  @RequestParam(required = true) String id) {
        JSONObject obj = new JSONObject();
        MySessionContext mysc = MySessionContext.getInstance();//通过sessionId得到session
        HttpSession session = mysc.getSession(sessionId);
        if (session != null) {
            try {
                PageData pd = new PageData();
                pd.put("id", id);
                User user = (User) session.getAttribute(Const.SESSION_USER);
                pd.put("modifier", user.getUserId());
                pd.put("modifydate", new Date());
                attachmentService.deleteFileInfoById(pd);
                obj.put("result", "success");
            } catch (Exception e) {
                obj.put("result", "error");
                obj.put("msg", "error");
                obj.put("error_desc", e.getMessage());
                log.error(e.getMessage());
            }
        } else {
            obj.put("result", "error");
            obj.put("msg", "sessionError"); // sessionError：没有登陆
        }
        return obj.toString();
    }

    /**
     * 下载
     *
     * @param sessionId
     * @param id        文件记录ID
     */
    @RequestMapping(value = "/download", produces = "application/json;charset=UTF-8")
    public String download(@RequestParam(required = true) String sessionId,
                           @RequestParam(required = true) String id,
                           HttpServletRequest request, HttpServletResponse response) {
        JSONObject obj = new JSONObject();
        MySessionContext mysc = MySessionContext.getInstance();//通过sessionId得到session
        HttpSession session = mysc.getSession(sessionId);
        if (session != null) {
            try {
                PageData pd = new PageData();
                pd.put("id", id);
                // 得到要下载的文件信息
                PageData fileInfo = attachmentService.getFileInfoById(pd);
                String fileUrl = fileInfo.getString("FILE_URL");
                String fileName = fileInfo.getString("FILE_NAME");
                // 找出文件
                String path = this.getRequest().getServletContext().getRealPath("/uploadFiles/attachment/");
                String filePath = path + File.separator + fileUrl + File.separator + fileInfo.getString("ID") + fileName.substring(fileName.lastIndexOf("."));
                File file = new File(filePath);
                if (!file.exists()) {
                    obj.put("result", "error");
                    obj.put("msg", "notExists");
                } else {
                    FileDownload.fileDownload(response, filePath, fileName);
                    obj.put("result", "success");
                }
            } catch (Exception e) {
                obj.put("result", "error");
                obj.put("msg", "error");
                obj.put("error_desc", e.getMessage());
                log.error(e.getMessage());
            }
        } else {
            obj.put("result", "error");
            obj.put("msg", "sessionError"); // sessionError：没有登陆
        }
        return obj.toString();
    }

}
