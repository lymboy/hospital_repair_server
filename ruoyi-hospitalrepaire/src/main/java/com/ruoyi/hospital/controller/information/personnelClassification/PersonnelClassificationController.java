package com.ruoyi.hospital.controller.information.personnelClassification;

import com.ruoyi.hospital.controller.base.BaseController;
import com.ruoyi.hospital.entity.Page;
import com.ruoyi.hospital.framework.exception.GenException;
import com.ruoyi.hospital.service.information.PersonnelClassificationService;
import com.ruoyi.hospital.util.PageData;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(value = "/personnelClassification")
public class PersonnelClassificationController extends BaseController {
    @Resource(name = "personnelClassificationService")
    private PersonnelClassificationService personnelClassificationService;

    /**
     * 跳到人员分类页面
     *
     * @return
     */
    @RequestMapping(value = "/toPersonnelClassificationlist")
    public ModelAndView toPersonnelClassificationlist() {
        ModelAndView mv = this.getModelAndView();
        mv.setViewName("information/personnelClassification/listPersonnelClassification");
        return mv;
    }

    /**
     * 查询人员分类
     *
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getPersonnelClassification", produces = "application/json;charset=UTF-8")
    public String getPersonnelClassification() {
        PageData pd = this.getPageData();
        List<PageData> pds = null;
        int cnt = 0;
        JSONObject obj = new JSONObject();

        try {
            cnt = personnelClassificationService.getEmployeeCNT(pd);
            pds = personnelClassificationService.getPersonnelClassification(pd);

            PageData pdc = new PageData();
            pdc.put("EVALUATION_GRADE_NEWEST", "浙江交工大桥分公司");
            pdc.put("CNT", cnt);
            pdc.put("level", 1);
            pdc.put("proportion", "100%");
            pdc.put("expanded", true);
            pdc.put("loaded", true);
            pdc.put("isLeaf", false);
            pdc.put("parent", "0");
            pdc.put("NUM", cnt);
            for (int i = 0; i < pds.size(); i++) {
                PageData pda = pds.get(i);
                int level = 2;
                pda.put("level", level);
                pda.put("expanded", false);
                pda.put("loaded", true);
                pda.put("isLeaf", true);
                String EVALUATION_GRADE_NEWEST = pda.getString("EVALUATION_GRADE_NEWEST");
                pda.put("EVALUATION_GRADE_NEWEST", EVALUATION_GRADE_NEWEST + "类人员");
                double proportion = (Integer.valueOf(pda.getString("CNT")) * 1.0 / cnt);
                int proportion1 = (int) Math.round(proportion * 100);
                pda.put("proportion", proportion1 + "%");
                pda.put("parent", pdc.getString("EVALUATION_GRADE_NEWEST"));
                pda.put("NUM", cnt);
            }

            List<PageData> pdList = new ArrayList<PageData>();
            pdList.add(pdc);
            pdList.addAll(pds);
            obj.put("rows", pdList);

        } catch (GenException e) {
            e.printStackTrace();
        }
        return obj.toString();
    }

    /**
     * 获取人员分类饼状图
     *
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getFlotData", produces = "application/json;charset=UTF-8")
    public String getFlotData() {
        PageData pd = this.getPageData();
        int cnt = 0;
        List<PageData> pds = new ArrayList<PageData>();
        List<PageData> pdList = new ArrayList<PageData>();

        JSONObject obj = new JSONObject();

        try {
            cnt = personnelClassificationService.getEmployeeCNT(pd);
            pds = personnelClassificationService.getPersonnelClassification(pd);

            if (pds != null && pds.size() > 0) {
                for (int i = 0; i < pds.size(); i++) {
                    PageData pda = pds.get(i);
                    double proportion = (Integer.valueOf(pda.getString("CNT")) * 1.0 / cnt);
                    //int proportion1 = (int) Math.round(proportion*100);
                    //pda.put("proportion", proportion1+"%");
                    String EVALUATION_GRADE_NEWEST = pda.getString("EVALUATION_GRADE_NEWEST");
                    pda.put("label", EVALUATION_GRADE_NEWEST + "类人员");
                    pda.put("data", proportion);
                    if ("A".equals(EVALUATION_GRADE_NEWEST)) {
                        pda.put("color", "#4E7FBB");
                    } else if ("B".equals(EVALUATION_GRADE_NEWEST)) {
                        pda.put("color", "#BD4E4C");
                    } else if ("C".equals(EVALUATION_GRADE_NEWEST)) {
                        pda.put("color", "#99B958");
                    }

                }
                pdList.addAll(pds);
            }

        } catch (GenException e) {
            e.printStackTrace();
        }

        if (pdList != null && pdList.size() > 0) {
            JSONArray json = JSONArray.fromObject(pdList);
            return json.toString();
        } else {
            return null;
        }
    }

    /**
     * 跳到人员列表页面
     *
     * @return
     */
    @RequestMapping(value = "/toEmployeeList")
    public ModelAndView toEmployeeList() {
        PageData pd = this.getPageData();
        ModelAndView mv = this.getModelAndView();
        String EVALUATION_GRADE_NEWEST = pd.getString("EVALUATION_GRADE_NEWEST");
        String POST_ID = pd.getString("POST_ID");
        String YEAR = pd.getString("YEAR");
        mv.addObject("EVALUATION_GRADE_NEWEST", EVALUATION_GRADE_NEWEST);
        mv.addObject("POST_ID", POST_ID);
        mv.addObject("YEAR", YEAR);
        mv.setViewName("information/personnelClassification/employee_list");
        return mv;
    }

    /**
     * 分页获取人员信息列表
     *
     * @param page
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getEmployeeListPage", produces = "application/json;charset=UTF-8")
    public String getEmployeeListPage(Page page)//{POST_ID=null, EVALUATION_GRADE_NEWEST=A, YEAR=null, nd=1489815742888, departmentId=undefined, page=1, sord=asc, rows=10, _search=false, sidx=}
    {
        PageData pd = this.getPageData();
        List<PageData> pds = null;
        JSONObject obj = new JSONObject();

        int start = Integer.valueOf(pd.getString("page"));//当前页
        int length = Integer.valueOf(pd.getString("rows"));//每页显示的记录数
        page.setCurrentPage(start);
        page.setShowCount(length);
        page.setPd(pd);
        try {
            pds = personnelClassificationService.getEmployeeListPage(page);
            obj.put("records", page.getTotalResult());//总记录数
            obj.put("page", page.getCurrentPage());//当前页
            obj.put("total", page.getTotalPage());//总页数
            obj.put("rows", pds);

        } catch (GenException e) {
            e.printStackTrace();
        }
        return obj.toString();
    }
}
