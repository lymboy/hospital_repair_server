package com.ruoyi.hospital.service.system.resources;

import com.ruoyi.hospital.dao.DaoSupport;
import com.ruoyi.hospital.framework.exception.GenException;
import com.ruoyi.hospital.util.PageData;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service(value="resourcesPrivilegeService")
public class ResourcesPrivilegeService {

	@Resource(name = "daoSupport")
	private DaoSupport dao;

	//目录列表
	@SuppressWarnings("unchecked")
	public List<PageData> getCatalogList() throws GenException {
		List<PageData> catalogList=null;
		try {
			catalogList=(List<PageData>) dao.findForList("ResourcesPrivilegeMapper.getCatalogList", null);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return catalogList;
	}

	//文件列表
	@SuppressWarnings("unchecked")
	public List<PageData> getDocumentList() throws GenException {
		List<PageData> documentList=null;
		try {
			documentList=(List<PageData>) dao.findForList("ResourcesPrivilegeMapper.getDocumentList", null);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return documentList;
	}

	//通过身份id得到资源权限列表
	@SuppressWarnings("unchecked")
	public List<PageData> getResourcesPrivilegeList(PageData pd) throws GenException {
		List<PageData> resourcesPrivilegeList=null;
		try {
			resourcesPrivilegeList=(List<PageData>) dao.findForList("ResourcesPrivilegeMapper.getResourcesPrivilegeList", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return resourcesPrivilegeList;
	}

	//添加资源权限
	public void addResourcesPrivilege(PageData pd) throws GenException {
		try {
			dao.save("ResourcesPrivilegeMapper.addResourcesPrivilege", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}

	//删除资源权限
	public void deleteResourcesPrivilege(PageData pd) throws GenException {
		try {
			dao.delete("ResourcesPrivilegeMapper.deleteResourcesPrivilege", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}

	//得到目录的下级
	private List<String> getAllCatalogLowId(PageData pd) throws GenException{
		List<String> catalogLowId=null;
		try {
			catalogLowId=(List<String>) dao.findForList("ResourcesPrivilegeMapper.getAllCatalogLowId", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return catalogLowId;
	}

	//得到目录的id
	public List<String> getAllCatalogId(PageData pd) throws GenException{
		List<String> catalogId=new ArrayList<String>();
		List<String> catalogLowId=null;
		List<String> catalogHeigthId=null;
		List<String> catalogHeigthIdList=new ArrayList<String>();
		try {
			catalogLowId=this.getAllCatalogLowId(pd);
			for(String cataId : catalogLowId){
				pd.put("PARENT_CATALOG_ID", cataId);
				//递归得到上级
				catalogHeigthId=this.getAllCatalogHeigthId(pd,catalogHeigthIdList);
				catalogId.addAll(catalogHeigthId);
				catalogId.addAll(catalogLowId);
				return catalogId;
			}
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return catalogId;
	}

	//递归得到目录的上级
	private List<String> getAllCatalogHeigthId(PageData pd,List<String> catalogHeigthIdList) throws GenException {
		String catalogHeigthId=new String();
		//List<String> catalogHeigthIdList=new ArrayList<String>();
		//通过上级id得到其上级的基本信息
		try {
			catalogHeigthId=(String) dao.findForObject("ResourcesPrivilegeMapper.getAllCatalogHeigthId", pd);
			if(catalogHeigthId !=null){
				pd.put("PARENT_CATALOG_ID", catalogHeigthId);
				catalogHeigthIdList.add(catalogHeigthId);
				this.getAllCatalogHeigthId(pd,catalogHeigthIdList);
			}

		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return catalogHeigthIdList;
	}

	//通过身份id所有
	public List<PageData> getAllDocument(PageData pd) throws GenException {
		List<PageData> documentList=null;
		try {
			documentList=(List<PageData>) dao.findForList("ResourcesPrivilegeMapper.getAllDocumentId", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return documentList;
	}

	//添加资源权限
	public void addResourcesP(PageData pd) throws GenException {
		try {
			dao.save("ResourcesPrivilegeMapper.addResourcesP", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}

}
