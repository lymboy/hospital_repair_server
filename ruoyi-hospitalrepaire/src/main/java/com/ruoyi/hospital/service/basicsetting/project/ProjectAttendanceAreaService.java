package com.ruoyi.hospital.service.basicsetting.project;

import com.ruoyi.hospital.dao.DaoSupport;
import com.ruoyi.hospital.entity.Page;
import com.ruoyi.hospital.framework.exception.GenException;
import com.ruoyi.hospital.util.PageData;
import com.ruoyi.hospital.util.UuidUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>  </p>
 *
 * @author Young
 * @since 2018-04-29
 */
@Service
public class ProjectAttendanceAreaService {

    @Resource(name="daoSupport")
    private DaoSupport dao;

    /**
     *
     * @param pd  PROJECT_ID
     * @return
     * @throws GenException
     */
    public List<PageData> getProjectAttendanceAreaListByProjectId(PageData pd) throws GenException {
        try {
            return (List<PageData>) dao.findForList("ProjectAttendanceAreaMapper.getProjectAttendanceAreaListByProjectId", pd);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     *
     * @param ID  ID
     * @return
     * @throws GenException
     */
    public PageData getProjectAttendanceAreaById(String ID) throws GenException {
        try {
            return (PageData) dao.findForObject("ProjectAttendanceAreaMapper.getProjectAttendanceAreaById", ID);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     * 新增 或 修改
     * @param pd
     * @return
     * @throws GenException
     */
    public int insertOrUpdateProjectAttendanceArea(PageData pd) throws GenException {
        try {
            String ID = String.valueOf(pd.get("ID"));
            if (ID == null || "".equals(ID)) {
                return -1;
            }
            if (getProjectAttendanceAreaById(ID) == null) {
                return (int) dao.update("ProjectAttendanceAreaMapper.insertProjectAttendanceArea", pd);
            }
            // 编辑
            return (int) dao.update("ProjectAttendanceAreaMapper.updateProjectAttendanceArea", pd);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     * 新增 或 修改
     * @param pd
     * @return
     * @throws GenException
     */
    public int deleteProjectAttendanceArea(PageData pd) throws GenException {
        try {
            String ID = String.valueOf(pd.get("ID"));
            if (ID == null || "".equals(ID)) {
                return -1;
            }
            // 编辑
            return (int) dao.update("ProjectAttendanceAreaMapper.deleteProjectAttendanceArea", pd);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    public int batchDeleteProjectAttendanceArea(String[] ids, String userId) throws GenException {
        try {
            int rows = 0;
            if (ids == null || ids.length == 0) {
                return rows;
            }
            for (int i = 0; i < ids.length; i++) {
                String ID = ids[i];
                PageData pd = new PageData();
                pd.put("ID", ID);
                pd.put("MODIFIER", userId);
                pd.put("MODIFYDATE", new Date());
                rows += (int) dao.save("ProjectAttendanceAreaMapper.deleteProjectAttendanceArea", pd);
            }
            return rows;
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    public int saveAttendanceAreaDept(String projectAttendanceAreaId, String[] deptIds, String userId) throws GenException {
        int rows = 0;
        try {
            // 删除原有的
            rows += (int) dao.delete("ProjectAttendanceAreaMapper.deleteAttendanceAreaDept", projectAttendanceAreaId);
            if (deptIds != null) {
                // 添加新的
                for (int i = 0; i < deptIds.length; i++) {
                    Map<String, Object> param = new HashMap<>();
                    param.put("ID", UuidUtil.get32UUID());
                    param.put("PROJECT_ATTENDANCE_AREA_ID", projectAttendanceAreaId);
                    param.put("DEPARTMENT_ID", deptIds[i]);
                    rows += (int) dao.save("ProjectAttendanceAreaMapper.insertAttendanceAreaDept", param);
                }
            }
            return rows;
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }
    public List<String> getProjectAttendanceAreaDeptIdsList(String projectAttendanceAreaId) throws GenException {
        try {
            return (List<String>) dao.findForList("ProjectAttendanceAreaMapper.getProjectAttendanceAreaDeptIdsList", projectAttendanceAreaId);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }


    /**
     *
     * @param pd  PROJECT_ATTENDANCE_AREA_ID
     * @return
     * @throws GenException
     */
    public List<PageData> getProjectAttendanceAreaDetailList(PageData pd) throws GenException {
        try {
            return (List<PageData>) dao.findForList("ProjectAttendanceAreaMapper.getProjectAttendanceAreaDetailList", pd);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    public List<PageData> getProjectAttendanceAreaDetailList1(PageData pd) throws GenException {
        try {
            return (List<PageData>) dao.findForList("ProjectAttendanceAreaMapper.getProjectAttendanceAreaDetailList1", pd);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     *
     * @param page  PROJECT_ATTENDANCE_AREA_ID
     * @return
     * @throws GenException
     */
    public List<PageData> getProjectAttendanceAreaDetaillistPage(Page page) throws GenException {
        try {
            return (List<PageData>) dao.findForList("ProjectAttendanceAreaMapper.getProjectAttendanceAreaDetaillistPage", page);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     * 从表 BY ID
     * @param ID
     * @return
     * @throws GenException
     */
    public PageData getProjectAttendanceAreaDetailById(String ID) throws GenException {
        try {
            return (PageData) dao.findForObject("ProjectAttendanceAreaMapper.getProjectAttendanceAreaDetailById", ID);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    public int insertOrUpdateProjectAttendanceAreaDetail(PageData pd, String userId) throws GenException {
        try {
            String ID = String.valueOf(pd.get("ID"));
            if (ID == null || "".equals(ID)) {
                ID = UuidUtil.get32UUID();
                pd.put("ID", ID);
            }
            if (getProjectAttendanceAreaDetailById(ID) == null) {
                // 新增
                pd.put("CREATER", userId);
                pd.put("CREATEDATE", new Date());
                return (int) dao.save("ProjectAttendanceAreaMapper.insertProjectAttendanceAreaDetail", pd);
            }
            pd.put("MODIFIER", userId);
            pd.put("MODIFYDATE", new Date());
            return (int) dao.save("ProjectAttendanceAreaMapper.updateProjectAttendanceAreaDetail", pd);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    public int batchDeleteProjectAttendanceAreaDetail(String[] ids, String userId) throws GenException {
        try {
            int rows = 0;
            if (ids == null || ids.length == 0) {
                return rows;
            }
            for (int i = 0; i < ids.length; i++) {
                String ID = ids[i];
                PageData pd = new PageData();
                pd.put("ID", ID);
                pd.put("MODIFIER", userId);
                pd.put("MODIFYDATE", new Date());
                rows += (int) dao.save("ProjectAttendanceAreaMapper.deleteProjectAttendanceAreaDetail", pd);
            }
            return rows;
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    public int batchInsertProjectAttendanceAreaDetail(List<PageData> pds, String userId) throws GenException {
        try {
            int rows = 0;
            if (pds == null || pds.size() == 0) {
                return 0;
            }
            for (int i = 0; i < pds.size(); i++) {
                PageData pd = pds.get(i);
                // 新增
                pd.put("ID", UuidUtil.get32UUID());
                pd.put("CREATER", userId);
                pd.put("CREATEDATE", new Date());
                rows += (int) dao.save("ProjectAttendanceAreaMapper.insertProjectAttendanceAreaDetail", pd);
            }
            return rows;
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    public List<PageData> getProjectAttendanceAreaListByDeptId(String departmentId) throws GenException {
        try {
            List<PageData> areaList = (List<PageData>) dao.findForList("ProjectAttendanceAreaMapper.getProjectAttendanceAreaListByDeptId", departmentId);
            if (areaList != null && areaList.size()>0) {
                for (int i = 0; i < areaList.size(); i++) {
                    PageData data = areaList.get(i);
                    PageData pdd = new PageData();
                    pdd.put("PROJECT_ATTENDANCE_AREA_ID", data.get("ID"));
                    List<PageData> detailList = getProjectAttendanceAreaDetailList(pdd);
                    data.put("pointList", detailList);
                }
            }
            return areaList;
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }
}
