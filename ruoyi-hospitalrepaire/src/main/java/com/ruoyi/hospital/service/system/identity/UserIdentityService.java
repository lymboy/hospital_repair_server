package com.ruoyi.hospital.service.system.identity;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.ruoyi.hospital.dao.DaoSupport;
import com.ruoyi.hospital.entity.Page;
import com.ruoyi.hospital.entity.project.Project;
import com.ruoyi.hospital.entity.system.PrivilegeButton;
import com.ruoyi.hospital.entity.system.UserIdentity;
import com.ruoyi.hospital.framework.exception.GenException;
import com.ruoyi.hospital.framework.log.Log;
import com.ruoyi.hospital.util.PageData;
import com.ruoyi.hospital.util.sequence;

/**
 * <p>Title: UserIdentityService</p>
 * @Desc: 用户身份管理
 * @author: 杨建军
 *	@Date: 2017年3月28日
 * @Time: 下午7:53:46
 * @version:
 */
@Service(value="userIdentityService")
public class UserIdentityService {

	@Resource(name = "daoSupport")
	private DaoSupport dao;

	/**
	 * 获取用户身份列表
	 * @throws GenException
	 */
	public List<PageData> getIdentityByUserId(PageData pd) throws GenException{
		List<PageData> pdList = null;
		try {
			pdList = (List<PageData>) dao.findForList("UserIdentityMapper.getIdentityByUserId", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pdList;
	}
	/**
	 * 获取用户默认身份
	 * @throws GenException
	 */
	public UserIdentity getDefaultIdentityByUserId(PageData pd) throws GenException{
		UserIdentity dentity = null;
		try {
			dentity = (UserIdentity) dao.findForObject("UserIdentityMapper.getDefaultIdentityByUserId", pd);
			if(dentity==null){
				dentity = (UserIdentity) dao.findForObject("UserIdentityMapper.getLastIdentityByUserId", pd);
			}
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return dentity;
	}

	/**
	 * 获取用户身份详细信息
	 * @throws GenException
	 */
	public UserIdentity getIdentityInfoByIdentityId(PageData pd) throws GenException{
		UserIdentity dentity = null;
		try {
			dentity = (UserIdentity) dao.findForObject("UserIdentityMapper.getIdentityInfoByIdentityId", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return dentity;
	}

	/**
	 * 分页  获取用户身份列表
	 * @throws GenException
	 */
	public List<PageData> getIdentityByUserIdlistPage(Page page) throws GenException{
		List<PageData> pdList = null;
		try {
			pdList = (List<PageData>) dao.findForList("UserIdentityMapper.getIdentityByUserIdlistPage", page);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pdList;
	}

	/**
	 * 获取用户列表
	 * @param page
	 * @return
	 * @throws GenException
	 */
	public List<PageData> getUserlistPage(Page page) throws GenException {
		List<PageData> pdList = null;
		try {
			pdList = (List<PageData>) dao.findForList("UserIdentityMapper.getUserlistPage", page);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pdList;
	}

	/**
	 * 删除 用户 身份
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	@Log(name="删除用户 身份-deleteUserIdentityById")
	public int deleteUserIdentityById(PageData pd) throws GenException {
		int num = 0;
		try {
			num = (Integer) dao.delete("UserIdentityMapper.deleteUserIdentityById", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return num;
	}
	/**
	 * 新增 用户 身份
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	@Log(name="新增用户 身份-addUserIdentity")
	public int addUserIdentity(PageData pd) throws GenException {
		int num = 0;
		try {
			num = (Integer) dao.save("UserIdentityMapper.addUserIdentity", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return num;
	}

	/**
	 * 获取 员工信息 BY userId
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public PageData getEmployeeInfoByUserId(PageData pd) throws GenException {
		PageData pdInfo = null;
		try {
			pdInfo = (PageData) dao.findForObject("UserIdentityMapper.getEmployeeInfoByUserId", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pdInfo;
	}

	/**
	 * 获取 用户身份个数
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public int getUserIdentityNum(PageData pd) throws GenException {
		int num = 0;
		try {
			num = (Integer) dao.findForObject("UserIdentityMapper.getUserIdentityNum", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return num;
	}

	/**
	 * 根据身份ID , 获取 对应的角色
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public List<PageData> getIdentityRoleList(PageData pd) throws GenException {
		List<PageData> pdList = null;
		try {
			pdList = (List<PageData>) dao.findForList("UserIdentityMapper.getIdentityRoleList", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pdList;
	}

	/**
	 * 获取所有角色
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public List<PageData> getAllRoleListByType(PageData pd) throws GenException {
		List<PageData> pdList = null;
		try {
			pdList = (List<PageData>) dao.findForList("UserIdentityMapper.getAllRoleListByType", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pdList;
	}
	/**
	 * 获取身份所拥有的角色
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public List<PageData> getIdentityRoleListByIdentityId(PageData pd) throws GenException {
		List<PageData> pdList = null;
		try {
			pdList = (List<PageData>) dao.findForList("UserIdentityMapper.getIdentityRoleListByIdentityId", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pdList;
	}

	/**
	 *  更新 用户身份 的角色
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public void updateIdentityRole(PageData pd) throws GenException{
		try {
			dao.delete("UserIdentityMapper.deleteIdentityRole", pd);// 先删， 后增
			String idstr = pd.getString("idstr");
			if(idstr!=null && !"".equals(idstr)){
				String[] idArr = idstr.split(",");
				for (String roleId : idArr) {
					pd.put("roleId", roleId);
					pd.put("identityRoleId", sequence.nextId());
					dao.save("UserIdentityMapper.addIdentityRole", pd);
				}
			}
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}

	/**
	 * 批量删除 用户身份 根据身份ID
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public int delAllIdentityByIds(PageData pd) throws GenException {
		int num=0;
		try {
			dao.update("UserIdentityMapper.delAllIdentityByIds", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return num;
	}

	/**
	 * 设置默认身份
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	@Log(name="设置默认身份-setDefaultIdentity")
	public int setDefaultIdentity(PageData pd) throws GenException {
		int num = 0;
		try {
			if (pd.get("userIdentityId") != null || !"".equals(String.valueOf(pd.get("userIdentityId")))) {
				num = (Integer) dao.update("UserIdentityMapper.setDefaultIdentity", pd);
			}
			if (pd.get("defaultIdentityId") != null || !"".equals(String.valueOf(pd.get("defaultIdentityId")))) {
				num = (Integer) dao.update("UserIdentityMapper.removeDefaultIdentity", pd);
			}
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return num;
	}

	/**
	 * 根据身份ID 获取 所有 按钮权限ID
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public List<Long> getAllPrivilegeButtonIdByIdentityId(PageData pd) throws GenException{
		 List<Long> buttonIdList = null;
		 try {
			 buttonIdList = (List<Long>) dao.findForList("UserIdentityMapper.getAllPrivilegeButtonIdByIdentityId", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return buttonIdList;
	}

	/**
	 * 获取身份所在项目部， 通过身份所在部门ID
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public Project getUserProjectByIdentityDepId(PageData pd) throws GenException{
		Project projectInfo = null;
		try {
			projectInfo = (Project) dao.findForObject("UserIdentityMapper.getUserProjectByIdentityDepId", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return projectInfo;
	}

	/**
	 * 分页 获取用户 身份 列表 通过 部门ID
	 * @throws GenException
	 */
	public List<PageData> getUserIdentityListByDepIdlistPage(Page page) throws GenException{
		List<PageData> pdList = null;
		try {
			pdList = (List<PageData>) dao.findForList("UserIdentityMapper.getUserIdentityListByDepIdlistPage", page);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pdList;
	}


	/**
	 * 通过身份ID设置用户默认身份
	 * @param identityId
	 * @param userId
	 * @return
	 */
	public int setUserDefaultIdentityByIdentityId(String identityId, String userId) throws GenException {
		if (identityId == null || "".equals(identityId)) {
			return 0;
		}
		int rows = 0;
		try {
			PageData pd = new PageData();
			pd.put("modifier", userId);
			pd.put("modifydate", new Date());

			pd.put("identityId", identityId);
			// 先取消默认
			rows += (Integer) dao.update("UserIdentityMapper.removeUserDefaultIdentityByIdentityId", pd);
			// 后设置默认
			pd.put("userIdentityId", identityId);
			rows += (Integer) dao.update("UserIdentityMapper.setDefaultIdentity", pd);
			return rows;
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}
}
