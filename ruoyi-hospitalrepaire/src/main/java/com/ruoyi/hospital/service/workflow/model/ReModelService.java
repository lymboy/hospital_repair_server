/**
 *
 */
package com.ruoyi.hospital.service.workflow.model;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.ruoyi.hospital.dao.DaoSupport;
import com.ruoyi.hospital.entity.Page;
import com.ruoyi.hospital.framework.exception.GenException;
import com.ruoyi.hospital.util.PageData;

/**
 * 2017年4月6日
 *
 */
@Service("reModelService")
public class ReModelService {
	@Resource(name = "daoSupport")
	private DaoSupport dao;
	/*
	*
	*/
	@SuppressWarnings("unchecked")
	public List<PageData> getResultlistPage(Page page) throws GenException{
		List<PageData> pda = null;
		try {
			pda= (List<PageData>) dao.findForList("ReModelMapper.getResultlistPage",page);
		} catch (Exception e) {
			throw new  GenException(e.getCause());
		}
		return pda;
	}
	@SuppressWarnings("unchecked")
	public List<PageData> getEmployeeByID(Page page) throws GenException{
		List<PageData> pda = null;
		try {
			pda= (List<PageData>) dao.findForList("ReModelMapper.getEmployeeByID",page);
		} catch (Exception e) {
			throw new  GenException(e.getCause());
		}
		return pda;
	}
}
