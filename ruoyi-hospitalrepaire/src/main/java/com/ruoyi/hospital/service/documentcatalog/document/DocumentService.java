package com.ruoyi.hospital.service.documentcatalog.document;

import com.ruoyi.hospital.dao.DaoSupport;
import com.ruoyi.hospital.entity.Page;
import com.ruoyi.hospital.framework.exception.GenException;
import com.ruoyi.hospital.framework.log.Log;
import com.ruoyi.hospital.util.PageData;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>Title: DocumentService</p>
 * @Desc: 资料文件管理
 * @author: 杨建军, 裴雅莉
 *	@Date: 2017年4月21日
 * @Time: 上午10:20:34
 * @version:
 */
@Service(value="documentService")
public class DocumentService {

	@Resource(name = "daoSupport")
	private DaoSupport dao;


	public List<PageData> getCatalogList() throws GenException{
		List<PageData> catalogList=null;
		try{
			catalogList=(List<PageData>) dao.findForList("DocumentMapper.getCatalogList", null);
		}catch(Exception e){
			throw new GenException(e.getCause());
		}
		return catalogList;
	}


	public List<PageData> getMaterialListPage(Page page) throws GenException{
		List<PageData> materialList=null;
		try{
			materialList=(List<PageData>) dao.findForList("DocumentMapper.getMateriallistPage", page);
		}catch(Exception e){
			throw new GenException(e.getCause());
		}
		return materialList;
	}
	public List<PageData> getMaterialListPageBySearch(Page page) throws GenException{
		List<PageData> materialList=null;
		try{
			materialList=(List<PageData>) dao.findForList("DocumentMapper.getMaterialListPageBySearch", page);
		}catch(Exception e){
			throw new GenException(e.getCause());
		}
		return materialList;
	}


	public List<PageData> getDocumentListPage(Page page) throws GenException{
		List<PageData> documentlList=null;
		try{
			documentlList=(List<PageData>) dao.findForList("DocumentMapper.getDocumentlistPage", page);
		}catch(Exception e){
			throw new GenException(e.getCause());
		}
		return documentlList;
	}
    //资料详细信息
	public PageData toMaterialDetail(PageData pd) throws GenException {
		PageData materialNews=null;
		try {
			materialNews=(PageData) dao.findForObject("DocumentMapper.toMaterialDetail", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return materialNews;
	}

	/**
	 * 删除资料
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	@Log(name="删除资料-deleteMaterialByMaterialId")
	public int deleteMaterialByMaterialId(PageData pd) throws GenException {
		int num = 0;
		try {
			num = (Integer) dao.delete("DocumentMapper.deleteMaterialByMaterialId", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return num;
	}

	/**
	 * 更新发布状态
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	@Log(name="更新发布状态-updateMaterialPublishStatus")
	public int updateMaterialPublishStatus(PageData pd) throws GenException{
		int num = 0;
		try {
			num = (Integer) dao.update("DocumentMapper.updateMaterialPublishStatus", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return num;

	}
	/**
	 * 更新发布状态为不发布
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	@Log(name="更新发布状态-updateMaterialPublishStatusToNot")
	public int updateMaterialPublishStatusToNot(PageData pd) throws GenException{
		int num = 0;
		try {
			num = (Integer) dao.update("DocumentMapper.updateMaterialPublishStatusToNot", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return num;
	}
	/**
	 * 获取文件 记录信息
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public PageData getFileInfoById(PageData pd) throws GenException {
		PageData pdInfo;
		try {
			pdInfo = (PageData) dao.findForObject("DocumentMapper.getFileInfoById", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pdInfo;
	}

	/**
	 * 获取文件个数 通过 资料ID [MATERAIL_ID]
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public int getFileNumByMaterailId(PageData pd) throws GenException {
		int num=0;
		try {
			num = (Integer) dao.findForObject("DocumentMapper.getFileNumByMaterailId", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return num;
	}

	/**
	 *  插入新 上传文件纪录
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	@Log(name="插入新 上传文件纪录-addFileInfo")
	public int addFileInfo(PageData pd) throws GenException {
		int num=0;
		try {
			num = (Integer) dao.save("DocumentMapper.addFileInfo", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return num;
	}

	/**
	 * 更新文件信息记录 的 业务ID [MASTER_ID]
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	@Log(name="更新文件信息记录的 业务ID[MASTER_ID]-updateMasterIdByMasterId")
	public int updateMaterialrIdByMaterialrId(PageData pd) throws GenException {
		int num=0;
		try {
			num = (Integer) dao.update("DocumentMapper.updateMaterialrIdByMaterialrId", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return num;
	}

	/**
	 * 删除文件记录 通过 主键
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	@Log(name="删除文件记录 通过 主键-deleteFileInfoById")
	public int deleteFileInfoById(PageData pd) throws GenException {
		int num=0;
		try {
			num = (Integer) dao.update("DocumentMapper.deleteFileInfoById", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return num;
	}
	@Log(name="保存资料")
	public int materialsave(PageData pd) throws GenException{
		int num;
		try {
			num = (Integer) dao.save("DocumentMapper.materialsave", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return num;
	}

	@Log(name="编辑资料")
	public int materialEdit(PageData pd) throws GenException{
		int num=0;
		try {
			num = (Integer) dao.save("DocumentMapper.materialEdit", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return num;
	}

	@Log(name="发布资料")
	public int materialpublish(PageData pd) throws GenException{
		int num;
		try {
			num = (Integer) dao.save("DocumentMapper.materialpublish", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return num;
	}

	//添加资料权限
	public void addDocumentPrivilege(PageData pd) throws GenException {
		try {
			dao.save("DocumentMapper.addDocumentPrivilege", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}

	}
	//获取资料数目
	public int getMaterialNum(PageData pd) throws GenException {
		int materialNum = 0;
		try {
			materialNum=(int) dao.findForObject("DocumentMapper.getMaterialNum", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return materialNum;
	}

}
