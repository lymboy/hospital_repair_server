package com.ruoyi.hospital.service.repair;

import com.ruoyi.hospital.dao.DaoSupport;
import com.ruoyi.hospital.entity.Page;
import com.ruoyi.hospital.framework.exception.GenException;
import com.ruoyi.hospital.util.PageData;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("informationUserService")
public class InformationUserService
{
	@Resource(name = "daoSupport")
	private DaoSupport dao;

	@SuppressWarnings("unchecked")
	public synchronized List<PageData> getInformationUserlistPage(Page page) {
		List<PageData> InformationUserlis = null;
		try {
			InformationUserlis = (List<PageData>)this.dao.findForList("InformationUserMapper.getInformationUserlistPage", page);
		} catch (Exception e) {
			e.printStackTrace();
		}
	    return InformationUserlis;
	}

	public PageData getInformationUserById(PageData pd) {
		PageData InformationUser = null;
	    try {
	    	InformationUser = (PageData)this.dao.findForObject("InformationUserMapper.getInformationUserById", pd);
		} catch (Exception e) {
			e.printStackTrace();
		}
	    return InformationUser;
	}

	public void addInformationUser(PageData pd) {
		try {
			dao.save("InformationUserMapper.addInformationUser", pd);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void editInformationUser(PageData pd) {
		try {
			dao.update("InformationUserMapper.editInformationUser", pd);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public Object findByUN(PageData pd) throws GenException {
		PageData pda = new PageData();
		try {
			pda = (PageData)dao.findForObject("InformationUserMapper.findByUN", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pda;
	}

	public Object findByUserName(PageData pd) throws GenException {
		PageData pda = new PageData();
		try {
			pda = (PageData)dao.findForObject("InformationUserMapper.findByUserName", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pda;
	}

	@SuppressWarnings("unchecked")
	public synchronized List<PageData> checkMarkInformationlistPage(Page page) {
		List<PageData> InformationUserlis = null;
		try {
			InformationUserlis = (List<PageData>)this.dao.findForList("InformationUserMapper.checkMarkInformationlistPage", page);
		} catch (Exception e) {
			e.printStackTrace();
		}
	    return InformationUserlis;
	}

}
