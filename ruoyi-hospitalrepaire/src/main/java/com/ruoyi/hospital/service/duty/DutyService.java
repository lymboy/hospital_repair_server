package com.ruoyi.hospital.service.duty;

import com.ruoyi.hospital.controller.app.WeChat1.Template;
import com.ruoyi.hospital.controller.app.WeChat1.Token;
import com.ruoyi.hospital.dao.DaoSupport;
import com.ruoyi.hospital.entity.Page;
import com.ruoyi.hospital.framework.exception.GenException;
import com.ruoyi.hospital.util.DateUtil;
import com.ruoyi.hospital.util.PageData;
import com.ruoyi.hospital.util.UuidUtil;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import javax.annotation.Resource;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service("dutyService")
public class DutyService
{
	@Resource(name = "daoSupport")
	private DaoSupport dao;
	@Autowired
	private Template template;

	DateFormat formart = new SimpleDateFormat("yyyy-MM-dd HH:mm");

	public int calendarUtil() {
		GregorianCalendar currentDay = new GregorianCalendar();
		 int weekday=currentDay.get(Calendar.DAY_OF_WEEK); // 星期是从周日开始的，所以要减一
	    return weekday;
	}

	/**
	 * 轮询分配
	 * 规则： 在上班时间，按照组别，工号从小到大依次分配任务
	 *
	 * */
	@SuppressWarnings({ "unchecked", "unused" })
	public PageData pollingDistribution(PageData pd) throws Exception{

		String departmentId = pd.getString("DEPARTMENT_ID"); 	// 获取问题科室
		PageData groupDictionary = (PageData) this.dao.findForObject("DutyMapper.getGroupIdByDepartmentID", departmentId); // 通过问题科室获取所在组别
		pd.put("GROUP_ID", groupDictionary.getString("GROUP_ID"));
		String strDay = DateUtil.getyM();
		List<PageData> memberList = (List<PageData>) dao.findForList("DutyMapper.getMemberByGroup", pd); 	// 获取到所在组别，没休假的人员
		PageData previousUser = (PageData) dao.findForObject("DutyMapper.getPreviousUser", pd); 			// 获取到上一个分配任务的用户的工号
	    List<PageData> dutyList = (List<PageData>)this.dao.findForList("DictionaryMapper.selectHosDuty", null);
	    PageData implemntUserInfo = pollingSwitch(previousUser,dutyList,pd,strDay,memberList);
	    implemntUserInfo.put("GROUP_ID", groupDictionary.getString("GROUP_ID"));
		return implemntUserInfo;
	}

	@SuppressWarnings("unused")
	public PageData pollingSwitch (PageData previousUser,List<PageData> dutyList,PageData pd,String strDay,List<PageData> memberList) throws Exception {
		PageData morningWork = (PageData) dao.findForObject("AttendanceSettingMapper.getAttendanceSettingMorning", null);
		PageData afternoonWork = (PageData) dao.findForObject("AttendanceSettingMapper.getAttendanceSettingAfternoon", null);
		String morningStartTime = "";
		String morningEndTime = "";
		String afternoonStartTime = "";
		String afternoonEndTime = "";
		if (morningWork != null) {
			morningStartTime = DateUtil.getDay()+" "+morningWork.getString("START_TIME");
			morningEndTime = DateUtil.getDay()+" "+morningWork.getString("END_TIME");
		}
		if (afternoonWork != null) {
			afternoonStartTime = DateUtil.getDay()+" "+afternoonWork.getString("START_TIME");
			afternoonEndTime = DateUtil.getDay()+" "+afternoonWork.getString("END_TIME");
		}
		int weekday = calendarUtil();
		for (PageData duty : dutyList) {
	    	if (StringUtils.equals(duty.getString("DUTY_DATE"), String.valueOf(strDay))) {
	    		if (StringUtils.equals(duty.getString("IS_HOLIDAY"), "1")) { 		// 判断今天是不是节假日
	    			pd.put("USER_NAME", duty.getString("USER_NAME")); 				// 如果是，直接取当天的值班人员
	    			pd.put("IMPLEMENT_ID",duty.getString("DUTY_ID")); 				// 如果是，直接取当天的值班人员
	    		}else if (StringUtils.equals(duty.getString("IS_WORK"), "1")){ 		// 判断今天是不是正常上班
	    			pd = pollingMonToFriday(previousUser,memberList,pd,duty,morningStartTime,afternoonEndTime,morningEndTime,afternoonStartTime);
	    		}else {
	    			switch ((weekday-1)) {
					case 6:		// 周六
						pd = pollingSaturday(previousUser,memberList,pd,duty,morningStartTime,morningEndTime);
						break;
					case 0: 	// 周日
						pd.put("IMPLEMENT_ID",duty.getString("DUTY_ID"));
						pd.put("USER_NAME", duty.getString("USER_NAME")); // 如果是，直接取当天的值班人员
						break;
					default: 	// 周一到周五
						pd = pollingMonToFriday(previousUser,memberList,pd,duty,morningStartTime,afternoonEndTime,morningEndTime,afternoonStartTime);
						break;
					}
		    	}
	    	}
	    }
		return pd;
	}

	public PageData pollingMonToFriday(PageData previousUser, List<PageData> memberList,PageData pd,PageData duty,String morningStartTime,String afternoonEndTime,String morningEndTime,String afternoonStartTime) throws Exception {

		Long morningStartparse = formart.parse(morningStartTime).getTime();      // 上午上班时间
		Long morningEndparse = formart.parse(morningEndTime).getTime();          // 上午下班时间
		Long afternoonStartparse = formart.parse(afternoonStartTime).getTime();  // 下午上班时间
		Long afternoonEndparse= formart.parse(afternoonEndTime).getTime();		 // 下午下班时间
		Long hourMinuteDate = formart.parse(DateUtil.getTime()).getTime(); 		 // 现在时间
		String str = "23:59";
		String frontDay = DateUtil.getAfterDate("-1");
		Long endTime = formart.parse(frontDay+" "+str).getTime();

		if (((hourMinuteDate-morningStartparse) > 0 && (hourMinuteDate - morningEndparse) < 0)
				|| ((hourMinuteDate-afternoonStartparse) > 0 && (hourMinuteDate - afternoonEndparse) < 0)
			) { 	// 属于 正常上班
			if (memberList.size() > 0) {
				for (int i=0;i<memberList.size();i++) {
					if (previousUser != null) {
						// 当存在上一个用户时,遍历集合取比上一个用户工号大的那个人
						int compareResult = memberList.get(i).getString("JOB_NUM").compareTo(previousUser.getString("MEMBER_JOB_NUM"));
						if (compareResult > 0) {  //  当count>0时，说明当前用户的工号比上个人的大
							pd = memberList.get(i);
							pd.put("IMPLEMENT_ID", memberList.get(i).getString("ID"));

							break;
						}else {
							if (i == memberList.size()-1) {
								pd = memberList.get(0);
								pd.put("IMPLEMENT_ID", memberList.get(0).getString("ID"));

								break;
							}
						}
					}else {
						// 当上一个分配任务的人员不存在时,直接取集合中工号最小的那个
						pd = memberList.get(0);
						pd.put("IMPLEMENT_ID", memberList.get(0).getString("ID"));

						break;
					}
				}
			}else {
				pd.put("IMPLEMENT_ID","");
			}
		}else if ((hourMinuteDate-endTime) > 0 && (hourMinuteDate - morningStartparse) < 0){
			PageData frontDuty = frontDuty();
		    pd.put("IMPLEMENT_ID",frontDuty.getString("DUTY_ID")); 		// 直接取当天的值班人员
			pd.put("USER_NAME", frontDuty.getString("USER_NAME")); 		// 如果是，直接取当天的值班人员
		}else {
			pd.put("IMPLEMENT_ID",duty.getString("DUTY_ID")); 			// 直接取当天的值班人员
			pd.put("USER_NAME", duty.getString("USER_NAME")); 			// 如果是，直接取当天的值班人员
		}
		return pd;
	}

	public PageData pollingSaturday(PageData previousUser, List<PageData> memberList,PageData pd,PageData duty,String morningStartTime,String morningEndTime) throws Exception {

		Long parse = formart.parse(morningStartTime).getTime();
		Long parse1= formart.parse(morningEndTime).getTime();
		Long hourMinuteDate = formart.parse(DateUtil.getTime()).getTime();
		String str = "23:59";
		Long endTime = formart.parse(DateUtil.getAfterDate("-1")+" "+str).getTime();
		String groupId = pd.getString("GROUP_ID");
		if ((hourMinuteDate-parse) > 0 && (hourMinuteDate - parse1) < 0) { // 属于正常上班
			if (memberList.size() > 0) {
				for (int i=0;i<memberList.size();i++) {
					if (previousUser != null) {
						// 当存在上一个用户时,遍历集合取比上一个用户工号大的那个人
						int count = memberList.get(i).getString("JOB_NUM").compareTo(previousUser.getString("MEMBER_JOB_NUM"));
						if (count > 0) {  //  当count>0时，说明当前用户的工号比上个人的大
							pd = memberList.get(i);
							pd.put("IMPLEMENT_ID", memberList.get(i).getString("ID"));

							break;
						}else {
							if (i == memberList.size()-1) {
								pd = memberList.get(0);
								pd.put("IMPLEMENT_ID", memberList.get(0).getString("ID"));

								break;
							}
						}
					}else {
						//  当上一个分配任务的人员不存在时,直接取集合中工号最小的那个
						pd = memberList.get(0);
						pd.put("IMPLEMENT_ID", memberList.get(0).getString("ID"));
					}
				}
			}else {
				pd.put("IMPLEMENT_ID","");
			}
		}else if ((hourMinuteDate-endTime) > 0 && (hourMinuteDate - parse) < 0){
			PageData frontDuty = frontDuty();
		    pd.put("IMPLEMENT_ID",frontDuty.getString("DUTY_ID")); 		// 直接取当天的值班人员
			pd.put("USER_NAME", frontDuty.getString("USER_NAME")); 		// 如果是，直接取当天的值班人员
		}else {
			pd.put("IMPLEMENT_ID",duty.getString("DUTY_ID")); 			// 直接取当天的值班人员
			pd.put("USER_NAME", duty.getString("USER_NAME")); 			// 如果是，直接取当天的值班人员
		}
		return pd;
	}

	//  获取到的执行人员插入辅助表中
	public void saveAuxiliary (String USER_NAME,String JOB_NUM,String groupId) {
		PageData auxiliary = new PageData();
		try {
			auxiliary.put("ID", UuidUtil.get32UUID());
			auxiliary.put("MEMBER_NAME", USER_NAME);
			auxiliary.put("MEMBER_JOB_NUM", JOB_NUM);
			auxiliary.put("GROUP_ID", groupId);
			auxiliary.put("CREATEDATE", DateUtil.getTime());

			dao.save("DutyMapper.saveAuxiliary", auxiliary);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}




	/**
	 * 自动分配
	 * 规则：周一到周五：8:00 - 17:00
	 *     周六上午 ：8:00 - 12:00   按照组别、是否休假、是否有未完成、积分的顺序自动分配
	 *     其余时间（包括节假日）：统一都分配给值班人员
	 *
	 * */
	@SuppressWarnings({ "unchecked", "unused" })
	public PageData automaticAllocation(PageData pd) throws Exception{

		String departmentId = pd.getString("DEPARTMENT_ID"); // 获取问题科室
		PageData groupDictionary = (PageData) this.dao.findForObject("DutyMapper.getGroupIdByDepartmentID", departmentId); // 通过问题科室获取所在组别
		pd.put("GROUP_ID", groupDictionary.getString("GROUP_ID"));
		String strDay = DateUtil.getyM();
		List<PageData> memberList = getMemberList(pd); // 获取到所在组别，没休假的人员

	    List<PageData> dutyList = (List<PageData>)this.dao.findForList("DictionaryMapper.selectHosDuty", null);
	    pd = pageData(dutyList,pd,strDay,memberList);
		return pd;
	}

	@SuppressWarnings("unused")
	public PageData pageData (List<PageData> dutyList,PageData pd,String strDay,List<PageData> memberList) throws Exception {
		PageData morningWork = (PageData) dao.findForObject("AttendanceSettingMapper.getAttendanceSettingMorning", null);
		PageData afternoonWork = (PageData) dao.findForObject("AttendanceSettingMapper.getAttendanceSettingAfternoon", null);
		String morningStartTime = "";
		String morningEndTime = "";
		String afternoonStartTime = "";
		String afternoonEndTime = "";
		if (morningWork != null) {
			morningStartTime = DateUtil.getDay()+" "+morningWork.getString("START_TIME");
			morningEndTime = DateUtil.getDay()+" "+morningWork.getString("END_TIME");
		}
		if (afternoonWork != null) {
			afternoonStartTime = DateUtil.getDay()+" "+afternoonWork.getString("START_TIME");
			afternoonEndTime = DateUtil.getDay()+" "+afternoonWork.getString("END_TIME");
		}
		int weekday = calendarUtil();
		for (PageData duty : dutyList) {
	    	if (StringUtils.equals(duty.getString("DUTY_DATE"), String.valueOf(strDay))) {
	    		if (StringUtils.equals(duty.getString("IS_HOLIDAY"), "1")) { //判断今天是不是节假日
	    			pd.put("USER_NAME", duty.getString("USER_NAME")); // 如果是，直接取当天的值班人员
	    			pd.put("IMPLEMENT_ID",duty.getString("DUTY_ID")); // 如果是，直接取当天的值班人员
	    		}else if (StringUtils.equals(duty.getString("IS_WORK"), "1")){ // 判断今天是不是正常上班
	    			pd = monToFriday(memberList,pd,duty,morningStartTime,afternoonEndTime,morningEndTime,afternoonStartTime);
	    		}else {
	    			switch ((weekday-1)) {
					case 6:// 周六
						pd = saturday(memberList,pd,duty,morningStartTime,morningEndTime);
						break;
					case 0: // 周日
						pd.put("IMPLEMENT_ID",duty.getString("DUTY_ID"));
						pd.put("USER_NAME", duty.getString("USER_NAME")); // 如果是，直接取当天的值班人员
						break;
					default: // 周一到周五
						pd = monToFriday(memberList,pd,duty,morningStartTime,afternoonEndTime,morningEndTime,afternoonStartTime);
						break;
					}
		    	}
	    	}
	    }
		return pd;
	}

	public PageData frontDuty () {
		String frontDay = DateUtil.getAfterDateMMD("-1");
		PageData frontDuty = null;
	    try {
			frontDuty = (PageData) this.dao.findForObject("DictionaryMapper.selectFrontDuty", frontDay);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return frontDuty;
	}

	@SuppressWarnings("unchecked")
	public PageData saturday(List<PageData> memberList,PageData pd,PageData duty,String morningStartTime,String morningEndTime) throws Exception {
		List<PageData> nullQuestionMember = new ArrayList<PageData>();
		List<PageData> questionList = null;

		Long parse = formart.parse(morningStartTime).getTime();
		Long parse1= formart.parse(morningEndTime).getTime();
		Long hourMinuteDate = formart.parse(DateUtil.getTime()).getTime();
		String str = "23:59";
		Long endTime = formart.parse(DateUtil.getAfterDate("-1")+" "+str).getTime();
		if ((hourMinuteDate-parse) > 0 && (hourMinuteDate - parse1) < 0) { // 属于正常上班
			for (int i=0;i<memberList.size();i++) {
				pd.put("memberID", memberList.get(i).getString("ID"));
				questionList = (List<PageData>) this.dao.findForList("DutyMapper.getQuestionListByMemberId", pd); // 查到当前人员是否有任务
				if (questionList.size() == 0) {
					nullQuestionMember.add(memberList.get(i));
				}
			}
			pd = distribution(nullQuestionMember,memberList, pd);
		}else if ((hourMinuteDate-endTime) > 0 && (hourMinuteDate - parse) < 0){
			PageData frontDuty = frontDuty();
		    pd.put("IMPLEMENT_ID",frontDuty.getString("DUTY_ID")); // 直接取当天的值班人员
			pd.put("USER_NAME", frontDuty.getString("USER_NAME")); // 如果是，直接取当天的值班人员
		}else {
			pd.put("IMPLEMENT_ID",duty.getString("DUTY_ID")); // 直接取当天的值班人员
			pd.put("USER_NAME", duty.getString("USER_NAME")); // 如果是，直接取当天的值班人员
		}
		return pd;
	}

	@SuppressWarnings("unchecked")
	public PageData monToFriday(List<PageData> memberList,PageData pd,PageData duty,String morningStartTime,String afternoonEndTime,String morningEndTime,String afternoonStartTime) throws Exception {
		List<PageData> nullQuestionMember = new ArrayList<PageData>();
		List<PageData> questionList = null;

		Long morningStartparse = formart.parse(morningStartTime).getTime();
		Long morningEndparse = formart.parse(morningEndTime).getTime();
		Long afternoonStartparse = formart.parse(afternoonStartTime).getTime();
		Long afternoonEndparse= formart.parse(afternoonEndTime).getTime();
		String str1 = DateUtil.getTime();
		Long hourMinuteDate = formart.parse(DateUtil.getTime()).getTime();
		String str = "23:59";
		String frontDay = DateUtil.getAfterDate("-1");
		Long endTime = formart.parse(frontDay+" "+str).getTime();
		if ((hourMinuteDate-morningStartparse) > 0 && (hourMinuteDate - morningEndparse) < 0) { // 属于上午正常上班
			for (int i=0;i<memberList.size();i++) {
				pd.put("memberID", memberList.get(i).getString("ID"));
				questionList = (List<PageData>) this.dao.findForList("DutyMapper.getQuestionListByMemberId", pd); // 查到当前人员是否有任务
				if (questionList.size() == 0) {
					nullQuestionMember.add(memberList.get(i));
				}
			}
			pd = distribution(nullQuestionMember,memberList, pd);
		}else if ((hourMinuteDate-afternoonStartparse) > 0 && (hourMinuteDate - afternoonEndparse) < 0) { // 属于下午正常上班
			for (int i=0;i<memberList.size();i++) {
				pd.put("memberID", memberList.get(i).getString("ID"));
				questionList = (List<PageData>) this.dao.findForList("DutyMapper.getQuestionListByMemberId", pd); // 查到当前人员是否有任务
				if (questionList.size() == 0) {
					nullQuestionMember.add(memberList.get(i));
				}
			}
			pd = distribution(nullQuestionMember,memberList, pd);
		}else if ((hourMinuteDate-endTime) > 0 && (hourMinuteDate - morningStartparse) < 0){
			PageData frontDuty = frontDuty();
		    pd.put("IMPLEMENT_ID",frontDuty.getString("DUTY_ID")); // 直接取当天的值班人员
			pd.put("USER_NAME", frontDuty.getString("USER_NAME")); // 如果是，直接取当天的值班人员
		}else {
			pd.put("IMPLEMENT_ID",duty.getString("DUTY_ID")); // 直接取当天的值班人员
			pd.put("USER_NAME", duty.getString("USER_NAME")); // 如果是，直接取当天的值班人员
		}
		return pd;
	}


	public PageData distribution (List<PageData> nullQuestionMember , List<PageData> memberList ,PageData pd) {
		if (nullQuestionMember.size() == 0) { // 当前所有人都有任务
			if (memberList.size() > 0) {
				Collections.sort(memberList,new Comparator<PageData>() {
					@Override
					public int compare(PageData o1, PageData o2) {
						return Integer.valueOf(o1.getString("TOTAL_MARK")).compareTo(Integer.valueOf(o2.getString("TOTAL_MARK")));
					}
				});
				String firstMark = memberList.get(0).getString("TOTAL_MARK");
				pd.put("TOTAL_MARK", firstMark);
				System.out.println("mark:" + memberList.get(0).getString("TOTAL_MARK"));
				List<PageData> memberList1 = new ArrayList<PageData>();
				for (int i = 0;i<memberList.size();i++) {
					if (StringUtils.equals(firstMark, memberList.get(i).getString("TOTAL_MARK"))) {
						memberList1.add(memberList.get(i));
					}
				}
				Collections.sort(memberList1,new Comparator<PageData>() {
					@Override
					public int compare(PageData o1, PageData o2) {
						return Integer.valueOf(o1.getString("JOB_NUM")).compareTo(Integer.valueOf(o2.getString("JOB_NUM")));
					}
				});
				System.out.println("JOB_NUM:"+memberList1.get(0).getString("JOB_NUM"));
				pd = memberList1.get(0);
				pd.put("IMPLEMENT_ID", memberList1.get(0).getString("ID"));

				if (StringUtils.equals(pd.getString("appMark"), "app")) {
					String openid = memberList1.get(0).getString("WX_OPENID");
					if (!StringUtils.isEmpty(openid)) {
						Token token = new Token();
						template.send_simp_message(token.getAPPID(), token.getAPPSECRET(), openid);
					}
				}

			}else {
				pd.put("IMPLEMENT_ID","");
			}
		}else {
			if (nullQuestionMember.size() > 0) {
				Collections.sort(nullQuestionMember,new Comparator<PageData>() {
					@Override
					public int compare(PageData o1, PageData o2) {
						return Integer.valueOf(o1.getString("TOTAL_MARK")).compareTo(Integer.valueOf(o2.getString("TOTAL_MARK")));
					}
				});
				System.out.println(nullQuestionMember);
				String firstMark = nullQuestionMember.get(0).getString("TOTAL_MARK");
				pd.put("TOTAL_MARK", firstMark);
				System.out.println("mark:" + nullQuestionMember.get(0).getString("TOTAL_MARK"));
				List<PageData> memberList1 = new ArrayList<PageData>();
				for (int i = 0;i<nullQuestionMember.size();i++) {
					if (StringUtils.equals(firstMark, nullQuestionMember.get(i).getString("TOTAL_MARK"))) {
						memberList1.add(nullQuestionMember.get(i));
					}
				}
				Collections.sort(memberList1,new Comparator<PageData>() {
					@Override
					public int compare(PageData o1, PageData o2) {
						return Integer.valueOf(o1.getString("JOB_NUM")).compareTo(Integer.valueOf(o2.getString("JOB_NUM")));
					}
				});
				System.out.println("JOB_NUM:"+memberList1.get(0).getString("JOB_NUM"));
				pd = memberList1.get(0);
				pd.put("IMPLEMENT_ID", memberList1.get(0).getString("ID"));

				if (StringUtils.equals(pd.getString("appMark"), "app")) {
					String openid = memberList1.get(0).getString("WX_OPENID");
					if (!StringUtils.isEmpty(openid)) {
						Token token = new Token();
						template.send_simp_message(token.getAPPID(), token.getAPPSECRET(), openid);
					}
				}

			}else {
				pd.put("IMPLEMENT_ID","");
			}
		}
		return pd;
	}

	public static void main(String[] args) throws java.text.ParseException {
		GregorianCalendar currentDay = new GregorianCalendar();
		int year= currentDay.get(Calendar.YEAR);
	    int month=currentDay.get(Calendar.MONTH);
	    int today=currentDay.get(Calendar.DAY_OF_MONTH);
	    int weekday=currentDay.get(Calendar.DAY_OF_WEEK);
	    int hour=currentDay.get(Calendar.HOUR_OF_DAY);
	    int minute=currentDay.get(Calendar.MINUTE);
	    String strDay = (month+1)+"-"+today;
	    System.out.println(strDay);

	    SimpleDateFormat sdf = new SimpleDateFormat("MM-dd");
		Date now = null;
		try {
			now = sdf.parse(strDay);
		} catch (java.text.ParseException e1) {
			e1.printStackTrace();
		}
		Long nowTime = now.getTime() - 86400000;
		String frontDay1 = sdf.format(new Date(nowTime));
		String[] str  = frontDay1.split("-");
		String frontDay = DateUtil.getYear()+str[0]+""+str[1];
		System.out.println(frontDay);

	}

	public List<PageData> getDutyListPage(Page page) throws Exception {
		List<PageData> dutyList = null;
		dutyList = (List<PageData>)this.dao.findForList("DutyMapper.getDutylistPage", page);
		return dutyList;
	}

	public PageData getDutyQuestionById(PageData pd) throws GenException {
		PageData pdInfo = null;
		try {
			pdInfo = (PageData)this.dao.findForObject("DutyMapper.getDutyQuestionById", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pdInfo;
	}

	/**
	 * 修改问题、问题的分配执行情况
	 * @param pd
	 * @return
	 * @throws Exception
	 */
	public int editDutyQuestion(PageData pd) throws Exception {
		int i = 0;
		//PageData pd1 = automaticAllocation(pd); //
		//PageData pd1 = pollingDistribution(pd);   //  修改问题，不影响分配

		//pd.put("IMPLEMENT_ID", pd1.getString("IMPLEMENT_ID"));

		String strDay = DateUtil.getyM();
		String DICTIONARY_ID = pd.getString("DICTIONARY_ID");
		PageData pd1 = new PageData();
		if (StringUtils.equals("1", DICTIONARY_ID)) {//1：硬件组
			List<PageData> dutyList = (List<PageData>)this.dao.findForList("DictionaryMapper.selectHosDuty", null);
			List<PageData> memberList = (List<PageData>) dao.findForList("DutyMapper.getMemberByDictionaryId", pd);

			String groupId = pd.getString("GROUP_ID");
			PageData previousUser = (PageData) dao.findForObject("DutyMapper.getPreviousUser", pd); 			// 获取到上一个分配任务的用户的工号
			pd1 = pollingSwitch(previousUser,dutyList,pd,strDay,memberList);
			pd1.put("GROUP_ID", groupId);//
		}else {
			pd1 = pollingDistribution(pd);
		}
		pd.put("IMPLEMENT_ID", pd1.getString("IMPLEMENT_ID"));

		i = (int) this.dao.update("DutyMapper.editDutyQuestion", pd);//更新问题内容
		if (StringUtils.isNotEmpty(pd.getString("ID"))) {
			PageData pageData = (PageData) this.dao.findForObject("DutyMapper.selectHosQuestionAsiByID", pd);
			if (pageData != null) {
				this.dao.update("DutyMapper.updateHosQuestionAsiByID", pd);//更新被转让人
			}else {
				pd.put("ASSIGNOR_ID", pd.getString("IMPLEMENT_ID"));
				pd.put("ASSIGNOR_DATE", new Date());
				pd.put("SUB_ID", UuidUtil.get32UUID());
				this.dao.save("DutyMapper.addHosQuestionAsi", pd);//添加问题分配执行情况
			}
		}

		//根据自动分配规则生成的执行人ID，获取执行人信息
		PageData findUserParam = new PageData();
		findUserParam.put("ID", pd.getString("IMPLEMENT_ID"));
		PageData userPageData = (PageData)dao.findForObject("UserMapper.findByUiId", findUserParam); // 通过执行人员ID获取该报修人员信息
		String openid = userPageData.getString("WX_OPENID");

		//保存 任务分配情况
		saveAuxiliary(userPageData.getString("USER_NAME"),userPageData.getString("JOB_NUM"),pd1.getString("GROUP_ID"));

		if (StringUtils.equals(pd.getString("appMark"), "app")) {
			if (!StringUtils.isEmpty(openid)) {
				Token token = new Token();
				template.send_simp_message(token.getAPPID(), token.getAPPSECRET(), openid);
			}
		}

		return i;
	}

	public int editDutyQuestion1(PageData pd) throws Exception {
		String statusId = pd.getString("STATUS_ID");
		  if (StringUtils.equals("2", statusId)) {
			  try {
				PageData quesDictionary = (PageData) dao.findForObject("DutyMapper.getDutyQuestionMarkById", pd);
				System.out.println("完成时。该问题的分数："+quesDictionary.getString("QUESTION_MARK"));
				String assignorId = pd.getString("ASSIGNOR_ID"); // 被转让人员
				String implementId = pd.getString("IMPLEMENT_ID");// 执行人员 这个可为空
				if (StringUtils.isNotEmpty(implementId)) {
					if (StringUtils.isNotEmpty(assignorId)) {
						quesDictionary.put("QUESTION_MARK", quesDictionary.getString("QUESTION_MARK"));
 						quesDictionary.put("MarkId", assignorId);
 						quesDictionary.put("ID", UuidUtil.get32UUID());
						quesDictionary.put("CREATEDATE", new Date());
						dao.update("InformationUserMapper.insertHosMemberMark", quesDictionary);
					}else {
						quesDictionary.put("QUESTION_MARK", quesDictionary.getString("QUESTION_MARK"));
						quesDictionary.put("MarkId", implementId);
						quesDictionary.put("ID", UuidUtil.get32UUID());
						quesDictionary.put("CREATEDATE", new Date());
						dao.update("InformationUserMapper.insertHosMemberMark", quesDictionary);
					}
				}else {
					quesDictionary.put("QUESTION_MARK", quesDictionary.getString("QUESTION_MARK"));
					quesDictionary.put("MarkId", assignorId);
					quesDictionary.put("ID", UuidUtil.get32UUID());
					quesDictionary.put("CREATEDATE", new Date());
					dao.update("InformationUserMapper.insertHosMemberMark", quesDictionary);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		  }
		int i = (int) this.dao.update("DutyMapper.editDutyQuestion", pd);
		if (!StringUtils.equals("2", statusId)) {
			this.dao.update("DutyMapper.updateHosQuestionAsiByID", pd);
		}
		return i;
	}


	/**
	 * 公众号添加问题、设置问题执行人（根据分配规则获取），并推送消息
	 * @param pd
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public void addDutyQuestion(PageData pd) throws Exception {
		String strDay = DateUtil.getyM();
		String DICTIONARY_ID = pd.getString("DICTIONARY_ID");
		PageData pd1 = new PageData();
		if (StringUtils.equals("1", DICTIONARY_ID)) {//1：硬件组
			List<PageData> dutyList = (List<PageData>)this.dao.findForList("DictionaryMapper.selectHosDuty", null);
			List<PageData> memberList = (List<PageData>) dao.findForList("DutyMapper.getMemberByDictionaryId", pd);

			String groupId = pd.getString("GROUP_ID");
			PageData previousUser = (PageData) dao.findForObject("DutyMapper.getPreviousUser", pd); 			// 获取到上一个分配任务的用户的工号
			pd1 = pollingSwitch(previousUser,dutyList,pd,strDay,memberList);
			pd1.put("GROUP_ID", groupId);//
		}else {
			pd1 = pollingDistribution(pd);
		}
		pd.put("IMPLEMENT_ID", pd1.getString("IMPLEMENT_ID"));
		this.dao.save("DutyMapper.addDutyQuestion", pd);
		if (StringUtils.isNotEmpty(pd.getString("IMPLEMENT_ID"))) {
			pd.put("ASSIGNOR_DATE", new Date());
			pd.put("SUB_ID", UuidUtil.get32UUID());
			pd.put("ASSIGNOR_ID", pd.getString("IMPLEMENT_ID"));
			this.dao.save("DutyMapper.addHosQuestionAsi", pd);
		}

		//根据自动分配规则生成的执行人ID，获取执行人信息
		PageData findUserParam = new PageData();
		findUserParam.put("ID", pd.getString("IMPLEMENT_ID"));
		PageData userPageData = (PageData)dao.findForObject("UserMapper.findByUiId", findUserParam); // 通过执行人员ID获取该报修人员信息
		String openid = userPageData.getString("WX_OPENID");

		//保存 任务分配情况
		saveAuxiliary(userPageData.getString("USER_NAME"),userPageData.getString("JOB_NUM"),pd1.getString("GROUP_ID"));

		if (StringUtils.equals(pd.getString("appMark"), "app")) {
			if (!StringUtils.isEmpty(openid)) {
				Token token = new Token();
				template.send_simp_message(token.getAPPID(), token.getAPPSECRET(), openid);
			}
		}
	}
	/**
	 * 后台添加问题、分配执行情况，并将消息推送给被转让人（新增时候与执行人相同）
	 * @param pd
	 * @throws Exception
	 */
	public void addDutyQuestionAndSendMsg(PageData pd) throws Exception {
		String openid = null;
		this.dao.save("DutyMapper.addDutyQuestion", pd);

		if (StringUtils.isNotEmpty(pd.getString("IMPLEMENT_ID"))) {
			pd.put("ASSIGNOR_DATE", new Date());
			pd.put("SUB_ID", UuidUtil.get32UUID());
			pd.put("ASSIGNOR_ID", pd.getString("IMPLEMENT_ID"));
			this.dao.save("DutyMapper.addHosQuestionAsi", pd);
		}
		if (StringUtils.isNotEmpty(pd.getString("ASSIGNOR_ID"))) {

			pd.put("ID", pd.getString("ASSIGNOR_ID"));
			PageData pageData = (PageData)dao.findForObject("UserMapper.findByUiId", pd); // 通过执行人员ID获取该报修人员信息
			openid = pageData.getString("WX_OPENID");
			//保存 任务分配情况
			saveAuxiliary(pageData.getString("USER_NAME"),pageData.getString("JOB_NUM"),pd.getString("GROUP_ID"));
		}
		if (!StringUtils.isEmpty(openid)) {
			Token token = new Token();
			template.send_simp_message(token.getAPPID(), token.getAPPSECRET(), openid);
		}

	}

	/**
	 * 后台修改问题、分配执行情况，给被转让人发送信息
	 * @param pd
	 * @return
	 * @throws Exception
	 */
	public int editDutyQuestionAndSendMsg(PageData pd) throws Exception {
		int i = this.editDutyQuestion(pd);

		//问题修改，给被转让人发送信息
		if (StringUtils.isNotEmpty(pd.getString("ASSIGNOR_ID"))) {
			pd.put("ID", pd.getString("ASSIGNOR_ID"));
			PageData pageData = (PageData)dao.findForObject("UserMapper.findByUiId", pd); // 通过执行人员ID获取该报修人员信息
			String openid = pageData.getString("WX_OPENID");

			if (!StringUtils.isEmpty(openid)) {
				Token token = new Token();
				template.send_simp_message(token.getAPPID(), token.getAPPSECRET(), openid);
			}
		}


		return i;
	}

	public void deleteDuty(PageData pd) throws Exception { this.dao.delete("DutyMapper.deleteDuty", pd); }

	public List<PageData> getQuestionDictionaryList() {
		List<PageData> questionDictionaryList = null;
		try {
			questionDictionaryList = (List<PageData>)this.dao.findForList("DutyMapper.getQuestionDictionaryList", null);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return questionDictionaryList;
	}

	public List<PageData> getGroupDictionaryList() {
		List<PageData> groupDictionaryList = null;
		try {
			groupDictionaryList = (List<PageData>)this.dao.findForList("DutyMapper.getGroupDictionaryList", null);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return groupDictionaryList;
	}

	public List<PageData> getStatusDictionaryList() {
		List<PageData> statusDictionaryList = null;
		try {
			statusDictionaryList = (List<PageData>)this.dao.findForList("DutyMapper.getStatusDictionaryList", null);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return statusDictionaryList;
	}

	public List<PageData> getAuxiliaryDictionaryList() {
		List<PageData> auxiliaryDictionaryList = null;
		try {
			auxiliaryDictionaryList = (List<PageData>)this.dao.findForList("DutyMapper.getAuxiliaryDictionaryList", null);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return auxiliaryDictionaryList;
	}

	public List<PageData> getMmberList() {
		List<PageData> memberList = null;
		try {
			memberList = (List<PageData>)this.dao.findForList("DutyMapper.getMmberList", null);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return memberList;
	}

	public void addHosQuestionAsi(PageData pd) {
		try {
			this.dao.save("DutyMapper.addHosQuestionAsi", pd);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void editDutyQuestionImg(PageData pd) {
		try {
			this.dao.update("DutyMapper.editDutyQuestionImg", pd);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	public List<PageData> getcondomMemberList() {
		List<PageData> memberList = null;
		try {
			memberList = (List<PageData>)this.dao.findForList("DutyMapper.getcondomMemberList", null);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return memberList;
	}

	public List<PageData> getMemberByDepartmentId(PageData pd) {
		List<PageData> memberList = null;
		try {
			memberList = (List<PageData>)this.dao.findForList("DutyMapper.getMemberByDepartmentId", pd);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return memberList;
	}

	public List<PageData> getquestionTypeList(PageData pd) {
		List<PageData> questionTypeList = null;
		try {
			questionTypeList = (List<PageData>)this.dao.findForList("DutyMapper.getquestionTypeList", pd);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return questionTypeList;
	}

	public List<PageData> getQuesDictionarySmallList() {
		List<PageData> questionTypeList = null;
		try {
			questionTypeList = (List<PageData>)this.dao.findForList("DutyMapper.getQuesDictionarySmallList", null);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return questionTypeList;
	}

	public List<PageData> getQuestionTypeById(PageData pd) {
		List<PageData> questionTypeList = null;
		try {
			questionTypeList = (List<PageData>)this.dao.findForList("DutyMapper.getQuestionTypeById", pd);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return questionTypeList;
	}

	public List<PageData> getMemberList(PageData pd) {
		List<PageData> memberList = null;
		try {
			memberList = (List<PageData>)this.dao.findForList("DutyMapper.getMemberList", pd);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return memberList;
	}

	public List<PageData> getQuestionTypeListByParentId(PageData pd) {
		List<PageData> memberList = null;
		try {
			memberList = (List<PageData>)this.dao.findForList("DutyMapper.getQuestionTypeListByParentId", pd);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return memberList;
	}

	@SuppressWarnings({ "unused", "unchecked", "null" })
	public PageData getMemberByGroupId(PageData pd) {
		pd.put("appMark", "client");
		String strDay = DateUtil.getyM();
		PageData pdMember = new PageData();
	    try {
	    	//List<PageData> memberList = getMemberList(pd); // 获取到所在组别，没休假的人员

	    	List<PageData> memberList = (List<PageData>) dao.findForList("DutyMapper.getMemberByGroup", pd);
		    List<PageData> dutyList = (List<PageData>)this.dao.findForList("DictionaryMapper.selectHosDuty", null);
		    PageData previousUser = (PageData) dao.findForObject("DutyMapper.getPreviousUser", pd); 			// 获取到上一个分配任务的用户的工号
		    pdMember = pollingSwitch(previousUser,dutyList,pd,strDay,memberList);
		    //pdMember = pageData(dutyList,pd,strDay,memberList);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pdMember;
	}

	public PageData getHosQuesDictionary(String DICTIONARY_ID_SMALL) {
		PageData quesDictionary = null;
		try {
			quesDictionary = (PageData) dao.findForObject("DutyMapper.getHosQuesDictionary", DICTIONARY_ID_SMALL);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return quesDictionary;
	}

	public void addDutyQuestion1(PageData pd) throws Exception {
		this.dao.save("DutyMapper.addDutyQuestion", pd);
	}

	public PageData getFileInfoById(PageData pd) {
		PageData quesDictionary = null;
		try {
			quesDictionary = (PageData) dao.findForObject("DutyMapper.getFileInfoById", pd);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return quesDictionary;
	}

	public void addDutyMark(PageData pd) {
		pd.put("QUESTION_MARK", pd.getString("MARK"));
		pd.put("MarkId", pd.getString("IMPLEMENT_ID"));
		pd.put("HOSMEMBERMARK_ID", UuidUtil.get32UUID());
		pd.put("CREATEDATE", new Date());
		try {
			dao.update("InformationUserMapper.insertHosMemberMark", pd);
			this.dao.update("DutyMapper.addDutyQuestionMark", pd);
			pd.put("ASSIGNOR_ID", pd.getString("IMPLEMENT_ID"));
			pd.put("ASSIGNOR_DATE", new Date());
			pd.put("SUB_ID", UuidUtil.get32UUID());
			this.dao.save("DutyMapper.addHosQuestionAsi", pd);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public PageData findMarkById(PageData pd) {
		PageData quesDictionary = null;
		try {
			quesDictionary = (PageData) dao.findForObject("DutyMapper.getHosQuesDictionary", pd);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return quesDictionary;
	}

	public List<PageData> getPartmentList(PageData pd) {
		List<PageData> quesDictionary = null;
		try {
			quesDictionary = (List<PageData>) dao.findForList("DutyMapper.getPartmentList", pd);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return quesDictionary;
	}

	@SuppressWarnings("unchecked")
	public PageData getMemberByDictionaryId(PageData pd) {
		List<PageData> memberList = null;
		PageData pdMember = null;
		String strDay = DateUtil.getyM();
		try {

		    List<PageData> dutyList = (List<PageData>)this.dao.findForList("DictionaryMapper.selectHosDuty", null);
		    memberList = (List<PageData>) dao.findForList("DutyMapper.getMemberByDictionaryId", pd);
		    //memberList = (List<PageData>) dao.findForList("DutyMapper.getMemberByGroup", pd); 					// 获取到所在组别，没休假的人员
		    PageData previousUser = (PageData) dao.findForObject("DutyMapper.getPreviousUser", pd); 			// 获取到上一个分配任务的用户的工号
		    pdMember = pollingSwitch(previousUser,dutyList,pd,strDay,memberList);
			//pdMember = pageData(dutyList,pd,strDay,memberList);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pdMember;
	}
}
