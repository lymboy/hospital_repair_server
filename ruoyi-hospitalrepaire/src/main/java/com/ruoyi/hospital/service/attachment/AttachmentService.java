package com.ruoyi.hospital.service.attachment;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.ruoyi.hospital.dao.DaoSupport;
import com.ruoyi.hospital.entity.Page;
import com.ruoyi.hospital.framework.exception.GenException;
import com.ruoyi.hospital.framework.log.Log;
import com.ruoyi.hospital.util.PageData;

/**
 * <p>Title: AttachmentService</p>
 * @Desc: 通用 附件管理
 * @author: 杨建军
 *	@Date: 2017年4月8日
 * @Time: 下午8:52:44
 * @version:
 */
@Service(value="attachmentService")
public class AttachmentService {

	@Resource(name = "daoSupport")
	private DaoSupport dao;


	/**
	 * 分页 查询文件列表 业务ID  [MASTER_ID] [MASTER_TYPE可选]
	 * @param page
	 * @return
	 * @throws GenException
	 */
	public List<PageData> getFileListPage(Page page) throws GenException {
		List<PageData> pds;
		try {
			pds = (List<PageData>) dao.findForList("AttachmentMapper.getFileListPage", page);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pds;
	}

	/**
	 * 查询文件列表 业务ID  [MASTER_ID] [MASTER_TYPE可选]
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public List<PageData> getFileList(PageData pd) throws GenException {
		List<PageData> pds;
		try {
			pds = (List<PageData>) dao.findForList("AttachmentMapper.getFileList", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pds;
	}

	/**
	 * 分页 获取 业务ID [MASTER_ID]  下的所有文件
	 * @param page
	 * @return
	 * @throws GenException
	 */
	public List<PageData> getFileListPageByMasterId(Page page) throws GenException {
		List<PageData> pds;
		try {
			pds = (List<PageData>) dao.findForList("AttachmentMapper.getFileListByMasterIdlistPage", page);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pds;
	}

	/**
	 * 获取 业务ID [MASTER_ID]  下的所有文件
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public List<PageData> getFileListByMasterId(PageData pd) throws GenException {
		 List<PageData> pds;
		try {
			pds = (List<PageData>) dao.findForList("AttachmentMapper.getFileListByMasterId", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pds;
	}

	/**
	 * 获取文件 记录信息
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public PageData getFileInfoById(PageData pd) throws GenException {
		PageData pdInfo;
		try {
			pdInfo = (PageData) dao.findForObject("AttachmentMapper.getFileInfoById", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pdInfo;
	}

	/**
	 * 获取文件个数 通过 业务ID [MASTER_ID]
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public int getFileNumByMasterId(PageData pd) throws GenException {
		int num=0;
		try {
			num = (Integer) dao.findForObject("AttachmentMapper.getFileNumByMasterId", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return num;
	}

	/**
	 *  插入新 上传文件纪录
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	@Log(name="插入新 上传文件纪录-addFileInfo")
	public int addFileInfo(PageData pd) throws GenException {
		int num=0;
		try {
			num = (Integer) dao.save("AttachmentMapper.addFileInfo", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return num;
	}

	/**
	 * 更新文件信息记录 的 业务ID [MASTER_ID]
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	@Log(name="更新文件信息记录的 业务ID[MASTER_ID]-updateMasterIdByMasterId")
	public int updateMasterIdByMasterId(PageData pd) throws GenException {
		int num=0;
		try {
			num = (Integer) dao.update("AttachmentMapper.updateMasterIdByMasterId", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return num;
	}
	/**
	 * 更新文件信息
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	@Log(name="更新文件信息-updateAttachInfoById")
	public int updateAttachInfoById(PageData pd) throws GenException {
		int num=0;
		try {
			num = (Integer) dao.update("AttachmentMapper.updateAttachInfoById", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return num;
	}

	/**
	 * 删除文件记录 通过 主键
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	@Log(name="删除文件记录 通过 主键-deleteFileInfoById")
	public int deleteFileInfoById(PageData pd) throws GenException {
		int num=0;
		try {
			num = (Integer) dao.update("AttachmentMapper.deleteFileInfoById", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return num;
	}

}
