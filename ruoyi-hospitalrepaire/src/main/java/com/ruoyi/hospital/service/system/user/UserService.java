package com.ruoyi.hospital.service.system.user;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import net.sf.json.JSONObject;
import org.springframework.stereotype.Service;

import com.ruoyi.hospital.dao.DaoSupport;
import com.ruoyi.hospital.entity.Page;
import com.ruoyi.hospital.entity.system.User;
import com.ruoyi.hospital.framework.exception.GenException;
import com.ruoyi.hospital.framework.log.Log;
import com.ruoyi.hospital.util.PageData;
import com.ruoyi.hospital.util.UuidUtil;

/**
 * <p>Title: UserService</p>
 * @Desc:
 * @author: 杨建军
 *	@Date: 2017年3月8日
 * @Time: 上午11:49:25
 * @version:
 */
@Service("userService")
public class UserService {

	@Resource(name = "daoSupport")
	private DaoSupport dao;

	//======================================================================================

	/*
	*用户列表(全部)
	*/
	public List<User> listAllUser(Page pd) throws GenException{
		List<User> user = null;
/*		if(1>0){
			throw new AppException("1大于0");
		}*/
		try {
			user= (List<User>) dao.findForList("UserMapper.getUserlistPage", pd);
		} catch (Exception e) {
			throw new  GenException(e.getCause());
		}
		return user;
	}

	/*
	 *用户列表(全部)
	 */
	public List<PageData> getAllUserlistPage(Page page) throws GenException{
		List<PageData> userList = null;
		/*		if(1>0){
			throw new AppException("1大于0");
		}*/
		try {
			userList= (List<PageData>) dao.findForList("UserMapper.getAllUserlistPage", page);
		} catch (Exception e) {
			throw new  GenException(e.getCause());
		}
		return userList;
	}

	/*
	* 通过id获取数据
	*/
	public PageData findByUiId(PageData pd)throws GenException{
		PageData pda = new PageData();
		try {
			pda = (PageData)dao.findForObject("UserMapper.findByUiId", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pda;
	}

	/*
	* 通过编号获取数据
	*/
	public PageData findByUN(PageData pd)throws GenException{
		PageData pda = new PageData();
		try {
			pda = (PageData)dao.findForObject("UserMapper.findByUN", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pda;
	}
	/*
	* 修改用户
	*/
	@Log(name="修改用户信息")
	public void updateU(PageData pd)throws GenException{
		try {
			dao.update("UserMapper.updateHosMember", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}
	/*
	* 通过loginname获取数据
	*/
	public PageData findByUId(PageData pd)throws GenException{
		PageData pda = new PageData();
		try {
			pda = (PageData)dao.findForObject("UserMapper.findByUId", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pda;
	}
	/*
	* 批量删除用户
	*/
	@Log(name="批量删除用户")
	public void deleteAllU(Map<String, Object> map)throws GenException{
		map.put("modifydate", new Date());
		try {
			//先删除员工
			dao.update("UserMapper.deleteAllEmp", map);
			//再删系统用户
			dao.delete("UserMapper.deleteAllU", map);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}
	/*
	* 保存用户
	*/
	@Log(name="保存新建用户信息")
	public void saveU(PageData pd)throws GenException{
		try {
			dao.save("UserMapper.saveHosMmber", pd);// 新增用户
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}
	/*
	* 删除用户
	*/
	@Log(name="删除用户")
	public void deleteU(PageData pd)throws GenException{
		try {
			//先删除员工
			dao.update("UserMapper.deleteEmp", pd);
			//再删系统用户
			dao.delete("UserMapper.deleteU", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}
	/*
	 * 获取用户总纪录数
	 */
	public Integer getAllUserCount() throws GenException {
		Integer num = 0;
		try {
			num = (Integer)dao.findForObject("UserMapper.getAllUserCount", "");
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return num;
	}

	/*
	* 登录判断
	*/
	public PageData getUserByNameAndPwd(PageData pd)throws GenException{
		PageData pda = new PageData();
		try {
			pda = (PageData)dao.findForObject("UserMapper.getUserInfo", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pda;
	}

	/*
	 * 存储登录用户的IP和登录时间
	 */
	@Log(name="存储登录用户的IP和登录时间")
	public void saveIP(PageData pd) throws GenException{
		try {
			dao.update("UserMapper.saveIP", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}

	//获取所有拥有该角色的用户
	public List<PageData> getUserListByRoleId(PageData pd) throws GenException {
		List<PageData> pds = null;
		try {
			pds = (List<PageData>) dao.findForList("UserMapper.getUserListByRoleId", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pds;
	}

	public void setSKIN(PageData pd) throws GenException {
		try {
			dao.save("UserMapper.editU", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}

	public int changePwd(PageData pd) throws GenException {
		try {
			return (int) dao.save("UserMapper.updateHosMember", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}

	public PageData confirmationPwd(PageData pd) throws GenException {
		try {
			return (PageData) dao.findForObject("UserMapper.confirmationPwd", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}

	public PageData findByUserName(PageData pd) throws GenException {
		PageData pda = new PageData();
		try {
			pda = (PageData)dao.findForObject("UserMapper.findByUserName", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pda;
	}

	 public void saveHosMmber(PageData pd) {
	    try {
	      this.dao.save("UserMapper.saveHosMmber", pd);
	    } catch (Exception e) {
	      e.printStackTrace();
	    }
	  }

	public void updateHosMmber(PageData pd) {
		try {
		      this.dao.save("UserMapper.updateHosMmber", pd);
		    } catch (Exception e) {
		      e.printStackTrace();
		    }

	}

	public PageData findByOpenId(PageData pd) throws GenException {
		PageData pda = new PageData();
		try {
			pda = (PageData)dao.findForObject("UserMapper.findByOpenId", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pda;
	}

	public PageData findByDepartmentId(PageData pd) throws GenException {
		PageData pda = new PageData();
		try {
			pda = (PageData)dao.findForObject("UserMapper.findByDepartmentId", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pda;

	}

	public int unbindWeChat(PageData pd) {
		int i = 0;
		try {
			i = (int) dao.update("UserMapper.unbindWeChat", pd);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return i;
	}
}
