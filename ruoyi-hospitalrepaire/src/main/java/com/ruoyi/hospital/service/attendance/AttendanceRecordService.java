package com.ruoyi.hospital.service.attendance;

import com.ruoyi.hospital.dao.DaoSupport;
import com.ruoyi.hospital.entity.Page;
import com.ruoyi.hospital.framework.exception.GenException;
import com.ruoyi.hospital.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p> 考勤记录 </p>
 *
 * @author Young
 * @since 2018/4/2
 */
@Service
public class AttendanceRecordService {

    @Autowired
    private DaoSupport dao;

    /**
     * 列表 分页
     * @param page
     * @return
     * @throws GenException
     */
    public List<PageData> getAttendanceRecordlistPage(Page page) throws GenException {
        try {

            return (List<PageData>) dao.findForList("AttendanceRecordMapper.getAttendanceRecordlistPagePrivilegeFilter", page);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     * 列表 分页---------------------------------------------------------===========================
     * @param page
     * @return
     * @throws GenException
     */
    public List<PageData> getAttendanceRecordlistPage1(Page page) throws GenException {
        try {
            return (List<PageData>) dao.findForList("AttendanceRecordMapper.getAttendanceRecordlistPagePrivilegeFilter", page);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }




    /**
     * 根据ID 查
     * @param ID
     * @return
     * @throws GenException
     */
    public PageData getAttendanceRecordById(String ID) throws GenException {
        try {
            return (PageData) dao.findForObject("AttendanceRecordMapper.getAttendanceRecordById", ID);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     * 判断 员工当前日期是否已经有过记录 条件 EMPLOYEE_ID，ATTENDANCE_DATE
     * @param pd
     * @return
     * @throws GenException
     */
    public PageData getAttendanceRecordByEmpIdAndAttendanceDate(PageData pd) throws GenException {
        try {
            return (PageData) dao.findForObject("AttendanceRecordMapper.getAttendanceRecordByEmpIdAndAttendanceDate", pd);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     * 新增
     * @param pd [ID,BILL_CODE,EMPLOYEE_ID,ATTENDANCE_DATE]
     * @return 0 添加失败， -1 数据已存在
     * @throws GenException
     */
    public int insertAttendanceRecord(PageData pd, String userId) throws GenException {
        try {
            if (getAttendanceRecordByEmpIdAndAttendanceDate(pd) != null) {
                return -1;
            }
            pd.put("EDITOR", pd.get("EMPLOYEE_ID"));
            pd.put("EDIT_DATE", new Date());
            pd.put("CREATEDATE", new Date());
            pd.put("CREATER", userId);
            return (int) dao.save("AttendanceRecordMapper.insertAttendanceRecord", pd);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     * 从表 列表
     * @param pd
     * @return
     * @throws GenException
     */
    public List<PageData> getAttendanceRecordDetailList(PageData pd) throws GenException {
        try {
            return (List<PageData>) dao.findForList("AttendanceRecordMapper.getAttendanceRecordDetailList", pd);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     * 从表 根据ID 查
     * @param ID
     * @return
     * @throws GenException
     */
    public PageData getAttendanceRecordDetailById(String ID) throws GenException {
        try {
            return (PageData) dao.findForObject("AttendanceRecordMapper.getAttendanceRecordDetailById", ID);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     * 从表 判断是否签过到，或者已经签退 条件：主表ID[ATTENDANCE_RECORD_ID]和类型[SIGN_TYPE]【69上班，70下班】
     * @param pd
     * @return
     * @throws GenException
     */
    public PageData getAttendanceRecordDetailByAttendanceRecordIdAndType(PageData pd) throws GenException {
        try {
            return (PageData) dao.findForObject("AttendanceRecordMapper.getAttendanceRecordDetailByAttendanceRecordIdAndType", pd);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     * 从表 新增
     * @param pd [ID,ATTENDANCE_RECORD_ID,SIGN_TIME,SIGN_TYPE,SIGN_ADDRESS,SIGN_ADDRESS]
     * @return 0 添加失败， 1 成功
     * @throws GenException
     */
    public int insertAttendanceRecordDetail(PageData pd, List<String> signImagesPath, String userId) throws GenException {
        try {
            PageData detail = getAttendanceRecordDetailByAttendanceRecordIdAndType(pd);
            String attendanceRecordDetailId = null;
            if (detail != null) {
                // 可在此更新
                attendanceRecordDetailId = String.valueOf(detail.get("ID"));
            }
            if (detail == null) {
                pd.put("CREATEDATE", new Date());
                pd.put("CREATER", userId);
                int row = 0;
                row = (Integer) dao.save("AttendanceRecordMapper.insertAttendanceRecordDetail", pd);
                if (row > 0) {
                    attendanceRecordDetailId = String.valueOf(pd.get("ID"));
                }
            }
            // 上传附件 签到图片
            if (attendanceRecordDetailId != null) {
                if (signImagesPath != null && signImagesPath.size() > 0) {
                    for (int i = 0; i < signImagesPath.size(); i++) {
                        String fileUploadPath = signImagesPath.get(i);
                        // 保存记录
                        PageData pdImg = new PageData();
                        pdImg.put("ID", UuidUtil.get32UUID());
                        pdImg.put("ATTENDANCE_RECORD_DETAIL_ID", attendanceRecordDetailId);
                        pdImg.put("IMG_PATH", fileUploadPath);
                        pdImg.put("UPLOADER", userId);
                        pdImg.put("UPLOAD_DATE", new Date());
                        pdImg.put("CREATEDATE", new Date());
                        pdImg.put("CREATER", userId);
                        dao.save("AttendanceRecordMapper.insertAttendanceRecordDetailImg", pdImg);
                    }
                }
            }
            if (attendanceRecordDetailId != null) {
                return 1;
            } else {
                return 0;
            }
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     * 从表 编辑
     * @param pd [SIGN_TIME,SIGN_ADDRESS]
     * @return
     * @throws GenException
     */
    public int updateAttendanceRecordDetail(PageData pd, String userId) throws GenException {
        try {
            pd.put("MODIFYDATE", new Date());
            pd.put("MODIFIER", userId);
            return (int) dao.save("AttendanceRecordMapper.updateAttendanceRecordDetail", pd);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     * 从表 删除
     * @param pd
     * @return
     * @throws GenException
     */
    public int deleteAttendanceRecordDetail(PageData pd, String userId) throws GenException {
        try {
            pd.put("MODIFYDATE", new Date());
            pd.put("MODIFIER", userId);
            return (int) dao.save("AttendanceRecordMapper.deleteAttendanceRecordDetail", pd);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     * 从表 签到照片 列表
     * @param pd 【ATTENDANCE_RECORD_DETAIL_ID】
     * @return
     * @throws GenException
     */
    public List<PageData> getAttendanceRecordDetailImgList(PageData pd) throws GenException {
        try {
            return (List<PageData>) dao.findForList("AttendanceRecordMapper.getAttendanceRecordDetailImgList", pd);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     * 从表 签到照片 根据ID 查
     * @param ID
     * @return
     * @throws GenException
     */
    public PageData getAttendanceRecordDetailImgById(String ID) throws GenException {
        try {
            return (PageData) dao.findForObject("AttendanceRecordMapper.getAttendanceRecordDetailImgById", ID);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     * 从表 签到照片 新增
     * @param multipartFileList
     * @return
     * @throws GenException
     */
    public int insertAttendanceRecordDetailImg(List<MultipartFile> multipartFileList, String attendanceRecordDetailId, String userId) throws GenException {
        try {
            if (multipartFileList == null || multipartFileList.size() == 0) {
                return 0;
            }
            int rows = 0;
            for (int i = 0; i < multipartFileList.size(); i++) {
                // 文件上传
                MultipartFile multipartFile = multipartFileList.get(i);
                String fileUploadPath = FileUtils.getFileUploadPath(Const.FILEPATHIMG, multipartFile.getOriginalFilename());
                FileUtils.uploadMultipartFile(Const.APP_UPLOAD_PATH + fileUploadPath, multipartFile);
                // 保存记录
                PageData pd = new PageData();
                pd.put("ID", UuidUtil.get32UUID());
                pd.put("ATTENDANCE_RECORD_DETAIL_ID", attendanceRecordDetailId);
                pd.put("IMG_PATH", fileUploadPath);
                pd.put("UPLOADER", userId);
                pd.put("UPLOAD_DATE", new Date());
                pd.put("CREATEDATE", new Date());
                pd.put("CREATER", userId);
                rows += (int) dao.save("AttendanceRecordMapper.insertAttendanceRecordDetailImg", pd);
            }
            return rows;
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     * 从表 签到照片 删除
     * @param pd
     * @return
     * @throws GenException
     */
    public int deleteAttendanceRecordDetailImg(PageData pd, String userId) throws GenException {
        try {
            pd.put("MODIFYDATE", new Date());
            pd.put("MODIFIER", userId);
            return (int) dao.save("AttendanceRecordMapper.deleteAttendanceRecordDetailImg", pd);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     * 本月考勤统计
     * @param pd
     * @return
     */
    public List<PageData> getEmployeeAttendanceRecordDataCountInCurrentMonth(PageData pd) throws GenException {
        try {
            return (List<PageData>) dao.findForList("AttendanceRecordMapper.getEmployeeAttendanceRecordDataCountInCurrentMonth", pd);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }
    /**
     * 机构本月考勤统计
     * @param pd
     * @return
     */
    public List<PageData> getEmployeeAttendanceRecordDataCountInCurrentMonthList(PageData pd) throws GenException {
        try {
            return (List<PageData>) dao.findForList("AttendanceRecordMapper.getEmployeeAttendanceRecordDataCountInCurrentMonthList", pd);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     * 机构或单人本月出勤率分组统计
     * @param pd
     * @return
     */
    public List<PageData> getEmployeeAttendanceRecordDataCountInCurrentMonthDetail(PageData pd) throws GenException {
        try {
            return (List<PageData>) dao.findForList("AttendanceRecordMapper.getEmployeeAttendanceRecordDataCountInCurrentMonthDetail", pd);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     * 获取 或 创建 员工考勤记录表主表数据[EMPLOYEE_ID][ATTENDANCE_DATE]
     * @param pd
     * @return
     */
    public PageData getOrCreateEmpAttendanceRecordSheet(PageData pd, String userId) throws GenException {
        try {
            PageData dataInfo = this.getAttendanceRecordByEmpIdAndAttendanceDate(pd);
            if (dataInfo == null) {
                // 创建
                dataInfo = new PageData();
                pd.put("ID", UuidUtil.get32UUID());
                pd.put("BILL_CODE", sequence.nextId());
                pd.put("EDITOR", pd.get("EMPLOYEE_ID"));
                pd.put("EDIT_DATE", new Date());
                pd.put("CREATEDATE", new Date());
                pd.put("CREATER", userId);
                int row = (Integer) dao.save("AttendanceRecordMapper.insertAttendanceRecord", pd);
                if (row>0) {
                    dataInfo.putAll(pd);
                }
            }
            return dataInfo;
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     * 员工月考勤记录列表
     * @param page
     * @return
     */
    public List<PageData> getEmployeeMonthAttendanceRecordlistPage(Page page) throws GenException {
        try {
            List<PageData> dataList = (List<PageData>) dao.findForList("AttendanceRecordMapper.getEmployeeMonthAttendanceRecordlistPage", page);
            if (dataList != null && dataList.size()>0) {
                for (int i = 0; i < dataList.size(); i++) {
                    PageData dataInfo = dataList.get(i);
                    PageData pd = new PageData();
                    pd.put("ATTENDANCE_RECORD_ID", String.valueOf(dataInfo.get("ID")));
                    dataInfo.put("ATTENDANCE_DATE_LUNAR", DateLunarUtil.getDateLunarInfo(String.valueOf(dataInfo.get("ATTENDANCE_DATE"))));
                    List<PageData> signDataList = (List<PageData>) dao.findForList("AttendanceRecordMapper.getAttendanceRecordDetailList", pd);
                    dataInfo.put("signDataList", signDataList);
                }
            }
            return dataList;
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     * 获取 考勤有效地址区域
     * @param departmentId
     * @return
     */
    public PageData getAttendanceSignAddressByDeptId(String departmentId) throws GenException {
        try {
            return (PageData) dao.findForObject("AttendanceRecordMapper.getAttendanceSignAddressByDeptId", departmentId);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     * 签到前 判断 是否请假
     * @param employeeId
     * @return
     * @throws GenException
     */
    public int getLeaveRecordNumForSignIn(String employeeId) throws GenException {
        try {
            return (int) dao.findForObject("AttendanceRecordMapper.getLeaveRecordNumForSignIn", employeeId);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }
    /**
     * 签退前 判断 是否请假
     * @param employeeId
     * @return
     * @throws GenException
     */
    public int getLeaveRecordNumForSignOut(String employeeId) throws GenException {
        try {
            return (int) dao.findForObject("AttendanceRecordMapper.getLeaveRecordNumForSignOut", employeeId);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     * 获取所有工区列表
     * */
    public PageData getDepartmentId(PageData pd) throws GenException {
        try {
            return (PageData) dao.findForObject("AttendanceRecordMapper.getDepartmentId", pd);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    public PageData getDepartmentById(PageData pd) throws GenException {
        try {
            return (PageData) dao.findForObject("AttendanceRecordMapper.getDepartmentById", pd);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    public List<PageData> getDepartmentList(PageData pd) throws GenException {
        try {
            return (List<PageData>) dao.findForList("AttendanceRecordMapper.getDepartmentList", pd);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     * 获取出勤人员
     * @return
     * @throws GenException
     */
    public List<PageData> getTurnOutList(PageData pd, List<String> EmployeeIdsList) throws GenException {
        try {
        	pd.put("EmployeeIdsList", EmployeeIdsList);
            return (List<PageData>) dao.findForList("AttendanceRecordMapper.getTurnOutList", pd);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    public List<PageData> getSignTime(PageData pd) throws GenException {
        try {
            return (List<PageData>) dao.findForList("AttendanceRecordMapper.getSignTime", pd);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }


    /**
     * 获取请假人员
     * @return
     * @throws GenException
     */
    public List<PageData> getLeaveList(PageData pd, List<String> EmployeeIdsList) throws GenException {
        try {
        	pd.put("EmployeeIdsList", EmployeeIdsList);
            return (List<PageData>) dao.findForList("AttendanceRecordMapper.getLeaveList", pd);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     * 获取旷工人员
     * @return
     * @throws GenException
     */
    public List<PageData> getWithoutLeaveCount(PageData pd,List<PageData> leaveList,List<PageData> turnOutList) throws GenException {
        try {
        	List<String> leaveString = new ArrayList<String>();
        	if (leaveList != null && leaveList.size() > 0) {
        		for (PageData leavePd : leaveList) {
           		 	String str = leavePd.getString("EMPLOYEE_ID");
           		 	leaveString.add(str);
        		}
        	}
        	List<String> turnOutString = new ArrayList<String>();
        	if (turnOutList != null && turnOutList.size() > 0) {
        		for (PageData turnOutPd : turnOutList) {
           		 	String str = turnOutPd.getString("EMPLOYEE_ID");
           		 	turnOutString.add(str);
        		}
        	}
        	pd.put("leaveString", leaveString);
        	pd.put("turnOutString", turnOutString);
            return (List<PageData>) dao.findForList("AttendanceRecordMapper.getWithoutLeaveCount", pd);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }
}
