package com.ruoyi.hospital.service.information;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.ruoyi.hospital.dao.DaoSupport;
import com.ruoyi.hospital.entity.Page;
import com.ruoyi.hospital.framework.exception.GenException;
import com.ruoyi.hospital.framework.log.Log;
import com.ruoyi.hospital.util.PageData;

/**
 * <p>Title: TransferService</p>
 * @Desc: 员工调动
 * @author: 杨建军
 *	@Date: 2017年3月25日
 * @Time: 下午1:46:38
 * @version:
 */
@Service(value="transferService")
public class TransferService {

	@Resource(name = "daoSupport")
	private DaoSupport dao;

	/**
	 * 部门改变   更新员工所在机构和部门
	 * @throws GenException
	 */
	@Log(name="部门改变   更新员工所在机构和部门 及插入履历 - updateEmpDepartmentById")
	public int transferEmployeeDep(PageData pd) throws GenException {
		int num=0;
		try {
			num = (Integer)dao.update("TransferMapper.updateEmpDepartmentById", pd);//部门改变   更新员工所在机构和部门
			num = (Integer)dao.save("TransferMapper.addNewPersonDetail", pd);//部门改变  插入履历
			// departmentId_old employeeId
			num = (Integer)dao.save("TransferMapper.updatePersonLeaveDateById", pd);//部门改变   更新上一个离职时间

			String deletePrivilege = pd.getString("deletePrivilege");
			if("true".equals(deletePrivilege)){// 删除员工权限，根据员工ID
				dao.delete("TransferMapper.deleteEmpPrivilegeAfterTransfer", pd);
			}
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return num;
	}

	/**
	 * 部门改变  插入履历
	 * @throws GenException
	 */
	@Log(name="部门改变  插入履历  - addNewPersonDetail")
	public int addNewPersonDetail(PageData pd) throws GenException {
		int num=0;
		try {
			num = (Integer)dao.save("TransferMapper.addNewPersonDetail", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return num;
	}
	/**
	 * 分页获取员工列表
	 * @param page
	 * @return
	 * @throws GenException
	 */
	public List<PageData> getEmployeeListPage(Page page) throws GenException {
		List<PageData> employeeList=null;
		try {
			employeeList=(List<PageData>) dao.findForList("TransferMapper.getEmployeelistPage", page);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return employeeList;
	}

}
