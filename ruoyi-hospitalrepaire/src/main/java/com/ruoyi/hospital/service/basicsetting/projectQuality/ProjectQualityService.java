package com.ruoyi.hospital.service.basicsetting.projectQuality;

import java.io.File;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Service;

import com.ruoyi.hospital.dao.DaoSupport;
import com.ruoyi.hospital.entity.Page;
import com.ruoyi.hospital.framework.exception.GenException;
import com.ruoyi.hospital.util.Const;
import com.ruoyi.hospital.util.PageData;
import com.ruoyi.hospital.util.UuidUtil;
import com.ruoyi.hospital.util.sequence;

@Service(value="projectQualityService")
public class ProjectQualityService {

	@Resource(name="daoSupport")
	private DaoSupport dao;

	//分页查询项目质量单元列表
	public List<PageData> projectQualityListPage(Page page) throws GenException {
		List<PageData> projectQuality=null;
		try {
			projectQuality=(List<PageData>) dao.findForList("ProjectQualityMapper.projectQualitylistPage", page);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return projectQuality;
	}

	//通过id查询
	public PageData getProjectQualityById(PageData pd) throws GenException{
		PageData projectQualityInfo=null;
		try {
			projectQualityInfo=(PageData) dao.findForObject("ProjectQualityMapper.getProjectQualityById", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return projectQualityInfo;
	}

	public PageData getProjectQualityDetailById(PageData pd) throws GenException{
		PageData projectQualityInfo=null;
		try {
			projectQualityInfo=(PageData) dao.findForObject("ProjectQualityMapper.getProjectQualityDetailById", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return projectQualityInfo;
	}

	public PageData getProjectQualityDetailByparentId(PageData pd) throws GenException{
		PageData projectQualityInfo=null;
		try {
			projectQualityInfo=(PageData) dao.findForObject("ProjectQualityMapper.getProjectQualityDetailByparentId", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return projectQualityInfo;
	}

	public PageData getProjectQualityDetailBySort(PageData pd) throws GenException{
		PageData projectQualityInfo=null;
		try {
			projectQualityInfo=(PageData) dao.findForObject("ProjectQualityMapper.getProjectQualityDetailBySort", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return projectQualityInfo;
	}

	//得到项目的条数
	public int getProjectQualityNum() throws GenException {
		int projectQualityNum=0;
		try {
			projectQualityNum=(int) dao.findForObject("ProjectQualityMapper.getProjectQualityNum", null);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return projectQualityNum;
	}

	// 生成 单据 编号 最后数字
	public int getMaxProjectQualityBillCodeSuffix(PageData pd) throws GenException {
		int billCodeNum = 0;
		try {
			String billCodeStr=(String) dao.findForObject("ProjectQualityMapper.getMaxProjectQualityBillCodeSuffix", pd);
			if (billCodeStr == null || "".equals(billCodeStr)) {
				return 0;
			}
			try {
				String[] billCodeCharArray = billCodeStr.split("-");
				Integer maxNum = Integer.valueOf(billCodeCharArray[billCodeCharArray.length-1]);
				if (maxNum != null) {
					billCodeNum = maxNum;
				}
			} catch (Exception e) {
				billCodeNum = 0;
			}
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return billCodeNum;
	}

	/**
	 * 新增 或 编辑  质量单元
	 * @param pd
	 * @param userId
	 * @return
	 * @throws GenException
	 */
	public int insertOrUpdateProjectQuality(PageData pd, String userId) throws GenException {
		try {
			PageData pdInfo = this.getProjectQualityById(pd);
			if(pdInfo != null) {
				pd.put("MODIFIER", userId);
				pd.put("MODIFYDATE", new Date());
				return (int) dao.update("ProjectQualityMapper.ProjectQualityInfoUpdate", pd); // 编辑
			}
			pd.put("CREATER", userId);
			pd.put("CREATEDATE", new Date());
			pd.put("VERSION_NO", 1);
			pd.put("FAST_APPROVE_TYPE", 0);
			return (int) dao.save("ProjectQualityMapper.ProjectQualityInfoAdd", pd); // 添加
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}

	//项目质量单元添加
	public void ProjectQualityInfoAdd(PageData pd) throws GenException {
		try {
			dao.save("ProjectQualityMapper.ProjectQualityInfoAdd", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}

	}

	//项目质量单元编辑更新
	public void ProjectQualityInfoUpdate(PageData pd) throws GenException {
		try {
			dao.update("ProjectQualityMapper.ProjectQualityInfoUpdate", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}

	}

	//项目质量单元删除
	public int deleteProjectQuality(PageData pd) throws GenException {
		int num=0;
		try {
			num=(Integer)dao.update("ProjectQualityMapper.deleteProjectQuality", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return num;
	}

	//修订
	public int updateProjectQualityInfo(PageData pd, String id) throws GenException {
		int num = 0;
		try {
			// 获取 修订前  上一条记录的数据
			PageData pdInfo = (PageData)dao.findForObject("ProjectQualityMapper.getProjectQualityById", pd);

			// 更新信息 增加一个版本
			String versionNoStr = pdInfo.getString("VERSION_NO");
			int versionNo = Integer.valueOf(versionNoStr) + 1;
			pdInfo.put("VERSION_NO", versionNo);
			pdInfo.put("FAST_APPROVE_TYPE", "0");
			pdInfo.put("CREATEDATE", new Date());
			pdInfo.put("CREATER", pd.getString("creater"));
			pdInfo.put("MODIFYDATE", new Date());
			pdInfo.put("MODIFIER", pd.getString("modifier"));
			pdInfo.put("ID", id);
			pdInfo.put("EDITOR", pd.getString("creater"));
			pdInfo.put("EDIT_DATE", new Date());
			pdInfo.put("FLAG", "1");
			pdInfo.put("PROJECT_BASE_NUM", pd.getString("PROJECT_BASE_NUM"));
			dao.update("ProjectQualityMapper.insertProjectQualityInfo", pdInfo); // 新版本
			// 从表 复制
			pd.put("PROJECT_QUALITY_ID", pd.getString("ID"));
			List<PageData> pdcList = (List<PageData>) dao.findForList("ProjectQualityMapper.getProjectQualityDetailListCopy", pd);
			String[] parentIdStr = new String[200];
			for (int i = 0; i < parentIdStr.length; i++) {
				parentIdStr[i]="0";
			}

			for (PageData pdc : pdcList) {
				String baseIdStr =  pdc.getString("BASE_ID");
				String newDetailId =  UuidUtil.get36UUID();
				if(baseIdStr!=null){
					String[] baseIdArray = baseIdStr.split("-");
					parentIdStr[baseIdArray.length]=newDetailId;
					pdc.put("PARENT_UNIT_ID", parentIdStr[baseIdArray.length-1]);
				}

				pdc.put("ID", newDetailId);
				pdc.put("PROJECT_QUALITY_ID", id);
				pdc.put("CREATEDATE", new Date());
				pdc.put("CREATER", pd.getString("creater"));
				pdc.put("MODIFYDATE", new Date());
				pdc.put("MODIFIER", pd.getString("modifier"));
				pdc.put("PROJECT_BASE_NUM", pd.getString("PROJECT_BASE_NUM"));
				dao.save("ProjectQualityMapper.copyProjectQualityDetail", pdc);
			}

			// 附件 复制
			pd.put("masterId", pd.getString("ID"));
			List<PageData> pdtList = (List<PageData>) dao.findForList("ProjectQualityMapper.getProjectQualityDetailAttachmentList", pd);
			for (PageData pdt : pdtList) {
				String oldId = pdt.getString("ID");
				String newId = UuidUtil.get36UUID();
				String oldMasterId = pdt.getString("MASTER_ID");
				pdt.put("ID", newId);
				pdt.put("MASTER_ID", id);
				pdt.put("CREATEDATE", new Date());
				pdt.put("CREATER", pd.getString("creater"));
				pdt.put("MODIFYDATE", new Date());
				pdt.put("MODIFIER", pd.getString("modifier"));
				dao.save("ProjectQualityMapper.copyProjectQualityDetailAttachment", pdt);

				// 文件 复制
				// 得到要下载的文件信息
				String fileUrl = pdt.getString("SUB_SYSTEM")+File.separator+ pdt.getString("MODULE_NAME");
				String fileName = pdt.getString("FILE_NAME");
				// 找出文件
				String path = Const.FILE_UPLOAD_ROOT_URL+File.separator+ "uploadFiles/attachment/";
				String filePath = path+File.separator+fileUrl+File.separator+oldMasterId+File.separator+oldId+fileName.substring(fileName.lastIndexOf("."));
				String filePathNew = path+File.separator+fileUrl+File.separator+id+File.separator+newId+fileName.substring(fileName.lastIndexOf("."));
				File file = new File(filePath);
				File fileNew = new File(filePathNew);
				if(file.exists()){
					File folder = new File(path+File.separator+fileUrl+File.separator+id);
					if(!folder.exists()) {
						folder.mkdirs();
					}
					if(!fileNew.exists()){
						FileUtils.copyFile(file, fileNew);
					}
				}
			}

			// 修订 原来的版本
			pd.put("projectQualityId", pd.getString("ID"));
			dao.update("ProjectQualityMapper.deleteProjectQuality", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return num;
	}

	//快审
	public int updateFastTrialById(PageData pd) throws GenException {
		int num = 0;
		try {
			num = (Integer) dao.update("ProjectQualityMapper.updateFastTrialById", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return num;
	}
	/*************************************** 从表 历史 查询 ***********************************/
	/**
	 * 分页得到项目质量单元划分的从表
	 * */
	public List<PageData> getProjectQualityDetailList(PageData pd) throws GenException {
		List<PageData>pds=null;
		try {
			pds=(List<PageData>) dao.findForList("ProjectQualityMapper.getProjectQualityDetailList", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pds;
	}

	public List<PageData> getProjectQualityDetailList1(PageData pd) throws GenException {
		List<PageData>pds=null;
		try {
			pds=(List<PageData>) dao.findForList("ProjectQualityMapper.getProjectQualityDetailList1", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pds;
	}

	/**
	 * 获取单据列表
	 * @param page
	 * @return
	 * @throws GenException
	 */
	public List<PageData> getAllProjectQualityHistorylistPage(Page page) throws GenException {
		List<PageData> pds = null;
		try {
			pds = (List<PageData>) dao.findForList("ProjectQualityMapper.getAllProjectQualityHistorylistPage", page);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pds;
	}

	/**
	 * 获取项目质量单元级别 BY PROJECT_QUALITY_ID
	 * @param projectQualityId
	 * @return
	 * @throws GenException
	 */
	public String getQualityUnitLevel(String projectQualityId) throws GenException{
		String projectQualityLevel = "";
		try {
			projectQualityLevel = (String) dao.findForObject("ProjectQualityMapper.getProjectQualityLevel", projectQualityId);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return projectQualityLevel;
	}

	/**
	 * 项目质量单元详细的编辑保存
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public int editProjectQualityDetailInfo(PageData pd) throws GenException{
		int num = 0;
		try {
			num = (Integer)dao.update("ProjectQualityMapper.editProjectQualityDetailInfo", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return num;
	}

	/**
	 * 获取项目质量单元树
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public List<PageData> getProjectQualityTree(PageData pd) throws GenException {
		List<PageData> pdList = null;
		try {
			pdList = (List<PageData>) dao.findForList("ProjectQualityMapper.getProjectQualityTree", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pdList;
	}

	//获取项目质量单元详细的条数
	public int getProjectQualityDetailNum() throws GenException {
		int num=0;
		try {
			num=(int) dao.findForObject("ProjectQualityMapper.getProjectQualityDetailNum", null);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return num;
	}

	//得到质量单元从表 数据
	public List<PageData> getQualityUnitDetailModelList(PageData pd) throws GenException {
		List<PageData> pds=null;
		try {
			pds=(List<PageData>) dao.findForList("ProjectQualityMapper.getQualityUnitDetailModelList",pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pds;
	}

	//得到标段质量单元从表 数据
		public List<PageData> getProjectQualityDetailModelList(PageData pd) throws GenException {
			List<PageData> pds=null;
			try {
				pds=(List<PageData>) dao.findForList("ProjectQualityMapper.getProjectQualityDetailModelList",pd);
			} catch (Exception e) {
				throw new GenException(e.getCause());
			}
			return pds;
		}

	/**
	 * 保存所选择的模板数据
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public int addQualityUnitModelData(PageData pd) throws GenException {
		int num = 0;
		try {
			// 获取所选的 质量单元划分模板数据  已知  PROJECT_QUALITY_ID qualityUnitId qualityUnitDetailId baseId
			pd.put("PROCESS_FILTER", true);
			String unitCode = String.valueOf(pd.get("unitCode"));
			String preUnitCode = "";
			if (unitCode != null && unitCode.length() > 0) {
				for (int i = 0; i < unitCode.length(); i++) {
					if (".".equals(String.valueOf(unitCode.charAt(i))) || "-".equals(String.valueOf(unitCode.charAt(i)))) {
						break;
					}
					preUnitCode += String.valueOf(unitCode.charAt(i));
				}
			}
			//pd.put("unitCode", pd.get("unitCode"));
			pd.put("preUnitCode", preUnitCode);
			pd.put("PROCESS_FILTER", true);
			List<PageData> squdList = (List<PageData>) dao.findForList("ProjectQualityMapper.getSelectQualityUnitDetailList", pd);

			String nodeid = pd.getString("nodeid");
			if(nodeid==null || "".equals(nodeid)){
				pd.put("nodeid", "0");
			}
			List<PageData>	qualityUnitDetailList =(List<PageData>) dao.findForList("ProjectQualityMapper.getProjectQualityDetailList", pd);
			int size = qualityUnitDetailList.size();
			boolean parentFlag = true;
			int parentBaseIdLength = 1000;
			String newid = sequence.nextId(); // 后缀 fghjkl_123456

			String[] fullNameText = new String[50];
			for (int i = 0; i < fullNameText.length; i++) {
				fullNameText[i] = "";
			}

			String maxFirstBaseId = (String) dao.findForObject("ProjectQualityMapper.getFirstMaxBaseId", num);
			if ("".equals(maxFirstBaseId) || maxFirstBaseId == null) {
				maxFirstBaseId = "00001";
			} else {
				int maxNum = Integer.valueOf(maxFirstBaseId) + 1;
				maxFirstBaseId = ("00000" + maxNum).substring(("00000" + maxNum).length()-5);
			}

			for (PageData pda : squdList) {
				if ("51".equals(pda.getString("UNIT_TYPE"))) {
					continue;
				}
				String UNIT_NAME = pda.getString("UNIT_NAME");
				String UNIT_CODE = pda.getString("UNIT_CODE");
				pda.put("MODEL_UNIT_NAME", UNIT_NAME);
				pda.put("MODEL_UNIT_CODE", UNIT_CODE);
				pda.remove("REMARK");
				pda.put("PROJECT_QUALITY_ID", pd.getString("PROJECT_QUALITY_ID"));
				pda.put("CREATEDATE", new Date());
				pda.put("CREATER", pd.getString("creater"));
				pda.put("MODIFYDATE", new Date());
				pda.put("MODIFIER", pd.getString("modifier"));
				pda.put("MODEL_QUALITY_UNIT_ID", pd.getString("qualityUnitId"));
				pda.put("PROJECT_BASE_NUM", pd.getString("PROJECT_BASE_NUM"));

				String id = pda.getString("ID");
				String pId = pda.getString("PARENT_UNIT_ID");
				String baseId = pda.getString("BASE_ID");

				int leave = baseId.split("-").length;
				id = id+"_"+newid;
				pId = pId+"_"+newid;
				pda.put("ID", id);
				if(baseId.length()<=parentBaseIdLength){
					if(parentFlag==true){
						parentBaseIdLength = baseId.length();
						pId = "0";
						parentFlag = false;
					}
					fullNameText[leave] = "["+UNIT_NAME+"]";
				} else {
					fullNameText[leave] = fullNameText[leave-1] + "-"+ "["+UNIT_NAME+"]";
				}
				pda.put("FULL_NAME", fullNameText[leave]);
				pda.put("PARENT_UNIT_ID", pId);

				pda.put("BASE_ID", maxFirstBaseId+"-"+baseId);
				pda.put("BASE_CODE", maxFirstBaseId+baseId);
				pda.put("SORT", ++size);
				dao.save("ProjectQualityMapper.addProjectQualityDetail", pda);
			}
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return num;
	}

	/**
	 * 删除 从表数据 --真删除
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public int deleteProjectQualityDetail(PageData pd) throws GenException{
		int num = 0;
		try {
			num = (Integer)dao.delete("ProjectQualityMapper.deleteProjectQualityDetail", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return num;
	}

	/**
	 * 保存从表
	 * @param pd
	 * @throws GenException
	 */
	public int updateDetailData(PageData pd) throws GenException {
		int num = 0;
		try {
			num += (Integer) dao.save("ProjectQualityMapper.saveDetailData", pd);

			PageData pdT = new PageData();
			pdT.put("TECHNICIAN", pd.get("TECHNICIAN"));
			pdT.put("PARENT_UNIT_ID", pd.get("ID"));
			num += (Integer) dao.update("ProjectQualityMapper.updateProjectQualityDetailTechnician", pdT);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return num;
	}

	public int saveDetailData(PageData pd) throws GenException {
		int num = 0;
		try {
			num += (Integer) dao.update("ProjectQualityMapper.addProjectQualityDetail", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return num;
	}

	/**
	 * 同步工序 PROJECT_QUALITY_ID
	 * @param pd
	 * @throws GenException
	 */
	public int synchronousProcess(PageData pd, String userId) throws GenException {

		try {
			if (pd == null || pd.get("PROJECT_QUALITY_ID") == null || "".equals(String.valueOf(pd.get("PROJECT_QUALITY_ID")))) {
				return 0;
			}
			PageData pdq = new PageData();
			pd.put("ID", pd.get("PROJECT_QUALITY_ID"));
			PageData projectQualityInfo = this.getProjectQualityById(pd);
			if (projectQualityInfo == null) {
				return 0;
			}

			List<PageData> hasNotProcessList = (List<PageData>) dao.findForList("ProjectQualityMapper.getProjectQualityDetailHasNotProcess", pd);
			if (hasNotProcessList == null || hasNotProcessList.size() == 0) {
				return 0;
			}
			int rows = 0;
			String UNIT_TYPE = "51"; // 工序
			String baseStr = "0000000000"; // 层级基础字符串， 截取 长度 5
			// 主表ID

			String MODEL_QUALITY_UNIT_ID = String.valueOf(pd.get("MODEL_QUALITY_UNIT_ID"));

			String PROJECT_BASE_NUM = String.valueOf(pd.get("PROJECT_BASE_NUM"));
			// 遍历没有下级的，非工序类型数据
			for (int i = 0; i < hasNotProcessList.size(); i++) {
				PageData detailPD = hasNotProcessList.get(i);

				// 根据 modelUnitCode 查下级的工序
				PageData pdp = new PageData();
				pdp.put("PROJECT_TYPE", String.valueOf(projectQualityInfo.get("PROJECT_TYPE")));
				pdp.put("UNIT_CODE", String.valueOf(detailPD.get("MODEL_UNIT_CODE")));
				List<PageData> childProcessList = (List<PageData>) dao.findForList("ProjectQualityMapper.getProcessListByUnitCode", pdp);
				if (childProcessList == null || childProcessList.size() == 0) {
					continue;
				}
				// 工序上级数据
				String id = String.valueOf(detailPD.get("ID"));
				String fullName = String.valueOf(detailPD.get("FULL_NAME"));
				String projectBaseNum = String.valueOf(detailPD.get("PROJECT_BASE_NUM"));
				String modelQualityUnitId = String.valueOf(detailPD.get("MODEL_QUALITY_UNIT_ID"));
				String projectQualityId = String.valueOf(detailPD.get("PROJECT_QUALITY_ID"));
				String baseCode = String.valueOf(detailPD.get("BASE_CODE"));
				String baseId = String.valueOf(detailPD.get("BASE_ID"));

				String baseUnitCode = String.valueOf(detailPD.get("MODEL_UNIT_CODE"));
				// 遍历工序
				for (int j = 0; j < childProcessList.size(); j++) {
					PageData childProcessPD = childProcessList.get(j);
					PageData childDetail = new PageData();

					childDetail.put("UNIT_NAME", childProcessPD.get("UNIT_NAME"));
					childDetail.put("UNIT_FILE", childProcessPD.get("UNIT_FILE"));
					childDetail.put("MODEL_UNIT_NAME", childProcessPD.get("UNIT_NAME"));
					childDetail.put("FULL_NAME", fullName + "-["+ childProcessPD.get("UNIT_NAME") +"]");
					childDetail.put("MODEL_UNIT_CODE", baseUnitCode+"-"+("00000"+(j+1)).substring(("00000"+(j+1)).length()-2));

					childDetail.put("TECHNICIAN", detailPD.get("TECHNICIAN"));
					childDetail.put("REMARK", null);
					childDetail.put("CREATEDATE", new Date());
					childDetail.put("CREATER", userId);
					childDetail.put("MODIFYDATE", new Date());
					childDetail.put("MODIFIER", userId);
					childDetail.put("FLAG", "1");

					childDetail.put("ID", UuidUtil.get36UUID());
					childDetail.put("PARENT_UNIT_ID", id);
					childDetail.put("PROJECT_QUALITY_ID", projectQualityId);
					childDetail.put("MODEL_QUALITY_UNIT_ID", modelQualityUnitId);
					childDetail.put("UNIT_TYPE", UNIT_TYPE);
					childDetail.put("PROJECT_BASE_NUM", projectBaseNum);

					childDetail.put("BASE_ID", baseId + "-" + (baseStr + (j+1)).substring((baseStr + (j+1)).length()-5));
					childDetail.put("BASE_CODE", baseCode+ "-" + (baseStr + (j+1)).substring((baseStr + (j+1)).length()-5));
					rows += (int) dao.save("addProjectQualityDetail", childDetail);
				}
			}
			return rows;
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}

	}

	/**
	 * 导入 数据
	 * @param pdsList
	 * @return
	 * @throws GenException
	 */
	public int importDetailData(List<PageData> pdsList) throws GenException {
		int num = 0;
		try {
			String maxFirstBaseId = (String) dao.findForObject("ProjectQualityMapper.getFirstMaxBaseId", num);
			if ("".equals(maxFirstBaseId) || maxFirstBaseId == null) {
				maxFirstBaseId = "00001";
			} else {
				int maxNum = Integer.valueOf(maxFirstBaseId) + 1;
				maxFirstBaseId = ("00000" + maxNum).substring(("00000" + maxNum).length()-5);
			}
			for (PageData pd : pdsList) {

				pd.put("BASE_ID", maxFirstBaseId + "-" + pd.getString("BASE_ID"));
				pd.put("BASE_CODE", maxFirstBaseId + pd.getString("BASE_CODE"));
				dao.save("ProjectQualityMapper.addProjectQualityDetail", pd);
			}
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return num;
	}

}
