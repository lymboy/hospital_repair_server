package com.ruoyi.hospital.service.app.api;

import com.ruoyi.hospital.dao.DaoSupport;
import com.ruoyi.hospital.entity.Page;
import com.ruoyi.hospital.framework.exception.GenException;
import com.ruoyi.hospital.framework.log.Log;
import com.ruoyi.hospital.util.Const;
import com.ruoyi.hospital.util.FileUtils;
import com.ruoyi.hospital.util.PageData;
import com.ruoyi.hospital.util.UuidUtil;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>  </p>
 *
 * @author Young
 * @since 2018-04-15
 */
@Service("apiQualityInspectService")
public class ApiQualityInspectService {
    @Resource(name = "daoSupport")
    private DaoSupport dao;


    /**
     * 统计不同审批状态个数
     * @param pd
     * @return
     */
    public List<PageData> getQualityInspectDataCount(PageData pd) throws GenException {
        try {
            return (List<PageData>) dao.findForList("ApiQualityInspectMapper.getQualityInspectDataCount", pd);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     * 工序质量验收 附件 列表
     * @param pd
     * @return
     */
    public List<PageData> getQualityInspectFileList(PageData pd) throws GenException {
        try {
            return (List<PageData>) dao.findForList("ApiQualityInspectMapper.getQualityInspectFileList", pd);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     * 工序质量验收 附件 列表 BY ID
     * @param ID
     * @return
     */
    public PageData getQualityInspectFileById(String ID) throws GenException {
        try {
            return (PageData) dao.findForObject("ApiQualityInspectMapper.getQualityInspectFileById", ID);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     * 工序质量验收 删除
     * @param pd
     * @param userId
     * @return
     */
    public int deleteQualityInspect(PageData pd, String userId) throws GenException {
        try {
            if (pd.get("ID") == null || "".equals(pd.get("ID"))) {
                return 0;
            }
            return  (int) dao.update("ApiQualityInspectMapper.deleteQualityInspect", pd);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     * 工序质量验收 附件上传
     * @param pd
     * @param multipartFileList
     * @param userId
     * @return
     */
    public List<PageData> insertQualityInspectFile(PageData pd, List<MultipartFile> multipartFileList, String userId, String employeeId) throws GenException {
        try {
            List<PageData> filePdInfoList = new ArrayList<>();
            if (multipartFileList == null || multipartFileList.size() == 0) {
                return filePdInfoList;
            }
            for (int i = 0; i < multipartFileList.size(); i++) {
                String ID = UuidUtil.get32UUID();
                // 文件上传
                MultipartFile multipartFile = multipartFileList.get(i);
                String midPath = "/uploadFiles/document";
                File folder = new File(Const.FILE_UPLOAD_ROOT_URL + midPath + File.separator);
                String fileName = multipartFile.getOriginalFilename();
                String fileSize = String.valueOf(multipartFile.getSize());
                String filePath = folder + File.separator + ID + fileName.substring(fileName.lastIndexOf("."));
                FileUtils.uploadMultipartFile(filePath, multipartFile);
                // 保存记录
                PageData pdFile = new PageData();
                pdFile.putAll(pd);
                pdFile.put("ID", ID);
                pdFile.put("EDITOR", employeeId);
                pdFile.put("UPLOAD_DATE", new Date());
                pdFile.put("CREATEDATE", new Date());
                pdFile.put("CREATER", userId);
                pdFile.put("FILE_NAME", multipartFile.getOriginalFilename());
                pdFile.put("FILE_SIZE", String.valueOf(multipartFile.getSize()));
                dao.save("ApiQualityInspectMapper.insertQualityInspectFile", pdFile);
                pdFile.put("FILE_URL", "/uploadFiles/document/"+ ID + fileName.substring(fileName.lastIndexOf(".")));
                pdFile.put("FILE_URL_big", "/uploadFiles/document/" + "/thumb_800_600" + "/"+ ID+fileName.substring(fileName.lastIndexOf(".")));
                pdFile.put("FILE_URL_small", "/uploadFiles/document/" + "/thumb_80_60" + "/"+ ID+fileName.substring(fileName.lastIndexOf(".")));
                filePdInfoList.add(pdFile);
            }
            return filePdInfoList;
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     * 工序质量验收 附件 编辑结论
     * @param pd
     * @param userId
     * @return
     */
    public int updateQualityInspectFile(PageData pd, String userId) throws GenException {
        try {
            pd.put("", userId);
            pd.put("", new Date());
            return (Integer) dao.save("ApiQualityInspectMapper.updateQualityInspectFile", pd);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     * 工序质量验收 附件 删除
     * @param pd
     * @param userId
     * @return
     */
    public int deleteQualityInspectFile(PageData pd, String userId) throws GenException {
        try {
            pd.put("", userId);
            pd.put("", new Date());
            return (Integer) dao.save("ApiQualityInspectMapper.deleteQualityInspectFile", pd);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     * 获取项目质量单元划分数据
     * @param pd
     * @return
     */
    public List<PageData> getProjectQualityDetailTreeDataList(PageData pd) throws GenException {
        try {
            return (List<PageData>) dao.findForList("ApiQualityInspectMapper.getProjectQualityDetailTreeDataList", pd);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

}
