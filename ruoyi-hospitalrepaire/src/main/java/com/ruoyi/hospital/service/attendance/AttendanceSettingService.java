package com.ruoyi.hospital.service.attendance;

import com.ruoyi.hospital.dao.DaoSupport;
import com.ruoyi.hospital.entity.Page;
import com.ruoyi.hospital.framework.exception.GenException;
import com.ruoyi.hospital.util.PageData;
import com.ruoyi.hospital.util.UuidUtil;
import com.ruoyi.hospital.util.sequence;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * <p> 考勤设置 </p>
 *
 * @author Young
 * @since 2018/4/2
 */
@Service
public class AttendanceSettingService {

    @Autowired
    private DaoSupport dao;

    /**
     * 分页 列表
     * @param page
     * @return
     * @throws GenException
     */
    public List<PageData> getAttendanceSettingListPage(Page page) throws GenException {
        try {
            return (List<PageData>) dao.findForList("AttendanceSettingMapper.getAttendanceSettinglistPage", page);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     * 获取明细 BY ID
     * @param ID
     * @return
     * @throws GenException
     */
    public PageData getAttendanceSettingInfoById(String ID) throws GenException {
        try {
            return (PageData) dao.findForObject("AttendanceSettingMapper.getAttendanceSettingInfoById", ID);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     * 新增或编辑
     * @param pd
     * @return
     */
    public int insertOrUpdate(PageData pd) throws GenException {
        try {
            String ID = String.valueOf(pd.get("ID"));
            if (ID == null || "".equals(ID)) {
                return -1;
            }
            if (getAttendanceSettingInfoById(ID) == null) {
                // 新增
                return (int) dao.update("AttendanceSettingMapper.insertAttendanceSetting", pd);
            }
            // 编辑
            return (int) dao.update("AttendanceSettingMapper.updateAttendanceSetting", pd);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     * 批量 删除
     * @param ids
     * @param userId
     * @return
     */
    public int batchDeleteAttendanceSetting(List<String> ids, String userId) throws GenException {
        try {
            if (ids == null || ids.size() == 0) {
                return 0;
            }
            int rows = 0;
            for (int i = 0; i < ids.size(); i++) {
                String ID = ids.get(i);
                PageData pd = new PageData();
                pd.put("ID", ID);
                pd.put("MODIFIER", userId);
                pd.put("MODIFYDATE", new Date());
                rows += (int) dao.update("AttendanceSettingMapper.deleteAttendanceSetting", pd);
            }
            return rows;
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     * 从表 分页 列表
     * @param page
     * @return
     */
    public List<PageData> getAttendanceSettingDetailListPage(Page page) throws GenException {
        try {
            return (List<PageData>) dao.findForList("AttendanceSettingMapper.getAttendanceSettingDetaillistPage", page);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     * 从表 更新
     * @param pd
     * @return
     * @throws GenException
     */
    public int updateAttendanceSettingDetail(PageData pd) throws GenException {
        try {
            return (int) dao.update("AttendanceSettingMapper.updateAttendanceSettingDetail", pd);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     * 从表 批量 删除
     * @param ids
     * @param userId
     * @return
     */
    public int batchDeleteAttendanceSettingDetail(List<String> ids, String userId) throws GenException {
        try {
            if (ids == null || ids.size() == 0) {
                return 0;
            }
            int rows = 0;
            for (int i = 0; i < ids.size(); i++) {
                String ID = ids.get(i);
                PageData pd = new PageData();
                pd.put("ID", ID);
                pd.put("MODIFIER", userId);
                pd.put("MODIFYDATE", new Date());
                rows += (int) dao.update("AttendanceSettingMapper.deleteAttendanceSettingDetail", pd);
            }
            return rows;
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     * 选择考勤人员 数据
     * @param page
     * @return
     */
    public List<PageData> getSelectEmpListPage(Page page) throws GenException {
        try {
            return (List<PageData>) dao.findForList("AttendanceSettingMapper.getSelectEmplistPage", page);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     * 批量保存 从表
     * @param empIds
     * @param attendanceSettingId
     * @param userId
     * @return
     */
    public int batchInsertAttendanceSettingDetail(List<String> empIds, String attendanceSettingId, String userId) throws GenException {
        try {
            if (empIds == null || empIds.size() == 0) {
                return 0;
            }
            int rows = 0;
            for (int i = 0; i < empIds.size(); i++) {
                String EMPLOYEE_ID = empIds.get(i);
                PageData pd = new PageData();
                pd.put("ID", UuidUtil.get32UUID());
                pd.put("ATTENDANCE_SETTING_ID", attendanceSettingId);
                pd.put("EMPLOYEE_ID", EMPLOYEE_ID);
                pd.put("CREATER", userId);
                pd.put("CREATEDATE", new Date());
                rows += (int) dao.update("AttendanceSettingMapper.insertAttendanceSettingDetail", pd);
            }
            return rows;
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }
}
