package com.ruoyi.hospital.service.documentcatalog;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.ruoyi.hospital.dao.DaoSupport;
import com.ruoyi.hospital.entity.Page;
import com.ruoyi.hospital.framework.exception.GenException;
import com.ruoyi.hospital.util.PageData;

@Service(value="resourcesService")
public class ResourcesService {

	@Resource(name = "daoSupport")
	private DaoSupport dao;

	//得到所有的下級catalog
		public List<PageData> getLowCatalog(PageData pd) throws GenException {
			List<PageData> lowCatalog=null;
			 	try {
					lowCatalog=(List<PageData>) dao.findForList("ResourcesMapper.getCatalogLowList", pd);
				} catch (Exception e) {
					throw new GenException(e.getCause());
				}
			return lowCatalog;
		}


		//得到所有的low文件
		public List<PageData> getAllLowDocument(PageData pd) throws GenException {
			List<PageData> lowDpocumentList=null;
			try {
				lowDpocumentList=(List<PageData>) dao.findForList("ResourcesMapper.lowDpocumentList", pd);
			} catch (Exception e) {
				throw new GenException(e.getCause());
			}
			return lowDpocumentList;
		}

		//得到当前登陆者所有的下级
		public List<PageData> getAllLowUser(Page page) throws GenException {
			List<PageData> lowUser=null;
			try {
				lowUser=(List<PageData>) dao.findForList("ResourcesMapper.getAllLowUserlistPage", page);
			} catch (Exception e) {
				throw new GenException(e.getCause());
			}
			return lowUser;
		}

		//通过身份权限表查看资源权限表
		public List<PageData> getAllRessourcesByIdentity(PageData pd) throws GenException {
			List<PageData> resourcesList=null;
			try {
				resourcesList=(List<PageData>) dao.findForList("ResourcesMapper.getAllRessourcesByIdentity", pd);
			} catch (Exception e) {
				throw new GenException(e.getCause());
			}
			return resourcesList;
		}

		//得到所有的文件资源
		public List<PageData> getAllDocumentByIdentity(PageData pd) throws GenException {
			List<PageData> resourcesList=null;
			try {
				resourcesList=(List<PageData>) dao.findForList("ResourcesMapper.getAllDocumentByIdentity", pd);
			} catch (Exception e) {
				throw new GenException(e.getCause());
			}
			return resourcesList;
		}

}
