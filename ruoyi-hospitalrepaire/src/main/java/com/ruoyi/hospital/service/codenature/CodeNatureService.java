package com.ruoyi.hospital.service.codenature;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.ruoyi.hospital.dao.DaoSupport;
import com.ruoyi.hospital.entity.Page;
import com.ruoyi.hospital.framework.exception.GenException;
import com.ruoyi.hospital.util.PageData;

/**
 * <p>Title: CodeNatureService</p>
 * @Desc: 常量 管理
 * @author: 杨建军
 *	@Date: 2017年4月16日
 * @Time: 下午12:18:08
 * @version:
 */
@Service(value="codeNatureService")
public class CodeNatureService {

	@Resource(name = "daoSupport")
	private DaoSupport dao;

	/**
	 * 获取常量根节点
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public List<PageData> getAllRootCodeNaturesList(PageData pd) throws GenException{
		List<PageData> pds = null;
		try {
			pds = (List<PageData>) dao.findForList("CodeNatureMapper.getAllRootCodeNaturesList", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pds;
	}

	/**
	 * 获取根节点的下一级节点	kindId
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public List<PageData> getSubRootCodeNatures(PageData pd) throws GenException{
		List<PageData> pds = null;
		try {
			pds = (List<PageData>) dao.findForList("CodeNatureMapper.getSubRootCodeNatures", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pds;
	}

	/**
	 *  获取子节点	typeId
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public List<PageData> getSubCodeNatures(PageData pd) throws GenException{
		List<PageData> pds = null;
		try {
			pds = (List<PageData>) dao.findForList("CodeNatureMapper.getSubCodeNatures", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pds;
	}













	/**
	 * 获取根节点详细	 kindId
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public PageData getRootCodeNatureInfo(PageData pd) throws GenException{
		PageData pdInfo = null;
		try {
			pdInfo = (PageData)dao.findForObject("CodeNatureMapper.getRootCodeNatureInfo", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pdInfo;
	}

	/**
	 * 获取子节点明细	 typeId
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public PageData getSubCodeNatureInfo(PageData pd) throws GenException{
		PageData pdInfo = null;
		try {
			pdInfo = (PageData)dao.findForObject("CodeNatureMapper.getSubCodeNatureInfo", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pdInfo;
	}

	/**
	 * 获取根节点个数
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public int getRootCodeNatureNum(PageData pd) throws GenException{
		int num=0;
		try {
			num = (Integer)dao.findForObject("CodeNatureMapper.getRootCodeNatureNum", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return num;
	}

	/**
	 *  获取根节点的下一级个数		kindId
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public int getSubRootCodeNatureNum(PageData pd) throws GenException{
		int num=0;
		try {
			num = (Integer)dao.findForObject("CodeNatureMapper.getSubRootCodeNatureNum", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return num;
	}

	/**
	 *   获取子节点个数 		typeId
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public int getSubCodeNatureNum(PageData pd) throws GenException{
		int num=0;
		try {
			num = (Integer)dao.findForObject("CodeNatureMapper.getSubCodeNatureNum", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return num;
	}

	/**
	 *   取消默认选中		kindId
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public int cancelDefaultChecked(PageData pd) throws GenException{
		int num=0;
		try {
			num = (Integer)dao.update("CodeNatureMapper.cancelDefaultChecked", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return num;
	}

	/**
	*  设置默认选中		typeId
	* @param pd
	* @return
	* @throws GenException
	*/
	public int setDefaultChecked(PageData pd) throws GenException{
		int num=0;
		try {
			num = (Integer)dao.update("CodeNatureMapper.cancelDefaultChecked", pd);
			num = (Integer)dao.update("CodeNatureMapper.setDefaultChecked", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return num;
	}

	/**
	 *  更新根节点	kindId pd
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public int updateRootCodeNature(PageData pd) throws GenException{
		int num=0;
		try {
			num = (Integer)dao.update("CodeNatureMapper.updateRootCodeNature", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return num;
	}

	/**
	 *  更新子节点	typeId pd
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public int updateSubCodeNature(PageData pd) throws GenException{
		int num=0;
		try {
			num = (Integer)dao.update("CodeNatureMapper.updateSubCodeNature", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return num;
	}

	/**
	 *  插入根节点	kindId pd
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public int addRootCodeNature(PageData pd) throws GenException{
		int num=0;
		try {
			num = (Integer)dao.save("CodeNatureMapper.addRootCodeNature", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return num;
	}

	/**
	 *  插入子节点	typeId pd
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public int addSubCodeNature(PageData pd) throws GenException{
		int num=0;
		try {
			num = (Integer)dao.save("CodeNatureMapper.addSubCodeNature", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return num;
	}

	/**
	 *  删除根节点 	kindId
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public int deleteRootCodeNature(PageData pd) throws GenException{
		int num=0;
		try {
			num = (Integer)dao.delete("CodeNatureMapper.deleteRootCodeNature", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return num;
	}

	/**
	 * 批量 删除子节点 	typeId
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public int deleteSubCodeNature(PageData pd) throws GenException{
		int num=0;
		try {
			String ids = pd.getString("typeIds");
			if(ids!=null && !"".equals(ids)){
				String[] idsArray = ids.split(",");
				for (String typeId : idsArray) {
					pd.put("typeId", typeId);
					num = (Integer)dao.delete("CodeNatureMapper.deleteSubCodeNature", pd);
				}
			}
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return num;
	}

}
