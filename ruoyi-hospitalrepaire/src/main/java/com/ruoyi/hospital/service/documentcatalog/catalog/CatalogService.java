package com.ruoyi.hospital.service.documentcatalog.catalog;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.ruoyi.hospital.dao.DaoSupport;
import com.ruoyi.hospital.entity.Page;
import com.ruoyi.hospital.framework.exception.GenException;
import com.ruoyi.hospital.framework.log.Log;
import com.ruoyi.hospital.util.PageData;

/**
 * <p>Title: CatalogService</p>
 * @Desc: 资料目录管理
 * @author: 杨建军
 *	@Date: 2017年4月21日
 * @Time: 上午10:20:34
 * @version:
 */
@Service(value="catalogService")
public class CatalogService {

	@Resource(name = "daoSupport")
	private DaoSupport dao;

	// 获取质量资料目录表信息
	public List<PageData> getCatalogList(PageData pageData) throws Exception {
		List<PageData> catalogList=null;
		try {
			catalogList=(List<PageData>) dao.findForList("CatalogMapper.getCatalogList", pageData);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	return catalogList;
	}

	// 新增质量资料目录
	@Log(name="新增质量资料目录-addCatalog")
	public int addCatalog(PageData pd) throws GenException {
		int num = 0;
		try {
			num = (Integer) dao.save("CatalogMapper.addCatalog", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return num;
	}

	// 获取质量资料目录的下级个数 BY PARENT_CATALOG_ID
	public int getCountByParentId(String parentId) throws GenException {
		 int num=0;
		try {
			num = (Integer)dao.findForObject("CatalogMapper.getCountByParentId", parentId);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return num;
	}

	/**
	* @Description:  删除质量资料目录
	 */
	@Log(name="删除质量资料目录-deleteCatalog")
	public int deleteCatalog(PageData pd) throws GenException {
		int num = 0;
		try {
			num = (Integer) dao.delete("CatalogMapper.deleteCatalog", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return num;
	}

	//根据user得到所有的身份
	@SuppressWarnings("unchecked")
	public List<PageData> findAllLowIdentity(PageData pd) throws GenException {
		List<PageData> lowIdentity=null;
			try {
				lowIdentity=(List<PageData>) dao.findForList("CatalogMapper.findAllLowIdentity", pd);
			} catch (Exception e) {
				throw new GenException(e.getCause());
			}
		return lowIdentity;
	}

	/**
	 *
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	@Log(name="更新修改的资料目录信息-updateCatalogByCatalogId")
	public int updateCatalogByCatalogId(PageData pd) throws GenException {
		int num=0;
		try {
			num = (Integer) dao.update("CatalogMapper.catalogEdit", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return num;
	}

	//添加目录资源
	public void addCatalogPrivilege(PageData pd) throws GenException {
		try {
			dao.save("CatalogMapper.addCatalogPrivilege", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}

	}
}








