package com.ruoyi.hospital.service.checkm;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.ruoyi.hospital.dao.DaoSupport;
import com.ruoyi.hospital.framework.exception.GenException;
import com.ruoyi.hospital.framework.log.Log;
import com.ruoyi.hospital.util.PageData;

@Service("organizationService")
public class OrganizationService {

	@Resource(name = "daoSupport")
	private DaoSupport dao;

	/**
	 * 通过部门id得到部门的基本信息
	 * @throws GenException
	 */
	public PageData getDepartmentInformation(PageData pd) throws GenException {
		PageData departmentInformation=null;
		try {
			departmentInformation=(PageData) dao.findForObject("OrganizationMapper.getDepartmentInformation", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return departmentInformation;
	}

	/**
	 * 添加部门
	 * @throws GenException
	 */
	public void addLowerLevelDpartment(PageData pd) throws GenException {
		int num=0;
		PageData parentData=null;
		int baseNum=0;
		try {
			num=(int) dao.findForObject("OrganizationMapper.selectDepartment", null);
			pd.put("DEPARTMENT_ORDER", String.valueOf(num+1));
			parentData=selectBaseIdByParentId(pd);
			baseNum=selectBaseNum(pd);
			String bsId = parentData.getString("BASE_ID");
			String baseNumStr = "0000"+String.valueOf(baseNum+1);
			String baseId = bsId+"-"+baseNumStr.substring(baseNumStr.length()-4);
			pd.put("BASE_ID", baseId);
			dao.save("OrganizationMapper.addLowerLevelDpartment", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}

	//查找父类id为partntId的个数
	private int selectBaseNum(PageData pd) throws GenException {
		int baseNum=0;
		try {
			baseNum=(int) dao.findForObject("OrganizationMapper.selectBaseNum", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return baseNum;
	}

	//通过父类id查找父类id
	private PageData selectBaseIdByParentId(PageData pd) throws GenException {
		PageData parentData=null;
		try {
			parentData=(PageData) dao.findForObject("OrganizationMapper.selectBaseIdByParentId", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return  parentData;
	}

	/**
	 * 删除本级
	 * @throws GenException
	 */
	public void deleteDepartment(PageData pd) throws GenException {
	try {
			pd.put("departmentId", pd.getString("departmentId"));
			dao.update("OrganizationMapper.updateDepartment", pd);//删除点击树
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}

	//递归删除下级部门
	public void deleteChildDepartment(PageData pd) throws GenException{
		List<PageData> childDepartmentList=null;
		try {
			childDepartmentList=findAllDepartmentById(pd);//得到所有的父类id是页面id
			if( childDepartmentList!=null  && childDepartmentList.size()!=0 ){
			for(PageData list : childDepartmentList){
				pd.put("departmentId",list.getString("ID"));//删除本级的下级部门
				deleteChildDepartment(pd);
				pd.put("departmentId",list.getString("ID"));
				//删除点击树的下级部门
				dao.update("OrganizationMapper.updateDepartment",pd);
				}
			}else{
				return;
			}
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}

	//根据父类id找到部门
	@SuppressWarnings("unchecked")
	private List<PageData> findAllDepartmentById(PageData pd) throws GenException {
		List<PageData> childDepartmentList=null;
		try {
			childDepartmentList=(List<PageData>) dao.findForList("OrganizationMapper.childDepartmentList", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return childDepartmentList;
	}

	//根据父类name找到部门
	public List<PageData> getDepartmentInformationByName(PageData pd) throws GenException {
		List<PageData> departmentInformation=null;
		try {
			departmentInformation= (List<PageData>) dao.findForList("OrganizationMapper.getDepartmentInformationByName", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return departmentInformation;
	}

	//机构调整
	public void departmentAdjust(PageData pd) throws GenException {
		PageData departmentInformation=null;
		PageData departmentTarge=null;
		try {
			departmentInformation=(PageData) dao.findForObject("OrganizationMapper.getDepartmentInformation", pd);
			if(departmentInformation.getString("DEPARTMENT_PARENT_ID").equals(pd.getString("parent_Id"))){
				System.out.println(departmentInformation.getString("DEPARTMENT_PARENT_ID"));
				//改变顺序
				departmentTarge=(PageData) dao.findForObject("OrganizationMapper.getTagerBaseId", pd);//得到目标id的baseId
				pd.put("baseId", departmentTarge.getString("BASE_ID"));
				dao.update("OrganizationMapper.changeId", pd); //改变调整id的baseId
				pd.put("baseId", departmentInformation.getString("BASE_ID"));
				dao.update("OrganizationMapper.changeTargeId", pd);//改变目标的baseId
			}else{
				pd.put("parent_Id", pd.getString("targetId"));
				dao.update("OrganizationMapper.updatedepartmentTager", pd);
			}
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}

	}

	//编辑保存部门信息
	public void saveDepartmentInformation(PageData pd) throws GenException {
		try {
			dao.update("OrganizationMapper.saveDepartmentInformation", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}

	/**
	 * 判断机构下是否有人，身份
	 * @param departmentId
	 * @return
	 */
    public PageData getDeptAndSubDeptEmpAndIdendityNum(String departmentId) throws GenException {
		try {
			return (PageData) dao.findForObject("OrganizationMapper.getDeptAndSubDeptEmpAndIdendityNum", departmentId);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
    }
}
