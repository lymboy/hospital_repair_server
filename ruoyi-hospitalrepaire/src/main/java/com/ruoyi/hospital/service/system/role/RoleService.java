package com.ruoyi.hospital.service.system.role;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.ruoyi.hospital.dao.DaoSupport;
import com.ruoyi.hospital.entity.Page;
import com.ruoyi.hospital.entity.system.Role;
import com.ruoyi.hospital.framework.exception.GenException;
import com.ruoyi.hospital.framework.log.Log;
import com.ruoyi.hospital.util.PageData;
import com.ruoyi.hospital.util.sequence;

/**
 * <p>Title: RoleService</p>
 * @Desc:
 * @author: 杨建军
 *	@Date: 2017年3月8日
 * @Time: 上午11:49:17
 * @version:
 */
@Service("roleService")
public class RoleService{

	@Resource(name = "daoSupport")
	private DaoSupport dao;

	/**
	 * 获取当前用户所有的角色
	 */
	public List<Long> listRoleIdsByPId(PageData pd) throws GenException {
		List<Long> longs = null;
		try {
			longs = (List<Long>)dao.findForList("RoleMapper.listRoleIdsByPId", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return longs;
	}

	/**
	 * 获取所有角色
	 */
	public List<PageData> listAllRoles(PageData pd) throws GenException {
		List<PageData> pds = null;
		try {
			pds = (List<PageData>)dao.findForList("RoleMapper.listAllRoles", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pds;
	}

	/**
	 * 分页获取所有角色
	 */
	public List<PageData> RolelistPage(Page page) throws GenException {
		List<PageData> pds = null;
		try {
			pds = (List<PageData>)dao.findForList("RoleMapper.RolelistPage", page);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pds;
	}

	/**
	 * 为用户添加新的角色
	 * @param pd
	 * @throws Exception
	 */
	@Log(name="为用户添加新的角色")
	public void insertUserRole(PageData pd) throws GenException {
		try {
			dao.save("RoleMapper.insertUserRole", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}
	/**
	 * 根据用户id和角色id删除角色用户表中的记录
	 * @param pd
	 * @throws Exception
	 */
	@Log(name="根据用户id和角色id删除角色用户表中的记录")
	public void deleteRoleByUserId(PageData pd) throws GenException {
		try {
			dao.delete("RoleMapper.deleteRoleByUserId", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}
	/**
	 * 获取用户对用的所有角色id
	 */
	public List<PageData> listAllUserRoles(PageData pd) throws GenException {
		List<PageData> pds = null;
		try {
			pds = (List<PageData>)dao.findForList("RoleMapper.listAllUserRoles", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pds;
	}
	/**
	 * 删除用户对应的用户角色表的信息
	 * @param userID
	 * @throws Exception
	 */
	@Log(name="删除用户对应的用户角色表的信息")
	public void deleteUserRoleByUserId(String[] userID) throws GenException {
		try {
			dao.delete("RoleMapper.deleteUserRoleByUserId", userID);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}
	/**
	 *删除用户所有角色对应信息
	 * @param pd
	 * @throws Exception
	 */
	@Log(name="删除用户所有角色对应信息")
	public void deleteURByUser(PageData pd) throws GenException {
		try {
			dao.delete("RoleMapper.deleteURByUser", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}
	/**
	 * 获取当前用户所有的角色
	 * @throws Exception
	 */
	public List<Role> listAllRolesByUserId(PageData pd) throws GenException {
		List<Role> roles = null;
		try {
			roles = (List<Role>) dao.findForList("RoleMapper.listAllRolesByUserId", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return roles;
	}

	public Integer getAllCount() throws GenException{
		Integer num = 0;
		try {
			num = (Integer)dao.findForObject("RoleMapper.getAllCount", "");
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return num;
	}

	/**
	 * 添加新角色
	 * @param pd
	 */
	public void addNewRole(PageData pd) throws GenException{
		try {
			dao.save("RoleMapper.addNewRole", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}


	/*
	 * 通过角色id获取角色信息
	 */
	public PageData findRoleByRoleId(PageData pd) throws GenException {
		PageData pda = new PageData();
		try {
			pda = (PageData)dao.findForObject("RoleMapper.findRoleByRoleId", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pda;
	}

	/*
	 * 修改角色，其实就是改个名
	 */
	public void editRole(PageData pd) throws GenException {
		try {
			dao.update("RoleMapper.editRole", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}

	/*
	 * 删除角色
	 */
	@Log(name="删除角色")
	public void deleteR(PageData pd) throws GenException{
		try {
			dao.delete("RoleMapper.deleteR", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}

	/*
	 * 删除拥有该角色的用户角色表
	 */
	@Log(name="删除拥有该角色的用户角色表")
	public void deleteURByRole(PageData pd) throws GenException{
		try {
			dao.delete("RoleMapper.deleteURByRole", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}

	/*
	 * 批量删除角色
	 */
	public void deleteAllR(Map<String, Object> map) throws GenException{
		try {
			dao.delete("RoleMapper.deleteAllR", map);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}

	/**
	 * 获取所有拥有该权限的角色
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public List<PageData> getRoleListByPrivilegeId(PageData pd) throws GenException {
		List<PageData> pds = null;
		try {
			pds = (List<PageData>) dao.findForList("RoleMapper.getRoleListByPrivilegeId", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pds;
	}

	/**
	 * 修改角色
	 * @param pd
	 * @throws GenException
	 */
	@Log(name = "修改角色")
	public void updateRole(PageData pd) throws GenException {
		try {
			dao.update("RoleMapper.editRole", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}

	/**
	 * 为角色分配用户身份
	 * @param pd
	 * @throws Exception
	 */
	@Log(name="为角色分配用户身份-saveChoseUserIdentity")
	public void saveChoseUserIdentity(PageData pd) throws GenException {
		try {
			String identityIdsStr = pd.getString("identityIds");
			if(identityIdsStr!=null && !"".equals(identityIdsStr)){
				String roleIdsStr = pd.getString("roleIds");
				if(roleIdsStr!=null && !"".equals(roleIdsStr)){
					String[] identityIdsArray = identityIdsStr.split(",");
					String[] roleIdsArray = roleIdsStr.split(",");
					for (String identityId : identityIdsArray) {
						pd.put("identityId", identityId);
						//dao.delete("RoleMapper.deleteIdentityRole", pd);// 先删， 后增
						for (String roleId : roleIdsArray) {
							pd.put("roleId", roleId);
							pd.put("identityRoleId", sequence.nextId());
							dao.save("RoleMapper.addIdentityRole", pd);
						}
					}
				}
			}
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}

}
