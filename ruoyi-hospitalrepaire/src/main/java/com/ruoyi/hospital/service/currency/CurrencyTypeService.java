package com.ruoyi.hospital.service.currency;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.ruoyi.hospital.dao.DaoSupport;
import com.ruoyi.hospital.framework.exception.GenException;
import com.ruoyi.hospital.util.PageData;

@Service("currencyTypeService")
public class CurrencyTypeService {

	@Resource(name = "daoSupport")
	private DaoSupport dao;

	/*
	 * 通用类型查询方法，参数为在code_currency_kind表中的代表
	 * 如有新的添加请完善注释
	 * 1	民族
	 *
	 */
	@SuppressWarnings("unchecked")
	public List<PageData> getCurrencyType(String kindId) throws GenException {
		List<PageData> pds = null;
		try {
			pds = (List<PageData>)dao.findForList("CurrencyTypeMapper.getCurrencyType", kindId);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pds;
	}

	/***
	 * 二级联动查询
	 * @param typeBelongto
	 * @return
	 * @throws GenException
	 */
	@SuppressWarnings("unchecked")
	public List<PageData> getCurrencyTypeBelongto(String typeBelongto) throws GenException {
		List<PageData> pds = null;
		try {
			pds = (List<PageData>)dao.findForList("CurrencyTypeMapper.getCurrencyTypeBelongto", typeBelongto);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pds;
	}
}
