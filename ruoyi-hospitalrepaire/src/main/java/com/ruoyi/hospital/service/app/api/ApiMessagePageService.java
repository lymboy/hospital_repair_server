package com.ruoyi.hospital.service.app.api;

import com.ruoyi.hospital.dao.DaoSupport;
import com.ruoyi.hospital.framework.exception.GenException;
import com.ruoyi.hospital.util.PageData;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * <p> 手机端 消息页面 显示待办任务， 已保存尚未提交的数据，【待办， 请假，整改，工序】 </p>
 *
 * @author Young
 * @since 2018-05-07
 */
@Service
public class ApiMessagePageService {

    @Resource(name = "daoSupport")
    private DaoSupport dao;

    /**
     * 请假
     * @param pd
     * @return
     */
    public List<PageData> getLeaveRecordForMessagePageList(PageData pd) throws GenException {
        try {
            return (List<PageData>) dao.findForList("ApiMessagePageMapper.getLeaveRecordForMessagePageList", pd);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     * 整改
     * @param pd
     * @return
     */
    public List<PageData> getQualityPatrolProblemForMessagePageList(PageData pd) throws GenException {
        try {
            return (List<PageData>) dao.findForList("ApiMessagePageMapper.getQualityPatrolProblemForMessagePageList", pd);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     * 工序
     * @param pd
     * @return
     */
    public List<PageData> getQualityInspectForMessagePageList(PageData pd) throws GenException {
        try {
            return (List<PageData>) dao.findForList("ApiMessagePageMapper.getQualityInspectForMessagePageList", pd);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }
}
