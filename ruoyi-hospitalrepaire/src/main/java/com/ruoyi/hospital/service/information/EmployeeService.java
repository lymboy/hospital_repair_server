package com.ruoyi.hospital.service.information;

import com.ruoyi.hospital.dao.DaoSupport;
import com.ruoyi.hospital.entity.Page;
import com.ruoyi.hospital.framework.exception.GenException;
import com.ruoyi.hospital.framework.log.Log;
import com.ruoyi.hospital.util.PageData;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author 翠翠
 * @Description 员工管理
 */
@Service("employeeService")
public class EmployeeService {

	@Resource(name = "daoSupport")
	private DaoSupport dao;


	@Log(name="得到全部部门列表")
	public List<PageData> getPartmentList(PageData pd) throws GenException {
		List<PageData> partmentList=null;
			try {
				partmentList=(List<PageData>) dao.findForList("EmployeeMapper.getPartmentList", pd);
			} catch (Exception e) {
				throw new GenException(e.getCause());
			}
		return partmentList;
	}

	@Log(name="权限过滤得到全部部门列表")
	public List<PageData> getPartmentListByPrivilege(PageData pd) throws GenException {
//		List<PageData> partmentList=null;
//		try {
//			Subject currentUser = SecurityUtils.getSubject();
//			Session session = currentUser.getSession();
//			User user = (User)session.getAttribute(Const.SESSION_USER);
//			String departmentId = user.getDepartmentId();
//			pd.put("departmentId", departmentId);
//			int privilege = 0;// 个人和部门
//			List<DataPrivilege> dataPrivilegeList = (List<DataPrivilege>) session.getAttribute(Const.SESSION_dataPrivilegeList);
//			for (DataPrivilege dataPrivilege : dataPrivilegeList) {
//				if(dataPrivilege.getDataPrivilegeId()==3){// 公司
//					privilege = 1;// 公司
//					break;
//				}
//			}
//			if(privilege==1){
//				partmentList=(List<PageData>) dao.findForList("EmployeeMapper.getPartmentList", pd);
//			}else{
//				partmentList=(List<PageData>) dao.findForList("EmployeeMapper.getPartmentListByPrivilege", pd);
//			}
//		} catch (Exception e) {
//			throw new GenException(e.getCause());
//		}
//		return partmentList;

		return null;
	}


	@SuppressWarnings("unchecked")
	@Log(name="得到员工列表")
	public List<PageData> getEmployeeListPage(Page page) throws GenException {
		List<PageData> employeeList=null;
		try {
			employeeList=(List<PageData>) dao.findForList("EmployeeMapper.getEmployeelistPage", page);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	return employeeList;
	}


	@Log(name="通过员工id得到履历列表")
	public List<PageData> getIntroductionListPage(Page page) throws GenException {
		List<PageData> introductionList=null;
		try {
			introductionList=(List<PageData>) dao.findForList("EmployeeMapper.getIntroductionlistPage", page);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	return introductionList;
	}

	@SuppressWarnings("unchecked")
	public List<PageData> getExportEmployee(PageData pd) throws GenException{
		List<PageData> employeeList=null;
		try {
			employeeList=(List<PageData>) dao.findForList("EmployeeMapper.getExportEmployee", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return employeeList;
	}

	/**
	 * 获取所有员工列表
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public List<PageData> getEmployeelist(PageData pd) throws GenException{
		List<PageData> employeeList=null;
		try {
			employeeList=(List<PageData>) dao.findForList("EmployeeMapper.getEmployeelist", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return employeeList;
	}

	/**
	 * 根据ID 获取员工信息
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public PageData getEmployeeInfoById(PageData pd) throws GenException{
		PageData pdInfo = null;
		try {
			pdInfo = (PageData) dao.findForObject("EmployeeMapper.getEmployeeInfoById", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pdInfo;
	}


	//员工系统信息
	public PageData OpenEmployeeManager(PageData pd) throws GenException {
		PageData employeeNews=null;
		try {
			employeeNews=(PageData) dao.findForObject("EmployeeMapper.OpenEmployeeManager", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return employeeNews;
	}


	//得到所有的机构
	public List<PageData> getAllOrgnaization() throws GenException {
		List<PageData> orgnaizationList=null;
		try {
			orgnaizationList=(List<PageData>) dao.findForList("EmployeeMapper.getAllOrgnaization", null);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return orgnaizationList;
	}


	//员工工作编辑保存
	public void addEmployeeListRow(PageData pd) throws GenException {
		try {
			deleteEmployeeById(pd);//行删除
			dao.save("EmployeeMapper.addEmployeeListRow", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}

	}


	//根据员工id删除员工
	private void deleteEmployeeById(PageData pd) throws GenException {
		try {
			dao.update("EmployeeMapper.deleteEmployeeById", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}

	}


	//员工编辑保存
	public void editEmployee(PageData pd) throws GenException {
		try {
			dao.update("EmployeeMapper.editEmployee", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}

	}


	//员工图片
	public void editEmployeePhoto(PageData pd) throws GenException {
		try {
			dao.update("EmployeeMapper.editEmployeePhoto", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}


	//弹出所有的系统账号
	public List<PageData> getSystemDutylistPage(Page page) throws GenException {
		List<PageData> systemDutyList=null;
			try {
				systemDutyList=(List<PageData>) dao.findForList("EmployeeMapper.getSystemDutylistPage", page);
			} catch (Exception e) {
				throw new GenException(e.getCause());
			}
		return systemDutyList;
	}

	/**
	 * 根据员工ids获取员工信息 不过滤
	 * @throws GenException
	 */
	@SuppressWarnings("unchecked")
	public List<PageData> getEmployeesNoFilterByIdslistPage(Page page) throws GenException
	{
		List<PageData> pds = null;
		try
		{
			pds = (List<PageData>) dao.findForList("EmployeeMapper.getEmployeesNoFilterByIdslistPage", page);
		} catch (Exception e)
		{
			throw new GenException(e.getCause());
		}
		return pds;
	}
	/**
	 * 根据员工ids获取员工信息 过滤
	 * @throws GenException
	 */
	@SuppressWarnings("unchecked")
	public List<PageData> getEmployeesByIdslistPage(Page page) throws GenException
	{
		List<PageData> pds = null;
		try
		{
			pds = (List<PageData>) dao.findForList("EmployeeMapper.getEmployeesByIdslistPage", page);
		} catch (Exception e)
		{
			throw new GenException(e.getCause());
		}
		return pds;
	}

	/**
	 * 通过审批人得到部门
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	@SuppressWarnings("unchecked")
	public List<PageData> getPartmentListByApproval(PageData pd) throws GenException
	{
		List<PageData> pds = null;
		try
		{
			pds = (List<PageData>) dao.findForList("EmployeeMapper.getPartmentListByApproval", pd);
		} catch (Exception e)
		{
			throw new GenException(e.getCause());
		}
		return pds;
	}

	//更新表的排序字段
	public void updateEmployeeListOrder(PageData pd) throws GenException {
		try {
			dao.update("EmployeeMapper.updateEmployeeListOrder", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}

	//得到员工之前的专业
	public List<PageData> getBeforProfession(String employeeId) throws GenException {
		List<PageData> beforProfession=null;
		try {
			beforProfession=(List<PageData>) dao.findForList("EmployeeMapper.getBeforProfession", employeeId);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return beforProfession;
	}

	//添加员工专业
	public void addProfession(PageData pd) throws GenException {
		try {
			dao.save("EmployeeMapper.addProfession", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}

	//删除专业
	public void deleteProfession(PageData pd) throws GenException {
		try {
			dao.delete("EmployeeMapper.deleteProfession", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}

	//得到职称级别树
	public List<PageData> getLevelList() throws GenException {
		List<PageData> levelList=null;
			try {
				levelList=(List<PageData>) dao.findForList("EmployeeMapper.getLevelList", null);
			} catch (Exception e) {
				throw new GenException(e.getCause());
			}
		return levelList;
	}

	//得到员工的教育列表
	public List<PageData> getEmployeeEdulistPage(Page page) throws GenException {
		List<PageData> employeeEduList=null;
		try {
			employeeEduList=(List<PageData>) dao.findForList("EmployeeMapper.getEmployeeEdulistPage", page);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return employeeEduList;
	}

	//得到员工的奖惩列表
	public List<PageData> getEmployeeRewpun(Page page) throws GenException {
		List<PageData> employeeRewpunList=null;
		try {
			employeeRewpunList=(List<PageData>) dao.findForList("EmployeeMapper.getEmployeeRewpunlistPage", page);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return employeeRewpunList;
	}

	//
	public void deleteIntroduction(PageData pd) throws GenException {
		try {
			dao.update("EmployeeMapper.deleteIntroduction", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}

	//删除教育
	public void deleteEdu(PageData pd) throws GenException {
		try {
			dao.update("EmployeeMapper.deleteEdu", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}

	}

	//删除奖惩
	public void deleteRewpun(PageData pd) throws GenException {
		try {
			dao.update("EmployeeMapper.deleteRewpun", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}

	//添加员工履历记录
	public void saveRecord(PageData pd) throws GenException {
		try {
			dao.save("EmployeeMapper.saveRecord", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}

	//员工履历更新
	public void updateRecord(PageData pd) throws GenException {
		try {
			dao.update("EmployeeMapper.updateRecord", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}

	//教育添加
	public void saveEdu(PageData pd) throws GenException {
		try {
			dao.save("EmployeeMapper.saveEdu", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}
	//教育更新
	public void updateEdu(PageData pd) throws GenException {
		try {
			dao.update("EmployeeMapper.updateEdu", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}

	//添加奖惩
	public void saveRewpun(PageData pd) throws GenException {
		try {
			dao.save("EmployeeMapper.saveRewpun", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}

	//奖惩更新
	public void updateRewpun(PageData pd) throws GenException {
		try {
			dao.update("EmployeeMapper.updateRewpun", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}

	/**
	 * 员工信息 弹出 选择
	 * @param page
	 * @return
	 */
    public List<PageData> getEmployeeInfoListForSelect(Page page) throws GenException {

		try {
			return (List<PageData>) dao.findForList("EmployeeMapper.getEmployeeInfoForSelectlistPage", page);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}

	/**
	 *
	 * @param pd
	 */
    public void editEmployeeIdentityCard(PageData pd) throws GenException {
		try {
			dao.update("EmployeeMapper.editEmployeeIdentityCard", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
    }

    /**
	 *
	 * @param pd
	 */
   public List<PageData> getProjectTechnichan(PageData pd) throws GenException {
		try {
			return (List<PageData>) dao.findForList("EmployeeMapper.getProjectTechnichan", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
   }

   /**
    * 选择复查人
    *
    * */
   public List<PageData> getEmployeelistByBaseId(PageData pd) throws GenException {
		try {
			return (List<PageData>) dao.findForList("EmployeeMapper.getEmployeelistByBaseId", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
  }

   @SuppressWarnings("unchecked")
	@Log(name="得到员工列表")
	public List<PageData> getEmployeeList(PageData pd) throws GenException {
		List<PageData> employeeList=null;
		try {
			employeeList=(List<PageData>) dao.findForList("EmployeeMapper.getEmployeeList", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	return employeeList;
	}

	public PageData GetMember(PageData pd) throws GenException {
		PageData levelList=null;
		try {
			levelList=(PageData) dao.findForObject("EmployeeMapper.GetMember", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	return levelList;
	}

	public PageData getEmployeeInfoByRoll(PageData pd) throws GenException {
		PageData levelList=null;
		try {
			levelList=(PageData) dao.findForObject("EmployeeMapper.getEmployeeInfoByRoll", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return levelList;
	}

	public void addINTEGRAL(PageData pd) {
		try {
			dao.save("EmployeeMapper.addINTEGRAL", pd);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void editCMember(PageData pd) {
		try {
			dao.update("EmployeeMapper.editCMember", pd);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public List<PageData> getPartmentListByParentId(PageData pd) throws GenException {
		List<PageData> employeeList=null;
		try {
			employeeList=(List<PageData>) dao.findForList("EmployeeMapper.getPartmentListByParentId", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	return employeeList;
	}

	public List<PageData> GetOwnerList() throws GenException {
		List<PageData> employeeList=null;
		try {
			employeeList=(List<PageData>) dao.findForList("EmployeeMapper.GetOwnerList", null);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return employeeList;
	}

	public List<PageData> GetVehicleList() throws GenException {
		List<PageData> employeeList=null;
		try {
			employeeList=(List<PageData>) dao.findForList("EmployeeMapper.GetVehicleList", null);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return employeeList;
	}

	public List<PageData> GetIntegralInfo(PageData pd) throws GenException {
		List<PageData> levelList=null;
		try {
			levelList=(List<PageData>) dao.findForList("EmployeeMapper.GetIntegralInfo", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return levelList;
	}

	public String GetMemberIntegral(PageData pd) throws GenException {
		String levelList=null;
		try {
			levelList=(String) dao.findForObject("EmployeeMapper.GetMemberIntegral", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return levelList;
	}

	public List<PageData> GetCarList(PageData pd) throws GenException {
		List<PageData> levelList=null;
		try {
			levelList=(List<PageData>) dao.findForList("EmployeeMapper.GetCarList", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return levelList;
	}

}
