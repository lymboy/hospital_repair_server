package com.ruoyi.hospital.service.app;


import com.ruoyi.hospital.dao.DaoSupport;
import com.ruoyi.hospital.framework.exception.GenException;
import com.ruoyi.hospital.util.PageData;
import com.ruoyi.hospital.util.sequence;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * <p>  </p>
 *
 * @author Young
 * @since 2018-05-20
 */
@Service
public class UserClientService {

    @Resource(name = "daoSupport")
    private DaoSupport dao;

    /**
     *
     * @return
     * @throws GenException
     */
    public int insertUserClient(String USER_ID, String EMPLOYEE_ID, String CID) throws GenException {
        try {
            int rows = 0;
            PageData pd = new PageData();
            pd.put("ID", sequence.nextId());
            pd.put("USER_ID", USER_ID);
            pd.put("EMPLOYEE_ID", EMPLOYEE_ID);
            pd.put("CLIENT_ID", CID);
            pd.put("LOGIN_TIME", new Date());
            rows += (int) dao.delete("UserClientMapper.deleteUserClient", pd);
            rows += (int) dao.save("UserClientMapper.insertUserClient", pd);
            return rows;
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     *
     * @param pd
     * @return
     * @throws GenException
     */
    public int deleteUserClient(PageData pd) throws GenException {
        try {
            return (int) dao.save("UserClientMapper.deleteUserClient", pd);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     * 查询客户端CID BY employeeId
     * @param employeeId
     * @return
     */
    public String getClientIdByEmployeeId(String employeeId) throws GenException {
        try {
            return (String) dao.findForObject("UserClientMapper.getClientIdByEmployeeId", employeeId);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     * 查询客户端CID BY userId
     * @param userId
     * @return
     */
    public String getClientIdByUserId(String userId) throws GenException {
        try {
            return (String) dao.findForObject("UserClientMapper.getClientIdByUserId", userId);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

}
