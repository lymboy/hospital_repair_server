package com.ruoyi.hospital.service.basicsetting.project;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.UncategorizedSQLException;
import org.springframework.stereotype.Service;

import com.ruoyi.hospital.dao.DaoSupport;
import com.ruoyi.hospital.entity.Page;
import com.ruoyi.hospital.framework.exception.GenException;
import com.ruoyi.hospital.util.PageData;

@Service
@Resource(name="projectService")
public class ProjectService
{
	@Resource(name="daoSupport")
	private DaoSupport dao;

	/**
	 * 分页查询项目列表
	 * @throws GenException
	 */
	@SuppressWarnings("unchecked")
	public List<PageData> getProjectListPage(Page page) throws GenException
	{
		List<PageData> pds = null;
		try
		{
			pds = (List<PageData>) dao.findForList("projectMapper.getProjectlistPage", page);
		} catch (Exception e)
		{
			throw new GenException(e.getCause());
		}

		return pds;
	}

	/**
	 * 查询项目列表(无分页)
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public List<PageData> getProjectList(PageData pd) throws GenException
	{
		List<PageData> pds = null;
		try
		{
			pds = (List<PageData>) dao.findForList("projectMapper.getProjectlist", pd);
		} catch (Exception e)
		{
			throw new GenException(e.getCause());
		}

		return pds;
	}


	/**
	 * 根据项目id查询
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public PageData getProjectById(PageData pd) throws GenException
	{
		PageData project = null;
		try
		{
			project = (PageData) dao.findForObject("projectMapper.getProjectById", pd);
		} catch (Exception e)
		{
			throw new GenException(e.getCause());
		}
		return project;
	}

	public PageData getProjectCountMax(PageData pd) throws GenException
	{
		PageData project = null;
		try
		{
			project = (PageData) dao.findForObject("projectMapper.getProjectCountMax", pd);
		} catch (Exception e)
		{
			throw new GenException(e.getCause());
		}
		return project;
	}

	/**
	 * 新增项目
	 * @throws GenException
	 */
	public void addProject(PageData pd) throws GenException
	{
		try
		{
			dao.save("projectMapper.addProject", pd);
		} catch (Exception e)
		{
			if(e instanceof UncategorizedSQLException) {
				////System.out.println(e.getMessage());
			}else{
				throw new GenException(e.getCause());
			}
		}
	}

	/**
	 * 删除一个项目
	 * @throws GenException
	 */
	public void deleteProject(PageData pd) throws GenException
	{
		try
		{
			dao.update("projectMapper.deleteProject", pd);
		} catch (Exception e)
		{
			throw new GenException(e.getCause());
		}
	}

	/**
	 * 编辑一个项目
	 * @param pd
	 * @throws GenException
	 */
	public void editProject(PageData pd) throws GenException
	{
		try
		{
			dao.update("projectMapper.editProject", pd);
		} catch (Exception e)
		{
			throw new GenException(e.getCause());
		}
	}

	/**
	 * 项目下标段 BY DEPARTMENT_ID
	 * @param pd
	 * @return
	 */
    public List<PageData> getProjectDeptList(PageData pd) throws GenException {
		try {
			return (List<PageData>) dao.findForList("projectMapper.getProjectDeptList", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}

	/**
	 * 新增 或 编辑项目
	 * @param pd
	 * @return
	 * @throws GenException
	 */
    public int insertOrUpdateProject(PageData pd) throws GenException {
		try {
			String ID = String.valueOf(pd.get("ID"));
			if (ID == null || "".equals(ID)) {
				return -1;
			}
			if (getProjectById(pd) == null) {
				// 新增
				PageData projectCountMax = getProjectCountMax(null);
				int max = 0;
				if(projectCountMax == null){
					max = max + 1;
				}else{
					String maxCount = projectCountMax.getString("maxcount");
					max = Integer.parseInt(maxCount) + 1;
				}
				pd.put("PROJECT_BASE_NUM", max);
				return (int) dao.update("projectMapper.addProject", pd);
			}
			// 编辑
			return (int) dao.update("projectMapper.editProject", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
    }
}
