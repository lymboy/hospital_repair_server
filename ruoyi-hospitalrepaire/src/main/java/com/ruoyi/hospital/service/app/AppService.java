//package com.ruoyi.hospital.service.app;
//
//import java.awt.image.BufferedImage;
//import java.io.File;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.text.SimpleDateFormat;
//import java.util.*;
//
//import javax.annotation.Resource;
//import javax.imageio.ImageIO;
//
//import com.sun.image.codec.jpeg.JPEGCodec;
//import com.sun.image.codec.jpeg.JPEGImageEncoder;
//import com.ruoyi.hospital.entity.project.FileInfo;
//import com.ruoyi.hospital.entity.project.Group;
//import com.ruoyi.hospital.entity.project.PageBean;
//import com.ruoyi.hospital.entity.project.ProjectQaAcceptance;
//import com.ruoyi.hospital.util.UuidUtil;
//import org.activiti.engine.history.HistoricTaskInstance;
//import org.activiti.engine.task.Task;
//import org.springframework.stereotype.Service;
//
//import com.ruoyi.hospital.dao.DaoSupport;
//import com.ruoyi.hospital.entity.Page;
//import com.ruoyi.hospital.entity.system.Menu;
//import com.ruoyi.hospital.framework.exception.GenException;
//import com.ruoyi.hospital.util.PageData;
//import org.springframework.web.multipart.MultipartFile;
//
///**
// * <p>Title: AppService.java</p>
// * @Desc:
// * @author 杨建军
// * @Date: 2017年4月27日
// * @Time: 下午4:47:14
// * @version:
// */
//@Service(value="appService")
//public class AppService {
//
//	@Resource(name = "daoSupport")
//	private DaoSupport dao;
//
//
//	//压缩图片时的宽高
//	private static final int s_width = 320;
//    private static final int s_height = 384;
//    private static final int m_width = 160;
//    private static final int m_height = 192;
//	/**
//	 * 手机端的按钮 根据用户身份ID获取
//	 * @param pd
//	 * @return
//	 * @throws GenException
//	 */
//	public List<Menu> listAllPhoneMenu(PageData pd) throws GenException {
//		List<Menu> pdsList = null;
//		try {
//			pdsList = (List<Menu>) dao.findForList("AppMapper.listAllPhoneMenu", pd);
//
//
//		} catch (Exception e) {
//			throw new GenException(e.getCause());
//		}
//		return pdsList;
//	}
//
//	/**
//	 * 根据身份部门获取项目部
//	 * @param departmentId
//	 * @return
//	 * @throws GenException
//	 */
//	public PageData getUserProjectByIdentityPartmentId(String departmentId) throws GenException{
//		PageData pdInfo = null;
//		try {
//			pdInfo = (PageData) dao.findForObject("AppMapper.getUserProjectByIdentityPartmentId", departmentId);
//		} catch (Exception e) {
//			throw new GenException(e.getCause());
//		}
//		return pdInfo;
//	}
//
//	//员工图片
//	public void editEmployeePhoto(PageData pd) throws GenException {
//		try {
//			dao.update("AppMapper.editEmployeePhoto", pd);
//		} catch (Exception e) {
//			throw new GenException(e.getCause());
//		}
//	}
//
//	//查询得到质检列表
//	@SuppressWarnings("unchecked")
//	public List<PageData> getQualityCheckList(Page page) throws GenException {
//		List<PageData> pds=null;
//		try {
//			pds=(List<PageData>) dao.findForList("AppMapper.getQualityChecklistPage", page);
//		} catch (Exception e) {
//			throw new GenException(e.getCause());
//		}
//		return pds;
//	}
//
//	//通过质量id得到质检信息
//	public PageData getQualityCheckInfo(String qualityId) throws GenException {
//		PageData pds=null;
//		try {
//			pds=(PageData) dao.findForObject("AppMapper.getQualityCheckInfo", qualityId);
//		} catch (Exception e) {
//			throw new GenException(e.getCause());
//		}
//		return pds;
//	}
//
//	//得到项目质量列表
//	public List<PageData> getProjectQualityList() throws GenException {
//		List<PageData> pds=null;
//		try {
//			pds=(List<PageData>) dao.findForList("AppMapper.getProjectQualityList", null);
//		} catch (Exception e) {
//			throw new GenException(e.getCause());
//		}
//		return pds;
//	}
//
//	//根据id查找质量单元验收从表
//	public List<PageData> getFileList(String qualityId) throws GenException {
//		List<PageData> pds=null;
//		try {
//			pds=(List<PageData>) dao.findForList("AppMapper.getFileList", qualityId);
//		} catch (Exception e) {
//			throw new GenException(e.getCause());
//		}
//		return pds;
//	}
//
//	//查询审批人名称
//	public String getApproveName(String assigneeId) throws GenException {
//		String approveName=null;
//		try {
//			approveName = (String)dao.findForObject("AppMapper.getApproveName", assigneeId);
//		} catch (Exception e) {
//			throw new GenException(e.getCause());
//		}
//		return approveName;
//	}
//
//	/**
//	 *  获取项目质量单元模板数据
//	 * @param pd
//	 * @return
//	 * @throws GenException
//	 */
//	public List<PageData> getProjectQualityDetailList(PageData pd) throws GenException {
//		List<PageData> pdList=null;
//		try {
//			pdList=(List<PageData>) dao.findForList("AppMapper.getProjectQualityDetailList", pd);
//		} catch (Exception e) {
//			throw new GenException(e.getCause());
//		}
//		return pdList;
//	}
//
//	/**
//	 * 根据项目Id找到项目
//	 * @param dataId
//	 * @return
//	 * @throws GenException
//	 */
//	public Map<String,String> getDetailDataById(String dataId) throws GenException {
//		Map<String,String> data = null;
//		try {
//			data = (Map<String,String>)dao.findForObject("AppMapper.getDetailDataById",dataId);
//		}catch (Exception e){
//			throw new GenException(e.getCause());
//		}
//		return data;
//	}
//
//    /**
//     * 根据参数从ieqm_proQaAcceptance表中查询数据
//     * @param pd
//     * @return
//     * @throws Exception
//     */
//	public Map<String,PageData> getDatas(PageData pd) throws Exception {
//		String pageSize = pd.getString("pageSize");
//		Page page = new Page();
//
//		if (pageSize.equals("")){
//			pd.put("pageSize",20);
//		}
//		String keyWord = pd.getString("keyWord").trim();
//
//		if (keyWord != null && !keyWord.equals("")){
//			pd.put("keyWord",
//					"%"+keyWord.replace(" ","%")+"%");	//将KeyWord设置为模糊查询格式
//		}
//
//		List<ProjectQaAcceptance> datas = (List<ProjectQaAcceptance>) dao.findForList("AppMapper.getProjectQaByProjectId",pd);	//	从数据库获取数据
//
//		PageData countDatas = (PageData) dao.findForObject("AppMapper.getDatasCount",pd);
//
//		pd.put("totalCount",countDatas.getString("totalCount"));	//获取记录总条数
//
//		pd.put("pageSize",pageSize);	//获取PageSize
//
//		pd.put("currentPage",Integer.valueOf(pd.getString("currentPage"))+1);
//
//		pd.put("passedCount",countDatas.getString("passedCount"));
//		pd.put("failedCount",countDatas.getString("failedCount"));
//		pd.put("checkingCount",countDatas.getString("checkingCount"));
//		pd.put("uncheckedCount",countDatas.getString("uncheckedCount"));
//
//		//获取文件组的信息
//		Map<String,Group> map = getPhotosInfo(datas);
//
//		//设置items并放入pd中返回
//        pd.put("data",bindItems(datas,map));
//
//		return pd;
//	}
//
//
//
//    /**
//     * 根据用户ID查询该用户参与的所有项目信息
//     * 和每个项目的待办任务数量
//     * @param userID
//     * @param taskList 待办任务列表
//     * @return
//     * @throws GenException
//     */
//	public List<PageData> getProjects(String userID,List<Task> taskList) throws GenException {
//        List<String> processInstanceIds = new ArrayList<>();
//        for (Task task :taskList){
//            processInstanceIds.add(task.getProcessInstanceId());
//        }
//		List<PageData> datas = new ArrayList<>();
//        List<PageData> counts = new ArrayList<>();
//		try {
//		    //得到用户所属的项目列表
//			datas = (List<PageData>) dao.findForList("AppMapper.getProjects",userID);
//			if (taskList != null && taskList.size()>0) {
//                //得到待办任务明细表的主键ID
//                List<String> ids =
//                        (List<String>) dao.findForList("AppMapper.getFlowWordIDsByProcessInstanceIDs", processInstanceIds);
//                //统计每个项目中待办任务的数量
//                counts = (List<PageData>) dao.findForList("AppMapper.getProjectQaTaskCount", ids);
//            }
//		} catch (Exception e) {
//			throw new GenException(e.getCause());
//		}
//		//将待办任务的统计信息放入对应的项目中
//        for (PageData project : datas) {
//		    String projectId = project.getString("projectId");
//		    String tasks = "0";
//            for (PageData count : counts) {
//                if (count.getString("projectId").equals(projectId)){
//                    tasks = count.getString("Tasks");
//                    break;
//                }
//            }
//            project.put("Tasks",tasks);
//        }
//
//		return datas;
//	}
//
//
//    /**
//     * 根据id删除行
//     * @param ID 主键ID
//     * @return boolean
//     */
//	public boolean deleteProjectQa(String ID){
//        try {
//           dao.delete("AppMapper.deleteProjectQa",ID);
//        } catch (Exception e) {
//            e.printStackTrace();
//            return false;
//        }
//        return true;
//    }
//
//    /**
//     * 添加新数据信息
//     * @param pd
//     * @return boolean值
//     */
//    public boolean addProjectQa(PageData pd) {
//        try {
//            dao.save("AppMapper.insertProjectQa",pd);
//        } catch (Exception e) {
//            e.printStackTrace();
//            return false;
//        }
//        return true;
//    }
//
//    /**
//     * 更新ProjectQa
//     * @param pd
//     * @return
//     */
//    public boolean updateProjectQa(PageData pd){
//        try {
//            int result = (int)dao.update("AppMapper.updateProjectQa",pd);
//            if(result > 0){
//                return true;
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//            return false;
//        }
//        return false;
//    }
//
//    /**
//     * 根据projectQa来获取其相关的附件信息
//     * @param qas  projectQaAcceptance对应的实例列表
//     * @return  返回Map集合，键为从表的ID(主表的billId)，值为Group对象，包含了文件信息和组的相关信息
//     * @throws Exception
//     */
//	private Map<String,Group> getPhotosInfo(List<ProjectQaAcceptance> qas) throws Exception {
//
//	    if (qas == null || qas.size() == 0){
//	       return new HashMap<>();
//        }
//	    //提取billNos
//	    List<String> IDs = new ArrayList<>();
//	    for (ProjectQaAcceptance qa : qas){
//	        IDs.add(qa.getId());
//        }
//        List<Map<String,String>> datas =(List<Map<String,String>>) dao.findForList("AppMapper.getPhotos",IDs);
//
//        Map<String,Group> groups = new HashMap<>();
//
//        //没一个itemID对应一个Group，按照itemID进行分组
//	    for (Map<String,String> data : datas){
//	        String itemID = data.get("itemID");
//	        if (groups.get(itemID) == null){
//	            groups.put(itemID,new Group(data));
//            }else{                                  //如果group已经存在，直接像其中添加pics
//	            groups.get(itemID).addPic(data);
//            }
//        }
//        return groups;
//    }
//
//    /**
//     *
//     * @param pd (Map类,里面应包含parentID，pageSize，currentPage)
//     * @return
//     * @throws GenException
//     */
//    public PageBean<PageData> getUnitItemByParentID(PageBean page,PageData pd) throws Exception{
//
//        String keyWord = pd.getString("keyWord").trim();
//
//        if (keyWord != null && !keyWord.equals("")){
//            pd.put("keyWord",
//                    "%"+keyWord.replace(" ","%")+"%");	//将KeyWord设置为模糊查询格式
//        }
//
//	    List<PageData> rows =
//                        (List<PageData>) dao.findForList("AppMapper.getUnitItemByParentID",pd);
//
//        int totalCount =
//                        (Integer) dao.findForObject("AppMapper.getUnitItemByParentIDCount",pd);
//
//        page.setRows(rows);
//
//        page.setTotalCount(totalCount);
//
//        return page;
//    }
//
//    /**
//     * 添加新的ProjectQa
//     * 主表是ieqm_proQaAcceptance,从表是ieqm_proQaAcceptanceItem,文件信息表是frame_fileInfo
//     * @param files 附件
//     * @param rootPath  项目根路径
//     * @param pd    参数
//     * @return
//     */
//    public String addProjectQa(MultipartFile[] files,String rootPath,PageData pd) throws GenException{
//        String id = UuidUtil.get32UUID();   //主表的主键
//        String itemId = UuidUtil.get32UUID(); //从表主键
//        String nowTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());   //当前日期
//        //存入主键
//        pd.put("ID",id);
//
//        int index = -1;
//
//        //判断是否有文件
//        for (int i = 0;i< files.length;i++){
//            if (!files[i].isEmpty()){
//                index = i;
//                break;
//            }
//        }
//
//        try {
//            //向主表中插入数据
//            dao.save("AppMapper.insertProjectQa", pd);
//            if (index != -1) {
//                //保存文件，得到文件信息的列表
//                List<FileInfo> fileInfos = saveFiles(files, rootPath, itemId,pd.getString("userID"));
//                //整理从表的参数
//                PageData itemParameters = new PageData();
//                itemParameters.put("ID", itemId);
//                itemParameters.put("billID", id);
//                itemParameters.put("remark", pd.getString("remark"));
//                itemParameters.put("fileName", files[index].getOriginalFilename());
//                itemParameters.put("fileID", UuidUtil.get32UUID());
//                itemParameters.put("fileSize", files[index].getSize());
//                itemParameters.put("userID", pd.getString("userID"));
//                itemParameters.put("uploadTime", nowTime);
//                //向从表中插入数据
//                dao.save("AppMapper.insertProjectQaItem", itemParameters);
//                //向文件表中插入数据
//                dao.batchSave("AppMapper.insertFileInfos",fileInfos);
//            }
//        }catch (Exception e){
//            e.printStackTrace();
//            throw new GenException(e.getCause());
//        }
//
//        return id;
//    }
//
//    /**
//     * 添加附件
//     * @param files
//     * @param userID
//     * @param ID
//     * @param rootPath
//     */
//    public boolean insertFile(MultipartFile[] files,String userID,String ID,String rootPath) throws GenException{
//
//        String nowTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());   //当前日期
//
//        PageData parameters = new PageData();
//        parameters.put("userID",userID);
//        parameters.put("ID",ID);
//        //先从从表中查找是否有相应的记录
//        String itemID = "";
//        try {
//            itemID = (String) dao.findForObject("AppMapper.getIDFromProQaItem",parameters);
//        } catch (Exception e) {
//            e.printStackTrace();
//            throw new GenException(e.getCause());
//        }
//        //如果从表中没有记录，则先向从表中插入记录
//        if (itemID == null || itemID.equals("")){
//            itemID = UuidUtil.get32UUID();
//            //整理从表的参数
//            PageData itemParameters = new PageData();
//            itemParameters.put("ID",itemID);
//            itemParameters.put("billID",ID);
//            itemParameters.put("remark","");
//            itemParameters.put("fileName",files[0].getOriginalFilename());
//            itemParameters.put("fileID",UuidUtil.get32UUID());
//            itemParameters.put("fileSize",files[0].getSize());
//            itemParameters.put("userID",userID);
//            itemParameters.put("uploadTime",nowTime);
//        }
//        //保存文件
//        List<FileInfo> fileInfos = saveFiles(files,rootPath,itemID,userID);
//
//        //将文件信息保存到数据库
//        try {
//            dao.save("AppMapper.insertFileInfos",fileInfos);
//        } catch (Exception e) {
//            e.printStackTrace();
//            throw new GenException(e.getCause());
//        }
//        return true;
//    }
//
//    /**
//     * 生成图片文件的小、中缩略图并保存到指定路径。
//     * 如果文件的格式不支持压缩，则直接不做处理，直接保存
//     * @param files
//     * @param rootPath  保存文件的实际目录会根据日期在<code>rootPath</code>自动生成)
//     * @param itemId
//     * @return 返回文件信息对象FileInfo的集合
//     */
//    private List<FileInfo> saveFiles(MultipartFile[] files,String rootPath,String itemId,String userID){
//        String date = new SimpleDateFormat("yyyyMM").format(new Date());
//        //保存路径的文件夹
//        String savePath = rootPath+"attachment"+"/"+date+"/";
//        new File(savePath).mkdirs();
//        //获取支持操作的图片类型
//        String types = Arrays.toString(ImageIO.getReaderFormatNames());
//
//        List<FileInfo> fileInfos = new ArrayList<>();
//
//        for (MultipartFile file : files){
//            if (file.isEmpty()){
//                continue;
//            }
//            //文件信息的封装对象
//            FileInfo info = new FileInfo();
//            String saveName = UuidUtil.get32UUID();
//            info.setSaveName(saveName);
//            info.setViewName(file.getOriginalFilename());
//            info.setFileSize(Long.toString(file.getSize()));
//            info.setID(UuidUtil.get32UUID());
//            info.setRootPath(savePath);
//            info.setUploadDate(new SimpleDateFormat("yyyyMM").format(new Date()));
//            info.setUserID(userID);
//            //构建源文件对象
//            File rootFile = new File(savePath,saveName+".jpg");
//
//            try {
//                //保存源文件
//                file.transferTo(rootFile);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            // 如果是图片格式，生成缩略图,如果为其它文件直接保存
//            // ImageIO 支持的图片类型 : [BMP, bmp, jpg, JPG, wbmp, jpeg, png, PNG,
//            // JPEG, WBMP, GIF, gif]
//            String suffix = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".")+1);
//            if (types.indexOf(suffix.toLowerCase())>0){
//                String middleSaveName = "middle" + saveName;
//                String middleSavePath = savePath + middleSaveName;
//                //压缩中等缩略图
//                rotatePhonePhoto(rootFile,middleSavePath,".jpg",m_width,m_height);
//                String smallSaveName = "small" + saveName;
//                String smallSavePath = savePath + smallSaveName;
//                //压缩小缩略图
//                rotatePhonePhoto(rootFile,smallSavePath,".jpg",s_width,s_height);
//
//                info.setMiddleSaveName(middleSaveName);
//                info.setMiddlePath(middleSavePath);
//                info.setSmallSaveName(smallSaveName);
//                info.setSmallPath(smallSavePath);
//                info.setID(UuidUtil.get32UUID());
//                info.setMasterID(itemId);
//            }
//            fileInfos.add(info);
//        }
//        return fileInfos;
//    }
//
//    /**
//     * 通过gileID删除附件
//     * @param fileID
//     * @return
//     */
//    public boolean deleteFileByID(String fileID){
//        FileInfo info = null;
//        try {
//            info = (FileInfo) dao.findForObject("AppMapper.getFileInfo",fileID);
//            dao.delete("AppMapper.deleteFile",fileID);
//        } catch (Exception e) {
//            e.printStackTrace();
//            return false;
//        }
//        if (info != null){
//            //删除源文件
//            deleteFile(info.getRootPath()+"/"+info.getSaveName());
//            //删除小缩略图
//            deleteFile(info.getSmallPath()+"/"+info.getSmallSaveName());
//            //删除中缩略图
//            deleteFile(info.getMiddlePath()+"/"+info.getMiddleSaveName());
//        }
//        return true;
//    }
//
//    /**
//     * 得到用户的所有任务列表
//     * 即当前任务和历史任务的合集
//     * @param taskList  当前任务列表
//     * @param historicTaskList  历史任务列表
//     * @param pageBean 分页实体对象
//     * @param pd 参数
//     * @return  将两者合并获取所有的任务列表
//     */
//    public PageBean<ProjectQaAcceptance> getAllTask( List<Task> taskList,List<HistoricTaskInstance> historicTaskList,
//                                                     PageBean<ProjectQaAcceptance> pageBean,
//                                                     PageData pd) throws Exception {
//        List<String> processInstanceIds = new ArrayList<>();
//        for (Task task :taskList){
//            processInstanceIds.add(task.getProcessInstanceId());
//        }
//        for (HistoricTaskInstance task : historicTaskList){
//            processInstanceIds.add(task.getProcessInstanceId());
//        }
//        return getProjectQaByID(processInstanceIds,pageBean,pd);
//    }
//
//    /**
//     * 根据projectQa的主键ID来查询ProjectQaAcceptance实例
//     * @param processInstanceIds  流程Id
//     * @param pageBean 分页实体对象
//     *@param parameters 查询参数(分页有关参数、checkTypeNo、keyWord)
//     * @return
//     * @throws Exception
//     */
//    private PageBean<ProjectQaAcceptance> getProjectQaByID(List<String> processInstanceIds,
//                                                           PageBean<ProjectQaAcceptance> pageBean,
//                                                           PageData parameters) throws Exception {
//        //获取FlowWordID
//        if (processInstanceIds == null || processInstanceIds.size()==0){
//            return new PageBean<>();
//        }
//        List<String> ids =
//                (List<String>) dao.findForList("AppMapper.getFlowWordIDsByProcessInstanceIDs",processInstanceIds);
//
//        if (ids == null || ids.size()== 0){
//            return new PageBean<>();
//        }
//        //整理参数
//        String keyWord = parameters.getString("keyWord").trim();
//        parameters.put("checkTypeNo",parameters.getString("checkTypeNo"));
//        if (!keyWord.equals("")){
//            keyWord ="%"+keyWord.replace(" ","%")+"%";	//将KeyWord设置为模糊查询格式
//            parameters.put("keyWord",keyWord);
//        }
//        parameters.put("ids",ids);
//        //从数据库获取projectQa的数据
//        List<ProjectQaAcceptance> datas =
//                    (List<ProjectQaAcceptance>) dao.findForList("AppMapper.getProjectQaByID",parameters);
//        //统计totalCount
//        int totalCount = (int) dao.findForObject("AppMapper.getProjectQaByIDCount",parameters);
//        //获取文件组信息
//        Map<String,Group> groups = getPhotosInfo(datas);
//
//        //根据每个group的billID和主表的ID相等，将group插入到对象中
//        bindItems(datas,groups);
//        pageBean.setRows(datas);
//        pageBean.setTotalCount(totalCount);
//
//        return pageBean;
//    }
//
//    /**
//     *根据历史任务类HistoryTask得到projetQa
//     * @param taskList
//     * @param pd        查询条件的参数
//     * @return
//     */
//    public PageBean<ProjectQaAcceptance> getProjectQaByHistoryTask(List<HistoricTaskInstance> taskList,
//                                                               PageBean pageBean,
//                                                               PageData pd) throws Exception {
//        List<String> processInstanceIds = new ArrayList<>();
//        for (HistoricTaskInstance task :taskList){
//            processInstanceIds.add(task.getProcessInstanceId());
//        }
//        return getProjectQaByID(processInstanceIds,pageBean,pd);
//    }
//
//    /**
//     * 根据任务类Task得到ProjectQa
//     * @param taskList
//     * @param pd
//     * @return
//     * @throws Exception
//     */
//    public PageBean<ProjectQaAcceptance> getProjectQaByTask(List<Task> taskList,
//                                                            PageBean pageBean,
//                                                            PageData pd) throws Exception {
//        List<String> processInstanceIds = new ArrayList<>();
//        for (Task task :taskList){
//            processInstanceIds.add(task.getProcessInstanceId());
//        }
//        return getProjectQaByID(processInstanceIds,pageBean,pd);
//    }
//
//    /**
//     * 删除一个系统文件
//     * @param filePath
//     * @return  路径所表示的文件不存在也视为删除成功。
//     */
//    private static boolean deleteFile(String filePath){
//        File file = new File(filePath);
//        if (file.isFile()){
//           return file.delete();
//        }
//        return true;
//    }
//
//
//    /**
//     *通过unitID得到Unit
//     * @param pd (Map类,里面应包含projID，pageSize，currentPage)
//     * @return
//     * @throws GenException
//     */
//    public PageBean<PageData> getUnitItemByProjID(PageBean page,PageData pd) throws Exception {
//
//        List<PageData> data
//                = (List<PageData>) dao.findForList("AppMapper.getUnitItemByProjID",pd);
//
//        int totalCount =(Integer) dao.findForObject("AppMapper.getUnitItemByProjIDCount",pd);
//
//        page.setRows(data);
//
//        page.setTotalCount(totalCount);
//
//        return page;
//    }
//
//    /**
//     *  将图片按照新的尺寸压缩并输出
//     * @param file          数据源
//     * @param saveName      输出文件的名称，
//     * @param suffix        输出文件的后缀
//     * @param w
//     * @param h
//     * @return
//     */
//    public File rotatePhonePhoto(File file, String saveName, String suffix, int w, int h) {
//
//        File newFile = new File(saveName+suffix);
//
//        FileOutputStream out = null;
//
//        BufferedImage src;
//        try {
//            src = ImageIO.read(file);
//            int src_width = src.getWidth(null);
//            int src_height = src.getHeight(null);
//
//            int swidth = src_width;
//            int sheight = src_height;
//
//
//            if ((swidth * 1.0) / w < (swidth * 1.0) / h) {
//                if (swidth > w) {
//                    Number dh = sheight * w / swidth;
//                    h = dh.intValue();
//                }
//            } else {
//                if (sheight > h) {
//
//                    Number dw = swidth * h / sheight;
//                    w = dw.intValue();
//                }
//            }
//
//            BufferedImage image = new BufferedImage(w, h,BufferedImage.TYPE_INT_RGB );
//            image.getGraphics().drawImage(src, 0, 0, w, h, null); // 绘制缩小后的图
//            out = new FileOutputStream(newFile); // 输出到文件流
//            // 可以正常实现bmp、png、gif转jpg
//            JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(out);
//            encoder.encode(image); // JPEG编码
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch(Exception e) {
//            e.printStackTrace();
//        } finally {
//            try {
//                if (out!=null) {
//                    out.close();
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//
//        return newFile;
//    }
//
//    /**
//     * 将group绑定到ProjectQaAcceptance对象中的items
//     * 根据ProjectQaAcceptance中的ID和group的billId相等来进行绑定
//     * @param datas ProjectQaAcceptance实例的集合
//     * @param groups   以billId为主键的文件信息组
//     * @return  绑定后的ProjectQaAcceptance实例集合
//     */
//    private List<ProjectQaAcceptance> bindItems(List<ProjectQaAcceptance> datas,Map<String,Group> groups){
//        for (int i=0;i<datas.size();i++){
//            for (Group item : groups.values()){
//                ProjectQaAcceptance data = datas.get(i);
//                if (item.getBillId().equals(data.getId())){
//                    data.setItems(item);
//                }
//            }
//        }
//        return datas;
//    }
//
//    public String getEmployeeIdByUserId(String userId){
//        String employeeId = "";
//        try {
//            employeeId = (String) dao.findForObject("AppMapper.getEmployeeIdByUserId",userId);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return employeeId;
//    }
//
//    public String getProcessInstanceIdByProQaId(String ID) throws GenException {
//        String id = "";
//        try {
//           id = (String) dao.findForObject("AppMapper.getProcessInstanceIdByProQaId",ID);
//        } catch (Exception e) {
//            e.printStackTrace();
//            throw new GenException(e.getCause());
//        }
//        return id;
//    }
//
//    public List<Map<String,String>> getEmployeeNamesByIds(List<String> employeeIDs){
//        if (employeeIDs == null || employeeIDs.size()==0){
//            return new ArrayList<>();
//        }
//        List<Map<String,String>> data = null;
//        try {
//            data =(List<Map<String,String>>) dao.findForObject("AppMapper.getEmployeesById",employeeIDs);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return data;
//    }
//
//}
