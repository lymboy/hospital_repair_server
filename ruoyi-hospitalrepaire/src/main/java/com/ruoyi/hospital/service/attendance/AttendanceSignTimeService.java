package com.ruoyi.hospital.service.attendance;

import com.ruoyi.hospital.dao.DaoSupport;
import com.ruoyi.hospital.framework.exception.GenException;
import com.ruoyi.hospital.util.PageData;
import com.ruoyi.hospital.util.UuidUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * <p> 考勤时间设置 </p>
 *
 * @author Young
 * @since 2018/4/2
 */
@Service
public class AttendanceSignTimeService {

    @Autowired
    private DaoSupport dao;

    /**
     * 根据原始数据初始化 基础数据，已有不改变， 添加缺失的，删除多余的
     * @param rowDataList
     * @return
     * @throws GenException
     */
    public boolean checkInitAttendanceSignTime(List<PageData> rowDataList, String userId) throws GenException {
        try {
            if (rowDataList == null || rowDataList.size() == 0) {
                return false;
            }
            PageData astPD = new PageData();
            List<PageData> attendanceSignTimeList = getAttendanceSignTimeList(astPD);
            for (int i = 0; i < rowDataList.size(); i++) {
                PageData rd = rowDataList.get(i);
                String rdId = String.valueOf(rd.get("TYPE_ID"));
                boolean useFlag = false;
                if (attendanceSignTimeList != null && attendanceSignTimeList.size() >0) {
                    for (int j = 0; j < attendanceSignTimeList.size(); j++) {
                        PageData ast = attendanceSignTimeList.get(j);
                        String astSignType = String.valueOf(ast.get("SIGN_TYPE"));
                        if (rdId != null && rdId.equals(astSignType)) {
                            useFlag = true;
                            ast.put("use_flag", true); // 表中数据当前可以存在
                            break;
                        }
                    }
                }
                // 表中没有，需要添加一条
                if (!useFlag) {
                    PageData newPd = new PageData();
                    newPd.put("ID", UuidUtil.get32UUID());
                    newPd.put("SIGN_TYPE", rdId);
                    newPd.put("CREATEDATE", new Date());
                    newPd.put("CREATER", userId);
                    dao.save("AttendanceSignTimeMapper.insertAttendanceSignTime", newPd);
                }
            }
            // 删掉不应该存在的数据
            if (attendanceSignTimeList != null && attendanceSignTimeList.size() >0) {
                for (int j = 0; j < attendanceSignTimeList.size(); j++) {
                    PageData ast = attendanceSignTimeList.get(j);
                    String astSignType = String.valueOf(ast.get("SIGN_TYPE"));
                    Object useFlag = ast.get("use_flag");
                    if (useFlag == null) {
                        PageData delPd = new PageData();
                        delPd.put("ID", UuidUtil.get32UUID());
                        delPd.put("SIGN_TYPE", astSignType);
                        delPd.put("CREATEDATE", new Date());
                        delPd.put("CREATER", userId);
                        dao.update("AttendanceSignTimeMapper.deleteAttendanceSignTime", delPd);
                    }
                }
            }
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
        return true;
    }

    /**
     * 列表
     * @return
     * @throws GenException
     */
    public List<PageData> getAttendanceSignTimeList(PageData pd) throws GenException {
        try {
            return (List<PageData>) dao.findForList("AttendanceSignTimeMapper.getAttendanceSignTimeList", pd);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     * 明细
     * @return
     * @throws GenException
     */
    public PageData getAttendanceSignTimeById(String ID) throws GenException {
        try {
            return (PageData) dao.findForObject("AttendanceSignTimeMapper.getAttendanceSignTimeById", ID);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     * 新增
     * @return
     * @throws GenException
     */
    public int insertAttendanceSignTime(PageData pd) throws GenException {
        try {
            return (int) dao.save("AttendanceSignTimeMapper.insertAttendanceSignTime", pd);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     * 编辑
     * @return
     * @throws GenException
     */
    public int updateAttendanceSignTime(PageData pd) throws GenException {
        try {
            return (int) dao.update("AttendanceSignTimeMapper.updateAttendanceSignTime", pd);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     * 删除
     * @return
     * @throws GenException
     */
    public int deleteAttendanceSignTime(PageData pd) throws GenException {
        try {
            return (int) dao.update("AttendanceSignTimeMapper.deleteAttendanceSignTime", pd);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }
}
