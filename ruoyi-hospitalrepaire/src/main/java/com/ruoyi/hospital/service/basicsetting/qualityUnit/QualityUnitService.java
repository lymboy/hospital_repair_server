package com.ruoyi.hospital.service.basicsetting.qualityUnit;

import java.io.File;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Service;

import com.ruoyi.hospital.dao.DaoSupport;
import com.ruoyi.hospital.entity.Page;
import com.ruoyi.hospital.framework.exception.GenException;
import com.ruoyi.hospital.util.Const;
import com.ruoyi.hospital.util.PageData;
import com.ruoyi.hospital.util.UuidUtil;

@Service(value="qualityUnitService")
public class QualityUnitService {

	@Resource(name="daoSupport")
	private DaoSupport dao;

	/**
	 * 分页查询质量单元列表
	 * @throws GenException
	 */
	@SuppressWarnings("unchecked")
	public List<PageData> getQualityUnitListPage(Page page) throws GenException{
		List<PageData> qualityUnit = null;
		try{
			qualityUnit = (List<PageData>) dao.findForList("QualityUnitMapper.getQualityUnitlistPage", page);
		} catch (Exception e){
			throw new GenException(e.getCause());
		}
		return qualityUnit;
	}

	//通过质量单元id查询基本信息
	public PageData OpenQualityUnitEdit(PageData pd) throws GenException {
		PageData qualityUnitInfo=null;
		try {
			qualityUnitInfo=(PageData) dao.findForObject("QualityUnitMapper.OpenQualityUnitEdit", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return qualityUnitInfo;
	}

	//得到质量单元的条数
	public int getBillNum() throws GenException {
		int billCodeNum = 0;
		try {
			String billCodeStr=(String) dao.findForObject("QualityUnitMapper.getMaxBillCode", null);
			if (billCodeStr == null || "".equals(billCodeStr)) {
				return 0;
			}
			try {
				String[] billCodeCharArray = billCodeStr.split("-");
				Integer maxNum = Integer.valueOf(billCodeCharArray[billCodeCharArray.length-1]);
				if (maxNum != null) {
					billCodeNum = maxNum;
				}
			} catch (Exception e) {
				billCodeNum = 0;
			}
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return billCodeNum;
	}

	//得到员工的基本信息
	public PageData getUserInfoById(PageData pd) throws GenException {
		PageData userInfo=null;
		try {
			userInfo=(PageData) dao.findForObject("QualityUnitMapper.getUserInfoById", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return userInfo;
	}

	//质量单元添加
	public void QualityUnitInfoAdd(PageData pd) throws GenException {
		try {
			dao.save("QualityUnitMapper.QualityUnitInfoAdd", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}

	}

	//质量单元编辑更新
	public void QualityUnitInfoUpdate(PageData pd) throws GenException {
		try {
			dao.update("QualityUnitMapper.QualityUnitInfoUpdate", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}

	}

	//质量单元删除
	public int deleteQualityUnit(PageData pd) throws GenException {
		int num=0;
		try {
			num=(Integer) dao.update("QualityUnitMapper.deleteQualityUnit", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return num;
	}



	//得到质量单元从表的基本信息
	public List<PageData> getQualityUnitDetailList(PageData pd) throws GenException {
		List<PageData> pds=null;
		try {
			pds=(List<PageData>) dao.findForList("QualityUnitMapper.getQualityUnitDetailList",pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pds;
	}


	/**
	 * 新增质量单元
	 * @param pd
	 * @throws GenException
	 */

	public int addQualityUnitDetail(PageData pd) throws GenException {
		int num = 0;
		try {
			num = (Integer) dao.save("QualityUnitMapper.addQualityUnitDetail", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return num;
	}

		/**
		 *  获取质量单元的下级个数 BY PARENT_UNIT_ID
		 * @return
		 * @throws GenException
		 */
		public int getCountByParentId(PageData pd) throws GenException {
			 int num=0;
			try {
				num = (Integer)dao.findForObject("QualityUnitMapper.getCountByParentId", pd);
			} catch (Exception e) {
				throw new GenException(e.getCause());
			}
			return num;
		}





		/**
		 * 删除质量单元从表里的质量单元
		 * @param pd
		 * @return
		 * @throws GenException
		 */
		public int deleteQualityUnitDetail(PageData pd) throws GenException {
			int num = 0;
			try {
				num = (Integer) dao.delete("QualityUnitMapper.deleteQualityUnit", pd);
			} catch (Exception e) {
				throw new GenException(e.getCause());
			}
			return num;
		}
		/**
		 * 删除质量单元从表里的质量单元
		 * @param pd
		 * @return
		 * @throws GenException
		 */
		public int deleteAllQualityUnitDetail(PageData pd) throws GenException {
			int num = 0;
			try {
				num = (Integer) dao.delete("QualityUnitMapper.deleteAllQualityUnitDetail", pd);
			} catch (Exception e) {
				throw new GenException(e.getCause());
			}
			return num;
		}

		//修订
		public int updateQualityUnitInfo(PageData pd, String id) throws GenException {
			int num = 0;
			try {
				// 获取 修订前  上一条记录的数据
				PageData pdInfo = (PageData)dao.findForObject("QualityUnitMapper.OpenQualityUnitEdit", pd);

				// 更新信息 增加一个版本
				String versionNoStr = pdInfo.getString("VERSION_NO");
				int versionNo = Integer.valueOf(versionNoStr) + 1;
				pdInfo.put("VERSION_NO", versionNo);
				pdInfo.put("CREATEDATE", new Date());
				pdInfo.put("CREATER", pd.getString("creater"));
				pdInfo.put("MODIFYDATE", new Date());
				pdInfo.put("MODIFIER", pd.getString("modifier"));
				pdInfo.put("QUALITY_ID", id);
				pdInfo.put("EDITOR", pd.getString("creater"));
				pdInfo.put("EDIT_DATE", new Date());
				dao.update("QualityUnitMapper.insertQualityUnitInfo", pdInfo); // 新版本

				// 从表 复制 第一级数据
				PageData pdde = new PageData();
				pdde.put("qualityId", pd.getString("qualityId"));
				List<PageData> firstList = (List<PageData>) dao.findForList("QualityUnitMapper.getFirstQualityUnitDetailListForCopy", pdde);
				if (firstList != null && firstList.size() > 0) {
					for (PageData first : firstList) {
						pd.put("QUALITY_ID", pd.getString("qualityId"));
						pd.put("FIRST_UNIT_CODE", first.get("UNIT_CODE"));
						List<PageData> pdcList = (List<PageData>) dao.findForList("QualityUnitMapper.getQualityUnitDetailListForCopy", pd);
						//String[] parentIdStr = {"0", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""};
						String[] parentIdStr = new String[200];
						parentIdStr[0] = "0";
						for (PageData pdc : pdcList) {
							String newDetailId =  UuidUtil.get36UUID();
							String baseIdStr =  pdc.getString("BASE_ID");
							if(baseIdStr!=null){
								String[] baseIdArray = baseIdStr.split("-");
								parentIdStr[baseIdArray.length]=newDetailId;
								pdc.put("PARENT_UNIT_ID", parentIdStr[baseIdArray.length-1]);
							}
							pdc.put("ID", newDetailId);
							pdc.put("QUALITY_ID", id);
							pdc.put("CREATEDATE", new Date());
							pdc.put("CREATER", pd.getString("creater"));
							pdc.put("MODIFYDATE", new Date());
							pdc.put("MODIFIER", pd.getString("modifier"));
							dao.save("QualityUnitMapper.copyQualityUnitDetail", pdc);
						}
					}
				}

				// 附件 复制
				pd.put("masterId", pd.getString("qualityId"));
				List<PageData> pdtList = (List<PageData>) dao.findForList("QualityUnitMapper.getQualityUnitDetailAttachmentList", pd);
				for (PageData pdt : pdtList) {
					String oldId = pdt.getString("ID");
					String newId = UuidUtil.get36UUID();
					String oldMasterId = pdt.getString("MASTER_ID");

					pdt.put("ID", newId);
					pdt.put("MASTER_ID", id);
					pdt.put("CREATEDATE", new Date());
					pdt.put("CREATER", pd.getString("creater"));
					pdt.put("MODIFYDATE", new Date());
					pdt.put("MODIFIER", pd.getString("modifier"));
					dao.save("QualityUnitMapper.copyQualityUnitDetailAttachment", pdt);

					// 文件 复制
					// 得到要下载的文件信息
					String fileUrl = pdt.getString("SUB_SYSTEM")+File.separator+ pdt.getString("MODULE_NAME");
					String fileName = pdt.getString("FILE_NAME");
					// 找出文件
					String path = Const.FILE_UPLOAD_ROOT_URL+File.separator+ "uploadFiles/attachment/";
					String filePath = path+File.separator+fileUrl+File.separator+oldMasterId+File.separator+oldId+fileName.substring(fileName.lastIndexOf("."));
					String filePathNew = path+File.separator+fileUrl+File.separator+id+File.separator+newId+fileName.substring(fileName.lastIndexOf("."));
					File file = new File(filePath);
					File fileNew = new File(filePathNew);
					if(file.exists()){
						File folder = new File(path+File.separator+fileUrl+File.separator+id);
						if(!folder.exists()) {
							folder.mkdirs();
						}
						if(!fileNew.exists()){
							FileUtils.copyFile(file, fileNew);
						}
					}
				}

				// 修订 原来的版本
				pd.put("qualityUnitId", pd.getString("qualityId"));
				dao.update("QualityUnitMapper.deleteQualityUnit", pd);
			} catch (Exception e) {
				throw new GenException(e.getCause());
			}
			return num;
		}
		//快审
		public int updateFastTrialById(PageData pd) throws GenException {
			int num = 0;
			try {
				num = (Integer) dao.update("QualityUnitMapper.updateFastTrialById", pd);
			} catch (Exception e) {
				throw new GenException(e.getCause());
			}
			return num;
		}


		/** *************************************   从表 历史 查询 ***********************************/
		/**
		 * 获取清单库列表
		 * @param page
		 * @return
		 * @throws GenException
		 */
		public List<PageData> getAllQualityUnitHistoryList(Page page) throws GenException {
			List<PageData> pds = null;
			try {
				pds = (List<PageData>) dao.findForList("QualityUnitMapper.getAllQualityUnitHistoryList", page);
			} catch (Exception e) {
				throw new GenException(e.getCause());
			}
			return pds;
		}

		/**
		 * 获取质量单元级别 BY QUALITY_ID
		 * @param qualityUnitId
		 * @return
		 * @throws GenException
		 */
		public String getQualityUnitLevel(String qualityUnitId) throws GenException{
			String qualityUnitLevel = "";
			try {
				qualityUnitLevel = (String) dao.findForObject("QualityUnitMapper.getQualityUnitLevel", qualityUnitId);
			} catch (Exception e) {
				throw new GenException(e.getCause());
			}

			return qualityUnitLevel;
		}


		/**
		 * 质量单元详细的编辑保存
		 * @param pd
		 * @return
		 * @throws GenException
		 */
		public int editQualityUnitDetailInfo(PageData pd) throws GenException{
			int num = 0;
			try {
				num = (Integer)dao.update("QualityUnitMapper.editQualityUnitDetailInfo", pd);
			} catch (Exception e) {
				throw new GenException(e.getCause());
			}
			return num;
		}
		/**
		 * 获取质量单元树
		 * @param pd
		 * @return
		 * @throws GenException
		 */
		public List<PageData> getQualityUnitTree(PageData pd) throws GenException {
			List<PageData> pdList = null;
			try {
				pdList = (List<PageData>) dao.findForList("QualityUnitMapper.getQualityUnitTree", pd);
			} catch (Exception e) {
				throw new GenException(e.getCause());
			}
			return pdList;
		}

		//获取质量单元条数
		public int getQualityUnitDetailNum() throws GenException {
			int num=0;
			try {
				num=(int) dao.findForObject("QualityUnitMapper.getQualityUnitDetailNum", null);
			} catch (Exception e) {
				throw new GenException(e.getCause());
			}
			return num;
		}


	/**
	 * 获取已经添加过的工程类型ID列表
	 * @return
	 * @throws GenException
	 */
    public List<String> getProjectTypeHasNotExist() throws GenException {
		try {
			return (List<String>) dao.findForList("QualityUnitMapper.getProjectTypeHasNotExist", null);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
    }
}
