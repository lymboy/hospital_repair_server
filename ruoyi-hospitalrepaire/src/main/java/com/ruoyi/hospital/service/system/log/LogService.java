package com.ruoyi.hospital.service.system.log;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.ruoyi.hospital.dao.DaoSupport;
import com.ruoyi.hospital.framework.exception.GenException;
import com.ruoyi.hospital.util.PageData;

@Service("logService")
public class LogService {

	@Resource(name = "daoSupport")
	private DaoSupport dao;

	public void addNewLog(PageData pd) throws GenException{
		try {
			dao.save("LogMapper.addNewLog", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}


}
