package com.ruoyi.hospital.service.basicsetting.projectSituation;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.ruoyi.hospital.dao.DaoSupport;
import com.ruoyi.hospital.entity.Page;
import com.ruoyi.hospital.framework.exception.GenException;
import com.ruoyi.hospital.framework.log.Log;
import com.ruoyi.hospital.util.PageData;

@Service
@Resource(name="projectSituationService")
public class ProjectSituationService
{
	@Resource(name="daoSupport")
	private DaoSupport dao;

	/**
	 * 分页查询项目列表
	 * @throws GenException
	 */
	@SuppressWarnings("unchecked")
	public List<PageData> getProjectListPage(Page page) throws GenException
	{
		List<PageData> pds = null;
		try
		{
			pds = (List<PageData>) dao.findForList("projectSituationMapper.getProjectlistPage", page);
		} catch (Exception e)
		{
			throw new GenException(e.getCause());
		}

		return pds;
	}

	/**
	 * 查询项目列表(无分页)
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public List<PageData> getProjectList(PageData pd) throws GenException
	{
		List<PageData> pds = null;
		try
		{
			pds = (List<PageData>) dao.findForList("projectSituationMapper.getProjectlist", pd);
		} catch (Exception e)
		{
			throw new GenException(e.getCause());
		}

		return pds;
	}



	/**
	 * 根据项目id查询
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public PageData getProjectById(PageData pd) throws GenException
	{
		PageData project = null;
		try
		{
			project = (PageData) dao.findForObject("projectSituationMapper.getProjectById", pd);
		} catch (Exception e)
		{
			throw new GenException(e.getCause());
		}
		return project;
	}

	public PageData getProject(PageData pd) throws GenException
	{
		PageData project = null;
		try
		{
			project = (PageData) dao.findForObject("projectSituationMapper.getProject", pd);
		} catch (Exception e)
		{
			throw new GenException(e.getCause());
		}
		return project;
	}

	/**
	 * 新增项目
	 * @throws GenException
	 */
	public void addProject(PageData pd) throws GenException
	{
		try
		{
			dao.save("projectSituationMapper.addProject", pd);
		} catch (Exception e)
		{
			throw new GenException(e.getCause());
		}
	}


	/**
	 * 删除一个项目
	 * @throws GenException
	 */
	public void deleteProject(PageData pd) throws GenException
	{
		try
		{
			dao.update("projectSituationMapper.deleteProject", pd);
		} catch (Exception e)
		{
			throw new GenException(e.getCause());
		}
	}

	/**
	 * 编辑一个项目
	 * @param pd
	 * @throws GenException
	 */
	public void editProject(PageData pd) throws GenException
	{
		try
		{
			dao.update("projectSituationMapper.editProject", pd);
		} catch (Exception e)
		{
			throw new GenException(e.getCause());
		}
	}

	/**
	 * 查询机构图从表(无分页)
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public List<PageData> getJGTList(PageData pd) throws GenException
	{
		List<PageData> pds = null;
		try
		{
			pds = (List<PageData>) dao.findForList("projectSituationMapper.getJGTList", pd);
		} catch (Exception e)
		{
			throw new GenException(e.getCause());
		}

		return pds;
	}

	/**
	 * 查询首件工程验收计划从表(无分页)
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public List<PageData> getGCYSList(PageData pd) throws GenException
	{
		List<PageData> pds = null;
		try
		{
			pds = (List<PageData>) dao.findForList("projectSituationMapper.getGCYSList", pd);
		} catch (Exception e)
		{
			throw new GenException(e.getCause());
		}

		return pds;
	}

	/**
	 * 新增机构图文件
	 * @throws GenException
	 */
	public void addFileInfo(PageData pd) throws GenException
	{
		try
		{
			dao.save("projectSituationMapper.addFileInfo", pd);
		} catch (Exception e)
		{
			throw new GenException(e.getCause());
		}
	}

	/**
	 * 新增首件工程验收计划从表文件
	 * @throws GenException
	 */
	public void addFileInfo_GCYS(PageData pd) throws GenException
	{
		try
		{
			dao.save("projectSituationMapper.addFileInfo_GCYS", pd);
		} catch (Exception e)
		{
			throw new GenException(e.getCause());
		}
	}

	/**
	 * 获取文件 记录信息
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public PageData getFileById(PageData pd) throws GenException {
		PageData pdInfo = null;
		try {
			pdInfo = (PageData) dao.findForObject("projectSituationMapper.getFileInfoById", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pdInfo;
	}

	/**
	* @Description:  删除机构图明细从表
	 */
	@Log(name="删除机构图明细从表信息-deleteJGT")
	public int deleteJGT(PageData pd) throws GenException {
		int num = 0;
		try {
			num = (Integer) dao.delete("projectSituationMapper.deleteJGT", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return num;
	}

	/**
	 *
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	@Log(name="更新修改的机构图明细从表-updateJGT")
	public int updateJGT(PageData pd) throws GenException {
		int num=0;
		try {
			num = (Integer) dao.update("projectSituationMapper.updateJGT", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return num;
	}

	public List<PageData> projectListPage(Page page) throws GenException {
		List<PageData> projectList=null;
		try {
			projectList=(List<PageData>) dao.findForList("projectSituationMapper.ProjectlistPage", page);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return projectList;
	}

	// 获取文件的路径
	public List<PageData> getFilePath(PageData pd) throws GenException {
		List<PageData> pdInfo = null;
		try {
			pdInfo = (List<PageData>)dao.findForList("projectSituationMapper.getFilePath", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pdInfo;
	}

}
