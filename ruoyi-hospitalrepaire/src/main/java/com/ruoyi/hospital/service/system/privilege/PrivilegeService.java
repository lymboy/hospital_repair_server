package com.ruoyi.hospital.service.system.privilege;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.ruoyi.hospital.dao.DaoSupport;
import com.ruoyi.hospital.entity.Page;
import com.ruoyi.hospital.framework.exception.GenException;
import com.ruoyi.hospital.framework.log.Log;
import com.ruoyi.hospital.util.PageData;

/**
 * <p>Title: PrivilegeService</p>
 * @Desc:
 * @author: 杨建军
 *	@Date: 2017年3月8日
 * @Time: 上午11:49:10
 * @version:
 */
@Service("privilegeService")
public class PrivilegeService {

	@Resource(name = "daoSupport")
	private DaoSupport dao;

	public List<PageData> getPrivilegeByRoleId(PageData pd)throws GenException {
		List<PageData> pds = null;
		try{
			pds = (List<PageData>)dao.findForList("PrivilegeMapper.getPrivilegeByRoleId", pd);
		}catch(Exception e){
			throw new GenException(e.getCause());
		}
		return pds;
	}

	/*
	 * 删除该角色所有权限
	 */
	@Log(name="删除角色所有权限")
	public void deleteRPByRole(PageData pd) throws GenException{
		try {
			dao.delete("PrivilegeMapper.deleteRPByRole", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}

	/*
	 * 通过roleid获取权限角色对应信息
	 */
	public List<PageData> listAllRolePrivileges(PageData pd) throws GenException{
		List<PageData> pds = null;
		try {
			pds = (List<PageData>)dao.findForList("PrivilegeMapper.listAllRolePrivileges", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pds;
	}

	/*
	 * 获取所有权限
	 */
	public List<PageData> listAllPrivileges() throws GenException {
		List<PageData> pds = null;
		try {
			pds = (List<PageData>)dao.findForList("PrivilegeMapper.listAllPrivileges", "");
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pds;
	}

	/*
	 * 添加之前角色权限表中没有的字段
	 */
	@Log(name="添加之前角色权限表中没有的字段")
	public void insertRolePrivilege(PageData pd) throws GenException {
		try {
			dao.save("PrivilegeMapper.insertRolePrivilege", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}

	/*
	 * 删除角色权限表记录通过角色id和权限id
	 */
	@Log(name="删除角色权限表记录")
	public void deleteRPByRPId(PageData pd) throws GenException {
		try {
			dao.delete("PrivilegeMapper.deleteRPByRPId", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}

	public Integer getAllCount() throws GenException{
		Integer num = 0;
		try {
			num = (Integer)dao.findForObject("PrivilegeMapper.getAllCount", "");
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return num;
	}

	/**
	 * 添加新权限
	 * @param pd
	 * @throws GenException
	 */
	public void addNewPrivilege(PageData pd) throws GenException {
		try {
			dao.save("PrivilegeMapper.addNewPrivilege", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}

	/**
	 * 通过权限id获取权限信息
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public PageData findPrivilegeByPrivilegeId(PageData pd) throws GenException {
		PageData pda = new PageData();
		try {
			pda = (PageData)dao.findForObject("PrivilegeMapper.findPrivilegeByPrivilegeId", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pda;
	}

	/**
	 * 修改权限
	 * @param pd
	 * @throws GenException
	 */
	@Log(name = "修改权限")
	public void editPrivilege(PageData pd) throws GenException {
		try {
			dao.update("PrivilegeMapper.editPrivilege", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}

	/**
	 * 删除权限
	 * @param pd
	 * @throws GenException
	 */
	@Log(name = "删除权限")
	public void deletePrivilege(PageData pd) throws GenException {
		try {
			dao.delete("PrivilegeMapper.deletePrivilege", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}

	/**
	 * 删除拥有该权限的角色权限表
	 * @param pd
	 * @throws GenException
	 */
	@Log(name = "删除拥有该权限的角色权限表")
	public void deleteRPByPrivilege(PageData pd) throws GenException {
		try {
			dao.delete("PrivilegeMapper.deleteRPByPrivilege", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}

	/**
	 * 根据角色id和权限id删除权限角色表中的记录
	 * @param pd
	 * @throws GenException
	 */
	@Log(name = "根据角色id和权限id删除权限角色表中的记录")
	public void deletePrivilegeByRoleId(PageData pd) throws GenException {
		try {
			dao.delete("PrivilegeMapper.deletePrivilegeByRoleId", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}

	/**
	 * 批量删除权限
	 * @param pd
	 * @throws GenException
	 */
	@Log(name = "批量删除权限")
	public void deleteAllPrivilege(Map<String, Object> map) throws GenException {
		try {
			dao.delete("PrivilegeMapper.deleteAllPrivilege", map);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}

	/**
	 * 获取所有拥有该菜单的权限
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public List<PageData> getPrivilegeListByMenuId(PageData pd) throws GenException {
		List<PageData> pds = null;
		try {
			pds = (List<PageData>) dao.findForList("PrivilegeMapper.getPrivilegeListByMenuId", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pds;
	}

	/**
	 * 修改权限
	 * @param pd
	 * @throws GenException
	 */
	@Log(name = "修改权限")
	public void updatePrivilege(PageData pd) throws GenException {
		try {
			dao.update("PrivilegeMapper.editPrivilege", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}

	/**
	 * 分页获取全部权限
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public List<PageData> getAllPrivilege(Page page) throws GenException {
		List<PageData> pds = null;
		try {
			pds = (List<PageData>) dao.findForList("PrivilegeMapper.getAllPrivilegelistPage", page);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pds;
	}
}
