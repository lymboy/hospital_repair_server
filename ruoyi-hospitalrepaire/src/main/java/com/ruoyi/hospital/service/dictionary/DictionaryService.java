package com.ruoyi.hospital.service.dictionary;

import com.ruoyi.hospital.dao.DaoSupport;
import com.ruoyi.hospital.entity.Page;
import com.ruoyi.hospital.framework.exception.GenException;
import com.ruoyi.hospital.util.PageData;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;




@Service("dictionaryService")
public class DictionaryService
{
	@Resource(name = "daoSupport")
	private DaoSupport dao;

	public List<PageData> getGroupListPage(Page page) {
		List<PageData> dutyList = null;
		try {
			dutyList = (List<PageData>)this.dao.findForList("DictionaryMapper.getGroupListPage", page);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dutyList;
	}

	public PageData getGroupQuestionById(PageData pd) {
		PageData Group = null;
		try {
			Group = (PageData)this.dao.findForObject("DictionaryMapper.getGroupQuestionById", pd);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Group;
	}

	public void editOwnerInformation(PageData pd) {
		try {
			this.dao.update("DictionaryMapper.editOwnerInformation", pd);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void addOwnerInformation(PageData pd) {
		try {
			this.dao.save("DictionaryMapper.addOwnerInformation", pd);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	public void deleteGroup(PageData pd) {
		try {
			this.dao.delete("DictionaryMapper.deleteGroup", pd);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public PageData getQuestionDictionaryById(PageData pd) {
		PageData Group = null;
		try {
			Group = (PageData)this.dao.findForObject("DictionaryMapper.getQuestionDictionaryById", pd);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Group;
	}

	public List<PageData> getQuestionDictionaryList(PageData pd) {
		List<PageData> dutyList = null;
		try {
			dutyList = (List<PageData>)this.dao.findForList("DictionaryMapper.getQuestionDictionaryList", pd);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dutyList;
	}

	public void addLowerLevelQuestionType(PageData pd) {
		try {
			this.dao.save("DictionaryMapper.addLowerLevelQuestionType", pd);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	public void deleteQuestionType(PageData pd) {
		try {
			this.dao.delete("DictionaryMapper.deleteQuestionType", pd);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	public void deleteChildQuestionType(PageData pd) throws GenException {
		List<PageData> childDepartmentList = null;
		try {
			childDepartmentList = findAllDepartmentById(pd);
			if (childDepartmentList != null && childDepartmentList.size() != 0) {
				for (PageData list : childDepartmentList) {
					pd.put("departmentId", list.getString("ID"));
					deleteChildQuestionType(pd);
					pd.put("departmentId", list.getString("ID"));

					this.dao.update("DictionaryMapper.deleteQuestionType", pd);
				}
			} else {
				return;
			}
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}


	private List<PageData> findAllDepartmentById(PageData pd) throws GenException {
		List<PageData> childDepartmentList = null;
		try {
			childDepartmentList = (List<PageData>)this.dao.findForList("DictionaryMapper.childQuestionTypeList", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return childDepartmentList;
	}

	public void saveQuestionType(PageData pd) {
		try {
			this.dao.update("DictionaryMapper.saveQuestionType", pd);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public int addHosDuty(PageData pd) {
		int i = 0;
		try {
			i = (int) this.dao.update("DictionaryMapper.addHosDuty", pd);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return i;
	}

	public PageData findByNum(PageData pd) throws GenException {
		PageData pda = new PageData();
		try {
			pda = (PageData)dao.findForObject("DictionaryMapper.findByNum", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pda;
	}

	public int updateHosDuty(PageData pd) {
		int i = 0;
		try {
			i = (int) this.dao.update("DictionaryMapper.updateHosDuty", pd);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return i;
	}

	public List<PageData> selectHosDuty(PageData pd) throws GenException {
		List<PageData> childDepartmentList = null;
		try {
			childDepartmentList = (List<PageData>)this.dao.findForList("DictionaryMapper.selectHosDuty", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return childDepartmentList;
	}
}
