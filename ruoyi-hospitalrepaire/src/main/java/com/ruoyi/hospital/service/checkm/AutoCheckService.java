package com.ruoyi.hospital.service.checkm;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.ruoyi.hospital.dao.DaoSupport;
import com.ruoyi.hospital.entity.Page;
import com.ruoyi.hospital.framework.exception.GenException;
import com.ruoyi.hospital.framework.log.Log;
import com.ruoyi.hospital.util.PageData;

@Service("autoCheckService")
public class AutoCheckService {

	@Resource(name = "daoSupport")
	private DaoSupport dao;

	@SuppressWarnings("unchecked")
	@Log(name="得到全部项目完工自检列表")
	public List<PageData> getAllQualityAutocheckList(Page page) throws GenException {
		List<PageData> qualityAutocheckList=null;
		try {
			qualityAutocheckList=(List<PageData>) dao.findForList("AutoCheckMapper.getAllQualityAutochecklistPagePrivilegeFilter", page);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	return qualityAutocheckList;
	}

	//通过工序id查询基本信息
	public PageData getAutoCheckInfo(PageData pd) throws GenException {
		PageData autoCheckInfo=null;
		try {
			autoCheckInfo=(PageData) dao.findForObject("AutoCheckMapper.getAutocheckInfo", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return autoCheckInfo;
	}

	//获取存在的记录总数
	public int getBillNum() throws GenException {
		int autoCheckNum = 0;
		try {
			autoCheckNum=(int) dao.findForObject("AutoCheckMapper.getBillNum", null);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return autoCheckNum;
	}

	public PageData getUserInfoById(PageData pd) throws GenException {
		PageData userInfo=null;
		try {
			userInfo=(PageData) dao.findForObject("AutoCheckMapper.getUserInfoById", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return userInfo;
	}

	//更新数据信息
	public void AutoCheckInfoUpdate(PageData pd) throws GenException {
		try {
			dao.update("AutoCheckMapper.updateAutoCheckInfo", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}

	//添加数据信息
	public void AutoCheckInfoAdd(PageData pd) throws GenException {
		try {
			dao.save("AutoCheckMapper.addAutoCheckInfo", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}

	public void deleteAutoCheck(PageData pd) throws GenException {
		try {
			dao.delete("AutoCheckMapper.deleteAutoCheck", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}

	}

	public void approveAutoCheck(PageData pd) throws GenException {
		try {
			dao.update("AutoCheckMapper.approveAutoCheck", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}

	}

	@SuppressWarnings("unchecked")
	public List<PageData> getProjectQualityDetailList(PageData pd) throws GenException {
		List<PageData> pds = null;
		try {
			pds = (List<PageData>)dao.findForList("AutoCheckMapper.getProjectQualityDetailList", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pds;
	}

	//上传新文件
	public int addFileInfo(PageData pd) throws GenException {
		int num=0;
		try {
			num = (Integer) dao.save("AutoCheckMapper.addFileInfo", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return num;
	}

	@SuppressWarnings("unchecked")
	public List<PageData> getFileListPage(Page page) throws GenException{
		List<PageData> fileList=null;
		try{
			fileList=(List<PageData>) dao.findForList("AutoCheckMapper.getFileListPage", page);
		}catch(Exception e){
			throw new GenException(e.getCause());
		}
		return fileList;
	}

	public void updateFileInfo(PageData pd) throws GenException {
		try {
			dao.update("AutoCheckMapper.updateFileInfo", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}

	/**
	 * 删除文件 通过 主键
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	@Log(name="删除文件 通过 主键-deleteFileById")
	public int deleteFileById(PageData pd) throws GenException {
		int num=0;
		try {
			num = (Integer) dao.update("AutoCheckMapper.deleteFileById", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return num;
	}

	public void updateFileRemark(PageData pd) throws GenException {
		try {
			dao.update("AutoCheckMapper.UpdateFileRemark", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}

	}

	/**
	 * 获取文件 记录信息
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public PageData getFileById(PageData pd) throws GenException {
		PageData pdInfo;
		try {
			pdInfo = (PageData) dao.findForObject("AutoCheckMapper.getFileInfoById", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pdInfo;
	}

	public PageData getAutoCheckStatus(PageData pd) throws GenException {
		PageData status = null;
		try {
			status = (PageData) dao.findForObject("AutoCheckMapper.getAutoCheckStatus", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return status;
	}

	public List<PageData> projectListPage(Page page) throws GenException {
		List<PageData> projectList=null;
		try {
			projectList=(List<PageData>) dao.findForList("AutoCheckMapper.ProjectlistPage", page);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return projectList;
	}

}
