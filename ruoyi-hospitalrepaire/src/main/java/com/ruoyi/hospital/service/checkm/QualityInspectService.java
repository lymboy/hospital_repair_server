package com.ruoyi.hospital.service.checkm;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import com.ruoyi.hospital.util.UuidUtil;
import org.springframework.stereotype.Service;

import com.ruoyi.hospital.dao.DaoSupport;
import com.ruoyi.hospital.entity.Page;
import com.ruoyi.hospital.framework.exception.GenException;
import com.ruoyi.hospital.framework.log.Log;
import com.ruoyi.hospital.util.PageData;
/**
 * 	工序质量验收
 * @author 吴同生
 * */
@Service("qualityInspectService")
public class QualityInspectService {

	@Resource(name = "daoSupport")
	private DaoSupport dao;

	@Log(name="得到全部工序质量验收列表")
	public List<PageData> getAllQualityInspectList(Page page) throws GenException {
		List<PageData> qualityInspectList=null;
		try {
			qualityInspectList=(List<PageData>) dao.findForList("QualityInspectMapper.getAllQualityInspectlistPagePrivilegeFilter", page);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return qualityInspectList;
	}


	//通过工序id查询基本信息
	public PageData getQualityInspectInfo(PageData pd) throws GenException {
		PageData qualityInspectInfo=null;
		try {
			qualityInspectInfo=(PageData) dao.findForObject("QualityInspectMapper.getQualityInspectInfo", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return qualityInspectInfo;
	}

	public List<PageData> getQualityInspectFileList(PageData pd) throws GenException{
		List<PageData> qualityInspectFileList=null;
		try {
			qualityInspectFileList=(List<PageData>) dao.findForObject("QualityInspectMapper.getQualityInspectInfo", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return qualityInspectFileList;
	}

	//上传新文件
	public int addFileInfo(PageData pd) throws GenException {
		int num=0;
		try {
			num = (Integer) dao.save("QualityInspectMapper.addFileInfo", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return num;
	}


	public List<PageData> getFileListPage(Page page) throws GenException{
		List<PageData> fileList=null;
		try{
			fileList=(List<PageData>) dao.findForList("QualityInspectMapper.getFileListPage", page);
		}catch(Exception e){
			throw new GenException(e.getCause());
		}
		return fileList;
	}

	/**
	 * 删除文件 通过 主键
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	@Log(name="删除文件 通过 主键-deleteFileById")
	public int deleteFileById(PageData pd) throws GenException {
		int num=0;
		try {
			num = (Integer) dao.update("QualityInspectMapper.deleteFileById", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return num;
	}

	/**
	 * 获取文件 记录信息
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public PageData getFileById(PageData pd) throws GenException {
		PageData pdInfo;
		try {
			pdInfo = (PageData) dao.findForObject("QualityInspectMapper.getFileInfoById", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pdInfo;
	}

	public void deleteQualityInspect(PageData pd) throws GenException {
		try {
			dao.delete("QualityInspectMapper.deleteQualityInspect", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}

	}

	public int getBillNum() throws GenException {
		int qualityInspectNum = 0;
		try {
			qualityInspectNum=(int) dao.findForObject("QualityInspectMapper.getBillNum", null);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return qualityInspectNum;
	}

	// 生成 单据 编号 最后数字
	public int getMaxQualityInspectBillCodeSuffix(PageData pd) throws GenException {
		int billCodeNum = 0;
		try {
			String billCodeStr=(String) dao.findForObject("QualityInspectMapper.getMaxQualityInspectBillCodeSuffix", pd);
			if (billCodeStr == null || "".equals(billCodeStr)) {
				return 0;
			}
			try {
				String[] billCodeCharArray = billCodeStr.split("-");
				Integer maxNum = Integer.valueOf(billCodeCharArray[billCodeCharArray.length-1]);
				if (maxNum != null) {
					billCodeNum = maxNum;
				}
			} catch (Exception e) {
				billCodeNum = 0;
			}
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return billCodeNum;
	}

	public PageData getUserInfoById(PageData pd) throws GenException {
		PageData userInfo=null;
		try {
			userInfo=(PageData) dao.findForObject("QualityInspectMapper.getUserInfoById", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return userInfo;
	}

	public void QualityInspectInfoUpdate(PageData pd) throws GenException {
		try {
			dao.update("QualityInspectMapper.UpdateQualityInspectInfo", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}

	}

	/**
	 * 新增 或 编辑
	 * @param pd
	 * @param userId
	 * @return
	 * @throws GenException
	 */
	public int insertOrUpdateQualityInspect(PageData pd, String userId) throws GenException {
		try {
			pd.put("qualityInspectId", pd.getString("ID"));
			PageData pdInfo = this.getQualityInspectInfo(pd);
			if(pdInfo != null) {
				pd.put("MODIFIER", userId);
				pd.put("MODIFYDATE", new Date());
				return (int) dao.update("QualityInspectMapper.updateQualityInspect", pd); // 编辑
			}
			pd.put("CREATER", userId);
			pd.put("CREATEDATE", new Date());
			return (int) dao.save("QualityInspectMapper.insertQualityInspect", pd); // 添加
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}

	public void QualityInspectInfoAdd(PageData pd) throws GenException {
		try {
			dao.save("QualityInspectMapper.AddQualityInspectInfo", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}

	public void updateFileRemark(PageData pd) throws GenException {
		try {
			dao.update("QualityInspectMapper.UpdateFileRemark", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}

	}

	@SuppressWarnings("unchecked")
	public List<PageData> getOpinionsByCondition(PageData pd) throws GenException {
		List<PageData> pds = null;
		try {
			pds = (List<PageData>)dao.findForList("QualityInspectMapper.getOpinionsByCondition", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pds;
	}

	public PageData getDepartmentInfo(String departmentId) throws GenException{
		PageData pd = null;
		try {
			pd = (PageData) dao.findForObject("QualityInspectMapper.getDepartmentInfo", departmentId);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pd;
	}

	public PageData getNewDepartmentInfo(String baseId) throws GenException{
		PageData pd = null;
		try {
			pd = (PageData) dao.findForObject("QualityInspectMapper.getNewDepartmentInfo", baseId);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pd;
	}

	public List<PageData> getProjectQualityDetailList(PageData pd) throws GenException {
		try {

			List<PageData> dataList = new ArrayList<>();
			List<PageData> pds = (List<PageData>)dao.findForList("QualityInspectMapper.getProjectQualityDetailList", pd);

			if (pds == null || pds.size() == 0) {
				return dataList;
			}
			// 判断是否为工序级别
			boolean processFlag = false;
			for (int i = 0; i < pds.size(); i++) {
				PageData pdInfo = pds.get(i);
				String unitType = String.valueOf(pdInfo.get("UNIT_TYPE"));
				if ("51".equals(unitType)) {
					processFlag = true;
					break;
				}
			}

			if (processFlag) { // 是工序级别
				// 查询 下级 工序列表 及 工序 使用状态
				PageData params = new PageData();
				params.put("checkTypeFilter", pd.get("checkTypeFilter"));
				params.put("PARENT_UNIT_ID", pd.get("nodeid"));
				params.put("TECHNICIAN", pd.get("technician"));
				//查询已检验过的工序
				List<PageData> processList = (List<PageData>) dao.findForList("QualityInspectMapper.getLastLevelProcessList", params);
				for (int i = 0; i < processList.size(); i++) {
					processList.get(i).put("processMark", "2");
				}
				//查询未检验过的工序
				List<PageData> uncheckedProcessList = (List<PageData>) dao.findForList("QualityInspectMapper.getLastNotLevelProcessList", params);
				if (!uncheckedProcessList.isEmpty()) {
					uncheckedProcessList.get(0).put("processMark", "0");
					for (int i = 1; i < uncheckedProcessList.size(); i++) {
						uncheckedProcessList.get(i).put("processMark", "1");
					}
				}
				dataList.addAll(processList);
				dataList.addAll(uncheckedProcessList);
			} else { // 不是工序
				for (int i = 0; i < pds.size(); i++) {
					PageData pdInfo = pds.get(i);
					String unitType = String.valueOf(pdInfo.get("UNIT_TYPE"));
					if (!"51".equals(unitType)) {
						// 查询 下级 工序 数量 和 已经使用过的数量
						PageData params = new PageData();
						params.put("checkTypeFilter", pd.get("checkTypeFilter"));
						params.put("TECHNICIAN", pd.get("technician"));
						params.put("BASE_ID", pdInfo.get("BASE_ID"));
						params.put("PARENT_UNIT_ID", pd.get("nodeid"));
						params.put("PROCESS_BASE_ID", pdInfo.get("BASE_ID")+"-00001");
						PageData process = (PageData) dao.findForObject("QualityInspectMapper.getProjectQualityDetail", params);
						if ("51".equals(process.getString("UNIT_TYPE"))) {
							PageData Rectification = (PageData) dao.findForObject("QualityInspectMapper.getProjectQualityRectification", params);
							pdInfo.put("procedureFlag", "procedure");
							if (Rectification != null) {
								pdInfo.put("processRectification", "1");
							}
							/*String status = String.valueOf(Rectification.get("HAS_USE_FLAG"));
							if ("1".equals(status)) {
								pdInfo.put("processRectification", "1");
							}*/
						}
						// 获取 子级 工序 使用比例 COUNT_USE_NUM / COUNT_TOTAL_NUM
						PageData processUseBiLi = (PageData)dao.findForObject("QualityInspectMapper.getChildProcessUseBiLi", params);
						pdInfo.putAll(processUseBiLi);
						dataList.add(pdInfo);
					}
				}
			}
			return dataList;
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}

	/**
	 * 查询所有的工序列表 未使用过的
	 * @return
	 */
	public List<PageData> getProjectQualityDetailOfProcessList(PageData pd) throws GenException {
		try {
			List<PageData> dataList = new ArrayList<>();
			List<PageData> list = (List<PageData>) dao.findForList("QualityInspectMapper.getProjectQualityDetailOfProcessList", pd);
			// 选择第一条 根据 PARENT_UNIT_ID
			if (list == null || list.size() == 0) {
				return dataList;
			}
			String lastParentUnitId = UuidUtil.get32UUID();
			for (int i = 0; i < list.size(); i++) {
				PageData pdInfo = list.get(i);
				String parentUnitId = String.valueOf(pdInfo.get("PARENT_UNIT_ID"));
				if (parentUnitId == null) {
					parentUnitId = UuidUtil.get32UUID();
				}
				if (lastParentUnitId.equals(parentUnitId)) {
					continue;
				}
				lastParentUnitId = parentUnitId;
				dataList.add(pdInfo);
			}
			return dataList;
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}

	public PageData getQualityInspectStatus(PageData pd) throws GenException {
		PageData status = null;
		try {
			status = (PageData) dao.findForObject("QualityInspectMapper.getQualityInspectStatus", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return status;
	}

	public List<PageData> projectListPage(Page page) throws GenException {
		List<PageData> projectList=null;
		try {
			projectList=(List<PageData>) dao.findForList("QualityInspectMapper.ProjectlistPage", page);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return projectList;
	}

	/**
	 * 分项工程统计
	 * param getprojectSubentryCountList
	 * */
	public List<PageData> getprojectSubentryCountList(PageData pd) throws GenException {
		List<PageData> projectList=null;
		try {
			projectList=(List<PageData>) dao.findForList("QualityInspectMapper.getprojectSubentryCountList", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return projectList;
	}

	/**
	 * 查询未检验工序
	 *
	 * */

	/**
	 * 分项工程统计
	 * param getprojectSubentryCountList
	 * */
	public PageData getgetNotInspectionProcedureList(PageData pd) throws GenException {
		PageData projectList=null;
		try {
			projectList=(PageData) dao.findForObject("QualityInspectMapper.getNotInspectionProcedure",pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return projectList;
	}

	@Log(name="得到全部工序质量验收列表")
	public List<PageData> getQualityInspectlist(PageData pd) throws GenException {
		try {
			return (List<PageData>) dao.findForList("QualityInspectMapper.getAllQualityInspectlist", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}

	public List<PageData> getQualityInspectlistPage(Page page) throws GenException {
		try {
			return (List<PageData>) dao.findForList("QualityInspectMapper.getQualityInspectlistPage", page);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}

	public List<PageData> getNotQualityInspectlistPage(Page page) throws GenException {
		try {
			return (List<PageData>) dao.findForList("QualityInspectMapper.getNotQualityInspectlistPage", page);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}

	/**
	 * 分页查询分项数据
	 *
	 * */
	public List<PageData> getprojectSubentryCountListPage(Page page) throws GenException {
		try {
			return (List<PageData>) dao.findForList("QualityInspectMapper.getprojectSubentryCountListPage", page);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}

	/**
	 * 分页查询分项明细数据
	 *
	 * */
	public List<PageData> getprojectSubentryDetailListPage(Page page) throws GenException {
		try {
			return (List<PageData>) dao.findForList("QualityInspectMapper.getprojectSubentryDetailListPage", page);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}

}
