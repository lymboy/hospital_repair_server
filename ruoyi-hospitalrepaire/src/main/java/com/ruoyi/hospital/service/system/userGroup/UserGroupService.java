package com.ruoyi.hospital.service.system.userGroup;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.ruoyi.hospital.dao.DaoSupport;
import com.ruoyi.hospital.entity.Page;
import com.ruoyi.hospital.framework.exception.GenException;
import com.ruoyi.hospital.util.PageData;

@Service("userGroupService")
public class UserGroupService {

	@Resource(name="daoSupport")
	private DaoSupport dao;


	//添加用户组
	public void addUserGroup(PageData pd) throws GenException {
		try {
			dao.save("UserGroupMapper.addUserGroup", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}


	//更新用户组
	public void updateUserGroup(PageData pd) throws GenException {
		try {
			dao.update("UserGroupMapper.updateUserGroup", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}

	}

	//分页得到用户组列表
	public List<PageData> getUserGroupListPage(Page page) throws GenException {
		List<PageData> userGroupList=null;
		try {
			userGroupList=(List<PageData>) dao.findForList("UserGroupMapper.getUserGrouplistPage", page);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return userGroupList;
	}


	//批量删除用户组
	public void deleteAllUserGroup(PageData pd) throws GenException {
		try {
			dao.update("UserGroupMapper.deleteAllUserGroup", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}

	//根据id删除用户组
	public void deleteUserGroupById(PageData pd) throws GenException {
		try {
			dao.update("UserGroupMapper.deleteUserGroupById", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}

	}

	//根据id得到用户组的基本信息
	public PageData getUserGroupInfo(PageData pd) throws GenException {
		PageData pds=null;
		try {
			pds = (PageData) dao.findForObject("UserGroupMapper.getUserGroupInfo", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pds;
	}


	//分页得到用户身份列表
	@SuppressWarnings("unchecked")
	public List<PageData> getUserIdentityListByDepIdlistPage(Page page) throws GenException {
		List<PageData> userIdentityList=null;
		try {
			userIdentityList=(List<PageData>) dao.findForList("UserGroupMapper.getUserIdentityListByDepIdlistPage", page);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return userIdentityList;
	}


	//添加身份用户组
	public void saveIdentityGroup(PageData pd) throws GenException {
		try {
			dao.save("UserGroupMapper.saveIdentityGroup", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}

	}

	//查找所有的数据,根据用户组id
	public List<PageData> findAllIdentityByGroup(PageData pd) throws GenException {
		List<PageData> identityCheckedList=null;
		try {
			identityCheckedList=(List<PageData>) dao.findForList("UserGroupMapper.findAllIdentityByGroup", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return identityCheckedList;
	}

	//得到所有的角色
	public List<PageData> getAllRole() throws GenException {
		List<PageData> roleList=null;
		try {
			roleList=(List<PageData>) dao.findForList("UserGroupMapper.getAllRole", null);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return roleList;
	}


	//查询用户组角色表数据
	public List<PageData> findAllGroupRoleList() throws GenException {
		List<PageData> pds=null;
		try {
			pds=(List<PageData>) dao.findForList("UserGroupMapper.findAllGroupRoleList", null);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pds;
	}

	//删除身份用户组信息
	public void deleteIdentityGroup(PageData pd) throws GenException {
		try {
			dao.delete("UserGroupMapper.deleteIdentityGroup",pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}

	}

	//插入用户组角色
	public void saveGroupRole(PageData pd) throws GenException {
		try {
			dao.save("UserGroupMapper.saveGroupRole", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}

	}

	//用户组的成员分配角色
	public void saveUserRoleByGroup(PageData pd) throws GenException {
		try {
			dao.save("UserGroupMapper.saveUserRoleByGroup", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}

	//根据用户组得到所有的身份
	public List<PageData> getAllUserByGroup(PageData pd) throws GenException {
		List<PageData> userByGroupList=null;
		try {
			userByGroupList=(List<PageData>) dao.findForList("UserGroupMapper.getAllUserByGroup", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return userByGroupList;
	}

	//根据用户组查找所有的用户组角色
	public List<PageData> getAllGroupRole(PageData pd) throws GenException {
		List<PageData> p=null;
		try {
			p=(List<PageData>) dao.findForList("UserGroupMapper.getAllGroupRole", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return p;
	}

	//删除用户组对应的角色
	public void deleteGroupRole(PageData pd) throws GenException {
		try {
			dao.delete("UserGroupMapper.deleteGroupRole", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}

	}

	//根据用户组删除所有的角色
	public void deleteAllRoleByGroupId(PageData pd) throws GenException {
		try {
			dao.delete("UserGroupMapper.deleteAllRoleByGroupId", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}

	}


}
