package com.ruoyi.hospital.service.basicsetting.projectGrid;

import java.io.File;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Service;

import com.ruoyi.hospital.dao.DaoSupport;
import com.ruoyi.hospital.entity.Page;
import com.ruoyi.hospital.framework.exception.GenException;
import com.ruoyi.hospital.framework.log.Log;
import com.ruoyi.hospital.util.Const;
import com.ruoyi.hospital.util.PageData;
import com.ruoyi.hospital.util.UuidUtil;


/**
 * @author 杨云波
 *
 */
@Service
@Resource(name="projectGridService")
public class ProjectGridService {

	@Resource(name="daoSupport")
	private DaoSupport dao;

	/**
	 * 分页查询网格化分工单元列表
	 * @throws GenException
	 */
	public List<PageData> getProjectGridListPage(Page page) throws GenException{
		List<PageData>  projectGrid= null;
		try{
			projectGrid = (List<PageData>) dao.findForList("ProjectGridMapper.getProjectGridlistPagePrivilegeFilter", page);
		} catch (Exception e){
			throw new GenException(e.getCause());
		}
		return projectGrid;
	}

	//通过网格单元id查询基本信息
		public PageData OpenProjectGridEdit(PageData pd) throws GenException {
			PageData projectGridInfo=null;
			try {
				projectGridInfo=(PageData) dao.findForObject("ProjectGridMapper.OpenProjectGridEdit", pd);
			} catch (Exception e) {
				throw new GenException(e.getCause());
			}
			return projectGridInfo;
		}

		//得到网格单元的条数
		public int getBillNum(PageData pd) throws GenException {
			int projectGridNum=0;
			try {
				projectGridNum=(int) dao.findForObject("ProjectGridMapper.getBillNum", pd);
			} catch (Exception e) {
				throw new GenException(e.getCause());
			}
			return projectGridNum;
		}
		//得到员工的基本信息
		public PageData getUserInfoById(PageData pd) throws GenException {
			PageData userInfo=null;
			try {
				userInfo=(PageData) dao.findForObject("ProjectGridMapper.getUserInfoById", pd);
			} catch (Exception e) {
				throw new GenException(e.getCause());
			}
			return userInfo;
		}

	/*	//得到项目信息
		public PageData getProjectInfoById(PageData pd) throws GenException {
			PageData projectInfo=null;
			try {
				projectInfo=(PageData) dao.findForObject("ProjectGridMapper.getProjectInfoByIdPage", pd);
			} catch (Exception e) {
				throw new GenException(e.getCause());
			}
			return projectInfo;
		}*/

		//单元添加
		public void ProjectGridInfoAdd(PageData pd) throws GenException {
			try {
				dao.save("ProjectGridMapper.ProjectGridInfoAdd", pd);
			} catch (Exception e) {
				throw new GenException(e.getCause());
			}
		}

		//网格单元编辑更新
		public void ProjectGridInfoUpdate(PageData pd) throws GenException {
			try {
				dao.update("ProjectGridMapper.ProjectGridInfoUpdate", pd);
			} catch (Exception e) {
				throw new GenException(e.getCause());
			}

		}
		//网格单元删除
		public void deleteProjectGrid(PageData pd) throws GenException {
			try {
				dao.update("ProjectGridMapper.deleteProjectGrid", pd);
			} catch (Exception e) {
				throw new GenException(e.getCause());
			}

		}
//		/getProjectGridDetailList
		//分页得到网格化分工划分的从表
		public List<PageData> getProjectGridDetailList(Page page) throws GenException {
			List<PageData> projectGridDetailList=null;
			try {
				projectGridDetailList=(List<PageData>) dao.findForList("ProjectGridMapper.getProjectGridDetaillistPage", page);
			} catch (Exception e) {
				throw new GenException(e.getCause());
			}
			return projectGridDetailList;
		}

		//得到网格化分工从表的基本信息
		public PageData getProjectGridServicetDetailInfo(PageData pd) throws GenException {
			PageData projectGridDetailInfo=null;
			try {
				projectGridDetailInfo=(PageData) dao.findForObject("ProjectGridMapper.projectGridDetailInfo", pd);
			} catch (Exception e) {
				throw new GenException(e.getCause());
			}
			return projectGridDetailInfo;
		}


		/**
		 * 添加检查从表行
		 * @throws GenException
		 */
		public void addProjectGridDetailList(PageData pd) throws GenException {
			try {
				dao.save("ProjectGridMapper.addProjectGridDetailList", pd);
				//新增 员工 责任范围明细
				String PGD_ID = pd.getString("ID");
				String employeeId = pd.getString("EMPLOYEE_ID");

				PageData pda = new PageData();
				pda.put("employeeId", employeeId);
				pda.put("PGD_ID", PGD_ID);
				List<String> idsList = (List<String>) dao.findForList("ProjectGridMapper.getProjectQualityIdListByEmpId", pda);
				for (String PQD_ID : idsList) {
					pda.put("ID", UuidUtil.get36UUID());
					pda.put("PQD_ID", PQD_ID);
					dao.save("ProjectGridMapper.addEmpDutyProjectGridQuiltyDetail", pda);
				}
			} catch (Exception e) {
				throw new GenException(e.getCause());
			}

		}


		//更新检查从表
		public void updateProjectGridDetailList(PageData pd) throws GenException {
			try {
				dao.update("ProjectGridMapper.updateProjectGridDetailList", pd);
			} catch (Exception e) {
				throw new GenException(e.getCause());
			}

		}

		//删除明细表行
		public void deleteProjectGridDetailList(PageData pd) throws GenException {
			try {
				dao.update("ProjectGridMapper.deleteProjectGridDetailList", pd);
			} catch (Exception e) {
				throw new GenException(e.getCause());
			}

		}


		@Log(name="快审-updateFastTrialById")
		public int updateFastTrialById(PageData pd) throws GenException {
			int num = 0;
			try {
				num = (Integer) dao.update("ProjectGridMapper.updateFastTrialById", pd);
			} catch (Exception e) {
				throw new GenException(e.getCause());
			}
			return num;
		}

		@Log(name="项目网格化分工修订-updateProjectGridVersionInfo")
		public int updateProjectGridVersionInfo(PageData pd, String id) throws GenException {
			int num = 0;
			try {
				// 获取 修订前  上一条记录的数据
				PageData pdInfo = (PageData)dao.findForObject("ProjectGridMapper.getProjectGridInfoById", pd);

				// 更新信息 增加一个版本
				String versionNoStr = pdInfo.getString("VERSION_NO");
				int versionNo = Integer.valueOf(versionNoStr) + 1;
				pdInfo.put("VERSION_NO", versionNo);
				pdInfo.put("CREATEDATE", new Date());
				pdInfo.put("CREATER", pd.getString("creater"));
				pdInfo.put("MODIFYDATE", new Date());
				pdInfo.put("MODIFIER", pd.getString("modifier"));
				pdInfo.put("ID", id);
				pdInfo.put("EDITOR", pd.getString("creater"));
				pdInfo.put("EDIT_DATE", new Date());
				dao.update("ProjectGridMapper.insertProjectGridInfo", pdInfo); // 新版本
				// 从表 复制
				pd.put("projectGridId", pd.getString("id"));
				List<PageData> pdcList = (List<PageData>) dao.findForList("ProjectGridMapper.getProjectGridDetailList", pd);
				for (PageData pdc : pdcList) {
					String newDetailId =  UuidUtil.get36UUID();
					String oldDetailId = pdc.getString("ID");
					pdc.put("ID", newDetailId);
					pdc.put("PROJECTGRID_ID", id);
					pdc.put("CREATEDATE", new Date());
					pdc.put("CREATER", pd.getString("creater"));
					pdc.put("MODIFYDATE", new Date());
					pdc.put("MODIFIER", pd.getString("modifier"));
					dao.save("ProjectGridMapper.copyProjectGridDetail", pdc);
					// 修订员工责任范围明细
					List<PageData> dutyPdList = (List<PageData>)dao.findForList("ProjectGridMapper.getAllEmpDutyByDetailIdForUpdate", oldDetailId);
					for (PageData dutyPd : dutyPdList) {
						dutyPd.put("ID", UuidUtil.get36UUID());
						dutyPd.put("PROJECT_GRID_DETAIL_ID", newDetailId);
						dao.save("ProjectGridMapper.copyEmpDutyForUpdate", dutyPd);
					}
				}
				// 附件 复制
				pd.put("masterId", pd.getString("id"));
				List<PageData> pdtList = (List<PageData>) dao.findForList("ProjectGridMapper.getProjectGridAttachmentList", pd);
				for (PageData pdt : pdtList) {
					String oldId = pdt.getString("ID");
					String newId = UuidUtil.get36UUID();
					String oldMasterId = pdt.getString("MASTER_ID");

					pdt.put("ID", newId);
					pdt.put("MASTER_ID", id);
					pdt.put("CREATEDATE", new Date());
					pdt.put("CREATER", pd.getString("creater"));
					pdt.put("MODIFYDATE", new Date());
					pdt.put("MODIFIER", pd.getString("modifier"));
					dao.save("ProjectGridMapper.copyProjectGridAttachment", pdt);

					// 文件 复制
					// 得到要下载的文件信息
					String fileUrl = pdt.getString("SUB_SYSTEM")+File.separator+ pdt.getString("MODULE_NAME");
					String fileName = pdt.getString("FILE_NAME");
					// 找出文件
					String path = Const.FILE_UPLOAD_ROOT_URL+File.separator+ "uploadFiles/attachment/";
					String filePath = path+File.separator+fileUrl+File.separator+oldMasterId+File.separator+oldId+fileName.substring(fileName.lastIndexOf("."));
					String filePathNew = path+File.separator+fileUrl+File.separator+id+File.separator+newId+fileName.substring(fileName.lastIndexOf("."));
					File file = new File(filePath);
					File fileNew = new File(filePathNew);
					if(file.exists()){
						File folder = new File(path+File.separator+fileUrl+File.separator+id);
						if(!folder.exists()) {
							folder.mkdirs();
						}
						if(!fileNew.exists()){
							FileUtils.copyFile(file, fileNew);
						}
					}
				}

				// 修订 原来的版本
				dao.update("ProjectGridMapper.updateVersionProjectGridInfoById", pd);
			} catch (Exception e) {
				throw new GenException(e.getCause());
			}
			return num;
		}

		/** *************************************   从表 历史 查询 ***********************************/
		/**
		 * 获取清单库列表
		 * @param page
		 * @return
		 * @throws GenException
		 */
		public List<PageData> getAllProjectGridHistorylistPage(Page page) throws GenException {
			List<PageData> pds = null;
			try {
				pds = (List<PageData>) dao.findForList("ProjectGridMapper.getAllProjectGridHistorylistPage", page);
			} catch (Exception e) {
				throw new GenException(e.getCause());
			}
			return pds;
		}

		/**
		 * 获取所有员工信息列表
		 * @return
		 * @throws GenException
		 */
		public List<PageData> getEmployeeList() throws GenException {
			List<PageData> pdList = null;
			try {
				pdList = (List<PageData>) dao.findForList("ProjectGridMapper.getEmployeeList", null);
			} catch (Exception e) {
				throw new GenException(e.getCause());
			}
			return pdList;
		}
		/**
		 * 获取所有岗位信息列表
		 * @return
		 * @throws GenException
		 */
		public List<PageData> getPostList() throws GenException {
			List<PageData> pdList = null;
			try {
				pdList = (List<PageData>) dao.findForList("ProjectGridMapper.getPostList", null);
			} catch (Exception e) {
				throw new GenException(e.getCause());
			}
			return pdList;
		}

		/**
		 * 导入 数据
		 * @param pdsList
		 * @return
		 * @throws GenException
		 */
		public int importDetailData(List<PageData> pdsList) throws GenException {
			int num = 0;
			try {
				for (PageData pd : pdsList) {
					dao.save("ProjectGridMapper.addProjectGridDetailList", pd);
				}
			} catch (Exception e) {
				throw new GenException(e.getCause());
			}
			return num;
		}

		//得到所有
	public List<PageData> projectListPage(Page page) throws GenException {
		List<PageData> projectList=null;
		try {
			projectList=(List<PageData>) dao.findForList("ProjectGridMapper.ProjectlistPage", page);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return projectList;
	}

	/**
	 * 根据员工Id【employeeId】 获取 相关 项目质量单元划分 从表记录 ID 列表
	 * @param pd
	 * @throws GenException
	 */
	public List<String> getProjectQualityIdListByEmpId(PageData pd) throws GenException {
		List<String> strList = null;
		try {
			strList = (List<String>) dao.findForList("ProjectGridMapper.getProjectQualityIdListByEmpId", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return strList;
	}
	/**
	 * 根据网格分工从表 【id】 获取 相关 项目质量单元划分 主表记录 ID 列表
	 * @param pd
	 * @throws GenException
	 */
	public List<String> getProjectQualityIdListByGDIdAndEmpId(PageData pd) throws GenException {
		List<String> strList = null;
		try {
			strList = (List<String>) dao.findForList("ProjectGridMapper.getProjectQualityIdListByGDIdAndEmpId", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return strList;
	}

	/**
	 * 员工 责任 范围明细
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public List<PageData> getEmpDutyDetailList(PageData pd) throws GenException {
		String idsListStr = pd.getString("idsList");
		List<String> idsList = null;
		if (idsListStr != null && idsListStr.length()>0 && idsListStr.contains(",")) {
			String[] idsListArray = idsListStr.split(",");
			idsList = Arrays.asList(idsListArray);

		}
		pd.put("idsList", idsList);

		List<PageData> pdsList = null;
		try {
			pdsList = (List<PageData>)dao.findForList("ProjectGridMapper.getEmpDutyDetailList", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pdsList;
	}

	/**
	 * 更新责任范围明细  施工班组 和  备注
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public int editEmpDutyQualityDetailById(PageData pd) throws GenException{
		int num  = 0;
		try {
			num = (Integer)dao.update("ProjectGridMapper.editEmpDutyQualityDetailById", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return num;
	}



}
