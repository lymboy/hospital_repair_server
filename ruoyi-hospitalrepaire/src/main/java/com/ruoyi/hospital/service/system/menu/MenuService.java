package com.ruoyi.hospital.service.system.menu;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.ruoyi.hospital.dao.DaoSupport;
import com.ruoyi.hospital.entity.Page;
import com.ruoyi.hospital.entity.system.Menu;
import com.ruoyi.hospital.framework.exception.GenException;
import com.ruoyi.hospital.framework.log.Log;
import com.ruoyi.hospital.util.PageData;
import org.springframework.stereotype.Service;

/**
 * <p>Title: MenuService</p>
 * @Desc:
 * @author: 杨建军
 *	@Date: 2017年3月8日
 * @Time: 上午11:49:04
 * @version:
 */
@Service("menuService")
public class MenuService{

	@Resource(name = "daoSupport")
	private DaoSupport dao;


	public List<Menu> listAllMenu(PageData pd) throws GenException {
		List<Menu> rl = null;
		try {
			rl = this.listAllParentMenu(pd);//systemType
			for(Menu menu : rl){
				pd.put("Parent_menu_id", menu.getMenuId());
				List<Menu> subList = this.listSubMenuByParentId(pd);
				menu.setSubMenu(subList);
			}
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return rl;
	}

	public List<Menu> listAllParentMenu(PageData pd) throws GenException {
		List<Menu> menus = null;
		try {
			menus = (List<Menu>) dao.findForList("MenuMapper.listAllParentMenu", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return menus;
	}

	public List<Menu> listSubMenuByParentId(PageData pd) throws GenException {
		List<Menu> menus = null;
		try {
			menus = (List<Menu>) dao.findForList("MenuMapper.listSubMenuByParentId", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return menus;
	}


	/**
	 * 获取该权限拥有的所有菜单
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public List<Menu> getMenuListByPrivilegeId(PageData pd) throws GenException {
		List<Menu> rl = null;
		try {
			rl = this.getParentMenuListByPrivilegeId(pd);
			for(Menu menu : rl){
				pd.put("Parent_menu_id", menu.getMenuId());
				List<Menu> subList = this.getSubMenuListByPrivilegeIdAndParentId(pd);
				menu.setSubMenu(subList);
			}
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return rl;
	}
	public List<Menu> getParentMenuListByPrivilegeId(PageData pd) throws GenException {
		List<Menu> menus = null;
		try {
			menus = (List<Menu>) dao.findForList("MenuMapper.getParentMenuListByPrivilegeId", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return menus;
	}
	public List<Menu> getSubMenuListByPrivilegeIdAndParentId(PageData pd) throws GenException {
		List<Menu> menus = null;
		try {
			menus = (List<Menu>) dao.findForList("MenuMapper.getSubMenuListByPrivilegeIdAndParentId", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return menus;
	}

	/**
	 * 删除权限拥有的菜单
	 * @param pd
	 * @throws GenException
	 */
	public void deletePMByPrivilege(PageData pd) throws GenException {
		try {
			dao.delete("MenuMapper.deletePMByPrivilege", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}

	/**
	 * 获取所有菜单
	 * @return
	 * @throws GenException
	 */
	public List<PageData> listAllMenus() throws GenException {
		List<PageData> rl = null;
		try {
			rl = this.getAllParentMenuList();
			for(PageData menu : rl) {
				PageData pd = new PageData();
				pd.put("Parent_menu_id", menu.get("MENU_ID"));
				List<PageData> subList = this.getAllSubMenuListByParentId(pd);
				menu.put("SubMenu", subList);
			}
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return rl;
	}
	public List<PageData> getAllParentMenuList() throws GenException {
		List<PageData> menus = null;
		try {
			menus = (List<PageData>) dao.findForList("MenuMapper.getAllParentMenuList", "");
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return menus;
	}
	public List<PageData> getAllSubMenuListByParentId(PageData pd) throws GenException {
		List<PageData> menus = null;
		try {
			menus = (List<PageData>) dao.findForList("MenuMapper.getAllSubMenuListByParentId", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return menus;
	}

	/**
	 * 查找权限拥有的菜单
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public List<PageData> getMenuByPrivilegeId(PageData pd) throws GenException {
		List<PageData> pds = null;
		try {
			pds = (List<PageData>) dao.findForList("MenuMapper.getMenuByPrivilegeId", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pds;
	}

	/**
	 * 为权限添加一个菜单
	 * @param pd
	 * @throws GenException
	 */
	public void insertPrivilegeMenu(PageData pd) throws GenException {
		try {
			dao.save("MenuMapper.insertPrivilegeMenu", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}

	/**
	 * 删除权限菜单表记录通过权限id和菜单id
	 * @param pd
	 * @throws GenException
	 */
	public void deletePMByPMId(PageData pd) throws GenException {
		try {
			dao.delete("MenuMapper.deletePMByPMId", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}

	/**
	 * 新增一个菜单
	 * @param menu
	 * @throws GenException
	 */
	public void insertMenu(Menu menu) throws GenException {
		try {
			dao.save("MenuMapper.insertMenu", menu);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}

	/**
	 * 根据id获取菜单信息
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public PageData getMenuById(PageData pd) throws GenException {
		PageData menupd = null;
		try {
			menupd = (PageData) dao.findForObject("MenuMapper.getMenuById", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return menupd;
	}

	/**
	 * 更改菜单信息
	 * @param pd
	 * @throws GenException
	 */
	public void editMenu(PageData pd) throws GenException {
		try {
			dao.update("MenuMapper.editMenu", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}

	/**
	 * 删除菜单信息
	 * @param pd
	 * @throws GenException
	 */
	public Map<String, Object> deleteMenuById(PageData pd) throws GenException {
		try {
			dao.delete("MenuMapper.deleteMenuById", pd);
			pd.put("Parent_menu_id", pd.get("menuId"));
			List<PageData> pdList = this.getAllSubMenuListByParentId(pd);
			List<Long> deletepd = new ArrayList<Long>();
			for(int i = 0; i < pdList.size(); i++) {
				deletepd.add(Long.valueOf(pdList.get(i).get("Parent_menu_id").toString()));
			}
			deletepd.add(Long.valueOf(pd.get("menuId").toString()));
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("item", deletepd);
//			this.deletePMByMenu(map);
			return map;
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}


	/**
	 * 通过菜单id批量删除权限菜单表
	 * @throws GenException
	 */
	public void deletePMByMenu(Map<String, Object> map) throws GenException {
		try {
			dao.delete("MenuMapper.deletePMByMenu", map);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}


	/***************** 按钮  START**********************/
	/**
	 * 获取该菜单的所有按钮
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public List<PageData> getMenuButtonsByMenuId(PageData pd) throws GenException {
		List<PageData> pds = null;
		try {
			pds = (List<PageData>) dao.findForList("MenuMapper.getMenuButtonsByMenuId", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pds;
	}
	/**
	 * 分页 获取该菜单的所有按钮
	 * @return
	 * @throws GenException
	 */
	public List<PageData> getMenuButtonsByMenuIdlistPage(Page page) throws GenException {
		List<PageData> pds = null;
		try {
			pds = (List<PageData>) dao.findForList("MenuMapper.getMenuButtonsByMenuIdlistPage", page);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pds;
	}

	/**
	 * 新增菜单按钮
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	@Log(name="新增菜单按钮-addMenuButton")
	public int addMenuButton(PageData pd) throws GenException {
		int num = 0;
		try {
			num = (Integer) dao.save("MenuMapper.addMenuButton", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return num;
	}

	/**
	 * 更新菜单按钮
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	@Log(name="更新菜单按钮-updateMenuButton")
	public int updateMenuButton(PageData pd) throws GenException {
		int num = 0;
		try {
			num = (Integer) dao.update("MenuMapper.updateMenuButton", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return num;
	}

	/**
	 * 删除菜单按钮
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	@Log(name="删除菜单按钮-deleteMenuButton")
	public int deleteMenuButton(PageData pd) throws GenException {
		int num = 0;
		try {
			num = (Integer) dao.update("MenuMapper.deleteMenuButton", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return num;
	}
	/***************** 按钮  END**********************/

	/**
	 * 获取所有菜单
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public List<PageData> getAllMenuList(PageData pd) throws GenException {
		List<PageData> pds = null;
		try {
			pds = (List<PageData>) dao.findForList("MenuMapper.getAllMenuList", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pds;
	}
	/**
	 * 获取所有 菜单 根据权限ID
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public List<PageData> getAllMenuListByPrivilegeId(PageData pd) throws GenException {
		List<PageData> pds = null;
		try {
			pds = (List<PageData>) dao.findForList("MenuMapper.getAllMenuListByPrivilegeId", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pds;
	}
	/**
	 * 获取所有按钮 将按钮拼接 加入 菜单集合
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public List<PageData> getAllButtonAsMenu(PageData pd) throws GenException {
		List<PageData> pds = null;
		try {
			pds = (List<PageData>) dao.findForList("MenuMapper.getAllButtonsAsMenu", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pds;
	}
	/**
	 * 获取所有 按钮  根据权限ID
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public List<PageData> getAllButtonsAsMenuByPrivilegeId(PageData pd) throws GenException {
		List<PageData> pds = null;
		try {
			pds = (List<PageData>) dao.findForList("MenuMapper.getAllButtonsAsMenuByPrivilegeId", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return pds;
	}

	/**
	 * 更新 按钮 权限
	 * @return
	 * @throws GenException
	 */
	public int updatePrivilegeButton(PageData pd,  List<PageData> buttonPrivilegeList) throws GenException {
		int num=0;
		try {
			dao.delete("MenuMapper.deleteMenuPrivilegeByPrivlegeId", pd);// 先删
			if(buttonPrivilegeList!=null){
				for (PageData pbpd : buttonPrivilegeList) {
					dao.save("MenuMapper.addMemuPrivilege", pbpd);// 后增
				}
			}
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return num;
	}

	//得到我的菜单
	public List<PageData> getPersonMenuList(Page page) throws GenException {
		List<PageData> personMenu=null;
		try {
			personMenu=(List<PageData>) dao.findForList("MenuMapper.getPersonMenulistPage", page);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return personMenu;
	}

	//得到我的菜单的所有id
	public List<PageData> AllPersonMenu(String employeeId) throws GenException {
		List<PageData> allPersonMenu=null;
		try {
			allPersonMenu=(List<PageData>) dao.findForList("MenuMapper.AllPersonMenu", employeeId);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return allPersonMenu;
	}

	//我的菜单的添加
	public void addPersonMenu(PageData pd) throws GenException {
		 try {
			dao.save("MenuMapper.addPersonMenu", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}

	//我的菜单删除
	public void deletePersonMenu(PageData pd) throws GenException {
		try {
			dao.delete("MenuMapper.deletePersonMenu", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
	}
}
