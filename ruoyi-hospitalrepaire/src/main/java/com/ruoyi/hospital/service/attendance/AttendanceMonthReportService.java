package com.ruoyi.hospital.service.attendance;

import com.ruoyi.hospital.dao.DaoSupport;
import com.ruoyi.hospital.entity.Page;
import com.ruoyi.hospital.entity.system.User;
import com.ruoyi.hospital.framework.exception.GenException;
import com.ruoyi.hospital.util.Const;
import com.ruoyi.hospital.util.DateUtil;
import com.ruoyi.hospital.util.PageData;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p> 考勤记录月报表 </p>
 *
 * @author Young
 * @since 2018/4/2
 */
@Service
public class AttendanceMonthReportService {

    @Autowired
    private DaoSupport dao;

    /**
     * 考勤机构列表
     * @param pd
     * @return
     * @throws GenException
     */
    public List<PageData> getAttendanceMonthReportDepList(PageData pd) throws GenException {
        try {
            return (List<PageData>) dao.findForList("AttendanceMonthReportMapper.getAttendanceMonthReportDepList", pd);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     * 人员考勤数据 机构ID， 考勤年月【DEPARTMENT_ID，ATTENDANCE_DATE_YM】
     * @param pd
     * @return
     */
    public List<PageData> getAttendanceMonthReportList(PageData pd) throws GenException {
        try {
            String departmentId = String.valueOf(pd.get("DEPARTMENT_ID"));
            String baseId = String.valueOf(pd.get("BASE_ID"));
            String attendanceDateYM = String.valueOf(pd.get("ATTENDANCE_DATE_YM"));
            if (departmentId == null || "".equals(departmentId) || attendanceDateYM == null || "".equals(attendanceDateYM)) {
                return null;
            }

            List<PageData> empList = (List<PageData>) dao.findForList("AttendanceMonthReportMapper.getAttendanceMonthReportList", pd);
            if (empList == null || empList.size() == 0) {
                return null;
            }

            for (int i = 0; i < empList.size(); i++) {
                PageData empPd = empList.get(i);
                String employeeId = String.valueOf(empPd.get("ID"));
                pd.put("EMPLOYEE_ID", employeeId);
                // 员工指定月份内请假情况
                List<PageData> empLeaveRecordList = (List<PageData>) dao.findForList("AttendanceMonthReportMapper.getEmpLeaveRecordList", pd);
                // 员工指定月份内的签到情况
                List<PageData> empAttendanceRecordList = (List<PageData>) dao.findForList("AttendanceMonthReportMapper.getEmpAttendanceRecordList", pd);
                // 计算 当前月份 考勤情况 关键字为 KEY_01,KEY_02 ,KEY_03 ....
                Map<String, Object> empMonthInfo = this.caculateEmpMonthInfoMap(baseId,attendanceDateYM, empLeaveRecordList, empAttendanceRecordList);
                empPd.putAll(empMonthInfo);
            }
            return empList;
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     * 计算 当前月份 考勤情况 存入Map 关键字为 KEY_1,KEY_2 ,KEY_3 ....
     *  VALUE: 1上班 2异常 0旷工
     * @param attendanceDateYM 考勤年月
     * @param empLeaveRecordList 员工指定月份内请假情况
     * @param empAttendanceRecordList 员工指定月份内的签到情况
     * @return
     */
    private Map<String,Object> caculateEmpMonthInfoMap(String baseId,
    												   String attendanceDateYM,
                                                       List<PageData> empLeaveRecordList,
                                                       List<PageData> empAttendanceRecordList) {
        Map<String, Object> empMonthInfo = new HashMap<>();
        Date attendanceDate = DateUtil.formatDateYM(attendanceDateYM);
        empMonthInfo.put("countGoToWork", 0);
        empMonthInfo.put("countHurtOnWork", 0);
        empMonthInfo.put("countHoliday", 0);
        empMonthInfo.put("countAskForLeave", 0);
        empMonthInfo.put("countNotToWork", 0);
        if (attendanceDate == null) {
            return empMonthInfo;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(attendanceDate);
        int monthCurrent = calendar.get(Calendar.MONTH)+1;
        int dayMaxOfMonthCurrent = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        initEmpMonthInfo(empMonthInfo, calendar);
        if (empAttendanceRecordList != null && empAttendanceRecordList.size() > 0) {
            for (int i = 0; i < empAttendanceRecordList.size(); i++) {
                PageData earlPd = empAttendanceRecordList.get(i);
                String employeeId = earlPd.getString("EMPLOYEE_ID");
                int day = Integer.valueOf(String.valueOf(earlPd.get("ATTENDANCE_DAY")));
                float attendanceDays = Float.valueOf(String.valueOf(earlPd.get("ATTENDANCE_DAYS")));
                if (attendanceDays == 1) { // 一天签到两次，正常
                    empMonthInfo.put("KEY_"+day, "1");
                    continue;
                }
                if (attendanceDays == 0.5) { // 一天签到一次，异常
                	/*boolean mark=false;
                	String[] str = baseId.split("-");
                 	for (int j=1;j<str.length;j++) {
                 		if (StringUtils.equals(baseId, "0001-0003")) {
                 			mark = true;
                 			break;
                 		}else {
                 			baseId = baseId.substring(0,baseId.length()-5);
                 			if (StringUtils.equals(baseId, "0001-0003")) {
                     			mark = true;
                     			break;
                 			}
                 		}
                 	}
                 	if (mark) {
                 		empMonthInfo.put("KEY_"+day, "1");
                 	}else {
                 		empMonthInfo.put("KEY_"+day, "2");
                 	}*/
                	empMonthInfo.put("KEY_"+day, "2");
                    continue;
                }
            }
        }
        if (empLeaveRecordList != null && empLeaveRecordList.size() > 0) {
            for (int i = 0; i < empLeaveRecordList.size(); i++) {
                PageData elrPd = empLeaveRecordList.get(i);
                String leaveType = String.valueOf(elrPd.get("LEAVE_TYPE"));
                String startDateStr = String.valueOf(elrPd.get("START_DATE"));
                String endDateStr = String.valueOf(elrPd.get("END_DATE"));
                Date startDate = DateUtil.formatDate(startDateStr);
                Date endDate = DateUtil.formatDate(endDateStr);
                Calendar calendarS = Calendar.getInstance();
                calendarS.setTime(startDate);
                int monthS = calendarS.get(Calendar.MONTH)+1;
                int dayS = calendarS.get(Calendar.DATE);
                Calendar calendarE = Calendar.getInstance();
                calendarE.setTime(endDate);
                int monthE = calendarE.get(Calendar.MONTH)+1;
                int dayE = calendarE.get(Calendar.DATE);
                int startIndex = 1;
                int endIndex = dayMaxOfMonthCurrent;
                if (monthS == monthCurrent) {
                    startIndex = dayS;
                }
                if (monthE == monthCurrent) {
                    endIndex = dayE;
                }
                for (int j = startIndex; j <= endIndex; j++) {
                    empMonthInfo.put("KEY_"+j, leaveType);
                }
            }
        }

        // 统计 出勤 工伤 休假 请假 旷工
        // 出勤 countGoToWork         KEY_:value: 1
        // 工伤 countHurtOnWork       KEY_:value: 67
        // 休假 countHoliday          KEY_:value: 65 || 66
        // 请假 countAskForLeave      KEY_:value: >= 60 && val <= 68
        // 旷工 countNotToWork        KEY_:value:  0 || 2
        double countGoToWork=0, countHurtOnWork=0, countHoliday=0, countAskForLeave=0, countNotToWork=0;
        if (empMonthInfo != null && empMonthInfo.size() > 0) {
            if (empMonthInfo.size() > 0) {
                for (String key : empMonthInfo.keySet()) {
                    if (!key.startsWith("KEY_")) {
                        continue;
                    }
                    String value = String.valueOf(empMonthInfo.get(key));
                    if (!isInteger(value)) {
                        continue;
                    }
                    int val = Integer.parseInt(value);
                    if (key.startsWith("KEY_")) {
                        if (1 == val) {
                            countGoToWork ++;
                        }
                        if (67 == val) {
                            countHurtOnWork ++;
                        }
                        if (65 == val || 66  == val) {
                            countHoliday ++;
                        }
                        if (val >= 60 && val <= 68) {
                            countAskForLeave ++;
                        }
                        if (0 == val) {
                            countNotToWork ++;
                        }
                        if (2 == val) {
                        	boolean mark=false;
                        	String[] str = baseId.split("-");
                         	for (int j=1;j<str.length;j++) {
                         		if (StringUtils.equals(baseId, "0001-0003")) {
                         			mark = true;
                         			break;
                         		}else {
                         			baseId = baseId.substring(0,baseId.length()-5);
                         			if (StringUtils.equals(baseId, "0001-0003")) {
                             			mark = true;
                             			break;
                         			}
                         		}
                         	}
                         	if (mark) {
                         		countGoToWork ++;
                         	}else {
                         		countNotToWork = countNotToWork+0.5;
                            	countGoToWork  = countGoToWork+0.5;
                         	}
                        }
                    }
                }
            }
            empMonthInfo.put("countGoToWork", countGoToWork);
            empMonthInfo.put("countHurtOnWork", countHurtOnWork);
            empMonthInfo.put("countHoliday", countHoliday);
            empMonthInfo.put("countAskForLeave", countAskForLeave);
            empMonthInfo.put("countNotToWork", countNotToWork);
        }
        return empMonthInfo;
    }

    /**
     * 初始化 默认 矿工
     * @param empMonthInfo
     * @param calendarTotal
     */
    private void initEmpMonthInfo(Map<String,Object> empMonthInfo, Calendar calendarTotal) {
        int dayMax = 0;
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        if (calendar.get(Calendar.YEAR) == calendarTotal.get(Calendar.YEAR) && calendar.get(Calendar.MONTH) == calendarTotal.get(Calendar.MONTH)) {
            dayMax = calendar.get(Calendar.DATE);
        } else {
            dayMax = calendarTotal.getActualMaximum(Calendar.DATE);
        }
        for (int i = 1; i <= dayMax; i++) {
            empMonthInfo.put("KEY_"+i, 0);
        }
    }

    private boolean isInteger(String value) {
        if (value == null || "".equals(value)) {
            return false;
        }
        try {
            Integer.parseInt(value);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

}
