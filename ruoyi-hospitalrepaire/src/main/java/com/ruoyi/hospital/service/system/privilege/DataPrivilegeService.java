package com.ruoyi.hospital.service.system.privilege;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.ruoyi.hospital.dao.DaoSupport;
import com.ruoyi.hospital.entity.system.DataPrivilege;
import com.ruoyi.hospital.framework.exception.GenException;
import com.ruoyi.hospital.util.PageData;
import com.ruoyi.hospital.util.sequence;

/**
 * <p>Title: DataPrivilegeService</p>
 * @Desc:
 * @author: 杨建军
 *	@Date: 2017年3月26日
 * @Time: 下午5:45:32
 * @version:
 */
@Service(value="dataPrivilegeService")
public class DataPrivilegeService {

	@Resource(name = "daoSupport")
	private DaoSupport dao;

	/**
	 * 角色关联的数据权限列表
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public List<PageData> getDataPrivilegeByRoleId(PageData pd)throws GenException {
		List<PageData> pds = null;
		try{
			pds = (List<PageData>)dao.findForList("DataPrivilegeMapper.getDataPrivilegeByRoleId", pd);
		}catch(Exception e){
			throw new GenException(e.getCause());
		}
		return pds;
	}

	/**
	 * 所有 数据权限列表
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public List<PageData> getDataPrivilegeList(PageData pd)throws GenException {
		List<PageData> pds = null;
		try{
			pds = (List<PageData>)dao.findForList("DataPrivilegeMapper.getDataPrivilegeList", pd);
		}catch(Exception e){
			throw new GenException(e.getCause());
		}
		return pds;
	}

	/*****************************数据 权限********************************/
	/**
	 * 角色  数据 权限 关联
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public List<PageData> getRoleDataPrivilegeList(PageData pd)throws GenException {
		List<PageData> pds = null;
		try{
			pds = (List<PageData>)dao.findForList("DataPrivilegeMapper.getRoleDataPrivilegeList", pd);
		}catch(Exception e){
			throw new GenException(e.getCause());
		}
		return pds;
	}

	/**
	 * 更新 角色 数据权限
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public int editRoleDataPrivilege(PageData pd) throws GenException {
		int num=0;
		try {
			dao.delete("DataPrivilegeMapper.deleteRoleDataPrivilegeByRoleId", pd); //根据 角色Id 删除关联的数据权限
			String dataPrivilegeIds =  pd.getString("dataPrivilegeIdArray");
			if(dataPrivilegeIds!=null && !"".equals(dataPrivilegeIds)){
				String[] dataPrivilegeIdArray = dataPrivilegeIds.split(",");
				for (String dataPrivilegeId : dataPrivilegeIdArray) {
					pd.put("roleDataPrivilegeId", sequence.nextId());
					pd.put("dataPrivilegeId", dataPrivilegeId);
					dao.save("DataPrivilegeMapper.addRoleDataPrivilege", pd);// 新增 角色  数据权限
				}
			}
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return num;
	}

	/**
	 *
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public List<DataPrivilege> getDataPrivilegeByUserId(PageData pd) throws GenException {
		List<DataPrivilege> dataPrivilegeList = null;
		try {
			dataPrivilegeList = (List<DataPrivilege>) dao.findForList("DataPrivilegeMapper.getDataPrivilegeByUserId", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return dataPrivilegeList;
	}
	/**
	 * 根据身份ID 获取数据权限列表
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public List<DataPrivilege> getDataPrivilegeByIdentityId(PageData pd) throws GenException {
		List<DataPrivilege> dataPrivilegeList = null;
		try {
			dataPrivilegeList = (List<DataPrivilege>) dao.findForList("DataPrivilegeMapper.getDataPrivilegeByIdentityId", pd);
		} catch (Exception e) {
			throw new GenException(e.getCause());
		}
		return dataPrivilegeList;
	}

}
