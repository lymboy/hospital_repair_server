package com.ruoyi.hospital.service.repair;

import com.ruoyi.hospital.controller.app.WeChat1.Template;
import com.ruoyi.hospital.controller.app.WeChat1.Token;
import com.ruoyi.hospital.dao.DaoSupport;
import com.ruoyi.hospital.entity.Page;
import com.ruoyi.hospital.util.PageData;
import com.ruoyi.hospital.util.UuidUtil;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;




@Service("repairService")
public class RepairService
{
	@Resource(name = "daoSupport")
	private DaoSupport dao;
	@Autowired
	private Template template;

	public List<PageData> getRepairlistPage(Page page) throws Exception {
		List<PageData> dutyList = null;
		dutyList = (List<PageData>)this.dao.findForList("RepairMapper.getRepairlistPage", page);
		return dutyList;
	}

	public PageData getRepairQuestionById(PageData pd) throws Exception {
		PageData repairQuestion = null;
		repairQuestion = (PageData)this.dao.findForObject("RepairMapper.getRepairQuestionById", pd);
		return repairQuestion;
	}

	public void editRepairQuestion(PageData pd) {
		String statusId = pd.getString("STATUS_ID");
		if (StringUtils.equals("2", statusId)) {
			try {
				PageData quesDictionary = (PageData) dao.findForObject("DutyMapper.getDutyQuestionMarkById", pd);
				System.out.println("完成时。该问题的分数："+quesDictionary.getString("QUESTION_MARK"));
				String assignorId = pd.getString("ASSIGNOR_ID"); // 被转让人员
				String implementId = pd.getString("IMPLEMENT_ID");// 执行人员 这个可为空

				if (StringUtils.isNotEmpty(implementId)) {
					if (StringUtils.isNotEmpty(assignorId)) {
						quesDictionary.put("QUESTION_MARK", quesDictionary.getString("QUESTION_MARK"));
						quesDictionary.put("MarkId", assignorId);
						quesDictionary.put("ID", UuidUtil.get32UUID());
						quesDictionary.put("CREATEDATE", new Date());
						dao.update("InformationUserMapper.insertHosMemberMark", quesDictionary);
					}else {
						quesDictionary.put("QUESTION_MARK", quesDictionary.getString("QUESTION_MARK"));
						quesDictionary.put("MarkId", implementId);
						quesDictionary.put("ID", UuidUtil.get32UUID());
						quesDictionary.put("CREATEDATE", new Date());
						dao.update("InformationUserMapper.insertHosMemberMark", quesDictionary);
					}
				}else {
					quesDictionary.put("QUESTION_MARK", quesDictionary.getString("QUESTION_MARK"));
					quesDictionary.put("MarkId", assignorId);
					quesDictionary.put("ID", UuidUtil.get32UUID());
					quesDictionary.put("CREATEDATE", new Date());
					dao.update("InformationUserMapper.insertHosMemberMark", quesDictionary);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		try {
			this.dao.update("DutyMapper.editDutyQuestion", pd);
			String openid = null;
			if (!StringUtils.equals("2", statusId)) {
				if (StringUtils.isNotEmpty(pd.getString("ID"))) {
					PageData pageData = (PageData) this.dao.findForObject("DutyMapper.selectHosQuestionAsiByID", pd);
					if (pageData != null) {
						this.dao.update("DutyMapper.updateHosQuestionAsiByID", pd);
					}else {
						pd.put("ASSIGNOR_ID", pd.getString("IMPLEMENT_ID"));
						pd.put("ASSIGNOR_DATE", new Date());
						pd.put("SUB_ID", UuidUtil.get32UUID());
						this.dao.save("DutyMapper.addHosQuestionAsi", pd);
					}
				}
				if (StringUtils.isNotEmpty(pd.getString("ASSIGNOR_ID"))) {

					pd.put("ID", pd.getString("ASSIGNOR_ID"));
					PageData pageData = (PageData)dao.findForObject("UserMapper.findByUiId", pd); // 通过执行人员ID获取该报修人员信息
					openid = pageData.getString("WX_OPENID");
				}
			}else {
				PageData pdInfo = (PageData)this.dao.findForObject("DutyMapper.getDutyQuestionById", pd); // 通过问题ID获取问题详情，拿到报修人员
				if (StringUtils.isNotEmpty(pdInfo.getString("CONDOM_USER"))) {
					pdInfo.put("ID", pdInfo.getString("CONDOM_USER"));
					PageData pageData1 = (PageData)dao.findForObject("UserMapper.findByUiId", pdInfo); // 通过报修人员ID获取该报修人员信息
					openid = pageData1.getString("WX_OPENID");
				}
			}
			if (!StringUtils.isEmpty(openid)) {
				Token token = new Token();
				template.send_simp_message(token.getAPPID(), token.getAPPSECRET(), openid);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public List<PageData> getRepairlist(PageData pd) {
		List<PageData> dutyList = null;
		try {
			dutyList = (List<PageData>)this.dao.findForList("RepairMapper.getRepairlist", pd);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dutyList;
	}

	public List<PageData> getRepairClientlistPage(Page page) throws Exception {
		List<PageData> dutyList = null;
		dutyList = (List<PageData>)this.dao.findForList("RepairMapper.getRepairClientlistPage", page);
		return dutyList;
	}
}
