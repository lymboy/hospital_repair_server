package com.ruoyi.hospital.service.information;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ruoyi.hospital.dao.DaoSupport;
import com.ruoyi.hospital.entity.Page;
import com.ruoyi.hospital.framework.exception.GenException;
import com.ruoyi.hospital.util.PageData;

@Service
@RequestMapping(value="personnelClassificationService")
public class PersonnelClassificationService
{
	@Resource(name="daoSupport")
	private DaoSupport dao;

	/**
	 * 人员分类查询
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	@SuppressWarnings("unchecked")
	public List<PageData> getPersonnelClassification(PageData pd) throws GenException
	{
		List<PageData> pds = null;
		try
		{
			pds = (List<PageData>) dao.findForList("personnelClassificationMapper.getPersonnelClassification", pd);
		} catch (Exception e)
		{
			throw new GenException(e.getCause());
		}
		return pds;
	}

	/**
	 * 获取人员总数
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	public int getEmployeeCNT(PageData pd) throws GenException
	{
		int num = 0;
		try
		{
			num = (int) dao.findForObject("personnelClassificationMapper.getEmployeeCNT", pd);
		} catch (Exception e)
		{
			throw new GenException(e.getCause());
		}
		return num;
	}

	/**
	 * 获取人员详细信息
	 * @param pd
	 * @return
	 * @throws GenException
	 */
	@SuppressWarnings("unchecked")
	public List<PageData> getEmployeeListPage(Page page) throws GenException
	{
		List<PageData> pds = null;
		try
		{
			pds = (List<PageData>) dao.findForList("personnelClassificationMapper.getEmployeelistPage", page);
		} catch (Exception e)
		{
			throw new GenException(e.getCause());
		}
		return pds;
	}
}

