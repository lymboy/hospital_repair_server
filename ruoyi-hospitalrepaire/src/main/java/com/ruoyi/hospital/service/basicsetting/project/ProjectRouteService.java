package com.ruoyi.hospital.service.basicsetting.project;

import com.ruoyi.hospital.dao.DaoSupport;
import com.ruoyi.hospital.entity.Page;
import com.ruoyi.hospital.framework.exception.GenException;
import com.ruoyi.hospital.util.BaiDuMapUtils;
import com.ruoyi.hospital.util.PageData;
import com.ruoyi.hospital.util.UuidUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

/**
 * <p>  </p>
 *
 * @author Young
 * @since 2018-05-13
 */
@Service
public class ProjectRouteService {
    @Resource(name="daoSupport")
    private DaoSupport dao;

    /**
     * 路线划分列表 BY PROJECT_ID
     * @param pd
     * @return
     * @throws GenException
     */
    public List<PageData> getProjectRouteList(PageData pd) throws GenException {
        try {
            return (List<PageData>) dao.findForList("ProjectRouteMapper.getProjectRouteList", pd);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     * 路线划分 BY ID
     * @param ID
     * @return
     * @throws GenException
     */
    public PageData getProjectRouteById(String ID) throws GenException {
        try {
            return (PageData) dao.findForObject("ProjectRouteMapper.getProjectRouteById", ID);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     *  路线划分 新增 或 编辑
     * @param pd
     * @param userId
     * @return
     * @throws GenException
     */
    public int insertOrUpdateProjectRoute(PageData pd, String userId) throws GenException {
        try {
            String ID = String.valueOf(pd.get("ID"));
            if (ID == null || "".equals(ID)) {
                ID = UuidUtil.get32UUID();
                pd.put("ID", ID);
            }
            if (getProjectRouteById(ID) == null) {
                // 新增
                pd.put("CREATER", userId);
                pd.put("CREATEDATE", new Date());
                return (int) dao.save("ProjectRouteMapper.insertProjectRoute", pd);
            }
            pd.put("MODIFIER", userId);
            pd.put("MODIFYDATE", new Date());
            return (int) dao.save("ProjectRouteMapper.updateProjectRoute", pd);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     * 路线划分 批量删除
     * @param ids
     * @param userId
     * @return
     * @throws GenException
     */
    public int batchDeleteProjectRoute(String[] ids, String userId) throws GenException {
        try {
            int rows = 0;
            if (ids == null || ids.length == 0) {
                return rows;
            }
            for (int i = 0; i < ids.length; i++) {
                String ID = ids[i];
                PageData pd = new PageData();
                pd.put("ID", ID);
                pd.put("MODIFIER", userId);
                pd.put("MODIFYDATE", new Date());
                rows += (int) dao.save("ProjectRouteMapper.deleteProjectRoute", pd);
            }
            return rows;
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }


    /**
     * 路线划分列表 从表 BY PROJECT_ROUTE_ID
     * @param pd
     * @return
     * @throws GenException
     */
    public List<PageData> getProjectRouteDetailList(PageData pd) throws GenException {
        try {
            return (List<PageData>) dao.findForList("ProjectRouteMapper.getProjectRouteDetailList", pd);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    public List<PageData> getProjectRouteDetailList1(PageData pd) throws GenException {
        try {
            return (List<PageData>) dao.findForList("ProjectRouteMapper.getProjectRouteDetailList1", pd);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     * 分页 路线划分列表 从表 BY PROJECT_ROUTE_ID
     * @param page
     * @return
     * @throws GenException
     */
    public List<PageData> getProjectRouteDetaillistPage(Page page) throws GenException {
        try {
            return (List<PageData>) dao.findForList("ProjectRouteMapper.getProjectRouteDetaillistPage", page);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     * 路线划分 从表 BY ID
     * @param ID
     * @return
     * @throws GenException
     */
    public PageData getProjectRouteDetailById(String ID) throws GenException {
        try {
            return (PageData) dao.findForObject("ProjectRouteMapper.getProjectRouteDetailById", ID);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     *  路线划分 从表 新增 或 编辑
     * @param pd
     * @param userId
     * @return
     * @throws GenException
     */
    public int insertOrUpdateProjectRouteDetail(PageData pd, String userId) throws GenException {
        try {
            String ID = String.valueOf(pd.get("ID"));
            if (ID == null || "".equals(ID)) {
                ID = UuidUtil.get32UUID();
                pd.put("ID", ID);
            }
            // 坐标转换
            String XA_X = String.valueOf(pd.get("XA_X"));
            String XA_Y = String.valueOf(pd.get("XA_Y"));
            Map<String, Object> pointMap = BaiDuMapUtils.getBaiduLongitudeAndLatitude(XA_X, XA_Y);
            pd.put("POINT_LONGITUDE", pointMap.get("longitude"));
            pd.put("POINT_LATITUDE", pointMap.get("latitude"));
            if (getProjectRouteDetailById(ID) == null) {
                // 新增
                pd.put("CREATER", userId);
                pd.put("CREATEDATE", new Date());
                return (int) dao.save("ProjectRouteMapper.insertProjectRouteDetail", pd);
            }
            pd.put("MODIFIER", userId);
            pd.put("MODIFYDATE", new Date());
            return (int) dao.save("ProjectRouteMapper.updateProjectRouteDetail", pd);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     *  路线划分 从表 新增
     * @param pd
     * @param userId
     * @return
     * @throws GenException
     */
    public int insertProjectRouteDetail(PageData pd, String userId) throws GenException {
        try {
            // 新增
            pd.put("ID", UuidUtil.get32UUID());
            pd.put("CREATER", userId);
            pd.put("CREATEDATE", new Date());
            return (int) dao.save("ProjectRouteMapper.insertProjectRouteDetail", pd);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     *  路线划分 从表 编辑
     * @param pd
     * @param userId
     * @return
     * @throws GenException
     */
    public int updateProjectRouteDetail(PageData pd, String userId) throws GenException {
        try {
            String ID = String.valueOf(pd.get("ID"));
            if (ID == null || "".equals(ID) || getProjectRouteDetailById(ID) == null) {
                return 0;
            }
            pd.put("MODIFIER", userId);
            pd.put("MODIFYDATE", new Date());
            return (int) dao.save("ProjectRouteMapper.updateProjectRouteDetail", pd);
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     * 路线划分 从表 删除 批量删除
     * @param ids
     * @param userId
     * @return
     * @throws GenException
     */
    public int batchDeleteProjectRouteDetail(String[] ids, String userId) throws GenException {
        try {
            int rows = 0;
            if (ids == null || ids.length == 0) {
                return rows;
            }
            for (int i = 0; i < ids.length; i++) {
                String ID = ids[i];
                PageData pd = new PageData();
                pd.put("ID", ID);
                pd.put("MODIFIER", userId);
                pd.put("MODIFYDATE", new Date());
                rows += (int) dao.save("ProjectRouteMapper.deleteProjectRouteDetail", pd);
            }
            return rows;
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     * 获取 距离最近的 桩号列表 通过经纬度坐标
     * @param longitude
     * @param latitude
     * @return
     */
    public List<PageData> getProjectRouteDetailsByGPS(String longitude, String latitude) throws GenException {
        try {
            PageData pd = new PageData();
            pd.put("longitude", longitude);
            pd.put("latitude", latitude);
            List<PageData> dataList = (List<PageData>) dao.findForList("ProjectRouteMapper.getProjectRouteDetailsByGPS", pd);
            List<PageData> datas = new ArrayList();
//            List<PageData> nextestPdList = new ArrayList();
            // 先取 方圆 300m 的
            if (Objects.nonNull(dataList) && dataList.size() > 0) {
                for (int i = 0; i < dataList.size(); i++) {
                    PageData data = dataList.get(i);
                    String longitudeB = String.valueOf(data.get("POINT_LONGITUDE"));
                    String latitudeB = String.valueOf(data.get("POINT_LATITUDE"));
                    double distance = BaiDuMapUtils.isNearestPoint(longitude, latitude, longitudeB, latitudeB);
                    if (distance != -1) {
                        data.put("distance", distance);
                        datas.add(data);
                    }
                }
            }
            // 排序 按 distance 从小到大
            if (datas != null && datas.size() > 1) {
                Collections.sort(datas, new Comparator<PageData>() {
                    @Override
                    public int compare(PageData o1, PageData o2) {
                        int ret = 0;
                        //比较两个对象的顺序，如果前者小于、等于或者大于后者，则分别返回-1/0/1
                        double d1 = Double.parseDouble(String.valueOf(o1.get("distance")));
                        double d2 = Double.parseDouble(String.valueOf(o2.get("distance")));
                        if (d1 > d2)
                            return 1;
                        if (d1< d2)
                            return -1;
                        return 0;
                    }
                });
            }

//            // 方圆300m, 是否超过 3 个点
//            if (datas.size() <= 3) {
//                nextestPdList.addAll(datas);
//            } else { // 超过 3个 过滤，取最近的 3个
//                int len = datas.size();
//                if (len%2 == 0) {
//                    nextestPdList.add(datas.get(len/2-2));
//                }
//                nextestPdList.add(datas.get(len/2-1));
//                nextestPdList.add(datas.get(len/2));
//                nextestPdList.add(datas.get(len/2+1));
//            }
            return datas;
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }

    /**
     * 导入 批量添加 桩号
     * @param pds
     * @param userId
     * @return
     * @throws GenException
     */
    public int batchInsertProjectRouteDetail(List<PageData> pds, String userId) throws GenException {
        try {
            int rows = 0;
            if (pds == null || pds.size() == 0) {
                return 0;
            }
            for (int i = 0; i < pds.size(); i++) {
                PageData pd = pds.get(i);
                // 新增
                pd.put("ID", UuidUtil.get32UUID());
                pd.put("CREATER", userId);
                pd.put("CREATEDATE", new Date());
                rows += (int) dao.save("ProjectRouteMapper.insertProjectRouteDetail", pd);
            }
            return rows;
        } catch (Exception e) {
            throw new GenException(e.getCause());
        }
    }
}
